 var mapcenter = [121.0421, 14.6819];
 var markers = []
 let map = tt.map({
     key: 'HwnjopRuAjAGH28B6VAQB5dRMfMvoKLv',
     language: "en-US",
     container: 'map',
     zoom: 14,
     center: mapcenter,
     dragPan: !isMobileOrTablet()
 });



 map.addControl(new tt.FullscreenControl());
 map.addControl(new tt.NavigationControl());


 var options = {
     minNumberofCharacters: 3,
     labels: {
         placeholder: "Pick up"
     },
     searchOptions: {
         key: 'HwnjopRuAjAGH28B6VAQB5dRMfMvoKLv',
         language: 'en-US',
         idxSet: 'POI',
         countrySet: "PH"

     },
     autocompleteOptions: {
         options: {
             key: 'HwnjopRuAjAGH28B6VAQB5dRMfMvoKLv',
             language: 'en-US',
             countrySet: 'PH',
             idxSet: 'POI',
             limit: 10,
         }

     }
 };

 var options2 = {
     minNumberofCharacters: 3,
     labels: {
         placeholder: "Destination"
     },
     searchOptions: {
         key: 'HwnjopRuAjAGH28B6VAQB5dRMfMvoKLv',
         language: 'en-US',
         countrySet: "PH",

         limit: 10
     },
     autocompleteOptions: {
         key: 'HwnjopRuAjAGH28B6VAQB5dRMfMvoKLv',
         language: 'en-US',

         region: 'PH'
     }
 };


 var ttSearchBox = new tt.plugins.SearchBox(tt.services, options);
 var searchBoxHTML = ttSearchBox.getSearchBoxHTML();

 var ttSearchBox2 = new tt.plugins.SearchBox(tt.services, options2);
 var searchBoxHTML2 = ttSearchBox2.getSearchBoxHTML();


 let originInput = document.getElementById("tdPickUpLocationdDiv")
 originInput.append(searchBoxHTML)


 let destinationInput = document.getElementById("tdDestinationDiv");
 destinationInput.append(searchBoxHTML2)

 ttSearchBox.on('tomtom.searchbox.resultselected', function (data) {
     console.log(data.data);

     const place = data.data.result
     const address = data.data.text
     document.getElementById("senderlatitude")
         .value = place.position.lat
     document.getElementById("senderlongitude")
         .value = place.position.lng
     document.getElementById("tdPickUpLocation")
         .value = address

     if (place.position.lat && place.position.lng) {

         var pickupmarker = [place.position.lng, place.position.lat];
         var marker = new tt.Marker()
             .setLngLat(pickupmarker)
             .addTo(map)


         markers.push(marker);
         movemap(pickupmarker)
         document.getElementById("divmodal")
             .style.display = "block";
         document.getElementById("divSenderInformation")
             .style.display = "block";
         document.getElementById("divrecipientInformation")
             .style.display = "none";
         document.getElementById("distance")
             .innerHTML = "0 KM";
         document.getElementById("basefare")
             .innerHTML = "₱ 0";
         document.getElementById("kmcharge")
             .innerHTML = "₱ 0";
         document.getElementById("labeltotal")
             .innerHTML = "₱ 0";
         document.getElementById("deliveryfeeSummary")
             .innerHTML = "₱ 0";
         document.getElementById("btnreview")
             .style.display = "none";

     }

 });

 var movemap = function (lnglat) {
     map.flyTo({
         center: lnglat,
         zoom: 14
     })
 }


 ttSearchBox2.on('tomtom.searchbox.resultselected', function (data) {
     console.log(data.data);

     const place = data.data.result;
     const address = data.data.text
     document.getElementById("recipientlatitude")
         .value = place.position.lat
     document.getElementById("recipientlongitude")
         .value = place.position.lng
     document.getElementById("tdDestination")
         .value = address
     document.getElementById("distance")
         .innerHTML = "0 KM";
     document.getElementById("basefare")
         .innerHTML = "₱ 0";
     document.getElementById("kmcharge")
         .innerHTML = "₱ 0";
     document.getElementById("labeltotal")
         .innerHTML = "₱ 0";
     document.getElementById("deliveryfeeSummary")
         .innerHTML = "₱ 0";
     document.getElementById("btnreview")
         .style.display = "none";

     if (place.position.lat && place.position.lng) {
         document.getElementById("divrecipientInformation")
             .style.display = "block";
         document.getElementById("divmodal")
             .style.display = "block";
         document.getElementById("divSenderInformation")
             .style.display = "none";

         var destinationmarker = [place.position.lng, place.position.lat];
         var marker = new tt.Marker()
             .setLngLat(destinationmarker)
             .addTo(map)

         markers.push(marker);
         movemap(destinationmarker)
         createRoute()
     }
 });

 var displayRoute = function (geoJSON) {
     routeLayer = map.addLayer({
         'id': 'route',
         'type': 'line',
         'source': {
             'type': 'geojson',
             'data': geoJSON
         },
         'paint': {
             'line-color': 'red',
             'line-width': 5
         }
     })
 }


 let createRoute = function () {
     var routeOptions = {
         key: "HwnjopRuAjAGH28B6VAQB5dRMfMvoKLv",
         locations: []
     }
     console.log(markers)
     for (mark of markers) {
         routeOptions.locations.push(mark.getLngLat())
     }

     //execute routing API 

     tt.services.calculateRoute(routeOptions)
         .then(
             function (routeData) {
                 console.log(routeData)
                 var geo = routeData.toGeoJson();
                 displayRoute(geo)
             }
         )
 }