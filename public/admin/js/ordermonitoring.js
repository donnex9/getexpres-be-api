$(document)
	.ready(function () {

		let mapcenterA = $("#mapcenterLat")
			.val()
		let mapcenterB = $("#mapcenterLng")
			.val();

		let mapData = []

		setInterval(showNotificationBadge, 5000)
		setInterval(checkScheduledBooking, 5000)
			//setInterval(deleteMarkers, 10000)
			//setInterval(ridermonitoring, 10000)

		/*function playAudio() {
			console.log("Log me every minute")
				//let audio = $("#audioNotif")[0];
				//audio.play();
		}*/
		$("#myDropdown #divdrop")
			.on("click", "a", function () {
				let val = $(this)
					.text()
				let postion = $(this)
					.data("latlng")
				$("#myInput")
					.val(val)
				let mapcoordinate = postion.split(",")
				mapcenterA = mapcoordinate[0]
				mapcenterB = mapcoordinate[1]
				newLocation()
			})

		function newLocation() {
			let baseurl = $("#base_urls")
			map.setCenter({
				lat: parseFloat(mapcenterA),
				lng: parseFloat(mapcenterB)
					//consoleicon: base_url + "public/admin/assets/svg/getpin.png"
			});
			$("#myDropdown #divdrop")
				.html("")
		}

		$("#myInput")
			.on('keyup', function () {
				let search = $(this)
					.val()
					.toLowerCase();
				let aTag = ""
				$("#myDropdown #divdrop")
					.html("")
				if (search != "") {
					let res = mapData.filter(function (mapdata) {
						return mapdata.infowindow_content.toLowerCase()
							.indexOf(search) !== -1
					})

					$.each(res, function (i, item) {
						//console.log(res)
						aTag += `<a data-latlng = "${item.position}" >${item.infowindow_content}</a>`
					})

				}


				$("#myDropdown #divdrop")
					.append(aTag)
					//console.log(val);
			})

		function checkScheduledBooking() {
			let baseurl = $("#base_urls")
				.val();
			let link = "cms/booking/checkScheduledBooking"


			axios.get(base_url + link, {
					headers: {
						'Content-Type': 'application/json; charset=UTF-8'
					}

				})
				.then(function (response) {

					console.log(response)

				})
				.catch(function (error) {
					$("#loading")
						.hide()
						//alert('oops');
					console.log(error);
				})

			return false;
		}

		function showNotificationBadge() {
			let baseurl = $("#base_urls")
				.val();
			let link = "cms/booking/monitorActiveBooking"


			axios.get(base_url + link, {
					headers: {
						'Content-Type': 'application/json; charset=UTF-8'
					}

				})
				.then(function (response) {
					// alert('yeah!');
					let totalActiveOrder = 0
						//console.log(response)

					$.each(response.data.data, function (i, item) {
						totalActiveOrder += parseInt(item.total)
						areaid = item.assigned_location

						if (areaid == "1") {
							$("#areabadge1")
								.html(item.total)

						} else if (areaid == "2") {
							$("#areabadge2")
								.html(item.total)
						} else if (areaid == "3") {
							$("#areabadge3")
								.html(item.total)
						} else if (areaid == "4") {
							$("#areabadge4")
								.html(item.total)
						} else if (areaid == "5") {
							$("#areabadge5")
								.html(item.total)
						} else if (areaid == "6") {
							$("#areabadge6")
								.html(item.total)
						} else if (areaid == "7") {
							$("#areabadge7")
								.html(item.total)
						}
					})
					$("#bellbadge")
						.html(totalActiveOrder)

					if (totalActiveOrder <= 0) {
						$("#areabadge1")
							.html("0")
						$("#areabadge2")
							.html("0")
						$("#areabadge3")
							.html("0")
						$("#areabadge4")
							.html("0")
						$("#areabadge5")
							.html("0")
						$("#areabadge6")
							.html("0")
						$("#areabadge7")
							.html("0")
						$("#bellicon2")
							.css({
								"display": "none"
							})
						$("#bellicon")
							.css({
								"display": "block"
							})
						$("#bellbadge")
							.css({
								"postion": "absolute",
								"top": "3px",
								"left": "15px"
							})

					} else {
						$("#bellicon2")
							.css({
								"display": "block"
							})
						$("#bellicon")
							.css({
								"display": "none"
							})
						$("#bellbadge")
							.css({
								"postion": "absolute",
								"top": "12px",
								"left": "30px"
							})

					}
					//console.log(response.data);


				})
				.catch(function (error) {
					$("#loading")
						.hide()
						//alert('oops');
					console.log(error);
				})

			return false;
		}


		function ridermonitoring() {

			let baseurl = $("#base_urls")
				.val();
			let link = "cms/riderlocation/updateriderslocation"

			axios.get(base_url + link)
				.then(function (response) {
					deleteMarkers()
					mapData = response.data.marker
					updateMap(response.data.marker)

					$("#loadingdata")
						.addClass("hideit")

					$("#myInput")
						.attr("disabled", false)
						//updateMap(response.data.marker);

					/*var myLatlng = new google.maps.LatLng(14.5751, 121.1930);
					var mapOptions = {
						zoom: 15,
						center: myLatlng,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					}
					var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
					let markers = response.data.marker;
					var marker = []*/
					/*$.each(mapData, function (i, val) {
						let post = val.position
						let latlng = post.split(",")
						var myLatlng = new google.maps.LatLng(parseFloat(latlng[0]), parseFloat(latlng[1]));
						marker = new google.maps.Marker({
							position: myLatlng,
							icon: val.icon,
							infowindow_content: val.infowindow_content
						});
						marker.setMap(map);

					})*/

					//console.log(response.data)



				})
				.catch(function (error) {

					//alert('oops');
					console.log(error);
				})

			return false;
		}


		function updatePosition(lat, lng) {
			//console.log("lat" + lat + "," + lng)
			mapcenterA = lat
			mapcenterB = lng
		}

		function deleteMarkers() {
			console.log(markers.length)
				//console.log(marker.length)

			for (var i = 0; i < markers.length; i++) {
				markers[i].setMap(null)

			}
			markers.length = 0
			markers = []
			console.log(markers.length)
		}


		function updateMap(data) {
			let baseurl = $("#base_urls")
				/*var map = new google.maps.Map(document.getElementById('map_canvas'), {
					zoom: 15,
					center: new google.maps.LatLng(parseFloat(mapcenterA), parseFloat(mapcenterB)),
					mapTypeId: google.maps.MapTypeId.ROADMAP
				});*/


			var infowindow = new google.maps.InfoWindow();
			var marker, i;
			var myMarker = new google.maps.Marker({
				position: new google.maps.LatLng(parseFloat(mapcenterA), parseFloat(mapcenterB)),
				draggable: true,
				map: map,


			});

			google.maps.event.addListener(map, 'dragend', function () {
				myMarker.setPosition(this.getCenter()); // set marker position to map center
				updatePosition(this.getCenter()
					.lat(), this.getCenter()
					.lng());
			});


			$.each(data, function (i, val) {
					let post = val.position
					let latlng = post.split(",")

					marker = new google.maps.Marker({
						position: new google.maps.LatLng(parseFloat(latlng[0]), parseFloat(latlng[1])),
						map: map,
						icon: val.icon
					});
					google.maps.event.addListener(marker, 'click', (function (marker, i) {
						return function () {
							infowindow.setContent(val.infowindow_content);
							infowindow.open(map, marker);
						}
					})(marker, i));
					//map.set(markers)
					markers.push(marker);

				}) //end of foreach 

			//markers = marker
			//console.log(`new marker-${JSON.parse(markers)}`)

		}

	})
