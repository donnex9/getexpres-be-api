"use strict";

"use strict";


function initMap() {
  const map = new google.maps.Map(document.getElementById("map"), {
    mapTypeControl: false,
    center: {
      lat: -33.8688,
      lng: 151.2195
    },
    zoom: 13,
  });

  new AutocompleteDirectionsHandler(map);
}

class AutocompleteDirectionsHandler {
  map;
  originPlaceId;
  destinationPlaceId;
  travelMode;
  directionsService;
  directionsRenderer;
  constructor(map) {
    this.map = map;
    this.originPlaceId = "";
    this.destinationPlaceId = "";
    this.travelMode = google.maps.TravelMode.WALKING;
    this.directionsService = new google.maps.DirectionsService();
    this.directionsRenderer = new google.maps.DirectionsRenderer();
    this.directionsRenderer.setMap(map);

    const originInput = document.getElementById("tdPickUpLocation");
    const destinationInput = document.getElementById("tdDestination");
    // const modeSelector = document.getElementById("mode-selector");
    const originAutocomplete = new google.maps.places.Autocomplete(originInput, {
      componentRestrictions: {
        country: "ph"
      }
    });

    originAutocomplete.addListener('place_changed', function () {
      //marker.setVisible(false);

      const place = originAutocomplete.getPlace();
      document.getElementById("senderlatitude")
        .value = place.geometry.location.lat()
      document.getElementById("senderlongitude")
        .value = place.geometry.location.lng()

      if (place.geometry.location.lat() && place.geometry.location.lng()) {
        document.getElementById("divSenderInformation")
          .style.display = "block";
        document.getElementById("divrecipientInformation")
          .style.display = "none";

      }

      //console.log(place.geometry.location.lat())

      if (!place.geometry) {
        // User entered the name of a Place that was not suggested and
        // pressed the Enter key, or the Place Details request failed.
        window.alert('No details available for input: \'' + place.name + '\'');
        return;
      }

    });



    // Specify just the place data fields that you need.
    //originAutocomplete.setFields(["place_id"]);

    const destinationAutocomplete = new google.maps.places.Autocomplete(
      destinationInput, {
        componentRestrictions: {
          country: "ph"
        }
      }
    );

    destinationAutocomplete.addListener('place_changed', function () {

      const place = destinationAutocomplete.getPlace();
      document.getElementById("recipientlatitude")
        .value = place.geometry.location.lat()
      document.getElementById("recipientlongitude")
        .value = place.geometry.location.lng()

      if (place.geometry.location.lat() && place.geometry.location.lng()) {
        document.getElementById("divSenderInformation")
          .style.display = "none";
        document.getElementById("divrecipientInformation")
          .style.display = "block";
      }

      if (!place.geometry) {
        // User entered the name of a Place that was not suggested and
        // pressed the Enter key, or the Place Details request failed.
        window.alert('No details available for input: \'' + place.name + '\'');
        return;
      }
      //renderAddress(place);
      // fillInAddress(place);
    });


  }

  setupPlaceChangedListener(autocomplete, mode) {
    autocomplete.bindTo("bounds", this.map);
    autocomplete.addListener("place_changed", () => {
      const place = autocomplete.getPlace();

      if (!place.place_id) {
        window.alert("Please select an option from the dropdown list.");
        return;
      }

      if (mode === "ORIG") {
        this.originPlaceId = place.place_id;
      } else {
        this.destinationPlaceId = place.place_id;
      }

      // this.route();
    });
  }

  route() {
    if (!this.originPlaceId || !this.destinationPlaceId) {
      return;
    }

    const me = this;

    this.directionsService.route({
        origin: {
          placeId: this.originPlaceId
        },
        destination: {
          placeId: this.destinationPlaceId
        },
        travelMode: this.travelMode,
      },
      (response, status) => {
        if (status === "OK") {
          me.directionsRenderer.setDirections(response);
        } else {
          window.alert("Directions request failed due to " + status);
        }
      }
    );
  }
}

//function initMap() {


/*const map = new google.maps.Map(document.getElementById("map"), {
  zoom: 10,
  center: {
    lat: 14.6766,
    lng: 121.0390
  },
  mapTypeControl: false,
  fullscreenControl: true,
  zoomControl: true,
  streetViewControl: true
});
const marker = new google.maps.Marker({
  map: map,
  draggable: false
});*/



/*const autocompleteInput2 = document.getElementById('tdDestination');
  const autocomplete2 = new google.maps.places.Autocomplete(autocompleteInput2, {
    fields: ["formatted_address", "geometry", "name"],
    types: ["establishment"],
    componentRestrictions: {
      country: "ph"
    }
  });
  autocomplete2.addListener('place_changed', function () {
    //marker.setVisible(false);
    document.getElementById("divSenderInformation")
      .style.display = "none";
    document.getElementById("divrecipientInformation")
      .style.display = "block";

    const place = autocomplete2.getPlace();
    document.getElementById("recipientlatitude")
      .value = place.geometry.location.lat()
    document.getElementById("recipientlongitude")
      .value = place.geometry.location.lng()

    if (!place.geometry) {
      // User entered the name of a Place that was not suggested and
      // pressed the Enter key, or the Place Details request failed.
      window.alert('No details available for input: \'' + place.name + '\'');
      return;
    }
    //renderAddress(place);
    // fillInAddress(place);
  });

  //pickup autocomplete address
  const autocompleteInput = document.getElementById('tdPickUpLocation');
  const autocomplete = new google.maps.places.Autocomplete(autocompleteInput, {
    fields: ["address_components", "geometry", "name"],
    types: ["address"],
    componentRestrictions: {
      country: "ph"
    }
  });

  autocomplete.addListener('place_changed', function () {
    //marker.setVisible(false);
    document.getElementById("divSenderInformation")
      .style.display = "block";
    document.getElementById("divrecipientInformation")
      .style.display = "none";

    const place = autocomplete.getPlace();
    document.getElementById("senderlatitude")
      .value = place.geometry.location.lat()
    document.getElementById("senderlongitude")
      .value = place.geometry.location.lng()

    //console.log(place.geometry.location.lat())

    if (!place.geometry) {
      // User entered the name of a Place that was not suggested and
      // pressed the Enter key, or the Place Details request failed.
      window.alert('No details available for input: \'' + place.name + '\'');
      return;
    }
    // renderAddress(place);
    //fillInAddress(place);
  });



  function renderAddress(place) {
    map.setCenter(place.geometry.location);
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);
  }
}*/
