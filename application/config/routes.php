<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['mypdf'] = "welcome/mypdf";

/*
| -------------------------------------------------------------------------
| Sample REST API Routes
| -------------------------------------------------------------------------
*/

# name of route, url, etc. -------------------- controller path #
$route['cms'] = 'cms/dashboard';

# My routes
$route['api/example/(:num)'] = 'api/example/single/$1';
$route['account_activation/(:any)/(:any)'] = 'welcome/account_activation/$1/$2';
$route['reset-password/(:any)/(:any)'] = 'welcome/reset_password/$1/$2'; // normal type of sign up
$route['reset_password/(:any)/(:any)'] = 'welcome/reset_password/$1/$2'; // normal type of sign up
$route['change-password'] = 'welcome/change_password'; // normal type of sign up


$route['payment/(:any)'] = 'welcome/payment/$1'; // normal type of sign up
$route['payment-balance/(:any)'] = 'welcome/payment_balance/$1'; // normal type of sign up
$route['payment/partner/balance/(:any)'] = 'welcome/payment_partner_balance/$1';
$route['payment/cart/order/(:any)/(:any)'] = 'welcome/payment_result/$1/$2';
$route['api/options/(:any)/(:any)'] = 'api/options/options/$1/$2'; // normal type of sign up
$route['api/options-mm/(:any)'] = 'api/options/maintenance_options/$1';

$route['api/forgot-password/(:any)'] = 'api/customers/forgot_password/$1'; // normal type of sign up

$route['api/sign-up/customer'] = 'api/customers/normal'; // normal type of sign up
$route['api/sign-in/customer'] = 'api/customers/sign_in'; // normal type of sign up
$route['api/sign-up-2/customer'] = 'api/customers/normal2'; // normal type of sign up
$route['api/sign-in-2/customer'] = 'api/customers/sign_in2'; // normal type of sign up
$route['api/sign-in/(:any)/otp'] = 'api/customers/sign_in_otp/$1'; // normal type of sign up
$route['api/sign-in/(:any)/otp/resend'] = 'api/customers/resend_otp/$1'; // normal type of sign up
$route['api/sign-out/customer'] = 'api/customers/sign_out'; // normal type of sign up
$route['api/social-sign-in/(:any)'] = 'api/customers/social_sign_in/$1'; // normal type of sign up
$route['api/visitedsabong/(:any)'] = 'api/customers/visitedsabong/$1'; // normal type of sign up
$route['api/deactivate/account/(:any)'] = 'api/customers/deactivateAccount/$1';

$route['api/riders/(:num)/vehicles/(:num)/make-active'] = 'api/vehicles/make_active/$1/$2'; // normal type of sign up
$route['api/sign-up/rider'] = 'api/riders/normal'; // normal type of sign up
$route['api/sign-in/rider'] = 'api/riders/sign_in'; // normal type of sign up
$route['api/sign-up-2/rider'] = 'api/riders/normal2'; // normal type of sign up
$route['api/sign-in-2/rider'] = 'api/riders/sign_in2'; // normal type of sign up
$route['api/sign-out/rider'] = 'api/riders/sign_out'; // normal type of sign up
$route['api/riders/(:num)/wallet'] = 'api/riders/wallet/$1'; // normal type of sign up

$route['api/riders/(:num)/balance'] = 'api/riders/balance/$1'; // normal type of sign up
$route['api/riders/(:num)/avail-offer-by-balance'] = 'api/riders/avail_offer_by_balance/$1'; // normal type of sign up
$route['api/riders/(:num)/ratings'] = 'api/riders/ratings/$1'; // normal type of sign up
$route['api/riders/(:num)/cash-on-hand'] = 'api/riders/cash_on_hand/$1'; // normal type of sign up
$route['api/riders/(:num)/location'] = 'api/riders/location/$1'; // normal type of sign up

$route['api/riders/wallet/offers'] = 'api/riders/wallet_offers'; // normal type of sign up
$route['api/riders/(:num)/vehicles'] = 'api/riders/add_vehicle/$1'; // normal type of sign up
$route['api/riders/(:num)/vehicles/(:num)/active'] = 'api/riders/make_vehicle_active/$1/$2'; // normal type of sign up

$route['api/vehicles'] = 'api/vehicles/index'; // normal type of sign up
$route['api/services/(:num)/vehicles/(:num)'] = 'api/services/services_vehicles/$1/$2'; // normal type of sign up
$route['api/services'] = 'api/services/index'; // normal type of sign up
$route['api/services/category/(:any)'] = 'api/services/food_grocery_category/$1';
$route['api/news'] = 'api/news/index'; // normal type of sign up
$route['api/news/(:num)'] = 'api/news/single/$1'; // normal type of sign up
$route['api/whatsnews/(:num)'] = 'api/news/whatsnews/$1';
$route['api/profile/new-address/(:num)'] = 'api/profile/new_address/$1'; // add new address
$route['api/profile/address/(:num)'] = 'api/profile/address/$1'; // update addressfa

$route['api/profile/(:any)/(:num)'] = 'api/profile/profile/$1/$2'; // normal type of sign up
$route['api/updateprofile/(:any)/(:num)'] = 'api/profile/updateProfile/$1/$2'; // update profile picture
             
// $route['api/profile/newaddress/(:num)'] = 'api/profile/newaddress/$1'; // normal type of sign up
// $route['api/profile/address/(:num)/(:num)'] = 'api/profile/address/$1/$2'; // normal type of sign up

$route['api/cart/(:num)/updateitem'] = 'api/cart/addOrUpdateItem/$1'; // riders change status
$route['api/cart/(:num)/finalize'] = 'api/cart/finalize/$1'; // riders change status
$route['api/cart/(:num)/finalize-2'] = 'api/cart/finalize2/$1'; // riders change status
$route['api/cart/(:num)/tip'] = 'api/cart/tip/$1'; // add tip after complete
$route['api/cart/(:num)/cancel'] = 'api/cart/cancel/$1'; // riders change status
$route['api/cart/(:num)/info'] = 'api/cart/info/$1'; // riders change status
$route['api/cart/(:num)/empty'] = 'api/cart/empty/$1'; // riders change status
$route['api/cart/(:num)/status'] = 'api/cart/status/$1'; // riders change status
$route['api/cart/(:num)/attempt-attach'] = 'api/cart/attempt_attach/$1'; // riders change status
$route['api/cart/(:num)/rider-location'] = 'api/cart/rider_location/$1'; // riders change status
$route['api/cart/riders/(:num)/active'] = 'api/cart/active_booking/$1'; // riders change status
$route['api/cart/customers/(:num)/active'] = 'api/cart/active_booking_customers/$1'; // riders change status
$route['api/cart/partners/(:num)/new/order'] = 'api/cart/newPlaceOrder/$1'; // new order
$route['api/cart/partners/(:num)/accepted/order'] = 'api/cart/acceptedOrderByMerchant/$1'; // Accepted order 
//by Merchant

//===========online payment via payloro API
 
 $route['api/cart/payin/gcash/(:num)'] = 'api/cart/payInGcash/$1'; 
 $route['api/cart/check/payment/status'] = 'api/cart/checkPaymentStatus';

//===========end 
$route['api/cart/voucher/(:any)/(:any)/(:any)'] = 'api/cart/applyVoucher/$1/$2/$3'; 
$route['api/cart/checkvoucher/(:any)'] = 'api/cart/voucherChecker/$1';
$route['api/cart/active/vouchers/(:any)/(:any)'] = 'api/cart/activeVouchers/$1/$2'; //get active voucher
$route['api/cart/inactive/vouchers/(:any)/(:any)'] = 'api/cart/inActiveVouchers/$1/$2'; //get Inactive voucher
$route['api/cart/partners/today/order/(:any)'] = 'api/cart/completedOrderByMerchantToday/$1'; // Completed order by Merchant
$route['api/cart/partners/yesterday/order/(:any)'] = 'api/cart/completedOrderByMerchantYesterday/$1'; // Completed order by yesterday


# KAREN
$route['api/vehicles/(:num)/delivery-details/(:num)'] = 'api/cart/delivery_details/$1/$2'; // get all inventory by partner
$route['api/cart/(:num)'] = 'api/cart/transact/$1'; // get all inventory by partner
$route['api/cart/(:num)/distance'] = 'api/cart/distance/$1'; // get all inventory by partner
$route['api/cart/initialize'] = 'api/cart/initialise_cart'; // get all inventory by partner
$route['api/cart/update/(:any)'] = 'api/cart/update_cart/$1'; // update cart
$route['api/cart/active/item/(:any)'] = 'api/cart/active_cart_customers/$1';

$route['api/partners/service/(:num)/(:num)'] = 'api/partners/service/$1/$2'; // get all partners by service
$route['api/partners/search/(:any)/(:num)'] = 'api/partners/search/$1/$2'; // get all partners by service
$route['api/partners/searchproduct/(:any)/(:num)'] = 'api/partners/searchProduct/$1/$2'; // get all product by like product name
$route['api/partners/searchstorename/(:any)/(:num)'] = 'api/partners/searchStoreName/$1/$2';
$route['api/partners/(:num)'] = 'api/partners/single/$1'; // get all partners by service
$route['api/partners/store/open/close/(:num)'] = 'api/partners/openCloseStore/$1'; // get all partners by service
$route['api/sign-in/partners'] = 'api/partners/sign_in'; // sign in
$route['api/sign-out/partners'] = 'api/partners/sign_out'; // sign out
$route['api/partner/balance/(:any)'] = 'api/partners/balance/$1'; // get partner balance
$route['api/partner/wallet/(:any)'] = 'api/partners/wallet/$1'; // get partner wallet 
$route['api/partner/topup/wallet/(:any)'] = 'api/partners/addWallet/$1'; // add partner wallet 
$route['api/partner/update/profile/(:any)'] = 'api/partners/updateProfile/$1'; // update partner profile';
$route['api/partner/pay/balance/(:any)'] = 'api/partners/payPayable/$1'; // add par

$route['api/inventory/partner/(:num)'] = 'api/inventory/partner/$1'; // get all inventory by partner
$route['api/inventory/item/(:num)'] = 'api/inventory/item/$1'; // get all inventory by partner
$route['api/inventory/categories/partner/(:num)'] = 'api/inventory/categories/$1'; // get all inventory by partner
$route['api/inventory/categories/keyword/(:num)'] = 'api/inventory/partnerSearcByCategory/$1';
$route['api/inventory/update/status/(:num)'] = 'api/inventory/updateItemStatus/$1';

$route['api/inventory/partner/(:num)?category=(:any)'] = 'api/inventory/partner/$1'; // get all inventory by partner
$route['api/cart/vehicles/(:num)'] = 'api/cart/find_customer/$1'; // riders look for customers
$route['api/cart/(:num)/accept/riders/(:num)'] = 'api/cart/accept/$1/$2'; // riders change status
$route['api/cart/(:num)/accept-2/riders/(:num)'] = 'api/cart/accept2/$1/$2'; // riders change status
$route['api/cart/(:num)/complete'] = 'api/cart/complete/$1'; // riders change status
$route['api/cart/(:num)/complete-2'] = 'api/cart/complete2/$1'; // riders change status
$route['api/cart/(:any)/(:num)/history'] = 'api/cart/history/$1/$2'; // get all history
$route['api/cart/(:any)/(:num)/history/services/(:num)'] = 'api/cart/history_by_service/$1/$2/$3'; // get all history
$route['api/cart/online/payment/(:num)'] = 'api/cart/onlinePayment/$1';
$route['api/cart/(:num)/history'] = 'api/cart/single_history/$1'; // get all history
$route['api/cart/payment/method/(:any)'] = 'api/cart/updatePaymentMethod/$1'; // update payment method
$route['api/cart/test/notification'] = 'api/cart/test_notification'; // get all history

$route['api/serviceablearea'] = 'api/options/serviceablearea';
$route['api/appversion/(:any)'] = 'api/options/appversion/$1';
$route['api/places/radius']   = 'api/options/radius_places';
$route['api/city/area/(:num)/(:any)/(:any)/(:any)']   = 'api/options/cityArea/$1/$2/$3/$4';

$route['api/profile/sampletest/(:num)'] = 'api/profile/sampletest/$1'; // normal type of sign up

$route['migrate/(:any)'] = 'migrate/index/$1';

#addedd GET dev

#online betting api link
$route['api/onlinebetting/pcc'] = 'api/onlinebettingapi/pcc';
$route['api/cart/distancematrix/(:num)'] = 'api/cart/distance/$1';

#test notification
$route['api/cart/test/notification'] = 'api/cart/test_notification';
#Test OTP
$route['api/customers/test/otp/(:num)/(:num)'] = 'api/customers/testOTP/$1/$2';



# Restserver default examples
$route['api/example/users/(:num)'] = 'api/example/users/id/$1';
$route['api/example/users/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api/example/users/id/$1/format/$3$4'; // Example 8

