<?php

/**
* returns the api url
* @param  object $class    the `$this` object
* @return string           example: http://localhost/restigniter-crud/api/crud/27
*
* @author: @jjjjcccjjf
*/
function api_url($class)
{
  return base_url() . "api/" . strtolower(get_class($class)) . "/";
}

/**
 * make sure we dont have null values
 * @param  [type] $obj [description]
 * @return [type]      [description]
 */
function disallowNull($obj) {
	if (!$obj) {
		return false;
	}
  
  if (is_object($obj)) {
  	foreach ($obj as $key => $value) {
  		$obj->{$key} = ($obj->{$key} === null) ? "" : $obj->{$key};
  	}
  }

	return $obj;
}
