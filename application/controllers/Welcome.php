<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
 
    function __construct()
    {
      parent::__construct();
      # Do not forget to load your model in your child class
      $this->load->model('api/customers_model');
      $this->load->model('api/riders_model');
      $this->load->model('api/cart_model');
      # Do not change the 2nd parameter because we need a generic model name for this
    }
 
	public function index()
	{
		redirect('cms/dashboard');
	}

	function account_activation($verification_token, $type)
	{
		$verification_token = base64_decode(urldecode($verification_token));
		$type = base64_decode(urldecode($type));
		$this->db->where('verification_token', $verification_token);
		$user = $this->db->get($type)->row();

		if ($user) {
			$this->db->reset_query();
			$this->db->where('id', $user->id);
			$this->db->update($type, ['is_email_verified' => 1, 'verification_token' => null]);

			$data['label'] = 'Account verified successfully';
			$this->load->view('verify', $data);
		} else {
			$data['label'] = 'Account already activated or Invalid Token.<br><hr><br>Please try to sign-in on the app. <br><br>If you are a rider and you are seeing this message, please wait for the Administrator to verify your account.';
			$this->load->view('verify', $data);
		}
	}

    public function reset_password($encrypted_email, $encrypted_type)
    {
	   $data['email'] = base64_decode(urldecode($encrypted_email));
	   $data['type'] = base64_decode(urldecode($encrypted_type));

		if ($this->customers_model->getByEmail($data['email'], $data['type'])) {
			# if contains @
			$this->load->view('reset_password', $data);
		} else {
			$this->load->view('reset_password_label', ['label' => 'Invalid change password token']);
		}
	 }

	public function change_password()
	{
		$user = $this->customers_model->getByEmail($this->input->post('email'), $this->input->post('type'));
		// var_dump($this->input->post()); die();
		$this->db->where('email', $this->input->post('email'));
		$this->db->update($this->input->post('type'), ['password' => password_hash($this->input->post('new_password'), PASSWORD_DEFAULT)]);

		if ($this->db->error()){
			$this->load->view('reset_password_label', ['label' => 'Password reset successfully']);
		} else {
			$this->load->view('reset_password_label', ['label' => 'Failed to reset password']);
		}
		return;
	}

	public function payment($status = 'failed')
	{
		$id = base64_decode($this->input->get('_'));
		$this->riders_model->updateWalletStatus($id);

		if ($status == 'failed'){
			$this->load->view('reset_password_label', ['label' => 'Transaction failed. Please return to the app for details.']);
		} else {
			$this->load->view('reset_password_label', ['label' => 'Transaction successful. You can now return to the app.']);
		}
		return;
	}
	
	public function payment_balance($status = 'failed')
	{
		$id = base64_decode($this->input->get('_'));
		$this->riders_model->updateWalletStatusforRider($id);

		if ($status == 'failed'){
			$this->load->view('reset_password_label', ['label' => 'Transaction failed. Please return to the app for details.']);
		} else {
			$this->load->view('reset_password_label', ['label' => 'Transaction successful.']);
		}
		return;
	}
      
    public function payment_partner_balance($status = 'failed')
	{
		$id = base64_decode($this->input->get('_'));
                $balance =  base64_decode($this->input->get('bal'));
                $this->partners_model->updateWalletStatusforPartner($id,$balance);

		if ($status == 'failed'){
			$this->load->view('reset_password_label', ['label' => 'Transaction failed. Please return to the app for details.']);
		} else {
			$this->load->view('reset_password_label', ['label' => 'Transaction successful.']);
		}
		return;
	 }


	public function mypdf(){

    $this->load->library('pdf');

   echo  $html = $this->load->view('mypdf', [], true);
    $this->pdf->createPDF($html, 'mypdf', false);

    }

  public function payment_result($status = 'failed',$customerId){

  	
		if ($status == 'failed'){
			$this->load->view('reset_password_label', ['label' => 'Transaction failed. Please return to the app for details.']);
		} else {

			$id = base64_decode($this->input->get('id'));
            $this->cart_model->updateCartPayment($id);

			$this->load->view('reset_password_label', ['label' => 'Transaction successful.']);

		}
		return;

  }
    
}
