<?php

class Cart extends Crud_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('api/cart_model', 'model');
    $this->load->model('api/cart_model', 'cart_model');
    $this->load->model('api/firebase_model');
    $this->load->model('api/notifications_model');
    $this->load->model('cms/admin_model');
    $this->load->model('api/partners_model');
    $this->load->model('api/payment_model');
  }

  function index_get()
  {
    $res = $this->cart_model->all();

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }

  function delivery_details_get($vehicle_type_id,$location)
  {
   // $location = $this->input->post("location");
    $res = $this->cart_model->getDeliveryDetails($vehicle_type_id,$location);

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }

  function test_notification_get(){
    //$res = $this->notifications_model->getActiverRiders2();
   
  //  foreach ($res as  $value)
  //  {

   $resp = $this->notifications_model->notifyStore("hello partner",'103');
   print_r($resp);
      //echo $value->user_id;
     // $this->notifications_model->sendPushNotif($res,['data' => ['type' => 'nearby_request']],['title' => "Grocery", 'body' => "A new order has been placed!"],false,false);
  //  }
    
}

  function active_booking_get($rider_id)
  {
    $res = $this->cart_model->getActiveBooking($rider_id);

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }

  function active_booking_customers_get($customer_id)
  {
    $res = $this->cart_model->getActiveBookingCustomer($customer_id);

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }

  function active_cart_customers_get($customer_id)
  {
    $res = $this->cart_model->getActiveCartCustomer($customer_id);

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }

  function newPlaceOrder_get($partnerId)
  {
    $res = $this->cart_model->getNewOrder($partnerId);
    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }

 function acceptedOrderByMerchant_get($partnerId)
  {
    $res = $this->cart_model->getAcceptedOrder($partnerId);
    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }


   function onlinePayment_post($cartId)
  {
    $amount = $this->input->post('amount');
    $type   = $this->input->post('type');
    $customerId = $this->input->post('customerId');
    $dataPost = ['amount'=>$amount,'type'=>$type,'cart_id'=>$cartId,"customer_id" => $customerId];
    $res      = $this->cart_model->onlinePayment($dataPost);

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }

  public function initialise_cart_post()
  {
    $cart = $this->cart_model->checkCartExists($this->input->post());
    if ($cart){
      $last_id = $cart->id;
    } else {
      $last_id = $this->cart_model->initializeCart($this->input->post());
    }



    # always update delivery loc
    $this->db->where('id', $last_id);
    $this->db->update('cart', ['delivery_location' => $this->input->post('delivery_location'),
                               'pickup_location' => $this->input->post('pickup_location'),
                               'assigned_location' => $this->input->post('assigned_location'), 
                               'voucher_amount'=>'0.00',
                               'status' =>'init',
                               'payment_method' =>'COD',
                               'rider_id' => 0,
                               'voucher_code'=>NULL,
                               'cart_type_id' => $this->input->post('cart_type_id'),
                               'notes' => $this->input->post('notes')  
                              ]);

    $res = $this->cart_model->get($last_id); # get cart contents

    if ($res) {
      $message = "Cart initialized";
      $code = "ok";
      $status = 200;
    } else {
      $message = "Malformed Syntax";
      $code = "bad_request";
      $status = 400;
    }

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => $message,
      'code' => $code,
      'status' => $status
    ];

    $this->response($response, $response->meta->status);
  }
 
 public function update_cart_post($cart_id)
  {
    # always update location
    $this->db->where('id', $cart_id);
    $this->db->update('cart', ['delivery_location' => $this->input->post('delivery_location'),
                               'pickup_location' => $this->input->post('pickup_location'),
                               'assigned_location' => $this->input->post('assigned_location'),
                               'cart_type_id' => $this->input->post('cart_type_id'),
                               'partner_id' => $this->input->post('partner_id')
                               ]);

    $res = $this->cart_model->get($cart_id); # get cart contents

    if ($res) {
      $message = "Cart updated";
      $code = "ok";
      $status = 200;
    } else {
      $message = "Malformed Syntax";
      $code = "bad_request";
      $status = 400;
    }

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => $message,
      'code' => $code,
      'status' => $status
    ];

    $this->response($response, $response->meta->status);
  }

  public function find_customer_get($vehicle_id)
  {
    $this->cart_model->forceExpireBookings(); # Enable this in live # Expire orders past 2 hours

    $message = "OK";
    $code = "ok";
    $status = 200;

    if ($this->input->get('rider_id')) {
      // $is_available = $this->cart_model->checkBookingAvailableUntil($this->input->get('rider_id'));
      // if ($is_available) {
        $res = $this->cart_model->getByVehicle($vehicle_id);
      // } else {
      //   $res = [];
      //   $message = "Not enough wallet credits";
      //   $code = "insufficient_credits";
      //   $status = 200;
      // }
    } else {
      $res = $this->cart_model->getByVehicle($vehicle_id);
    }

    $response = (object)[];
    $response->data = $res;
    $booking = $this->cart_model->getActiveBooking($this->input->get('rider_id'));
    $response->active_cart_id = (array)$booking ? $booking->id : "";
    $response->active_cart_type_id = (array)$booking ? $booking->cart_type_id : "";
    $response->meta = (object)[
        'message' => $message,
        'code' => $code,
        'status' => $status
    ];

    $this->response($response, $response->meta->status);
  }

  function info_get($id)
  {
    //$res1 = $this->cart_model->riderDistanceByMatrix($id); 
    //print_r($res);
    $res = $this->cart_model->get($id);

  // die();

    if ($res) {
      $message = "OK";
      $code = "ok";
      $status = 200;
    } else {
      $message = "Not found";
      $code = "not_found";
      $status = 404;
    }

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => $message,
      'code' => $code,
      'status' => $status
    ];

    $this->response($response, $response->meta->status);
  }

  public function accept_post($cart_id, $rider_id)
  {
    $rider = $this->riders_model->get($rider_id);
    $cash_on_hand = @$rider->cash_on_hand ?: 0;

    $cart = $this->cart_model->get($cart_id);
    $grand_total = $this->cart_model->findGrandTotal($cart);

    $res = null;

    if (($cash_on_hand >= $grand_total || $cart->cart_type_id == 4) && $cart->status == 'pending') {
      $res = $this->cart_model->acceptBooking($cart_id, $rider_id);
    }

    // var_dump($res); die();
    if ($res && $res != (object)[]) {
      # PUSH NOTIF BLOCK NOTIFICATION
      $this->notifications_model->sendPushNotif(
        $res->customer_id,
        'customers',
        ['data' => ['type' => 'accept_booking', 'status' => $res->status_text]],
        ['title' => 'Your booking has been accepted', 'body' => 'Hang tight! Your rider will be on the way soon']
      );
      # / PUSH NOTIF BLOCK NOTIFICATION
    }

    if ($cart->status == 'cancelled') {
      $res = false;
    }

    if ($cart->status == 'accepted') {
      $res = 'accepted_na';
    }

    if ($res === 'accepted_na') {
      $message = "Order has already been accepted by another rider";
      $code = "order_already_accepted";
      $status = 400;
    } else if ($res === false) {
      $message = "Booking was cancelled. Failed to accept.";
      $code = "booking_cancelled";
      $status = 400;
    } else if ($res === null) {
      $message = "Not enough cash on hand to accept this order.";
      $code = "not_enough_cash_on_hand";
      $status = 400;
    } else {
      $message = "Status changed.";
      $code = "ok";
      $status = 200;
    }

    $response = (object)[];
    $response->data = $res ?: (object)[];
    $response->meta = (object)[
      'message' => $message,
      'code' => $code,
      'status' => $status
    ];

    $this->response($response, $response->meta->status);
  }

  public function accept2_post($cart_id, $rider_id)
  {
    $rider = $this->riders_model->get($rider_id);
    $cash_on_hand = @$rider->cash_on_hand ?: 0;

    $cart = $this->cart_model->get($cart_id);
    $grand_total = $this->cart_model->findGrandTotal($cart);

    $partnerId = $cart->partner_id;

    $res = null;

    $rider_wallet_balance = $this->riders_model->walletBalancesum($rider->id);
    $location = $cart->assigned_location;
    $min = $this->admin_model->getMeta('minimum_wallet',$location);

    $minimum_wallet_amount = $min;

    if (!($rider_wallet_balance >= $minimum_wallet_amount)) {
          $res = 'minimum_wallet_amount_not_met';
    }

    if (in_array($cart->status, ['accepted'])) {
      $res = 'accepted_na';
    } 
    // else {
    //  $res = 'order_not_yet_finalized';
    // }

    // var_dump($cash_on_hand >= $grand_total, $cart->status == 'pending', $rider_wallet_balance >= $minimum_wallet_amount); die();
    if ($cash_on_hand >= $grand_total && $cart->status == 'pending' && $rider_wallet_balance >= $minimum_wallet_amount) {
      $res = $this->cart_model->acceptBooking($cart_id, $rider_id);

      if(in_array($cart->cart_type_id, [2,5]))
         {
           //$pickup_location = $new_res->pickup_location;
          $this->notifications_model->notifyStore("Incoming new Order",$partnerId);

         }


    }else if(($cart->cart_type_id == 4 || $cart->cart_type_id == 1) && $cart->status == 'pending' && $rider_wallet_balance >= $minimum_wallet_amount)
    {
      $res = $this->cart_model->acceptBooking($cart_id, $rider_id);
    }

    // var_dump($res); die();
    if ($res && $res != (object)[] && !in_array($res, ['minimum_wallet_amount_not_met', 'accepted_na', 'order_not_yet_finalized', false, null])) {
      # PUSH NOTIF BLOCK NOTIFICATION
      $this->notifications_model->sendPushNotif(
        $res->customer_id,
        'customers',
        ['data' => ['type' => 'accept_booking', 'status' => $res->status_text]],
        ['title' => 'Your booking has been accepted', 'body' => 'Hang tight! Your rider will be on the way soon']
      );
      # / PUSH NOTIF BLOCK NOTIFICATION
    }

    if ($cart->status == 'cancelled') {
      $res = false;
    }

    if ($cart->status == 'accepted') {
      $res = 'accepted_na';
    }

    if ($res === 'accepted_na') {
      $message = "Order has already been accepted by another rider";
      $code = "order_already_accepted";
      $status = 400;
    } else if ($res === false) {
      $message = "Booking was cancelled. Failed to accept.";
      $code = "booking_cancelled";
      $status = 400;
    } else if ($res === 'order_not_yet_finalized') {
      $message = "Order not yet finalized! Please try again later.";
      $code = "order_not_yet_finalized";
      $status = 400;
    } else if ($res === 'minimum_wallet_amount_not_met') {
      $message = "Minimum wallet amount of $minimum_wallet_amount not met";
      $code = "minimum_wallet_amount_not_met";
      $status = 400;
    } else if ($res === null) {
      $message = "Not enough cash on hand to accept this order.";
      $code = "not_enough_cash_on_hand";
      $status = 400;
    } else {
      $message = "Status changed.";
      $code = "ok";
      $status = 200;
    }

    $response = (object)[];
    $response->data = $res ?: (object)[];
    $response->meta = (object)[
      'message' => $message,
      'code' => $code,
      'status' => $status
    ];

    $this->response($response, $response->meta->status);
  }

  public function complete_post($cart_id)
  {
    $this->cart_model->setTotalPrice($cart_id);

    $this->cart_model->completeBooking($this->input->post(), $cart_id);
    $res = $this->cart_model->get($cart_id);
    
    $message = "Booking complete";
    $code = "ok";
    $status = 200;

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => $message,
      'code' => $code,
      'status' => $status
    ];

    $this->response($response, $response->meta->status);
  }

  public function complete2_post($cart_id)
  {

    $res = $this->cart_model->get($cart_id);
    // var_dump($res->basket); die();
    if ($res->status != 'completed') {
      if (in_array($res->cart_type_id, [4,1,7])) {
        // code...
        $deducted_amount = $res->basket['getapp_commission'];
      } else {
        $deducted_amount = $res->basket->getapp_commission;
      }
      $this->riders_model->deductCommission($deducted_amount, $res->rider_id);
      if (in_array($res->cart_type_id, [2,5])) {
        // code...
         $deducted_partner_amount = $res->basket->getapp_comission_partner;
         $this->partners_model->deductPartnerCommission($deducted_partner_amount, $res->partner_id,$cart_id);
      }
    }

    $this->cart_model->setTotalPrice($cart_id);

    $this->cart_model->completeBooking($this->input->post(), $cart_id);

    $res = $this->cart_model->get($cart_id);
    
   $basket = (object)@$res->basket;
    
   if($res->voucher_code != null)
   {
    $completedBookingWithVoucherCode = [
      'booking_num'=>$res->id,
      'booking_date' =>$res->completed_at ,
      'rider_id' =>$res->rider_id ,
      'customer_id' =>$res->customer_id,
      'service_type' =>$res->cart_type_id,
      'voucher_code'=>$res->voucher_code,
      'voucher_amount'=>$res->voucher_amount,
      'delivery_fee' => $basket->delivery_fee,
      'status' =>'unpaid' ,
      'assigned_location' =>$res->assigned_location];
     $insertLastId = $this->cart_model->completedBookingwithVoucher($completedBookingWithVoucherCode);
     $voucherCode = $res->voucher_code;
       $isVoucherForFirstTimeUser = $this->cart_model->checkifVoucherIsFtu($res->assigned_location,$voucherCode);
       if( $isVoucherForFirstTimeUser == true){
         $updateData = array('is_ftu' => 'N' );
         $this->cart_model->updateData($res->customer_id,$updateData,"customers");
       }
      
 }
    

    $message = "Booking complete";
    $code = "ok";
    $status = 200;

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => $message,
      'code' => $code,
      'status' => $status
    ];

    $this->response($response, $response->meta->status);
  }

public function tip_post($cart_id)
  {

    $res = $this->cart_model->get($cart_id);
    //$tips = $this->input->post('tip');
    $this->cart_model->addTip($cart_id,$this->input->post());
    $res = $this->cart_model->get($cart_id);

    $message = "Tip has been Added.";
    $code = "ok";
    $status = 200;

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => $message,
      'code' => $code,
      'status' => $status
    ];

    $this->response($response, $response->meta->status);
  }

public function voucherChecker_get($cart_id)
  {

    $res = $this->cart_model->get($cart_id);

     $data["hasVoucher"] = $this->cart_model->checkIfAvailtheVoucher($res->customer_id);

    $message = "Voucher Code";
    $code = "ok";
    $status = 200;

    $response = (object)[];
    $response->data = $data;
    $response->meta = (object)[
      'message' => $message,
      'code' => $code,
      'status' => $status
    ];

    $this->response($response, $response->meta->status);
  }


  public function attempt_attach_post($cart_id)
  {
    $this->cart_model->update($cart_id, ['payment_method_id' => $this->input->post('payment_method_id')]);
    $res = $this->cart_model->attemptAttach($cart_id);

    $message = "OK";
    $code = "ok";
    $status = 200;

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => $message,
      'code' => $code,
      'status' => $status
    ];

    $this->response($response, $response->meta->status);
  }

   public function updatePaymentMethod_post($cart_id)
  {
    $this->cart_model->update($cart_id, ['payment_method' => $this->input->post('payment_method'),
          'payment_status' => $this->input->post('payment_method'),
       ]);
    $res = $this->cart_model->get($cart_id);

    $message = "OK";
    $code = "ok";
    $status = 200;

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => $message,
      'code' => $code,
      'status' => $status
    ];

    $this->response($response, $response->meta->status);
  }




  

  public function finalize_post($cart_id)
  {
    $res = $this->cart_model->get($cart_id);

    if ($res) {
    // if (($this->input->post('delivery_location') && $this->input->post('pickup_location')) || ($this->input->post('pickup_location') && in_array($res->cart_type_id, [2,5]))) {
//      $frJsonDecode = json_decode($this->input->post('pickup_location'));
  //    $toJsonDecode = json_decode($this->input->post('delivery_location'));
    //  $assignedlocation = $this->cart_model->setBaseFare($cart_id,$frJsonDecode,$toJsonDecode);
        $assignedlocation = $this->input->post('assigned_location');
      // var_dump('a'); die();
      $res = $this->cart_model->finalizeCart($this->input->post(), $cart_id, $this->input->get('status_change'));
      //$this->cart_model->setDistanceByMatrix($cart_id);



      // var_dump($res); die();
      $res = $this->cart_model->get($cart_id);
      if (in_array($res->cart_type_id, [4])) { # delivery, re init payload
        $this->cart_model->transactDelivery(unserialize($res->payload), $cart_id,$assignedlocation,$res);
      }

      /*if (in_array($res->cart_type_id, [4]) && $res->vehicle_id == 11) { # delivery, re init payload
        $this->cart_model->transactDeliveryTrike(unserialize($res->payload), $cart_id,$assignedlocation,$res);
      }*/


      if (in_array($res->cart_type_id, [1]) && $res->vehicle_id != 11) { # delivery, re init payload
        $this->cart_model->transactRide(unserialize($res->payload), $cart_id,$assignedlocation, $res);
      }

      if (in_array($res->cart_type_id, [1]) && $res->vehicle_id == 11 ) { # delivery, re init payload
        $this->cart_model->transactRideTrike(unserialize($res->payload), $cart_id,$assignedlocation, $res);
      }


      $new_res = $this->cart_model->get($cart_id);

      //if ($this->input->get('status_change') != 'false') {
        $code = 'ok';

        $pickup_location = $new_res->pickup_location;
        //$this->notifications_model->notifyAllActiveRiders("Get " . ucwords($new_res->service_type), @$pickup_location->latitude, @$pickup_location->longitude,$assignedlocation);
      /*} else {
        $code = 'ok2';
      }*/

      // var_dump($new_res); die();
      $response = (object)[];
      $response->data = $new_res;
      $response->meta = (object)[
        'message' => 'OK',
        'code' => $code,
        'status' => 200,
        'partner' => (object)[]
      ];

      if(in_array($new_res->cart_type_id, [2,5])) {
        @$response->meta->partner = $this->cart_model->getPartnerByCartId($cart_id);
        // var_dump($response->meta->partner); die();
        if (!(@$response->meta->partner->store_availability_meta && @$response->meta->partner->store_schedule_meta)) {
            $response->meta->status = 400;
            $response->meta->message = 'Sorry, our partner is unavailable at the moment';
        }

      }
      // var_dump($new_res); die();

      $this->response($response, $response->meta->status);
    } else {
      $response = (object)[];
      $response->data = (object)[];
      $response->meta = (object)[
        'message' => 'Please update your app to the latest version',
        'code' => 'update_required',
        'status' => 400,
        'partner' => (object)[]
      ];
      $this->response($response, $response->meta->status);

      // $response = (object)[];
      // $response->data = (object)[];
      // $response->meta = (object)[
      //   'message' => 'Please set a pickup/delivery location',
      //   'code' => 'location_cannot_be_empty',
      //   'status' => 400,
      //   'partner' => (object)[]
      // ];
      // $this->response($response, $response->meta->status);
    }

  }


  public function finalize2_post($cart_id)
  {
    $res = $this->cart_model->get($cart_id);
   
    if (($this->input->post('delivery_location') && $this->input->post('pickup_location')) || ($this->input->post('pickup_location') && in_array($res->cart_type_id, [2,5]))) {
        $assignedlocation = $this->input->post('assigned_location'); 
         $res = $this->cart_model->finalizeCart($this->input->post(), $cart_id);
        $new_res = $this->cart_model->get($cart_id);

      // var_dump($new_res); die();
        $code = "ok";

      if ($this->input->get('status_change') != 'false') {
        $code = 'ok';

         if(!in_array($res->cart_type_id, [2,5]))
         {
           $pickup_location = $new_res->pickup_location;
           $this->notifications_model->notifyAllActiveRiders("Get " . ucwords($new_res->service_type), 
           @$pickup_location->latitude,
           @$pickup_location->longitude,$assignedlocation,$new_res->vehicle_id);
         } 
          
      } else {
        $code = 'ok2';
      }

      // var_dump($new_res); die();
      $response = (object)[];
      $response->data = $new_res;
      $response->meta = (object)[
        'message' => 'OK',
        'code' => $code,
        'status' => 200,
        'partner' => (object)[]
      ];

     $this->response($response, $response->meta->status);

    } else {
      $response = (object)[];
      $response->data = (object)[];
      $response->meta = (object)[
        'message' => 'Please set a pickup/delivery location',
        'code' => 'location_cannot_be_empty',
        'status' => 400,
        'partner' => (object)[]
      ];
      $this->response($response, $response->meta->status);
    }           
 }

function  returnTheVoucherCode($cart)
{
 $available = false;
 $used = 0;
 $assignedlocation = $cart->assigned_location;
 $vouchercode = $cart->voucher_code;
 if ($vouchercode != null || $vouchercode != "") {
      $used = $this->cart_model->countVoucher($vouchercode,$assignedlocation);
      $this->cart_model->updatevoucherCode($vouchercode,$assignedlocation,$used);
   }
 /*if($cart->voucher_code != NULL || $cart->voucher_code != ""){
  $res = $this->cart_model->voucherCode($vouchercode,$assignedlocation);
    if($res){
       foreach ($res as $key => $value) {
        $expiredate = $this->compareDates($value->expired_at);
          if($expiredate == true){
             $available = false;
          }else if($value->status == 0)
          {
            $available = false;
          }else if($value->used  >= $value->limit)
          {
            $available = false;

          }else{
                $available = true;
                $used = $value->used;
          }
       }
    }
}

 if($available == true){
       $used = $used - 1;
       $this->cart_model->updatevoucherCode($vouchercode,$assignedlocation,$used);
    }*/
}


  public function cancel_post($cart_id)
  {
    $cart = $this->cart_model->get($cart_id);
    $this->returnTheVoucherCode($cart);
    if (in_array($cart->status, ['pending', 'cancelled', 'init'])) {
      $res = $this->cart_model->cancelCart($cart_id);
      $response = (object)[];
      $response->data = $res;
      $response->meta = (object)[
        'message' => 'OK',
        'code' => 'ok',
        'status' => 200
      ];
    } else {
      $response = (object)[];
      // $response->data = $cart;
      $response->data = (object)[];
      $response->meta = (object)[
        'message' => 'Cancellation failed. Order is already in progress or completed.',
        'code' => 'already_accepted',
        'status' => 409
      ];
    }

    $this->response($response, $response->meta->status);
  }

  function history_by_service_get($fk, $customer_id, $service_id)
  {
    $res = $this->cart_model->getHistoryByService($fk, $customer_id, $service_id);

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, $response->meta->status);
  }

  function history_get($fk, $customer_id)
  {
    $res = $this->cart_model->history($fk, $customer_id);

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, $response->meta->status);
  }

  public function addOrUpdateItem_post($cart_id)
  {
    $res   = $this->cart_model->get($cart_id);
    $location = $this->input->post("assigned_location");
    $trans = $this->cart_model->AddOrUpdateItem($this->input->post(),$cart_id,$location);

     if ($trans === null) {
      $message = "Sorry, $res->service_label is currently undergoing maintenance. Please try another service for now.";
      $code =  'maintenance_mode_active';
      $status = 400;
    } else {
      $message = 'OK';
      $code =  'ok';
      $status = 200;
    }

    $res = $this->cart_model->get($cart_id);

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => $message,
      'code' => $code,
      'status' => $status
    ];

    $this->response($response, $response->meta->status);
  }

  public function transact_post($cart_id)
  {
    $this->cart_model->setDistanceByMatrix($cart_id); 
    $res   = $this->cart_model->get($cart_id);
    
   // var_dump($res);
   // die();
    $location = $this->input->post("assigned_location");
    //$trans = $this->cart_model->delegateTransaction($this->input->post(), $cart_id,$location);
    if($res->cart_type_id == 1)
    {
      $trans = $this->cart_model->delegateTransactionTrike($this->input->post(), 
        $cart_id,$location);
    }else
    {
      $trans = $this->cart_model->delegateTransaction($this->input->post(), $cart_id,$location);
    }

    if ($trans === null) {
      $message = "Sorry, $res->service_label is currently undergoing maintenance. Please try another service for now.";
      $code =  'maintenance_mode_active';
      $status = 400;
    } else {
      $message = 'OK';
      $code =  'ok';
      $status = 200;
    }

    $res = $this->cart_model->get($cart_id);

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => $message,
      'code' => $code,
      'status' => $status
    ];

    $this->response($response, $response->meta->status);
  }

  public function distance_post($cart_id)
  {

    $this->cart_model->setDistance($this->input->post('distance'), $cart_id);
    $res = $this->cart_model->get($cart_id);

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, $response->meta->status);
  }

  public function status_post($cart_id)
  {
    $checkifOrderCancelled = $this->cart_model->isOrderCancelled($cart_id);
    if($checkifOrderCancelled == false){
       $this->cart_model->changeStatus($this->input->post(), $cart_id);
    }else{
      $this->cart_model->changeStatus(['status' => 'cancelled'], $cart_id);
    }
   

    $res = $this->cart_model->get($cart_id);

    # PUSH NOTIF BLOCK NOTIFICATION

    $payload = ['type' => 'change_status', 'status' => $res->status_text];
    if ($this->input->post('status') == 'got_items') {
      $payload = array_merge($payload, ['force_payment' => 1]);
    }

    if ($this->input->post('status') == 'completed') {
      $this->cart_model->setTotalPrice($cart_id);
    }

    if($this->input->post('status')== 'pending')
    {
       if(in_array($res->cart_type_id, [2,5]))
         {
           $pickup_location = $res->pickup_location;
           $this->notifications_model->notifyAllActiveRiders("Get " . ucwords($res->service_type),
           @$pickup_location->latitude,
           @$pickup_location->longitude,$res->assigned_location,$res->vehicle_id);
         }
    }


    if($this->input->post('status')== 'accepted')
    {
       if(in_array($res->cart_type_id, [2,5]))
         {
           $pickup_location = $res->pickup_location;
           $this->notifications_model->notifyAllActiveRiders("Get " . ucwords($res->service_type),
           @$pickup_location->latitude,
           @$pickup_location->longitude,$res->assigned_location,$res->vehicle_id);
         }
    }



    /*$this->notifications_model->sendPushNotif(
      $res->customer_id,
      'customers',
      ['data' => $payload],
      ['title' => $res->status_text] //, 'body' => 'Hang on tight!'
    );*/

    # / PUSH NOTIF BLOCK NOTIFICATION

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, $response->meta->status);
  }

  public function rider_location_post($cart_id)
  {
    $this->cart_model->changeRiderLocation($this->input->post(), $cart_id);

    $this->cart_model->riderDistanceByMatrix($cart_id);
   
    $res = $this->cart_model->get($cart_id);

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, $response->meta->status);
  }

  public function empty_post($cart_id)
  {

    $this->cart_model->emptyCart($cart_id);
    $res = $this->cart_model->get($cart_id);

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'Cart emptied',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, $response->meta->status);
  }

 public function activeVouchers_get($customer_id,$location)
  {
    $res = $this->cart_model->getActiveVouchers($customer_id,$location);

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }

  public function inActiveVouchers_get($customer_id,$location)
  {
    $res = $this->cart_model->getInActiveVouchers($customer_id,$location);

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }

 public function applyVoucher_get($vouchercode,$assignedlocation,$cart_Id)
  {
   $res2 = $this->cart_model->get($cart_Id);
   $custmers_Id = $res2->customer_id;
   $message = "Invalid voucher code.";
   $available = false;
   $used = 0;
   $voucherAmount = 0.00;
   $voucherCode = "";
   $isCustomerAvailVoucher    = $this->checkifUserAvailtheVoucher($custmers_Id,$assignedlocation,$vouchercode);
   $isVoucherForFirstTimeUser = $this->cart_model->checkifVoucherIsFtu($assignedlocation,$vouchercode);
   $isCustomerFtu             = $this->cart_model->checkifCustomerIsFtu($custmers_Id);

   $res = $this->cart_model->voucherCode($vouchercode,$assignedlocation);

    if($res){
       foreach ($res as $key => $value) {
        $expiredate = $this->compareDates($value->expired_at);
          if($expiredate == true){
            $message = "Expired Voucher Code";
          }else if($value->status == 0)
          {
           $message = "Inactive Voucher Code.";
          }else if($value->status == 0)
          {
           $message = "Inactive Voucher Code.";
          }else if($value->used  >= $value->limit)
          {
           $message = "Voucher Code had been reach its limit.";

          }else{
                $available = true;
                $used = $value->used;
                $voucherAmount = $value->amount;
                $voucherCode = $value->voucher_code;
          }
       }
    }

   if($available == true && $isCustomerAvailVoucher == false && $isVoucherForFirstTimeUser == true 
      &&  $isCustomerFtu == true ){
       $used = $used + 1;
       $this->cart_model->updatevoucherCode($vouchercode,$assignedlocation,$used);
       $this->db->where('id',$cart_Id);
       $this->db->update('cart',array('voucher_amount'=>$voucherAmount,"voucher_code"=>$vouchercode));
       $message = "Voucher Code successfully applied";
    }else if($available == true && $isCustomerAvailVoucher == false && $isVoucherForFirstTimeUser == false 
      &&  $isCustomerFtu == false ){
       $used = $used + 1;
       $this->cart_model->updatevoucherCode($vouchercode,$assignedlocation,$used);
       $this->db->where('id',$cart_Id);
       $this->db->update('cart',array('voucher_amount'=>$voucherAmount,"voucher_code"=>$vouchercode));
       $message = "Voucher Code successfully applied";
       
    }else if($available == true && $isCustomerAvailVoucher == false && $isVoucherForFirstTimeUser == false 
      &&  $isCustomerFtu == true ){
       $used = $used + 1;
       $this->cart_model->updatevoucherCode($vouchercode,$assignedlocation,$used);
       $this->db->where('id',$cart_Id);
       $this->db->update('cart',array('voucher_amount'=>$voucherAmount,"voucher_code"=>$vouchercode));
       $message = "Voucher Code successfully applied";
    }else if($available == true && $isCustomerAvailVoucher == false && $isVoucherForFirstTimeUser == true 
      &&  $isCustomerFtu == false ){
       $res = [];
       $message = "Sorry, this voucher is for first time user only.";
    }else if($available == true && $isCustomerAvailVoucher == true){
      $res = [];
       $message = "You reached voucher usage per day, try next time.";
    }else{
      $res = [];
    }   
    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' =>$message,
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, $response->meta->status);
  }


public function checkifUserAvailtheVoucher($custmers_Id,$assignedlocation,$vouchercode)
  {
    $dateToday = date('Y-m-d');
    $response = $this->cart_model->userAvailVoucher($custmers_Id,$assignedlocation,$vouchercode,$dateToday);
    $voucherLimitPerDay = $this->admin_model->getMeta("voucherlimitperday",$assignedlocation);

    if($response >= $voucherLimitPerDay){ return true;}else{return false; }
  }


 public function compareDates($d1)
  {
    $date1 = date("Y-m-d",strtotime($d1));
    $date2 = date("Y-m-d",strtotime(date('Y-m-d')));

    $d1 = strtotime($date1);
    $d2 = strtotime($date2);

    if($d2 <= $d1)
    {
      return false;
    }else{
      return true;
    }
  }

 //for update to prod

function completedOrderByMerchantToday_get($partnerId)
  {
    $res = $this->cart_model->getCompletedOrderToday($partnerId);
    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }

   function completedOrderByMerchantYesterday_get($partnerId)
  {
    $res = $this->cart_model->getCompletedOrderYesterday($partnerId);
    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }


  //payloro online payment option

  function payInGcash_post($cartId){

   $data = $this->input->post();
   $res = $this->payment_model->payinGcash($data);
   $uRes = [];

   $response = (object)[];
   $response->data = $res;

   if(trim($res->message) == trim("success"))
   {
    $dataToUpdate = [
                     "payment_status"    => $res->data->orderStatus,
                     "payment_intent_id" => $res->data->platOrderNo,
                     "payment_method_id" => $res->data->merchantOrderNo
                    ];
    $uRes =  $this->cart_model->onlinePaymentMethod($dataToUpdate,'id',$cartId);
  
   }
  
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }

  //payloro check payment status

  function checkPaymentStatus_post(){

   $data = $this->input->post();
   $res = $this->payment_model->checkPaymentStatus($data);

   if(trim($res->data->orderStatus) == trim("SUCCESS"))
   {
    $dataToUpdate = [
                     "payment_status" => $res->data->orderStatus,
                     "payment_method" => "GCASH"
                    ];
    $uRes =  $this->cart_model->onlinePaymentMethod($dataToUpdate,'payment_method_id',$res->data->merchantOrderNo);
  
   }

   $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }


 } //end of the class
