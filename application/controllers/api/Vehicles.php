<?php

class Vehicles extends Crud_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('api/vehicles_model');
    $this->load->model('api/riders_model');
  }

  function index_get()
  {
    $res = $this->vehicles_model->all();

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }

  public function make_active_post($rider_id, $rider_vehicles_pk)
  {
    $this->vehicles_model->makeActive($rider_id, $rider_vehicles_pk);
    $res = $this->riders_model->get($rider_id);

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'Active vehicle changed successfully',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }

}
