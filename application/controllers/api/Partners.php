<?php

class Partners extends Crud_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('api/partners_model', 'partners_model');
    $this->load->model('api/user_device_ids_model', 'udi_model');
  }

  function index_get()
  {
    $res = $this->model->all();

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }


  function single_get($id)
  {
    $res = $this->partners_model->get($id);
    if ($res != (object)[]) {
      $response = (object)[];
      $response->data = $res;
      $response->meta = (object)[
        'message' => 'OK',
        'code' => 'ok',
        'status' => 200
      ];
    } else {
      $response = (object)[];
      $response->data = $res;
      $response->meta = (object)[
        'message' => 'Not found',
        'code' => 'not_found',
        'status' => 404
      ];
    }

    $this->response($response, $response->meta->status);
  }

function openCloseStore_get($id){
     $res2 = $this->partners_model->updateStoreAvailability($id,$this->input->get('is_open'));   
   
    if ($res2) {
      $res = $this->partners_model->get($id);
      $response = (object)[];
      $response->data = $res;
      $response->meta = (object)[
        'message' => 'OK',
        'code' => 'ok',
        'status' => 200
      ];
    } else {
      $response = (object)[];
      $response->data = $res;
      $response->meta = (object)[
        'message' => 'Not found',
        'code' => 'not_found',
        'status' => 404
      ];
    }
    $this->response($response, $response->meta->status);

  }

  public function service_get($service_id,$assignedLocation)
  {
  	$res = $this->partners_model->getByService($service_id,$assignedLocation);

    $response = (object)[];
    $response->data = $res;

    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }

  public function search_get($keyword = '',$assignedLocation)
  {
  	$res = $this->partners_model->search($keyword,$assignedLocation);
    if ($res) {
      $message = "Search found.";
      $code = "ok";
      $status = 200;
    } else {
      $message = "No search results";
      $code = "not_found";
      $status = 404;
    }

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      	'message' => $message,
      	'code' => $code,
      	'status' => $status
    ];

    $this->response($response, $response->meta->status);
  }

 public function searchProduct_get($keyword,$assignedLocation)
  {
    $res = $this->partners_model->searchProduct($keyword,$assignedLocation);
    if ($res) {
      $message = "Search found.";
      $code = "ok";
      $status = 200;
    } else {
      $message = "No search results";
      $code = "not_found";
      $status = 404;
    }

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
        'message' => $message,
        'code' => $code,
        'status' => $status
    ];

    $this->response($response, $response->meta->status);
  }

  public function searchStoreName_get($keyword,$assignedLocation)
  {

    $res = $this->partners_model->searchStoreName($keyword,$assignedLocation);
    if ($res) {
      $message = "Search found.";
      $code = "ok";
      $status = 200;
    } else {
      $message = "No search results";
      $code = "not_found";
      $status = 404;
    }

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
        'message' => $message,
        'code' => $code,
        'status' => $status
    ];

    $this->response($response, $response->meta->status);
  }

 //sign in merchant

  function sign_in_post()
  {
    $_POST['email'] = strtolower($this->input->post('email'));

    $res = (object)[];
    $signin = $this->partners_model->signIn($this->input->post());
    if ($signin === 'account_exists_in_a_different_signin_method') {
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Account registered to another sign-in method. Please try a different sign-in method', 'code' => 'account_exists_in_a_different_signin_method', 'status' => 409];
      $this->response($res, 409);
    } else if ($signin === 'email_not_verified') {
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Account not yet activated', 'code' => 'account_not_active', 'status' => 409];
      $this->response($res, 409);
    } else if ($signin === 'account_is_banned') {
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Sorry, you have been banned from this app', 'code' => 'account_is_banned', 'status' => 409];
      $this->response($res, 409);
    } else if ($signin){
      $res->data = $signin;
      $res->meta = (object)['message' => 'Sign-in successful', 'code' => 'ok', 'status' => 200];
      $this->response($res, 200);
    } else {
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Invalid credentials', 'code' => 'invalid_credentials', 'status' => 409];
      $this->response($res, 409);
    }

  }

 public function sign_out_post()
  {
    # Set Defaults
    $logout = $this->udi_model->logoutDevice($this->input->post('device_id'), 'partners');

    $response = (object)[];
    $response->data = (object)[];
    $response->meta = (object)[
      'message' => 'Logout success. Device notifications set to inactive state.',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, $response->meta->status);
  }
  
  public function balance_get($partnerId)
 {
     $res = $this->partners_model->partnerBalanceSum($partnerId);
     $response = (object)[];
     $response->data = $res;
     $response->meta = (object)[
       'message' => 'OK',
       'code' => 'ok',
       'status' => 200
     ];
     $this->response($response, $response->meta->status);
 } 

 public function wallet_get($partnerId)
 {
     $res = $this->partners_model->partnerWalletSum($partnerId);
     $response = (object)[];
     $response->data = $res;
     $response->meta = (object)[
       'message' => 'OK',
       'code' => 'ok',
       'status' => 200
     ];
     $this->response($response, $response->meta->status);
 }

  public function addWallet_post($partnerId)
 {
   
   if($this->input->post('amount') < 100){

     $response = (object)[];
     $response->data = (object)[];
     $response->meta = (object)[
       'message' => 'Invalid Amount. Minimum top up amount is  100.',
       'code' => 'invalid_amount',
       'status' => 400
     ];
     $this->response($response, $response->meta->status);
   }
   else{
     $res = $this->partners_model->partnerTopUpWallet($this->input->post(), $partnerId);

     $response = (object)[];
     $response->data = $res;
     $response->meta = (object)[
       'message' => 'OK',
       'code' => 'ok',
       'status' => 200
     ];
     $this->response($response, $response->meta->status);
   }
 }

  public function updateProfile_post($partnerId)
 {

     $res = $this->partners_model->updatePartnerProfile($partnerId,$this->input->post());
     $response = (object)[];
     $response->data = $res;
     $response->meta = (object)[
       'message' => 'OK',
       'code' => 'ok',
       'status' => 200
     ];
     $this->response($response, $response->meta->status);
 }


 public function payPayable_post($partnerId)
 {
   
   if($this->input->post('balance') < 100){

     $response = (object)[];
     $response->data = (object)[];
     $response->meta = (object)[
       'message' => 'Invalid Amount. Amount must be higher than 100.',
       'code' => 'invalid_amount',
       'status' => 400
     ];
     $this->response($response, $response->meta->status);
   }
   else{
     $res = $this->partners_model->partnerPayBalance($this->input->post(), $partnerId);

     $response = (object)[];
     $response->data = $res;
     $response->meta = (object)[
       'message' => 'OK',
       'code' => 'ok',
       'status' => 200
     ];
     $this->response($response, $response->meta->status);
   }
 }


} // end of the class
