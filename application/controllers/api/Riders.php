<?php

class Riders extends Crud_controller
{

  function __construct()
  {
    parent::__construct();
    # Do not forget to load your model in your child class
    $this->load->model('api/riders_model');
    $this->load->model('api/rider_vehicles_model');
    $this->load->model('api/notifications_model');
    $this->load->model('cms/admin_model');
    # Do not change the 2nd parameter because we need a generic model name for this
  }

  function normal_post()
  {
    $_POST['is_email_verified'] = 1;
    $_POST['email'] = strtolower($this->input->post('email'));

    $data = array_merge($this->input->post(), $this->riders_model->upload('identification_document'));

    $last_id = $this->riders_model->add($data);

    if($last_id){
      // $this->rider_vehicles_model->addVehicle($vehicle, $last_id);
      $obj = $this->riders_model->get($last_id);

      $res = (object)[];
      $res->data = $obj;

      if ($this->input->post('is_email_verified')) {
        $res->meta = (object)['message' => 'Account created successfully! Please wait for the Administrator to activate your account before you could sign-in.', 'code' => 'ok', 'status' => 201];
      } else {
          $res->meta = (object)['message' => 'Account created successfully! Please check your email at '. $this->input->post('email') .' for verification.', 'code' => 'ok', 'status' => 201];
      }

      $this->response_header('Location', api_url($this) .  $last_id);
      $this->response($res, 201);
    }elseif ($last_id === null){
      $res = (object)[];
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Email already exists', 'status' => 409, 'code' => 'email_exists'];

      $this->response($res,409);
    }else{
      $res = (object)[];
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Malformed syntax', 'status' => 400, 'code' => 'malformed_syntax'];

      $this->response($res, 400);
    }
  }

  function normal2_post()
  {
    $_POST['is_email_verified'] = 1;
    $_POST['email'] = strtolower($this->input->post('email'));

    $data = array_merge($this->input->post(), $this->riders_model->upload('identification_document'));

    if ($this->riders_model->mobileNumExists($this->input->post('mobile_num'))) {
      $res = (object)[];
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Mobile number already exists', 'status' => 400, 'code' => 'mobile_num_exists'];

      $this->response($res, 400);
    } else {

      $last_id = $this->riders_model->add2($data);

      if($last_id){
        // $this->rider_vehicles_model->addVehicle($vehicle, $last_id);
        $obj = $this->riders_model->get($last_id);

        $res = (object)[];
        $res->data = $obj;

        if (!$obj->mobile_otp) {
          $res->meta = (object)['message' => 'Account created successfully! You may now sign-in.', 'code' => 'ok', 'status' => 201];
        } else {
          $res->meta = (object)['message' => 'Account created successfully! Please complete the OTP challenge to proceed.', 'code' => 'otp_challenge', 'status' => 201];
        }

        $this->response_header('Location', api_url($this) .  $last_id);
        $this->response($res, 201);
      }elseif ($last_id === null){
        $res = (object)[];
        $res->data = (object)[];
        $res->meta = (object)['message' => 'Email already exists', 'status' => 409, 'code' => 'email_exists'];

        $this->response($res, 409);
      }else{
        $res = (object)[];
        $res->data = (object)[];
        $res->meta = (object)['message' => 'Malformed syntax', 'status' => 400, 'code' => 'malformed_syntax'];

        $this->response($res, 400);
      }

    }


  }

  # Social sign-in for rider is now using Customers/social_sign_in_post
  function location_post($rider_id)
  {
    $res = (object)[];
    $this->riders_model->setLatLong($this->input->post(), $rider_id);

    $res->data = (object)[];
    $res->meta = (object)['message' => 'OK', 'code' => 'ok', 'status' => 200];
    $this->response($res, 200);
  }

  # Social sign-in for rider is now using Customers/social_sign_in_post
  function sign_in_post()
  {
    $res = (object)[];
    $signin = $this->customers_model->signIn($this->input->post(), 'riders');
    if ($signin === 'account_exists_in_a_different_signin_method') {
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Account registered to another sign-in method. Please try a different sign-in method', 'code' => 'account_exists_in_a_different_signin_method', 'status' => 409];
      $this->response($res, 409);
    } else if ($signin === 'account_is_banned') {
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Sorry, you have been banned from this app', 'code' => 'account_is_banned', 'status' => 409];
      $this->response($res, 409);
    } else if ($signin === 'otp_challenge') {
      $res->data = (object)[];
      $res->meta = (object)['message' => 'OTP challenge required. Please update to a newer version to continue.', 'code' => 'otp_challenge', 'status' => 409];
      $this->response($res, 409);
    } else if ($signin === 'email_not_verified') {
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Activate your account first and/or wait for the email validation from the Administrator', 'code' => 'account_not_active_or_verified', 'status' => 409];
      $this->response($res, 409);
    } else if ($signin){
      $res->data = $signin;
      $res->meta = (object)['message' => 'Sign-in successful', 'code' => 'ok', 'status' => 200];
      $this->response($res, 200);
    } else {
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Invalid credentials', 'code' => 'invalid_credentials', 'status' => 409];
      $this->response($res, 409);
    }

  }

  # Social sign-in for rider is now using Customers/social_sign_in_post
  function sign_in2_post()
  {
    $res = (object)[];
    $signin = $this->customers_model->signIn($this->input->post(), 'riders');
    if ($signin === 'account_exists_in_a_different_signin_method') {
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Account registered to another sign-in method. Please try a different sign-in method', 'code' => 'account_exists_in_a_different_signin_method', 'status' => 409];
      $this->response($res, 409);
    } else if ($signin === 'account_is_banned') {
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Sorry, you have been banned from this app', 'code' => 'account_is_banned', 'status' => 409];
      $this->response($res, 409);
    } else if ($signin === 'otp_challenge') {
      $res->data = $this->customers_model->getByEmail($this->input->post('email'), 'riders');
      $res->meta = (object)['message' => 'Please complete the OTP challenge to proceed.', 'code' => 'otp_challenge', 'status' => 409];
      $this->response($res, 200);
    } else if ($signin === 'email_not_verified') {
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Almost there! Please wait for the Administrator to validate your account before signing-in', 'code' => 'account_not_active_or_verified', 'status' => 409];
      $this->response($res, 409);
    } else if ($signin){
      $res->data = $signin;
      $res->meta = (object)['message' => 'Sign-in successful', 'code' => 'ok', 'status' => 200];
      $this->response($res, 200);
    } else {
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Invalid credentials', 'code' => 'invalid_credentials', 'status' => 409];
      $this->response($res, 409);
    }

  }

   public function sign_out_post()
  {
    # Set Defaults
    $logout = $this->udi_model->logoutDevice($this->input->post('device_id'), 'riders');

    $response = (object)[];
    $response->data = (object)[];
    $response->meta = (object)[
      'message' => 'Logout success. Device notifications set to inactive state',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, $response->meta->status);
  }

  public function add_vehicle_post($rider_id)
  {
    # Set Defaults
    $this->rider_vehicles_model->addVehicle($this->input->post(), $rider_id);
    $res = $this->riders_model->get($rider_id);

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, $response->meta->status);
  }

  public function wallet_post($rider_id)
 {
   // var_dump($post['amount']); die();

   if($this->input->post('amount') <= 100){

     $response = (object)[];
     $response->data = (object)[];
     $response->meta = (object)[
       'message' => 'Invalid Amount. Topup must be higher than 100.',
       'code' => 'invalid_amount',
       'status' => 400
     ];
     $this->response($response, $response->meta->status);
   }
   else{
     $res = $this->riders_model->topUpWallet($this->input->post(), $rider_id);

     $response = (object)[];
     $response->data = $res;
     $response->meta = (object)[
       'message' => 'OK',
       'code' => 'ok',
       'status' => 200
     ];
     $this->response($response, $response->meta->status);
   }
 }

 public function balance_post($rider_id)
 {
   if($this->input->post('amount') < 100){

     $response = (object)[];
     $response->data = (object)[];
     $response->meta = (object)[
       'message' => 'Invalid Amount. Topup must be higher than 100.',
       'code' => 'invalid_amount',
       'status' => 400
     ];
     $this->response($response, $response->meta->status);
   }
   else{
    
     $res = $this->riders_model->topUpWalletforRider($this->input->post(), $rider_id);

     $response = (object)[];
     $response->data = $res;
     $response->meta = (object)[
       'message' => 'OK',
       'code' => 'ok',
       'status' => 200
     ];
     $this->response($response, $response->meta->status);
   }
 }

 public function balance_get($rider_id)
 {

     $res = $this->riders_model->walletBalancesum($rider_id) ?: 0;

     $response = (object)[];
     $response->data = $res;
     $response->meta = (object)[
       'message' => 'OK',
       'code' => 'ok',
       'status' => 200
     ];

     $this->response($response, $response->meta->status);

 }

 public function avail_offer_by_balance_post($rider_id)
 {
    $remainbal = $this->riders_model->walletBalancesum($rider_id);

    if ($this->input->post('amount') > $remainbal){

     $response = (object)[];
     $response->data = (object)[];
     $response->meta = (object)[
       'message' => 'insufficient balance. Top-up to avail promos.',
       'code' => 'insufficient_balance',
       'status' => 400
     ];
     $this->response($response, $response->meta->status);

    }
    else{
     $res = $this->riders_model->availOfferForRider($this->input->post(), $rider_id);

     $response = (object)[];
     $response->data = $res;
     $response->meta = (object)[
       'message' => 'OK',
       'code' => 'ok',
       'status' => 200
     ];
     $this->response($response, $response->meta->status);
     }
 }

  public function ratings_post($rider_id)
  {
    $res = $this->riders_model->rateRider($this->input->post('rating'), $rider_id);

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, $response->meta->status);
  }

  public function cash_on_hand_post($rider_id)
  {
    $res = $this->riders_model->updateCashOnHand($this->input->post('amount'), $rider_id);

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, $response->meta->status);
  }

  public function ratings_get($rider_id)
  {
    $res = number_format($this->riders_model->getRiderAverageRating($rider_id)?:0, 1);

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, $response->meta->status);
  }

  public function wallet_get($rider_id)
  {
    $res = $this->riders_model->getWalletTransactionHistory($rider_id);

    $rider = $this->riders_model->get($rider_id);
    // var_dump($rider); die();
    $this->riders_model->notifRiderWallets($rider);

    $rider = $this->riders_model->get($rider_id);
    $response = (object)[];
    $response->data = $res;
    $response->booking_available_until = $rider->booking_available_until ?: "";
    $response->booking_available_until_f = $rider->booking_available_until? date('F j, Y g:i:s A', strtotime($rider->booking_available_until)): "";
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, $response->meta->status);
  }

  public function wallet_offers_get()
  {
    $location = $this->session->userdata('assignedlocation');
    $res = $this->admin_model->getRiderWalletOffers($this->input->get('vehicle_id'));
    $res = $this->riders_model->formatWalletOffers($res);

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'topup_description' => $this->admin_model->getMeta('topup_description',$location),
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, $response->meta->status);
  }

  public function make_vehicle_active_post($rider_id, $vehicle_pk)
  {
    # Set Defaults
    $this->rider_vehicles_model->makeVehicleActive($rider_id, $vehicle_pk);
    $res = $this->riders_model->get($rider_id);

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, $response->meta->status);
  }

}
