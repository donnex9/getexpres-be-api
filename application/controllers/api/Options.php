 <?php

class Options extends Crud_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('cms/admin_model', 'model');
    $this->load->model('cms/admin_model');
  }

 public function options_get($meta_key,$location)
  {
    
    $res = $this->admin_model->getMeta($meta_key,$location);

    $response = (object)[];
    $response->data = (object)["meta_value" => $res,"meta_key" => $meta_key];
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }

  public function maintenance_options_get($location)
  {
    $activeMenuArr = array('get_car_mm','get_grocery_mm','get_pabili_mm','get_delivery_mm','get_food_mm','get_entertainment_mm','get_laundry_mm');
    $arrres = [];

    for($i=0;$i< count($activeMenuArr);$i++){
      $res = $this->admin_model->getMeta($activeMenuArr[$i],$location);
       array_push($arrres,(object)["meta_key"=>$activeMenuArr[$i],"meta_value"=>$res]);
    }
    
    $response = (object)[];
    $response->data = $arrres;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }

  public function radius_places_get()
  {
    $res = $this->admin_model->getDynamicPlaces();

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }


function pointInPolygon($point, $polygon, $pointOnVertex = true) {
    $this->pointOnVertex = $pointOnVertex;

    // Transform string coordinates into arrays with x and y values
    $point = $this->pointStringToCoordinates($point);
    $vertices = array(); 
    foreach ($polygon as $vertex) {
        $vertices[] = $this->pointStringToCoordinates($vertex); 
    }

    // Check if the lat lng sits exactly on a vertex
    if ($this->pointOnVertex == true and $this->pointOnVertex($point, $vertices) == true) {
        return "vertex";
    }

    // Check if the lat lng is inside the polygon or on the boundary
    $intersections = 0; 
    $vertices_count = count($vertices);

    for ($i=1; $i < $vertices_count; $i++) {
        $vertex1 = $vertices[$i-1]; 
        $vertex2 = $vertices[$i];
        if ($vertex1['y'] == $vertex2['y'] and $vertex1['y'] == $point['y'] and $point['x'] > min($vertex1['x'], $vertex2['x']) and $point['x'] < max($vertex1['x'], $vertex2['x'])) { // Check if point is on an horizontal polygon boundary
            return "boundary";
        }
        if ($point['y'] > min($vertex1['y'], $vertex2['y']) and $point['y'] <= max($vertex1['y'], $vertex2['y']) and $point['x'] <= max($vertex1['x'], $vertex2['x']) and $vertex1['y'] != $vertex2['y']) { 
            $xinters = ($point['y'] - $vertex1['y']) * ($vertex2['x'] - $vertex1['x']) / ($vertex2['y'] - $vertex1['y']) + $vertex1['x']; 
            if ($xinters == $point['x']) { // Check if lat lng is on the polygon boundary (other than horizontal)
                return "boundary";
            }
            if ($vertex1['x'] == $vertex2['x'] || $point['x'] <= $xinters) {
                $intersections++; 
            }
        } 
    } 
    // If the number of edges we passed through is odd, then it's in the polygon. 
    if ($intersections % 2 != 0) {
        return true; //"inside polygon"
    } else {
        return false; //"öutside polygon"
    }
}

function pointOnVertex($point, $vertices) {
  foreach($vertices as $vertex) {
      if ($point == $vertex) {
          return true;
      }
  }

}

function pointStringToCoordinates($pointString) {
    $coordinates = explode(";-", $pointString);
    return array("x" => $coordinates[0], "y" => $coordinates[1]);
}
// Function to check lat lng
function check(){
    $points = array("22.367582 70.711816", "21.43567582 72.5811816","22.367582117085913 70.71181669186944","22.275334996986643 70.88614147123701","22.36934302329968 70.77627818998701"); // Array of latlng which you want to find
    $polygon = array(
        "22.367582117085913 70.71181669186944",
        "22.225161442616514 70.65582486840117",
        "22.20736264867434 70.83229276390898",
        "22.18701840565626 70.9867880031668",
        "22.22452581029355 71.0918447658621",
        "22.382709129816103 70.98884793969023",
        "22.40112042636022 70.94078275414336",
        "22.411912121843205 70.7849142238699",
        "22.367582117085913 70.71181669186944"
    );
    // The last lat lng must be the same as the first one's, to "close the loop"
    foreach($points as $key => $point) {
        echo "(Lat Lng) " . ($key+1) . " ($point): " . $this->pointInPolygon($point, $polygon) . "<br>";
    }
}

public function cityArea_get($assigned_location,$latitude,$longitude,$serviceType)
{
    $res = $this->admin_model->getCityArea($assigned_location);
    $boundaries = []; 
    $bound = []; 
    $finalbound = []; 
    $polygon = [];
    $polygonBoundaries = [];

    //$point = "14.6819;-121.0421"; // x-coordinate of the point to test
    $point =  "$latitude;-$longitude";
    $arr = [];   

    foreach ($res as $key => $val) {
         $polygon= [];
         //$boundaries = [];
          $boundaries = $this->admin_model->getBoundaries($val["assigned_location"],$val["id"],0);
           foreach ($boundaries as $key => $b) {
             array_push($polygon,$b["latitude"].";-".$b["longitude"]);
             
           }
 

         //$points_polygon = count($vertices_x) - 1;
          //array_push($arr,$boundaries);

         if(count($polygon)>0)
         {
          array_push($bound,
            ['city_id'=> $val["id"],
             'zone_id'=>0,
             'iszone' => $val["is_zone"],
             'assigned_location'=>$val["assigned_location"],
             'isInsidePolygon'=>$this->pointInPolygon($point, $polygon),
             'polygonBoundaries'=>$boundaries
            ]);
         }
      }
      

    foreach($bound as $key => $boundary){
      if($boundary["isInsidePolygon"] == true && $boundary["iszone"]==1 && $serviceType ==1)
      {
       $finalbound = $this->zonearea($latitude,$longitude,$boundary["assigned_location"],$boundary["city_id"]);
      }else if($boundary["isInsidePolygon"] == true)
      {
        $finalbound = [$boundary];
      }
    }  

    $response = (object)[];
    $response->data = (object)["polygonArea" => $finalbound];
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
}

  public function zonearea($latitude,$longitude,$assigned_location,$muni)
  {
    $res = $this->admin_model->getZoneArea($assigned_location,$muni);
    $boundaries = []; 
    $bound = []; 
    
    $polygon = [];
    $finalbound = [];
    //$point = "14.6819;-121.0421"; // x-coordinate of the point to test
    $point =  "$latitude;-$longitude";
    $arr = [];   

    foreach ($res as $key => $val) {
         $polygon= [];
         $boundaries = [];
         $boundaries = $this->admin_model->getBoundaries($val["assigned_location"],$val["mun_cities"],$val["id"]);
        
         foreach ($boundaries as $key => $b) {
           array_push($polygon,$b["latitude"].";-".$b["longitude"]);
           
         }

         //$points_polygon = count($vertices_x) - 1;
         if(count($polygon)>0)
         {
          array_push($bound,
            ['city_id'=> $val["mun_cities"],'zone_id'=>$val["id"],
             'iszone' => 1,
             'assigned_location'=>$val["assigned_location"],
             'isInsidePolygon'=>$this->pointInPolygon($point, $polygon),
             'polygonBoundaries'=>$boundaries
            ]);
         }
        
         //array_push($arr,$boundaries);
        
    }

    foreach($bound as $key => $boundary){
       if($boundary["isInsidePolygon"] == true)
      {
        $finalbound = [$boundary];
      }else{
        $finalbound = [];
      }
    }  

   return $finalbound;

   /* $response = (object)[];
    $response->data = (object)["polygonArea" => $bound];
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);*/
  }

  public function serviceablearea_get(){
      $res = $this->admin_model->getDynamicPlaces();

    $response = (object)[];
    $response->data =   $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }

  public function appversion_get($app){
    $this->db->where('app',$app);
    $res = $this->db->get('app_version')->row();

    $response = (object)[];
    $response->data =   $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }

}

