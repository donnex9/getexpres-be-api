<?php

class Inventory extends Crud_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('api/inventory_model', 'inventory_model');
  }

  function partner_get($partner_id)
  {
    $response = (object)[];
    $res = $this->inventory_model->getInventory($partner_id);

    if($res){ # Respond with 404 when the resource is not found
      $response->data = $res;
      $response->meta = (object)[
        'message' => 'OK',
        'code' => 'ok',
        'status' => 200
      ];
    }else{
      $response->data = (object)[];
      $response->meta = (object)[
        'message' => 'Not found',
        'code' => 'not_found',
        'status' => 404
      ];
    }

    $this->response($response, $response->meta->status);
  }

  function item_get($id)
  {
    $response = (object)[];
    $res = $this->inventory_model->get($id);

    if($res){ # Respond with 404 when the resource is not found
      $response->data = $res;
      $response->meta = (object)[
        'message' => 'OK',
        'code' => 'ok',
        'status' => 200
      ];
    }else{
      $response->data = (object)[];
      $response->meta = (object)[
        'message' => 'Not found',
        'code' => 'not_found',
        'status' => 404
      ];
    }

    $this->response($response, $response->meta->status);
  }

  function categories_get($partner_id)
  {
    $response = (object)[];
    $res = $this->inventory_model->getCategories($partner_id);

    if ($res) {
      $response->data = $res;
      $response->meta = (object)[
        'message' => 'OK',
        'code' => 'ok',
        'status' => 200
      ];
    } else {
      $response->data = (object)[];
      $response->meta = (object)[
        'message' => 'Not found',
        'code' => 'not_found',
        'status' => 404
      ];
    }

    $this->response($response, $response->meta->status);
  }


 function partnerSearcByCategory_get($location)
  {
    $response = (object)[];
    $res = $this->inventory_model->getInventoryByKeyword($location);

    if($res){ # Respond with 404 when the resource is not found
      $response->data = $res;
      $response->meta = (object)[
        'message' => 'OK',
        'code' => 'ok',
        'status' => 200
      ];
    }else{
      $response->data = (object)[];
      $response->meta = (object)[
        'message' => 'Not found',
        'code' => 'not_found',
        'status' => 404
      ];
    }
    $this->response($response, $response->meta->status);
  }

}
