<?php

class Profile extends Crud_controller
{

  function __construct()
  {
    parent::__construct();
    # Do not forget to load your model in your child class

    $this->load->model('api/profile_model');
    # Do not change the 2nd parameter because we need a generic model name for this
  }

  function profile_get($type, $id)
  {

    $res = $this->profile_model->get($id, $type);

    $response = (object)[];

    if($res){ # Respond with 404 when the resource is not found
      $response->data = $res;
      $response->meta = (object)[
        'message' => 'OK',
        'code' => 'ok',
        'status' => 200
      ];
      $this->response($response, 200);
    }else{
      $response->data = (object)[];
      $response->meta = (object)[
        'message' => 'Not found',
        'code' => 'not_found',
        'status' => 404
      ];
      $this->response($response, 404);
    }
  }

  function profile_post($type, $id)
  {
    $data = $this->input->post();

    if ($type == 'riders') {
      $data = array_merge($data, $this->riders_model->upload('identification_document'));
      $data = array_merge($data, $this->riders_model->upload('profile_picture'));
    } else {
      $data = array_merge($data, $this->customers_model->upload('profile_picture'));
    }

    $this->profile_model->updateProfile($id, $data, $type);
    $obj = $this->profile_model->get($id, $type);

    $res = (object)[];
    $res->data = $obj;
    $res->meta = (object)['message' => 'OK', 'code' => 'ok', 'status' => 200];
    $this->response($res, 200);
  }

function updateProfile_post($type, $id)
  {
    $data = $this->input->post();

    if ($type == 'riders') {
     
      $data = array_merge($data, $this->riders_model->upload('profile_picture'));
    } else {
      $data = array_merge($data, $this->customers_model->upload('profile_picture'));
    }

    $this->profile_model->updateProfile($id, $data, $type);
    $obj = $this->profile_model->get($id, $type);

    $res = (object)[];
    $res->data = $obj;
    $res->meta = (object)['message' => 'OK', 'code' => 'ok', 'status' => 200];
    $this->response($res, 200);
  }

  function new_address_post($customer_id){
    $res = $this->profile_model->addAddress($this->input->post(), $customer_id);
    if ($res) {
      $message = "New address added.";
      $code = "ok";
      $status = 201;
    } else {
      $message = "Malformed Syntax";
      $code = "bad_request";
      $status = 400;
    }

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => $message,
      'code' => $code,
      'status' => $status
    ];

    $this->response($response, $response->meta->status);
  }

  public function address_post($address_id)
  {
    $res = $this->profile_model->updateAddress($this->input->post(), $address_id);

    if($res) {
        $message = "Address updated";
        $status = "200";
        $code = "ok";
    } else {
      $message = "Malformed Syntax.";
      $status  = "400";
      $code = "error";
    }

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => $message,
      'code' => $code,
      'status' => $status
    ];
    $this->response($response, $response->meta->status);
  }

}
