<?php

class Services extends Crud_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('api/services_model');
  }

  function index_get()
  {
    $this->db->where_in('id', [3,4,5]);
    $res = $this->services_model->all();

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }

  public function services_vehicles_get($service_id,$location)
  {
    $res = $this->services_model->getVehiclesByService($service_id, $this->input->get('show_inactive'),$location);

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }
 
 public function food_grocery_category_get($isTopCategory,$serviceId)
  {
    $res = $this->services_model->getCategory($isTopCategory,$serviceId);

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  } 

}
