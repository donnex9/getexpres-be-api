<?php

class News extends Crud_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('api/news_model');
  }
 
  function index_get()
  {

    $res = $this->news_model->all();

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }

  function single_get($id)
  {
    $res = $this->news_model->get($id);

    $response = (object)[];

    if($res){ # Respond with 404 when the resource is not found
      $response->data = $res;
      $response->meta = (object)[
        'message' => 'OK',
        'code' => 'ok',
        'status' => 200
      ];
      $this->response($response, 200);
    }else{
      $response->data = (object)[];
      $response->meta = (object)[
        'message' => 'Not found',
        'code' => 'not_found',
        'status' => 404
      ];
      $this->response($response, 404);
    }
  }

   function whatsnews_get($location)
  {
    $res = $this->news_model->whatsnewsPerLocation($location);

    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' => 'OK',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, 200);
  }


}
