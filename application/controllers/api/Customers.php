<?php

class Customers extends Crud_controller
{

  function __construct()
  {
    parent::__construct();
    # Do not forget to load your model in your child class
    $this->load->model('api/customers_model');
    $this->load->model('api/user_device_ids_model', 'udi_model');
    $this->load->library('email');
    # Do not change the 2nd parameter because we need a generic model name for this
  }

  function testOTP_get($otp,$mobileNum){
    $res = (object)[];
    //$res = $this->customers_model->sendOTP($otp,$mobileNum);
   // $res = $this->customers_model->checkifcodeisexist("GP5jGJztEC");
    //$this->response($res, 400);
  }

  function normal_post()
  {
    $_POST['is_email_verified'] = 1;
    $_POST['email'] = strtolower($this->input->post('email'));

    if($last_id = $this->customers_model->add($this->input->post())){
      $obj = $this->customers_model->get($last_id);

      $res = (object)[];
      $res->data = $obj;

      if ($this->input->post('is_email_verified')) {
        $res->meta = (object)['message' => 'Account created successfully! You may now sign-in.', 'code' => 'ok', 'status' => 201];
      } else {
        $res->meta = (object)['message' => 'Account created successfully! Please check your email at '. $this->input->post('email') .' for verification.', 'code' => 'ok', 'status' => 201];
      }

      $this->response_header('Location', api_url($this) .  $last_id);
      $this->response($res, 201);
    }elseif ($last_id === null){
      $res = (object)[];
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Email already exists', 'status' => 409, 'code' => 'email_exists'];

      $this->response($res, 409);
    }else{
      $res = (object)[];
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Malformed syntax', 'status' => 400, 'code' => 'malformed_syntax'];

      $this->response($res, 400);
    }
  }

  function normal2_post()
  {
    $_POST['is_email_verified'] = 1;
    $_POST['email'] = strtolower($this->input->post('email'));
    $isReferralCodeExist = true;
    $dataRec = $this->input->post();
    if (@$dataRec['referral_code']) {
      $isReferralCodeExist = $this->customers_model->checkifcodeisexist($this->input->post('referral_code'));
    }
   
    if ($this->customers_model->mobileNumExists($this->input->post('mobile_num'))) {
      $res = (object)[];
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Mobile number already exists', 'status' => 400, 'code' => 'mobile_num_exists'];
      $this->response($res, 400);
    } else if($isReferralCodeExist == false){
      $res = (object)[];
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Invalid referral code', 'status' => 400, 'code' => 'referral_code_not_exist'];

      $this->response($res, 400);
    }

    else {



      if($last_id = $this->customers_model->add2($this->input->post())){
        $obj = $this->customers_model->get($last_id);

        $res = (object)[];
        $res->data = $obj;

        if (!$obj->mobile_otp) {

          $res->meta = (object)['message' => 'Account created successfully! You may now sign-in.', 'code' => 'ok', 'status' => 201];
        } else {
           //$this->customers_model->sendOTP($obj->mobile_otp,$obj->mobile_num);
          $res->meta = (object)['message' => 'Account created successfully! Please complete the OTP challenge to proceed.', 'code' => 'otp_challenge', 'status' => 201];
        }

        // if ($this->input->post('is_email_verified')) {
        //   $res->meta = (object)['message' => 'Account created successfully! You may now sign-in.', 'code' => 'ok', 'status' => 201];
        // } else {
        //   $res->meta = (object)['message' => 'Account created successfully! Please check your email at '. $this->input->post('email') .' for verification.', 'code' => 'ok', 'status' => 201];
        // }

        $this->response_header('Location', api_url($this) .  $last_id);
        $this->response($res, 201);
      }elseif ($last_id === null){
        $res = (object)[];
        $res->data = (object)[];
        $res->meta = (object)['message' => 'Email already exists', 'status' => 409, 'code' => 'email_exists'];

        $this->response($res, 409);
      }else{
        $res = (object)[];
        $res->data = (object)[];
        $res->meta = (object)['message' => 'Malformed syntax', 'status' => 400, 'code' => 'malformed_syntax'];

        $this->response($res, 400);
      }

    }

  }

  function sign_in_post()
  {
    $_POST['email'] = strtolower($this->input->post('email'));

    $res = (object)[];
    $signin = $this->customers_model->signIn($this->input->post());
    if ($signin === 'account_exists_in_a_different_signin_method') {
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Account registered to another sign-in method. Please try a different sign-in method', 'code' => 'account_exists_in_a_different_signin_method', 'status' => 409];
      $this->response($res, 409);
    } else if ($signin === 'otp_challenge') {
      $res->data = (object)[];
      $res->meta = (object)['message' => 'OTP challenge required. Please update to a newer version to continue.', 'code' => 'otp_challenge', 'status' => 409];
      $this->response($res, 409);
    } else if ($signin === 'email_not_verified') {
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Account not yet activated', 'code' => 'account_not_active', 'status' => 409];
      $this->response($res, 409);
    } else if ($signin === 'account_is_banned') {
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Sorry, you have been banned from this app', 'code' => 'account_is_banned', 'status' => 409];
      $this->response($res, 409);
    } else if ($signin){
      $res->data = $signin;
      $res->meta = (object)['message' => 'Sign-in successful', 'code' => 'ok', 'status' => 200];
      $this->response($res, 200);
    } else {
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Invalid credentials', 'code' => 'invalid_credentials', 'status' => 409];
      $this->response($res, 409);
    }

  }

  function sign_in2_post()
  {
    $_POST['email'] = strtolower($this->input->post('email'));

    $res = (object)[];
    $signin = $this->customers_model->signIn($this->input->post());
    if ($signin === 'account_exists_in_a_different_signin_method') {
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Account registered to another sign-in method. Please try a different sign-in method', 'code' => 'account_exists_in_a_different_signin_method', 'status' => 409];
      $this->response($res, 409);
    } else if ($signin === 'email_not_verified') {
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Account not yet activated', 'code' => 'account_not_active', 'status' => 409];
      $this->response($res, 409);
    } else if ($signin === 'otp_challenge') {
      $res->data = $this->customers_model->getByEmail($this->input->post('email'), 'customers');
      $res->meta = (object)['message' => 'Please complete the OTP challenge to proceed.', 'code' => 'otp_challenge', 'status' => 409];
      $this->response($res, 200);
    } else if ($signin === 'account_is_banned') {
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Sorry, you have been banned from this app', 'code' => 'account_is_banned', 'status' => 409];
      $this->response($res, 409);
    } else if ($signin){
      $res->data = $signin;
      $res->meta = (object)['message' => 'Sign-in successful', 'code' => 'ok', 'status' => 200];
      $this->response($res, 200);
    } else {
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Invalid credentials', 'code' => 'invalid_credentials', 'status' => 409];
      $this->response($res, 409);
    }

  }

  function sign_in_otp_post($table)
  {
    $_POST['email'] = strtolower($this->input->post('email'));

    $res = (object)[];
    $signin = $this->customers_model->verifyOTP($this->input->post(), $table);
    // var_dump($signin); die();
    if ($table == 'riders') {

      if (!@$signin->is_admin_verified && $signin != false){
        $res->data = $signin;
        $res->meta = (object)['message' => 'OTP challenge successful! Please wait for the Administrator to verify your account then try to sign-in again.', 'code' => 'account_not_active_or_verified', 'status' => 409];
        $this->response($res, 200);
      } else if (@$signin->is_admin_verified && $signin != false){
        $res->data = $signin;
        $res->meta = (object)['message' => 'Sign-in successful', 'code' => 'ok', 'status' => 200];
        $this->response($res, 200);
      } else {
        $res->data = (object)[];
        $res->meta = (object)['message' => 'Invalid one-time-pin', 'code' => 'invalid_credentials', 'status' => 409];
        $this->response($res, 409);
      }

    } else if ($table == 'customers') {

      if ($signin){
         
        $customerId  = $signin->id;
        $partnerCode = $signin->referral_code;
        $validatecode = $this->customers_model->validatepartnercode($partnerCode);

        if ($validatecode["status"] == "SUCCESS") {
          $newcode     =  $this->customers_model->generateNewreferralCode(true);   
          $isInserted  =  $this->customers_model->insertnewGpPartner($newcode,$customerId,$validatecode["dataLayer"],true);
        }
       
        $res->data = $signin;
        $res->meta = (object)['message' => 'Sign-in successful', 'code' => 'ok', 'status' => 200];
        $this->response($res, 200);
      } else {
        $res->data = (object)[];
        $res->meta = (object)['message' => 'Invalid one-time-pin', 'code' => 'invalid_credentials', 'status' => 409];
        $this->response($res, 409);
      }

    }


  }

  function resend_otp_post($table)
  {
    $_POST['email'] = strtolower($this->input->post('email'));

    $res = (object)[];
    $signin = $this->customers_model->resendOTP($this->input->post('email'), $table);

    if ($signin){
      $res->data = (object)[];
      $res->meta = (object)['message' => 'OTP resent successfully', 'code' => 'ok', 'status' => 200];
      $this->response($res, 200);
    } else {
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Unable to resend OTP', 'code' => 'bad_request', 'status' => 409];
      $this->response($res, 409);
    }

  }

  function social_sign_in_post($table)
  {
    $res = (object)[];
    $signin = $this->customers_model->socialSignIn($this->input->post(), $table);
    if ($signin === 'safe_to_sign_up'){
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Safe to sign-up', 'code' => $signin, 'status' => 200];
      $this->response($res, 200);
    } else if ($signin === null) { # ok
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Account registered to another sign-in method. Please try a different sign-in method', 'code' => 'account_exists_in_a_different_signin_method', 'status' => 409];
      $this->response($res, 409);
    } else if ($signin === 'account_is_banned') {
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Sorry, you have been banned from this app', 'code' => 'account_is_banned', 'status' => 409];
      $this->response($res, 409);
    } else if ($signin === 'social_token_mismatch') { # ok
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Social token mismatch', 'code' => 'social_token_mismatch', 'status' => 409];
      $this->response($res, 409);
    } else if ($signin === false) { # ok
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Rider account not yet verified. Please wait for the administrator to verify your account', 'code' => 'rider_unverified', 'status' => 409];
      $this->response($res, 409);
    } else if (is_object($signin)){
      $res->data = $signin;
      $res->meta = (object)['message' => 'Sign-in successful', 'code' => 'ok', 'status' => 200];
      $this->response($res, 200);
    } else {
      $res->data = (object)[];
      $res->meta = (object)['message' => 'Malformed Syntax', 'code' => 'malformed_syntax', 'status' => 400];
      $this->response($res, 400);
    }

  }

   public function sign_out_post()
  {
    # Set Defaults
    $logout = $this->udi_model->logoutDevice($this->input->post('device_id'), 'customers');

    $response = (object)[];
    $response->data = (object)[];
    $response->meta = (object)[
      'message' => 'Logout success. Device notifications set to inactive state.',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, $response->meta->status);
  }

   public function deactivateAccount_get($userid)
  {
    # Set Defaults
    $deactivated = $this->customers_model->deactivateAccount($userid);

    $response = (object)[];
    $response->data = (object)[];
    $response->meta = (object)[
      'message' => 'Account Successfully deleted.',
      'code' => 'ok',
      'status' => 200
    ];

    $this->response($response, $response->meta->status);
  }

  function sendEmail_post($type){
   $to = strtolower($this->input->post('email'));
   $subject = "This is a test";
   $message = "Hello test message";
 
  // $this->load->library('email');
   $this->email->from('eanoblejr@gmail.com');
   $this->email->to('donnexnoble@gmail.com');
   $this->email->subject($subject);
   $this->email->message($message);
  try{  
 //    if($this->email->send()){
     $mess =  $this->email->send();
   //  }else{
    // $mess =  'Message unsent.';
    //}
  }catch(Exception $e){
    $mess =  $e->getMessage();
  }
  // $mess =  $this->customers_model->sendMail($to, $subject, $message);

   $response = (object)[];
    $response->data = (object)[];
    $response->meta = (object)[
      'message' => $mess,
      'code' => '105',
      'status' => 'status'
    ];

    $this->response($response, $response->meta->status);
 }

  function forgot_password_post($type)
  {
    # Set Defaults
    $email = strtolower($this->input->post('email'));
    $res = $this->customers_model->forgotPassword($email, $type);
    if ($res) {
      $mess = "Password reset link was sent to $email";
      $code = "ok";
      $status = 200;
    } else {
      $mess = "$email does not exist in our records or failure in mail system.";
      $code = "bad_request";
      $status = 409;
    }

    $response = (object)[];
    $response->data = (object)[];
    $response->meta = (object)[
      'message' => $mess,
      'code' => $code,
      'res'  => $res,
      'status' => $status
    ];

    $this->response($response, $response->meta->status);
  }

 function visitedsabong_post($customerid)
  {
    
    $res = $this->customers_model->esabongMonitoring($customerid);
    
     if ($res) {
      $mess = "Successfully updated";
      $code = "ok";
      $status = 200;
    } else {
      $mess = "Error occured";
      $code = "bad_request";
      $status = 409;
    }

    $response = (object)[];
    $response->data = (object)[];
    $response->meta = (object)[
      'message' => $mess,
      'code' => $code,
      'status' => $status
    ];

    $this->response($response, $response->meta->status);
  }

}
