<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends Admin_core_controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->model('cms/news_model');
    $this->load->model('cms/partners_model');
  }

  public function index()
  {
    $res = $this->news_model->all();

    $data['partners'] = $this->partners_model->all_partners();
    $data['res'] = $res;
    $data['allLocation'] = $this->session->userdata('allLocation');
    $data['startingPK'] = $this->input->get('page') && $this->input->get('page') != 1  ? (($this->news_model->per_page) * ($this->input->get('page') - 1)) + 1: 1;

    $this->news_model->squery(['title']);
    $data['total_pages'] = $this->news_model->getTotalPages();

    $this->wrapper('cms/news', $data);
  }

  public function update()
  {
    $id = $this->input->post('id');
    unset($_POST['id']);

    $data = array_merge($this->input->post(), $this->news_model->upload('banner_image'));

    if($this->news_model->update($id, $data)){
      $this->session->set_flashdata('flash_msg', ['message' => 'Item updated successfully', 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'Error updating item', 'color' => 'red']);
    }
    redirect('cms/news');
  }

  public function add()
  {
    $data = array_merge($this->input->post(), $this->news_model->upload('banner_image'));

    $last_id = $this->news_model->add($data);
    if($last_id){
      $this->session->set_flashdata('flash_msg', ['message' => 'Item added successfully', 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'Error adding item', 'color' => 'red']);
    }
    redirect('cms/news');
  }

  public function delete()
  {
    # todo delete related
    if($this->news_model->delete($this->input->post('id'))){
      $this->session->set_flashdata('flash_msg', ['message' => 'Item deleted successfully', 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'Error deleting item', 'color' => 'red']);
    }
    redirect('cms/news');
  }
}
