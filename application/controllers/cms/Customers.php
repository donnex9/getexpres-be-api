<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends Admin_core_controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->model('cms/customers_model');
    $this->load->model('api/notifications_model');
  }

  public function index()
  {
    $res = $this->customers_model->all();

    $data['res'] = $res;
    $data['startingPK'] = $this->input->get('page') && $this->input->get('page') != 1  ? ((($this->customers_model->per_page) * ($this->input->get('page') - 1) + 1)): 1;
    

    //$this->db->where('assigned_location', $this->session->userdata('location'));
    $this->db->order_by('id', 'DESC');
    $this->customers_model->squery(['full_name']);
    $data['total_pages'] = $this->customers_model->getTotalPages();

    $this->wrapper('cms/customers', $data);
  }

  public function update()
  {
    $id = $this->input->post('id');
    unset($_POST['id']);

    $data = array_merge($this->input->post(), $this->customers_model->upload('profile_picture'));

    if($this->customers_model->update($id, $data)){
      $this->session->set_flashdata('flash_msg', ['message' => 'Item updated successfully', 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'Error updating item', 'color' => 'red']);
    }
    redirect('cms/customers');
  }

  public function add()
  {
    $data = array_merge($this->input->post(), $this->customers_model->upload('profile_picture'));

    $last_id = $this->customers_model->add($data);
    if($last_id){
      $this->session->set_flashdata('flash_msg', ['message' => 'Item added successfully', 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'Error adding item', 'color' => 'red']);
    }
    redirect('cms/customers');
  }

  public function delete()
  {
    # todo delete related
    if($this->customers_model->delete($this->input->post('id'))){
      $this->session->set_flashdata('flash_msg', ['message' => 'Item deleted successfully', 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'Error deleting item', 'color' => 'red']);
    }
    redirect('cms/customers');
  }

  public function banning($type)
  {
    // var_dump($type, $_POST); die();
    if ($type == 'ban') {
      $res = $this->customers_model->manageBan(date('Y-m-d H:i:s'), 'customers', $this->input->post('id'));
      if ($res) {
        $this->notifications_model->sendPushNotif(
          $this->input->post('id'),
          'customers',
          ['data' => ['force_logout' => 1]],
          ['title' => 'Your account has been banned', 'body' => 'Please contact the Administrator if you want to send an appeal']
        );
      }
      $this->session->set_flashdata('flash_msg', ['message' => 'User banned successfully', 'color' => 'green']);
    } else if ($type == 'unban') {
      $this->customers_model->manageBan(null, 'customers' , $this->input->post('id'));
      $this->session->set_flashdata('flash_msg', ['message' => 'User unbanned successfully', 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'Error deleting item', 'color' => 'red']);
    }

    redirect('cms/customers');
  }
}
