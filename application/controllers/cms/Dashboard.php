<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_core_controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('cms/admin_model', 'admin_model');
    $this->load->model('api/vehicles_model');
    $this->load->model('api/services_model');
  }

  public function index()
  {
    $this->dashboard();
  }

  public function dashboard()
  {
    
    if($this->session->role == 'administrator'){
      $res = $this->admin_model->all();
      $resLocation = $this->getLocation();
      $data['allLocation'] = $resLocation;
      $data['res'] = $res;
      $data["userLevel"] = $this->db->get('tbl_user_level')->result();
      $userlevel =  $this->session->userdata('userlevel'); 
      $data["accesslevel"] = $this->admin_model->getuseraccess('22',$userlevel);

      $this->wrapper('cms/index', $data);
    }else if($this->session->role == 'partner')
    {
      redirect('cms/partners');
    }
    else
    {
       $this->session->sess_destroy();
        redirect('cms/login');
    }
    
  }

  public function getLocation()
  {
    $query = $this->db->get("dynamic_radius_places");
    return $query->result_array();
  }

  public function accessmanagement()
  {
    $this->db->order_by('menu','ASC');
    $data['menu'] = $this->db->get('tbl_menu')->result();

    $data['userlevel'] = $this->db->get('tbl_user_level')->result();
    $data['accesslevel'] = $this->db->get('tbl_access_level')->result();
    
    $data['flash_msg'] = $this->session->flash_msg;
    $this->wrapper('cms/access_management', $data);
  }

  public function saveaccesslevel()
  {

   $datarec = json_decode(file_get_contents("php://input"), true);
   $aid     =  $datarec["aid"];
   $response = (object)[];
   $data = [];
   
   $data = [
             "can_add"=>$datarec["can_add"],
             "can_delete" => $datarec["can_delete"],
             "can_edit"=>$datarec["can_edit"],
             "can_view" => $datarec["can_view"]
           ];       
    
    $this->db->where('aid',$aid);
    $res = $this->db->update('tbl_access_level',$data);

    $response->data = $res ? true: false;
    if($res)
    {
      $response->meta = (object)[
      'message' => "Successfully Updated.",
      'code' => '200',
      'status' => 'ok'
    ];

    }else
    {
      $response->meta = (object)[
      'message' => "Sorry, error occured upon saving the data.",
      'code' => '201',
      'status' => 'error'
      ];
    }
    
    print_r(json_encode($response));

  }

  public function update()
  {

    $data = json_decode(file_get_contents("php://input"), true);
    $id = $data['id'];
    $forupdate = $data["data"];
    if($forupdate["area_assigned"] != null || $forupdate["area_assigned"] != ""){
      $areaAssigned = implode(',',$forupdate["area_assigned"]);
    }else{
      $areaAssigned = null;
    }
    
    $forupdate["area_assigned"] = $areaAssigned;
    //unset($_POST['id']);
    $res = (object)[];
    $res = $this->admin_model->updateAdmin($id,$forupdate);
  //   $this->db->where('id',$id);
   // $res = $this->db->update('admin',$forupdate);
   
   print_r(json_encode($res));
  }

  public function add()
  {
    $last_id = $this->admin_model->add($this->input->post());
    if($last_id){
      $this->session->set_flashdata('flash_msg', ['message' => 'Item added successfully', 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'Error adding item', 'color' => 'red']);
    }
    redirect('cms/dashboard');
  }

  public function delete()
  {
    # todo delete related
    if($this->admin_model->delete($this->input->post('id'))){
      $this->session->set_flashdata('flash_msg', ['message' => 'Item deleted successfully', 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'Error deleting item', 'color' => 'red']);
    }
    redirect('cms/dashboard');
  }

  public function fees_management()
  {
    if($this->session->role == 'administrator')
    {
       $data['food_fee'] = (double)$this->admin_model->getMeta('food_fee',$this->session->userdata('location'));
    $data['pabili_fee'] = (double)$this->admin_model->getMeta('pabili_fee',$this->session->userdata('location'));
    $data['grocery_fee'] = (double)$this->admin_model->getMeta('grocery_fee',$this->session->userdata('location'));

    $data['food_per_km'] = (double)$this->admin_model->getMeta('food_per_km',$this->session->userdata('location'));
    $data['grocery_per_km'] = (double)$this->admin_model->getMeta('grocery_per_km',$this->session->userdata('location'));
    $data['pabili_per_km'] = (double)$this->admin_model->getMeta('pabili_per_km',$this->session->userdata('location'));

    $data['flash_msg'] = $this->session->flash_msg;

    $this->wrapper('cms/fees_management', $data);
    }else
    {
      echo "Sorry Sorry, you are not allowed to access the page.";
    }
    
  }

  public function fare_management()
  {
    if($this->session->role == 'administrator')
    {
      $data['car_fare'] =(double) $this->admin_model->getMeta('car_fare',$this->session->userdata('location'));
      $data['delivery_fare'] = (double)$this->admin_model->getMeta('delivery_fare',$this->session->userdata('location'));
      $data['delivery_per_km'] = (double)$this->admin_model->getMeta('delivery_per_km',$this->session->userdata('location'));

      

      $data['flash_msg'] = $this->session->flash_msg;
       $this->wrapper('cms/fare_management', $data);
    }else
    {
      echo "Sorry Sorry, you are not allowed to access the page.";
    }

  }

  public function surge_fees()
  {

    if($this->session->role == 'administrator')
    {
      $data['rainy_day_surge_rate'] = (double) $this->admin_model->getMetaSurgeFees('rainy_day_surge_rate',$this->session->userdata['location']);
    $data['rainy_day_surge_label'] = $this->admin_model->getMetaSurgeFees('rainy_day_surge_label',$this->session->userdata['location']);
    $data['rainy_day_surge_is_active'] = (int) $this->admin_model->getMetaSurgeFees('rainy_day_surge_is_active',$this->session->userdata['location']);

    $data['scheduled_surge_rate'] = (double) $this->admin_model->getMetaSurgeFees('scheduled_surge_rate',$this->session->userdata['location']);
    $data['scheduled_surge_label'] = $this->admin_model->getMetaSurgeFees('scheduled_surge_label',$this->session->userdata['location']);
    $data['scheduled_surge_days'] = unserialize($this->admin_model->getMetaSurgeFees('scheduled_surge_days',$this->session->userdata['location']));
    $data['scheduled_surge_time'] = unserialize($this->admin_model->getMetaSurgeFees('scheduled_surge_time',$this->session->userdata['location']));
    $data['scheduled_surge_is_active'] = (int) $this->admin_model->getMetaSurgeFees('scheduled_surge_is_active',$this->session->userdata['location']);

    $data['flash_msg'] = $this->session->flash_msg;
    $userlevel =  $this->session->userdata('userlevel'); 
    $data["accesslevel"] = $this->admin_model->getuseraccess('12',$userlevel);

    $this->wrapper('cms/surge_fees', $data);
    }else
    {
      echo "Sorry Sorry, you are not allowed to access the page.";
    }

    
  }

  public function delivery_management()
  {
    if($this->session->role == 'administrator')
    {
       $data['package_options'] = $this->admin_model->getMeta('package_options',$this->session->userdata('location'));
    $userlevel =  $this->session->userdata('userlevel'); 
    $data["accesslevel"] = $this->admin_model->getuseraccess('11',$userlevel);
    $data['flash_msg'] = $this->session->flash_msg;
    $this->wrapper('cms/delivery_management', $data);
    }else
    {
      echo "Sorry Sorry, you are not allowed to access the page.";
    }

   
  }

  public function vehicles_management()
  {
    $data['res'] = $this->vehicles_model->all();
    // var_dump($data); die();
    $data['flash_msg'] = $this->session->flash_msg;

    $userlevel =  $this->session->userdata('userlevel'); 
    $data["accesslevel"] = $this->admin_model->getuseraccess('16',$userlevel);

    $this->wrapper('cms/vehicles_management', $data);
  }

  public function rider_wallet_management()
  {
    if($this->session->role == 'administrator')
    {
      $data['rider_wallet_offers'] = $this->admin_model->getRiderWalletOffers(3);
    $data['topup_description'] = $this->admin_model->getMeta('topup_description',$this->session->userdata('location'));
    $data['flash_msg'] = $this->session->flash_msg;

    $this->wrapper('cms/rider_wallet_management', @$data);
    }else
    {
      echo "Sorry Sorry, you are not allowed to access the page.";
    }
  }


  public function vehicle_per_service()
  {
    if($this->session->role == 'administrator')
    {
      $this->db->select('services_vehicles.id, services_vehicles.vehicle_id, services_vehicles.service_id,
    vehicles.name, services.label');
    $this->db->join('vehicles', 'services_vehicles.vehicle_id = vehicles.id', 'left');
    $this->db->join('services', 'services_vehicles.service_id = services.id', 'left');
    $this->db->where('dynamic_location',$this->session->userdata('location'));
    $res = $this->db->get('services_vehicles')->result();

    $data['services'] = $this->services_model->all();
    $data['vehicles'] = $this->vehicles_model->all();

    $data['res_1'] = [];
    $data['res_2'] = [];
    $data['res_3'] = [];
    $data['res_4'] = [];
    $data['res_5'] = [];
    $data['res_7'] = [];
    foreach ($res as $key => $value) {
      switch ($value->service_id) {
        case 1:
          $data['res_1'][] = $value; #car
          break;
        case 2:
          $data['res_2'][] = $value; #grocery
          break;
        case 3:
          $data['res_3'][] = $value; #pabili
          break;
        case 4:
          $data['res_4'][] = $value; #delivery
          break;
        case 5:
          $data['res_5'][] = $value; #food
          break;

          case 7:
          $data['res_7'][] = $value; #Laundry
          break;

        default:
          break;
      }
    }
    // var_dump($data['res_1']); die();
    $userlevel =  $this->session->userdata('userlevel'); 
    $data["accesslevel"] = $this->admin_model->getuseraccess('15',$userlevel);

    $this->wrapper('cms/vehicle_per_service', $data);

    }else
    {
      echo "Sorry Sorry, you are not allowed to access the page.";
    }

    
  }


  public function rates_management()
  {
    if($this->session->role == 'administrator')
    {
      $data['rider_wallet_offers'] = $this->admin_model->getRiderWalletOffers(3);
    $data['topup_description'] = $this->admin_model->getMeta('topup_description',$this->session->userdata('location'));
    $data['flash_msg'] = $this->session->flash_msg;

    $data['vehicles'] = $this->vehicles_model->all();
    $data['services'] = $this->services_model->all();
    $data['rates'] = $this->admin_model->getAllRates();

   
    $data['pabili_fee_additional'] = (double)$this->admin_model->getMeta('pabili_fee_additional',$this->session->userdata('location'));
    $data['assisted_booking_fee'] = (double)$this->admin_model->getMeta('assisted_booking_fee',$this->session->userdata('location'));
    $data['laundry_service_fee'] = (double)$this->admin_model->getMeta('laundry_service_fee',$this->session->userdata('location'));
    
    $data['ride_per_minute_fee']   = (double)$this->admin_model->getMeta('ride_per_minute_fee',$this->session->userdata('location'));
    $userlevel =  $this->session->userdata('userlevel'); 
      $data["accesslevel"] = $this->admin_model->getuseraccess('13',$userlevel);

    
    $this->wrapper('cms/rates_management', @$data);
    }else
    {
      echo "Sorry Sorry, you are not allowed to access the page.";
    }
 
  }


  public function tricycle_rates_management()
  {
    if($this->session->role == 'administrator')
    {
      
    $data['rider_wallet_offers'] = $this->admin_model->getRiderWalletOffers(3);
    $data['topup_description'] = $this->admin_model->getMeta('topup_description',$this->session->userdata('location'));
    $data['flash_msg'] = $this->session->flash_msg;

    $data['cities'] = $this->admin_model->getcities($this->session->userdata('location'));
    $data['zone']   = $this->admin_model->getzone($this->session->userdata('location'));
    $data['services'] = $this->services_model->all();
    $data['rates']    = $this->admin_model->getAllTrikeRates();
   
    $data['pabili_fee_additional'] = (double)$this->admin_model->getMeta('pabili_fee_additional',$this->session->userdata('location'));
    $data['ride_per_minute_fee']   = (double)$this->admin_model->getMeta('ride_per_minute_fee',$this->session->userdata('location'));
    $userlevel =  $this->session->userdata('userlevel'); 
      $data["accesslevel"] = $this->admin_model->getuseraccess('14',$userlevel);

    $this->wrapper('cms/tricycle_rate_management', @$data);
    }else
    {
      echo "Sorry Sorry, you are not allowed to access the page.";
    }
 
  }

  function clean($string) {
   $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.

    return $string; // Removes special chars.
 }

  public function tricycle_rate_update()
  {
    
    $this->db->where('id', $this->input->post('id'));
    $this->db->update('tricycle_rates', ['base_fare' => $this->input->post('basefare'), 'per_km_fare' => $this->input->post('perkmfare'), 'getapp_commission_percentage' => $this->input->post('getappcommission'),
   'passenger_limit' => $this->input->post('passlimit'), 'admin_fee' => $this->input->post('adminfee'),
   'pabili_fee'=>$this->input->post('pabilifee')]);
    

    $this->session->set_flashdata('flash_msg', ['message' => $jsondata, 'color' => 'green']);
    redirect('cms/dashboard/tricycle_rates_management?meta_key=tricycle_rates_management');

  }

  public function update_services_vehicles()
  {
      $this->db->where('service_id', $this->input->post('service_id'));
      $this->db->where('dynamic_location', $this->session->userdata('location'));
      $this->db->delete('services_vehicles');

      foreach ($this->input->post('vehicle_ids[]') as $key => $value) {
      
        $this->db->insert('services_vehicles', ['vehicle_id' => $value, 'service_id' => $this->input->post('service_id'),'dynamic_location'=>$this->session->userdata('location')]);
      }

      $this->session->set_flashdata('flash_msg', ['message' => 'Updated successfully', 'service_id'=>$this->input->post('service_id'), 'color' => 'green']);

      redirect('cms/dashboard/vehicle_per_service');
  }


  public function update_rate()
  {
    $this->session->set_flashdata('flash_msg', ['message' => 'Updated successfully', 'color' => 'green']);

    $this->db->where('service_id', $this->input->post('service_id'));
    $this->db->where('vehicle_id', $this->input->post('vehicle_id'));
    $this->db->where('dynamic_location', $this->session->userdata('location'));

    $this->db->update('rates', ['base_fare' => $this->input->post('base_fare'), 'per_km_fare' => $this->input->post('per_km_fare'), 'getapp_commission_percentage' => $this->input->post('getapp_commission_percentage'),
   'surge_multiplier' => $this->input->post('surge_multiplier'), 'per_minute_fee' => $this->input->post('per_minute_fee')]);
    redirect('cms/dashboard/rates_management?meta_key=' . $this->input->post('meta_key'));
  }

  public function update_vehicle_availability()
  {
    $this->session->set_flashdata('flash_msg', ['message' => 'Updated successfully', 'color' => 'green']);

    $this->db->where('id', $this->input->post('id'));
    $this->db->update('vehicles', ['is_active' => $this->input->post('is_active')]);
    redirect('cms/dashboard/rates_management?meta_key=' . $this->input->post('meta_key'));
  }

  public function radius_editor()
  {
    if($this->session->role == 'administrator')
    {
          $data['stores_radius'] = $this->admin_model->getStoreRadius($this->session->userdata('location'));
          $data['map_center']    = $this->admin_model->getMapCenter($this->session->userdata('location'));
          $data['flash_msg']     = $this->session->flash_msg;
          $userlevel =  $this->session->userdata('userlevel'); 
          $data["accesslevel"] = $this->admin_model->getuseraccess('17',$userlevel);
          $this->wrapper('cms/radius_editor', @$data);
    }else
    {
      echo "Sorry Sorry, you are not allowed to access the page.";
    }


  }


 public function rider_radius_editor()
  {
    if($this->session->role == 'administrator')
    {
       $data['rider_radius_in_m'] = $this->admin_model->getMeta('rider_radius_in_m',$this->session->userdata('location'));
    $data['flash_msg']         = $this->session->flash_msg;
    $userlevel =  $this->session->userdata('userlevel'); 
    $data["accesslevel"] = $this->admin_model->getuseraccess('18',$userlevel);

    $this->wrapper('cms/rider_radius_editor', @$data);
    }else
    {
      echo "Sorry Sorry, you are not allowed to access the page.";
    }

   
  }

public function updateStoreRadius_editor()
  {
    if($this->session->role == 'administrator')
    {
      $col2 = $this->input->post('meta_key');
    if($this->input->post('meta_key') == "store_radius")
    {
     $col2 = "stores_radius";
    }
    
    $colum =  $this->input->post('meta_key');

    $this->db->where('dynamic_location',$this->session->userdata('location'));
    $this->db->where('meta_key', $col2);
    $this->db->update('options', ['meta_value' => $this->input->post('meta_value')]);

    $this->session->set_flashdata('flash_msg', ['message' => 'Updated successfully', 'color' => 'green']);
    $this->db->where('id',$this->session->userdata('location'));
    $this->db->update('dynamic_radius_places', [ $colum => $this->input->post('meta_value')]);
    redirect($this->input->post('from') . '?meta_key=' . $this->input->post('meta_key'));
    }else
    {
      echo "Sorry Sorry, you are not allowed to access the page.";
    }

  }


  public function maintenance_mode()
  {
    if($this->session->role == 'administrator')
    {
      $maintenance = [];
      $loc = $this->session->userdata('location');

      $maintenance['Get Car']['mm'] = $this->admin_model->getMeta('get_car_mm',$loc);
      $maintenance['Get Food']['mm'] = $this->admin_model->getMeta('get_food_mm',$loc);
      $maintenance['Get Pabili']['mm'] = $this->admin_model->getMeta('get_pabili_mm',$loc);
      $maintenance['Get Delivery']['mm'] = $this->admin_model->getMeta('get_delivery_mm',$loc);
      $maintenance['Get Grocery']['mm'] = $this->admin_model->getMeta('get_grocery_mm',$loc);
      $maintenance['Get Entertainment']['mm'] = $this->admin_model->getMeta('get_entertainment_mm',$loc);
      $maintenance['Get Laundry']['mm'] = $this->admin_model->getMeta('get_laundry_mm',$loc);

      $maintenance['Get Car']['key'] = 'get_car_mm';
      $maintenance['Get Food']['key'] = 'get_food_mm';
      $maintenance['Get Pabili']['key'] = 'get_pabili_mm';
      $maintenance['Get Delivery']['key'] = 'get_delivery_mm';
      $maintenance['Get Grocery']['key'] = 'get_grocery_mm';
      $maintenance['Get Entertainment']['key'] = 'get_entertainment_mm';
      $maintenance['Get Laundry']['key'] = 'get_laundry_mm';

      $maintenance['Get Car']['icon'] = $this->services_model->get(1)->icon;
      $maintenance['Get Food']['icon'] = $this->services_model->get(5)->icon;
      $maintenance['Get Pabili']['icon'] = $this->services_model->get(3)->icon;
      $maintenance['Get Delivery']['icon'] = $this->services_model->get(4)->icon;
      $maintenance['Get Grocery']['icon'] = $this->services_model->get(2)->icon;
      $maintenance['Get Entertainment']['icon'] = $this->services_model->get(6)->icon;
      $maintenance['Get Laundry']['icon'] = $this->services_model->get(7)->icon;

      $data['maintenance'] = $maintenance;
      $data['flash_msg'] = $this->session->flash_msg;

      $userlevel =  $this->session->userdata('userlevel'); 
      $data["accesslevel"] = $this->admin_model->getuseraccess('21',$userlevel);

      $this->wrapper('cms/maintenance_mode', @$data);

    }else
    {
      echo "Sorry Sorry, you are not allowed to access the page.";
    }

   
  }

  public function update_option_serialized()
  {
    $this->session->set_flashdata('flash_msg', ['message' => 'Updated successfully', 'color' => 'green']);

    $id = $this->input->post('id');
    $from = $this->input->post('from');

    unset($_POST['id']);
    unset($_POST['from']);

    $this->db->where('id', $id);
    $this->db->update('options', ['meta_value' => serialize((object)$_POST)]);

    redirect($from . '?offer_id=' . $id);
  }

  public function update_option()
  {
    $this->session->set_flashdata('flash_msg', ['message' => 'Updated successfully', 'color' => 'green']);
    
    $this->db->where('dynamic_location',$this->session->userdata('location'));
    $this->db->where('meta_key', $this->input->post('meta_key'));
    $this->db->update('options', ['meta_value' => $this->input->post('meta_value')]);
    redirect($this->input->post('from') . '?meta_key=' . $this->input->post('meta_key'));
  }

 
  public function update_rainy_day_surge()
  {
    $this->session->set_flashdata('flash_msg', ['message' => 'Updated successfully', 'color' => 'green']);

    $this->db->where('meta_key', 'rainy_day_surge_is_active');
    $this->db->where('dynamic_location', $this->session->userdata('location'));
    $this->db->update('options', ['meta_value' => $this->input->post('rainy_day_surge_is_active') ?: 0]);

    $this->db->where('meta_key', 'scheduled_surge_is_active');
    $this->db->where('dynamic_location', $this->session->userdata('location'));
    $this->db->update('options', ['meta_value' => 0]);

    $this->db->where('meta_key', 'rainy_day_surge_rate');
    $this->db->where('dynamic_location', $this->session->userdata('location'));
    $this->db->update('options', ['meta_value' => $this->input->post('rainy_day_surge_rate') ?: 1]);

    $this->db->where('meta_key', 'rainy_day_surge_label');
     $this->db->where('dynamic_location', $this->session->userdata('location'));
    $this->db->update('options', ['meta_value' => $this->input->post('rainy_day_surge_label')]);

    redirect($this->input->post('from') . '?meta_key=' . $this->input->post('meta_key'));
  }

  public function update_scheduled_surge()
  {
    $this->session->set_flashdata('flash_msg', ['message' => 'Updated successfully', 'color' => 'green']);

    $this->db->where('meta_key', 'rainy_day_surge_is_active');
    $this->db->where('dynamic_location', $this->session->userdata('location'));
    $this->db->update('options', ['meta_value' => 0]);

    $this->db->where('meta_key', 'scheduled_surge_is_active');
    $this->db->where('dynamic_location', $this->session->userdata('location'));
    $this->db->update('options', ['meta_value' => $this->input->post('scheduled_surge_is_active') ?: 0]);

    $this->db->where('meta_key', 'scheduled_surge_rate');
    $this->db->where('dynamic_location', $this->session->userdata('location'));
    $this->db->update('options', ['meta_value' => $this->input->post('scheduled_surge_rate') ?: 1]);

    $this->db->where('meta_key', 'scheduled_surge_label');
    $this->db->where('dynamic_location', $this->session->userdata('location'));
    $this->db->update('options', ['meta_value' => $this->input->post('scheduled_surge_label')]);

    $this->db->where('meta_key', 'scheduled_surge_time');
    $this->db->where('dynamic_location', $this->session->userdata('location'));
    $this->db->update('options', ['meta_value' => serialize([$this->input->post('from_hour'), $this->input->post('to_hour')])]);

    $this->db->where('meta_key', 'scheduled_surge_days');
    $this->db->where('dynamic_location', $this->session->userdata('location'));
    $this->db->update('options', ['meta_value' => serialize($this->input->post('scheduled_surge_days[]'))]);

    redirect($this->input->post('from') . '?meta_key=' . $this->input->post('meta_key'));
  }

  public function update_vehicle()
  {
    $this->session->set_flashdata('flash_msg', ['message' => 'Updated successfully', 'color' => 'green']);
    $this->db->where('id', $this->input->post('id'));
    unset($_POST['id']);
    $this->db->update('vehicles', $this->input->post());

    redirect('cms/dashboard/vehicles_management');
  }

}

