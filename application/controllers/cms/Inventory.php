<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends Admin_core_controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->model('cms/inventory_model');
  }

  public function index()
  {
    $res = $this->inventory_model->all();
    $data['parent_products'] = $this->inventory_model->getParentProducts();
    $data['unique_categories'] = $this->inventory_model->getUniqueCategories();

    $data['res'] = $res;
    $data['startingPK'] = $this->input->get('page') && $this->input->get('page') != 1  ? (($this->inventory_model->per_page) * ($this->input->get('page') - 1)): 1;

    $this->inventory_model->squery(['product_name']);
    $data['total_pages'] = $this->inventory_model->getTotalPages();

    $this->wrapper('cms/inventory', $data);
  }

  public function update()
  {
    $id = $this->input->post('id');
    unset($_POST['id']);

    $data = array_merge($this->input->post(), $this->inventory_model->upload('image'));

    if($this->inventory_model->update($id, $data)){
      $this->session->set_flashdata('flash_msg', ['message' => 'Item updated successfully', 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'Error updating item', 'color' => 'red']);
    }
    redirect('cms/inventory');
  }

  public function add()
  {
    $data = array_merge($this->input->post(), $this->inventory_model->upload('image'));
    // var_dump($data); die();
    $last_id = $this->inventory_model->add($data);
    if($last_id){
      $this->session->set_flashdata('flash_msg', ['message' => 'Item added successfully', 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'Error adding item', 'color' => 'red']);
    }
    redirect('cms/inventory');
  }

  public function delete()
  {
    # todo delete related
    if($this->inventory_model->delete($this->input->post('id'))){
      $this->session->set_flashdata('flash_msg', ['message' => 'Item deleted successfully', 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'Error deleting item', 'color' => 'red']);
    }
    redirect('cms/inventory');
  }

  public function hide()
  {
    # todo delete related
    if($this->inventory_model->hide($this->input->post('id'))){
      $this->session->set_flashdata('flash_msg', ['message' => 'Item hidden successfully', 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'Error hiding the item', 'color' => 'red']);
    }
    redirect('cms/inventory');
  }

  public function unhide()
  {
    # todo delete related
    if($this->inventory_model->unhide($this->input->post('id'))){
      $this->session->set_flashdata('flash_msg', ['message' => 'Item hidden successfully', 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'Error hiding the item', 'color' => 'red']);
    }
    redirect('cms/inventory');
  }
}
