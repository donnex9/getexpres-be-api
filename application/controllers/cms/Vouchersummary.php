<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vouchersummary extends Admin_core_controller {

  public function __construct()
  {
    parent::__construct();
     $this->load->model('cms/voucher_summary_model');
   }


 

  public function index()
  {
    if($this->session->role == 'administrator')
    {
   
    $this->voucher_summary_model->squery(['voucher_code']);
    $this->db->where('assigned_location', $this->session->userdata('location'));
    $this->db->order_by('date(booking_date)', 'desc');
    $res = $this->voucher_summary_model->all();

    $data['res'] = $res;
    $data['allLocation'] = $this->session->userdata('allLocation');
    $data['startingPK'] = $this->input->get('page') && $this->input->get('page') != 1  ? (30 * ($this->input->get('page') - 1) + 1): 1;


    // var_dump($res); die();


   // $this->riders_model->squery(['full_name']);
    $this->db->where('assigned_location', $this->session->userdata('location'));
    $data['total_pages'] = $this->voucher_summary_model->getTotalPages();

    $this->wrapper('cms/voucher_summary', $data);
    }else
    {
      echo "Sorry Sorry, you are not allowed to access the page.";
    }

    
  }


  public function update()
  {
    $id = $this->input->post('id');
    unset($_POST['id']);
    $admin_id = $this->session->userdata('id');
    $data = array_merge($this->input->post(),array('updated_at' => date("Y-m-d H:i:s"),'updated_by'=>$admin_id));
    
    if($this->voucher_summary_model->update($id, $data)){

      $this->session->set_flashdata('flash_msg', ['message' => "Item successfully updated", 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'Error updating item', 'color' => 'red']);
    }
    redirect('cms/vouchersummary');
  }

}

