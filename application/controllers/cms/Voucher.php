<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Voucher extends Admin_core_controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->model('cms/voucher_model');
    $this->load->model('cms/rider_balance_model');
    $this->load->model('cms/customers_model');
    $this->load->model('api/notifications_model');
    $this->load->model('api/Rider_vehicles_model');
    $this->load->model('api/vehicles_model');
     $this->load->model('cms/admin_model');
  }


  public function transactions()
  {
    $this->db->where('assigned_location', $this->session->userdata('location'));
    $res = $this->riders_model->getAllTransactions();

    $data['res'] = $res;
    $data['startingPK'] = $this->input->get('page') && $this->input->get('page') != 1  ? (($this->riders_model->per_page) * ($this->input->get('page') - 1)) + 1: 1;


    
    $data['total_pages'] = $this->riders_model->getTotalPagesRiderWallet();

    $this->wrapper('cms/rider_transactions', $data);
  }

  
  

  public function index()
  {
    if($this->session->role == 'administrator')
    {
   
    $this->voucher_model->squery(['voucher_code']);
    $this->db->where('assigned_location', $this->session->userdata('location'));
     $this->db->where('is_serial',0);
    $this->db->order_by('id', 'desc');
    $res = $this->voucher_model->all();

    $data['res'] = $res;
    $data['allLocation'] = $this->session->userdata('allLocation');
    $data['startingPK'] = $this->input->get('page') && $this->input->get('page') != 1  ? (($this->voucher_model->per_page) * ($this->input->get('page') - 1) + 1): 1;

    // var_dump($res); die();
    $userlevel =  $this->session->userdata('userlevel'); 
    $data["accesslevel"] = $this->admin_model->getuseraccess('19',$userlevel);

   // $this->riders_model->squery(['full_name']);

    $this->db->where('is_serial',0);
    $this->db->where('assigned_location', $this->session->userdata('location'));
    $data['total_pages'] = $this->voucher_model->getTotalPages();

    $this->wrapper('cms/voucher_code', $data);
    }else
    {
      echo "Sorry Sorry, you are not allowed to access the page.";
    }

    
  }

  public function serial()
  {
    if($this->session->role == 'administrator')
    {
   
    $this->voucher_model->squery(['voucher_code']);
    $this->db->where('assigned_location', $this->session->userdata('location'));
    $this->db->where('is_serial',1);
    $this->db->order_by('id', 'desc');
    $res = $this->voucher_model->allSerial();

    $voucherLimitPerDay = $this->admin_model->getMeta("voucherlimitperday",$this->session->userdata('location'));

    $data['res'] = $res;
    $data['voucherlimitperday'] = $voucherLimitPerDay;
    $data['allLocation'] = $this->session->userdata('allLocation');
    $data['startingPK'] = $this->input->get('page') && $this->input->get('page') != 1  ? (($this->voucher_model->per_page) * ($this->input->get('page') - 1) + 1): 1;

    // var_dump($res); die();
    $userlevel =  $this->session->userdata('userlevel'); 
    $data["accesslevel"] = $this->admin_model->getuseraccess('19',$userlevel);

   // $this->riders_model->squery(['full_name']);
    $this->db->where('is_serial',1);
    $this->db->where('assigned_location', $this->session->userdata('location'));
    $data['total_pages'] = $this->voucher_model->getTotalPages();

    $this->wrapper('cms/voucher_serial', $data);
    }else
    {
      echo "Sorry Sorry, you are not allowed to access the page.";
    }
    
  }

  public function generateSerialVoucher()
  {
    $admin_id = $this->session->userdata('id');
    
    $initialCode = $this->input->post('voucher_code');
    $numberOfPieces = $this->input->post('vpiecies');
    $posts = $this->input->post();
    
    unset($posts['voucher_code']);
    unset($posts['vpiecies']);
    $data = array_merge($posts,array('created_at' => date("Y-m-d H:i:s"),'created_by'=>$admin_id,'is_serial'=>1));

    $isSuccess =  $this->voucher_model->addGenerateSerialVoucher($data,$initialCode,$numberOfPieces);

   //$last_id = $this->voucher_model->add($data);
    if($isSuccess == true){
        $this->session->set_flashdata('flash_msg', ['message' => 'Item added successfully', 'color' => 'green']);
    } else {
        $this->session->set_flashdata('flash_msg', ['message' => 'Error adding item', 'color' => 'red']);
    } 
     
    redirect('cms/voucher/serial');
  }


  public function update()
  {
    $id = $this->input->post('id');
    unset($_POST['id']);
    $admin_id = $this->session->userdata('id');
    $data = array_merge($this->input->post(),array('updated_at' => date("Y-m-d H:i:s"),'updated_by'=>$admin_id));
    
    if($this->voucher_model->update($id, $data)){

      $this->session->set_flashdata('flash_msg', ['message' => "Item successfully updated", 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'Error updating item', 'color' => 'red']);
    }
    redirect('cms/voucher');
  }

  public function updateVoucherLimitPerDay()
  {
    
    $data = array_merge($this->input->post(),array('updated_at' => date("Y-m-d H:i:s")));
   // var_dump($data); die();
    
    if($this->voucher_model->updateMeta("voucherlimitperday", $data,$this->session->userdata('location'))){

      $this->session->set_flashdata('flash_msg', ['message' => "Item successfully updated", 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'Error updating item', 'color' => 'red']);
    }
    redirect('cms/voucher/serial');
  }

public function updateSerial()
  {
    $id = $this->input->post('id');
    unset($_POST['id']);
    $admin_id = $this->session->userdata('id');
    $data = array_merge($this->input->post(),array('updated_at' => date("Y-m-d H:i:s"),'updated_by'=>$admin_id));
    
    if($this->voucher_model->update($id, $data)){

      $this->session->set_flashdata('flash_msg', ['message' => "Item successfully updated", 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'Error updating item', 'color' => 'red']);
    }
    redirect('cms/voucher/serial');
  }



  public function addNewVoucher()
  {
    $admin_id = $this->session->userdata('id');
    $data = array_merge($this->input->post(),array('created_at' => date("Y-m-d H:i:s"),'created_by'=>$admin_id));

    $last_id = $this->voucher_model->add($data);
	  if($last_id){
	      $this->session->set_flashdata('flash_msg', ['message' => 'Item added successfully', 'color' => 'green']);
	  } else {
	      $this->session->set_flashdata('flash_msg', ['message' => 'Error adding item', 'color' => 'red']);
	  }	
     
    redirect('cms/voucher');
  }

  

  public function delete()
  {
    # todo delete related
    if($this->voucher_model->delete($this->input->post('id'))){
      $this->session->set_flashdata('flash_msg', ['message' => 'Item deleted successfully', 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'There was an error in banning the user', 'color' => 'red']);
    }
    redirect('cms/voucher');
  }




}

