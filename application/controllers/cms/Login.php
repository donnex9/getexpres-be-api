<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Admin_core_controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->model('cms/login_model', 'login');
    $this->load->model('cms/partners_model');
    $this->load->driver('cache');
     
  }

  public function index()
  {
    $this->login();
  }

  public function login()
  {
    $this->load->view('cms/login');
  }

  public function logout()
  {
    
    $this->session->sess_destroy();


    redirect('cms/login');
    die();
  }

  
  
  public function updatesession($newlocation,$pageToReturn)
  {
    $pageReturn = str_replace("-","/",$pageToReturn);
    $this->session->set_userdata("location",$newlocation);
    //print_r($newlocation);
    redirect($pageReturn);
  }

  public function attempt() # attempt to login
  {
    $email = $this->input->post('email');
    $password = $this->input->post('password');

    $res = $this->login->getByEmail($email);
    $resLocation = $this->login->getLocation();
    $useraccess  = $this->login->useraccess($res->userlevel);
    if($res && password_verify($password, $res->password)){
      $this->session->set_userdata(['role' => 'administrator', 'id' => $res->id, 'name' => $res->name,'useraccess'=>$useraccess,"userlevel"=>$res->userlevel,
        'location'=>$res->default_location,"allLocation"=>$resLocation,"vhID"=>1,"currentPage"=>1,"areaAssigned" => $res->area_assigned]);
      if($res->userlevel == 1)
      {
        redirect('cms/dashboard');
      }else{
        redirect('cms/news');
      }
      
    } else {
      $this->session->set_flashdata('login_msg', ['message' => 'Incorrect email or password', 'color' => 'red']);
      redirect('cms/login');
    }

  }


 

  public function attempt_partner() # attempt to login
  {
    $email = $this->input->post('email');
    $password = $this->input->post('password');

    $res = $this->partners_model->getByEmail($email);
     $resLocation = $this->login->getLocation();
     //var_dump($resLocation);
     //die();

    if($res && password_verify($password, @$res->password)){

      $this->session->set_userdata(['role' => 'partner', 'id' => $res->id, 'name' => $res->name, 'image' => base_url(). 'uploads/partners/' . $res->image,'location'=>$res->assigned_location,"allLocation"=>$resLocation,"hasbackendbooking"=>$res->hasbackendbooking]);
      redirect('cms/inventory');
    } else {
      $this->session->set_flashdata('login_msg_partner', ['message' => 'Incorrect email or password', 'color' => 'red']);
      redirect('cms/login');
    }

  }

  public function forgot_password()
  {
    $email = $this->input->post('email');
    $type = $this->input->post('type');
    $res = $this->login->forgotPassword($email, $type);

    if($res){
      $this->session->set_flashdata('login_msg_partner', ['message' => 'Password reset link was sent to ' . $email, 'color' => 'green']);
      redirect('cms/login');
    } else {
      $this->session->set_flashdata('login_msg_partner', ['message' => 'Sorry, ' . $this->input->post('email') . ' doesn\'t exist in ' . $this->input->post('type'), 'color' => 'red']);
      redirect('cms/login');
    }

  }


}

