<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Riders extends Admin_core_controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->model('cms/riders_model');
    $this->load->model('cms/rider_balance_model');
//    $this->load->model('cms/customers_model','customers_model');
    $this->load->model('api/notifications_model');
    $this->load->model('api/Rider_vehicles_model');
    $this->load->model('api/vehicles_model');
  }


  public function transactions()
  {
    $this->db->where('assigned_location', $this->session->userdata('location'));
    $res = $this->riders_model->getAllTransactions();

    $data['res'] = $res;
    $data['startingPK'] = $this->input->get('page') && $this->input->get('page') != 1  ? (($this->riders_model->per_page) * ($this->input->get('page') - 1)) + 1: 1;


    
    $data['total_pages'] = $this->riders_model->getTotalPagesRiderWallet();

    $this->wrapper('cms/rider_transactions', $data);
  }

   public function riderTopup()
  {
    $res = $this->rider_balance_model->getAllTransactions();

    $data['res'] = $res;
    $data['startingPK'] = $this->input->get('page') && $this->input->get('page') != 1  ? (($this->rider_balance_model->per_page) * ($this->input->get('page') - 1)) + 1: 1;

    $data['total_pages'] = $this->rider_balance_model->getTotalPagesRiderBalance();

    $this->wrapper('cms/rider_topup', $data);
  }

  public function add_rider_balance()
  {
    $res = $this->riders_model->getAllRidersWithBalance();
    $data['res'] = $res;
    $data['minimum_wallet'] = $this->admin_model->getMeta('minimum_wallet',$this->session->userdata('location'));

    // var_dump($res); die();

    $this->wrapper('cms/add_rider_balance', $data);
  }

  public function add_rider_balance_post()
  {
      if($this->riders_model->addWalletBalanceBulk($this->input->post('rider_ids'), $this->input->post('amount'),$this->session->userdata('location'))){
        $this->session->set_flashdata('flash_msg', ['message' => 'Balance added successfully', 'color' => 'green']);
      } else {
        $this->session->set_flashdata('flash_msg', ['message' => 'Error adding balance', 'color' => 'red']);
      }

      redirect('cms/riders/add_rider_balance');
  }

  public function index()
  {
    $this->riders_model->squery(['full_name']);
    $this->db->where('assigned_location', $this->session->userdata('location'));
    $this->db->order_by('full_name', 'asc');
    $res = $this->riders_model->all();



    $data['res'] = $res;
    $data['allLocation'] = $this->session->userdata('allLocation');
    $data['vehicles']    = $this->vehicles_model->all();
    $data['startingPK'] = $this->input->get('page') && $this->input->get('page') != 1  ? (($this->riders_model->per_page) * ($this->input->get('page') - 1) + 1): 1;

   $this->session->set_userdata(['currentPage' => $this->input->get('page')]);

    // var_dump($res); die();

    $data['get24_purchases'] = $this->riders_model->countAllByPromo('GET24',$this->session->userdata('location'));
    $data['get6_purchases'] = $this->riders_model->countAllByPromo('GET6',$this->session->userdata('location'));
    $data['get3_purchases'] = $this->riders_model->countAllByPromo('GET3',$this->session->userdata('location'));

    $data['active_riders_daily'] = $this->riders_model->getActiveRiders('daily',$this->session->userdata('location'));
    $data['active_riders_weekly'] = $this->riders_model->getActiveRiders('weekly',$this->session->userdata('location'));
    // var_dump($data['active_riders_weekly']); die();
    $data['active_riders_monthly'] = $this->riders_model->getActiveRiders('monthly',$this->session->userdata('location'));

    $this->riders_model->squery(['full_name']);
    $this->db->where('assigned_location', $this->session->userdata('location'));
    $data['total_pages'] = $this->riders_model->getTotalPages();

    $this->wrapper('cms/riders', $data);
  }

  public function update()
  {
    $id = $this->input->post('id');
    unset($_POST['id']);

    $data = array_merge($this->input->post(), $this->riders_model->upload('profile_picture'),$this->riders_model->upload('identification_document'));
    // $data = array_merge($data, $this->riders_model->upload('identification_document'));

    if($this->riders_model->update($id, $data)){
      $this->session->set_flashdata('flash_msg', ['message' => 'Item updated successfully', 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'Error updating item', 'color' => 'red']);
    }
    $currentPage = $this->session->userdata('currentPage');
    redirect('cms/riders/?page='.$currentPage.'&squery=');
  }

  public function verify($rider_id, $vehicle_pk)
  {
    if($this->rider_vehicles_model->makeVehicleVerified($rider_id, $vehicle_pk)){
      $this->session->set_flashdata('flash_msg', ['message' => 'Item updated successfully', 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'Error updating item', 'color' => 'red']);
    }
    $currentPage = $this->session->userdata('currentPage');
    redirect('cms/riders/?page=$currentPage&squery=');
  }

  public function addNewRider()
  {
   $data = array_merge($this->input->post(), $this->riders_model->upload('profile_picture'), $this->riders_model->upload('identification_document'));

    $checkNumEmailExist = $this->riders_model->checkEmailNumberExist($data);

    if(!$checkNumEmailExist == null)
    {
     $this->session->set_flashdata('flash_msg', ['message' => $checkNumEmailExist, 'color' => 'red']);
    }else
    {
      $last_id = $this->riders_model->addRiderCms($data);
	  if($last_id){
	      $this->session->set_flashdata('flash_msg', ['message' => 'Item added successfully', 'color' => 'green']);
	  } else {
	      $this->session->set_flashdata('flash_msg', ['message' => 'Error adding item', 'color' => 'red']);
	  }	
    }
    
    redirect('cms/riders');
  }

  public function add()
  {
    $data = array_merge($this->input->post(), $this->riders_model->upload('profile_picture'));

    $last_id = $this->riders_model->add($data);
    if($last_id){
      $this->session->set_flashdata('flash_msg', ['message' => 'Item added successfully', 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'Error adding item', 'color' => 'red']);
    }
    redirect('cms/riders');
  }

  public function delete()
  {
    # todo delete related
    if($this->riders_model->delete($this->input->post('id'))){
      $this->session->set_flashdata('flash_msg', ['message' => 'Item deleted successfully', 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'There was an error in banning the user', 'color' => 'red']);
    }
    redirect('cms/riders');
  }

  public function verify_vehicle()
  {
    # todo delete related
    
    if($this->rider_vehicles_model->verifyRidersVehicle($this->input->post('id'),
      ['is_verified' => 1,'vehicle_model'=> $this->input->post('vehiclemodel'),'plate_number'=>$this->input->post('plateNumber')])){
      $this->session->set_flashdata('flash_msg', ['message' => 'Vehicle verified successfully', 'color' => 'green']);
    } else {
        $this->session->set_flashdata('flash_msg', ['message' => 'There was an error in verifying the vehicle', 'color' => 'red']);
    }
    redirect('cms/riders');
  }

  public function banning($type)
  {
    // var_dump($type, $_POST); die();
    if ($type == 'ban') {
      $res = $this->riders_model->manageBan(date('Y-m-d H:i:s'), 'riders', $this->input->post('id'));

      if ($res) {
        $this->notifications_model->sendPushNotif(
          $this->input->post('id'),
          'riders',
          ['data' => ['force_logout' => 1]],
          ['title' => 'Your account has been banned', 'body' => 'Please contact the Administrator if you want to send an appeal']
        );
      }

      $this->session->set_flashdata('flash_msg', ['message' => 'User banned successfully', 'color' => 'green']);
    } else if ($type == 'unban') {
      $this->riders_model->manageBan(null, 'riders' , $this->input->post('id'));
      $this->session->set_flashdata('flash_msg', ['message' => 'User unbanned successfully', 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'There was an error in banning the user', 'color' => 'red']);
    }

    redirect('cms/riders');
  }

public function export_report()
  {
    // output headers so that the file is downloaded rather than displayed
    header('Content-type: text/csv');
    header('Content-Disposition: attachment; filename="' . date('Y-m-d') . "_topups" . '_.csv"');
    // do not cache the file
    header('Pragma: no-cache');
    header('Expires: 0');
    // create a file pointer connected to the output stream
    $file = fopen('php://output', 'w');
    // send the column headers
    fputcsv($file, array('Rider ID', 'Full Name', 'Wallet Balance', 'type', 'Date' ));
    
    $this->db->limit(18446744073709551615);
    $this->db->select('rider_id, full_name, wallet_balance, type, rider_balance.created_at');
    $this->db->join('riders', 'riders.id = rider_balance.rider_id', 'left');
    $this->rider_balance_model->filters('rider_balance');
    $this->db->order_by('rider_balance.created_at DESC');
    $res = $this->db->get('rider_balance')->result();

    $new_res = [];
    foreach ($res as $key => $value) {
      $new_res[] = array(
        $value->rider_id,
        $value->full_name,
        $value->wallet_balance,
        $value->type,
        $value->created_at
      );
    }
    $data = $new_res;

    foreach ($data as $row)
    {
      fputcsv($file, $row);
    }
    exit();
  }

}
