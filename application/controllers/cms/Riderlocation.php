<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Riderlocation extends Admin_core_controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('cms/admin_model', 'admin_model');
    $this->load->model('cms/booking_model');
    $this->load->model('api/vehicles_model');
    $this->load->model('api/services_model');
    $this->load->model('cms/inventory_model');
    $this->load->model('api/customers_model');
    $this->load->model('api/cart_model', 'model');
    $this->load->model('api/cart_model', 'cart_model');
    $this->load->model('api/firebase_model');
    $this->load->model('api/notifications_model');
     $this->load->library('googlemaps');
  }

  
 public function getriderslocation()
 {

  $data["assigned_location"] = $this->session->userdata('location');
  $mapcenter = $this->mapcenter($this->session->userdata('location'));
  $riderlocation = $this->riderLocation($this->session->userdata('location'));
 // echo  base_url();
  //var_dump($riderlocation);
  //die();

  $config['center'] = $mapcenter;//$this->input->post('mapposition');
  $config['zoom'] = '15';
  $this->googlemaps->initialize($config);


  
  $marker = array();
  foreach($riderlocation as $key => $val)
  {
   
    $marker['position'] = $val->rider_lat.",".$val->rider_lng;
    if($val->vh == 3){
      $marker['icon'] = base_url()."public/admin/assets/svg/deliver-icon.png";
    }else if($val->vh == 11){
     $marker['icon'] = base_url()."public/admin/assets/svg/trike-icon.png";
    }
    //$marker['icon'] = base_url()."public/admin/assets/svg/deliver-icon.png";
    $marker['onmouseover'] =  true;
    $marker['infowindow_content'] = $val->name;
     $this->googlemaps->add_marker($marker);
  }
  
   
        //$marker['position'] = 'india,hyderabad';

 
  $data['map'] = $this->googlemaps->create_map();
  

       // $this->load->view('demos/maps', $data);

    //$this->load->view('cms/riderlocation', $data);
  $this->wrapper('cms/riderlocation',$data);
 }

 public function mapcenter($al)
 {
  $this->db->where('id',$al);
  $mapcenter = $this->db->get('dynamic_radius_places')->row()->map_center;
  return $mapcenter;
 }
  

public function riderLocation($al)
  {
    
    $this->db->select('rider_vehicles.vehicle_id as "vh",riders.rider_latitude as "rider_lat", riders.rider_longitude as "rider_lng",riders.full_name as "name"');
     $this->db->where('type', 'riders');
     $this->db->where('riders.is_admin_verified', 1);
     $this->db->where('user_device_ids.is_active', 1);
     $this->db->where('riders.rider_latitude != ""');
     $this->db->where('riders.rider_longitude != ""');
     $this->db->where('riders.assigned_location',$al);
     $this->db->where_in('rider_vehicles.vehicle_id',['11','3']);
     $this->db->group_by('riders.id');
     
     $this->db->limit(99999999);
  
     $this->db->join('riders', 'riders.id = user_device_ids.user_id', 'inner');
     $this->db->join('rider_vehicles', 'riders.id = rider_vehicles.rider_id', 'inner');
     $riders = $this->db->get('user_device_ids')->result();
     
     return $riders;

  }


  public function searchrider()
  {

            
    $pickup_lat       = $this->input->post("pickuplat");
    $assignedlocation = $this->input->post("al");
    $pickup_long      = $this->input->post("pickuplng");
    $vehicleid        = $this->input->post("vehicleid");
     
    $this->db->select('riders.rider_latitude, riders.rider_longitude, riders.id,riders.full_name');
     $this->db->where('type', 'riders');
     $this->db->where('rider_vehicles.vehicle_id', $vehicleid);
     $this->db->where('riders.is_admin_verified', 1);
     $this->db->where('user_device_ids.is_active', 1);
     $this->db->where('riders.rider_latitude != ""');
     $this->db->where('riders.rider_longitude != ""');
     $this->db->where('riders.assigned_location',$assignedlocation);
     $this->db->like("LOWER(full_name)", strtolower($this->input->post("ridername")),'after');
     $this->db->group_by('riders.id');
     
     $this->db->limit(99999999);
  
     $this->db->join('riders', 'riders.id = user_device_ids.user_id', 'inner');
     $this->db->join('rider_vehicles', 'riders.id = rider_vehicles.rider_id', 'inner');
     $riders = $this->db->get('user_device_ids')->result();
     
     $arrayriders = array();

     $rider_radius_in_m  = $this->admin_model->getMeta('rider_radius_in_m',$assignedlocation) ?: 7000;

     foreach($riders as $key => $value){
    
      $d = $this->partners_model->getCoordsDiff((float)$value->rider_latitude, $value->rider_longitude, (float)$pickup_lat, (float)$pickup_long, "K");
       $d =  $d * 1000;
       
        
       if ($d > 0 && $d <= $rider_radius_in_m){  
       // echo $value->firebase_id;
         array_push($arrayriders,$value);
       }
     }

    print_r(json_encode($riders));

  }

  

  

  

  
}

