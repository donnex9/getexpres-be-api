<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends Admin_core_controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('cms/admin_model', 'admin_model');
    $this->load->model('cms/booking_model');
    $this->load->model('api/vehicles_model');
    $this->load->model('api/services_model');
    $this->load->model('cms/inventory_model');
    $this->load->model('api/customers_model');
    $this->load->model('api/cart_model', 'model');
    $this->load->model('api/cart_model', 'cart_model');
    $this->load->model('api/firebase_model');
    $this->load->model('api/notifications_model');
    $this->load->model('api/riders_model');
    $this->load->model('api/payment_model');
  }

  

  public function manualbooking()
  {

    $usertype = $this->session->role;
    $id = $this->session->id;
    $data["assigned_location"] = $this->session->userdata('location');
    $data["availableVehicles"] = $this->services_model->getActiveVehiclesByService($this->session->userdata('location'));
    $data['minimum_wallet'] = $this->admin_model->getMeta('minimum_wallet',$this->session->userdata('location'));
    $data['package_options'] = $this->admin_model->getMeta('package_options',$this->session->userdata('location'));
   // die();

    $data['customers'] =$this->customers_model->selectedcustomers($usertype,$id,$this->session->userdata('location'));

    $data['flash_msg'] = $this->session->flash_msg;
    
    $this->wrapper('cms/manualbooking',$data);
   
  }

  public function partnerbooking()
  {
    $usertype = $this->session->role;
    $id = $this->session->id;
    $data["assigned_location"] = $this->session->userdata('location');
    $data["availableVehicles"] = $this->services_model->getActiveVehiclesByService($this->session->userdata('location'));
    //echo "1";//$this->session->userdata('location');
    $data['package_options'] = $this->admin_model->getMeta('package_options',$this->session->userdata('location'));
   // die();


    $data['customers'] =$this->customers_model->selectedcustomers($usertype,$id,$this->session->userdata('location'));

    $data['flash_msg'] = $this->session->flash_msg;
  
      $this->wrapper('cms/partnerbooking',$data);
   
    
  }

  public function updatebooking($cartId)
  {
    $usertype = $this->session->role;
    $id = $this->session->id;
    $data["assigned_location"] = $this->session->userdata('location');
    $data["availableVehicles"] = $this->services_model->getActiveVehiclesByService($this->session->userdata('location'));
    //echo "1";//$this->session->userdata('location');
    $data['package_options'] = $this->admin_model->getMeta('package_options',$this->session->userdata('location'));
   // die();

    $data['customers'] =$this->customers_model->selectedcustomers($usertype,$id,$this->session->userdata('location'));

    $res = $this->cart_model->get($cartId);
    $data['res'] = $res;
    $this->wrapper('cms/updatebooking', @$data);
  }

  public function searchrider()
  {

    /*$this->db->select("id,full_name");
    $this->db->like('full_name', $this->input->post("ridername"),'after');
    $this->db->where('full_name', $this->input->post("ridername"),'after');
    $response = $this->db->get('riders')->result();*/
            
    $pickup_lat       = $this->input->post("pickuplat");
    $assignedlocation = $this->input->post("al");
    $pickup_long      = $this->input->post("pickuplng");
    $vehicleid        = $this->input->post("vehicleid");
     
    $this->db->select('riders.rider_latitude, riders.rider_longitude, riders.id,riders.full_name,rb.wb');
     $this->db->where('type', 'riders');
     $this->db->where('rider_vehicles.vehicle_id', $vehicleid);
     $this->db->where('riders.banned_at is null');
     $this->db->where('riders.is_admin_verified', 1);
     $this->db->where('user_device_ids.is_active', 1);
     $this->db->where('riders.rider_latitude != ""');
     $this->db->where('riders.rider_longitude != ""');
     $this->db->where('riders.assigned_location',$assignedlocation);
     $this->db->like("LOWER(full_name)", strtolower($this->input->post("ridername")),'after');
     $this->db->group_by('riders.id');
     
     $this->db->limit(99999999);
  
     $this->db->join('riders', 'riders.id = user_device_ids.user_id', 'inner');
     $this->db->join('rider_vehicles', 'riders.id = rider_vehicles.rider_id', 'inner');
     $this->db->join("(SELECT SUM(wallet_balance) as wb,rider_id FROM rider_balance where status = 'paid'  GROUP BY rider_id) as rb", 'riders.id = rb.rider_id', 'inner');
     $riders = $this->db->get('user_device_ids')->result();
     
     $arrayriders = array();

     /*$rider_radius_in_m  = $this->admin_model->getMeta('rider_radius_in_m',$assignedlocation) ?: 7000;*/

     /*foreach($riders as $key => $value){
     //$rider_wallet_balance = $this->riders_model->walletBalancesum($value->id);
      /*$d = $this->partners_model->getCoordsDiff((float)$value->rider_latitude, $value->rider_longitude, (float)$pickup_lat, (float)$pickup_long, "K");
       $d =  $d * 1000;*/
       
      //  $merge = array_combine($value,["waller"=>$rider_wallet_balance]);
      // if ($d > 0 && $d <= $rider_radius_in_m){  
       // @$value["riderwallet"] = (double)$rider_wallet_balance;
        // array_push($arrayriders,$merge);
       //}
   //  }

    print_r(json_encode($riders));

  }

  public function booknow()
  {
   $datarec = json_decode(file_get_contents("php://input"), true);
   $cartid  =  $datarec["cartid"];
   $riderid =  $datarec["riderid"];
   $grandtotal = $datarec["total"];
   $customerid = $datarec["customer_id"];
   $vcode =  $datarec["vcode"];
   $vAmount = $datarec["vamount"];
   $isSchedule = $datarec["isScheduled"];
   $dateTimeShed = $datarec["dateTimeSched"];
   $phpdate = strtotime($dateTimeShed);
   $dateTimeShed = date( 'Y-m-d H:i:s', $phpdate );
   $deliveryStatus = "pending";
   $temresp = [];
  
   $response = (object)[];
   $data = [];

   if ($riderid > 0) {
    $this->db->select("rider_latitude,rider_longitude");
    $this->db->where('id', $riderid);
    $resp = $this->db->get('riders')->row();
    $riderLatitude = $resp->rider_latitude;
    $riderLongitude = $resp->rider_longitude;
  
   /* $data = ["rider_id"=>(INT)$riderid,
             "status"=>"accepted",
             "rider_current_location_latitude" =>$riderLatitude,
             "rider_current_location_longitude" =>$riderLongitude,
             "total_price" => (double) $grandtotal,
             "customer_id" =>$customerid
            ];*/
     $data = [
             "voucher_code"=>$vcode,
             "voucher_amount" => (double) $vAmount,
             "status"=>"init",
             "total_price" => (double) $grandtotal,
             "customer_id" =>$customerid];       
     $this->db->where('id',$cartid);
     $temresp = $this->db->update('cart',$data);

    if ($temresp) 
    {
      $response = $this->accepted($cartid,$riderid,$isSchedule,$dateTimeShed);
    }        
   
   }else
   {
    if($isSchedule == 1){
      $deliveryStatus = "scheduled_delivery";
    }

    $data=["status"=>$deliveryStatus,"customer_id" =>$customerid,
            "voucher_code"=>$vcode,
            "voucher_amount" => (double) $vAmount,
            "total_price" => (double) $grandtotal,
            'is_scheduled'=>$isSchedule,
            "scheduled_at" =>$dateTimeShed
          ];
     $this->db->where('id',$cartid);
    $res = $this->db->update('cart',$data);

    $response->data = $res ? true: false;
    if($res)
    {
      $response->meta = (object)[
      'message' => "Successfully booked.",
      'code' => '200',
      'status' => 'ok'
    ];

    }else
    {
      $response->meta = (object)[
      'message' => "Sorry, error occured upon saving the data.",
      'code' => '201',
      'status' => 'error'
      ];
    }
    

   }
   

/*    if($response)
    {
      $new_res = $this->cart_model->get($cartid);
      $pickup_location = $new_res->pickup_location;
       $this->notifications_model->notifyAllActiveRiders("Get " . ucwords($new_res->service_type), @$pickup_location->latitude, @$pickup_location->longitude,$new_res->assigned_location,
        $new_res->vehicle_id);
    }
    */
   
    print_r(json_encode($response));

  }


  public function accepted($cart_id, $rider_id,$isSchedule,$dateTimeShed)
  {
    $rider = $this->riders_model->get($rider_id);
    $cash_on_hand = @$rider->cash_on_hand ?: 0;

    $cart = $this->cart_model->get($cart_id);
    $grand_total = $this->cart_model->findGrandTotal($cart);

    $res = "";

    $rider_wallet_balance = $this->riders_model->walletBalancesum($rider->id);
    $location = $cart->assigned_location;
    $min = $this->admin_model->getMeta('minimum_wallet',$location);

    $minimum_wallet_amount = $min;

    if (!($rider_wallet_balance >= $minimum_wallet_amount)) {
          $res = 'minimum_wallet_amount_not_met';
    }

    if (in_array($cart->status, ['accepted'])) {
      $res = 'accepted_na';
    } 
    // else {
    //  $res = 'order_not_yet_finalized';
    // }

    // var_dump($cash_on_hand >= $grand_total, $cart->status == 'pending', $rider_wallet_balance >= $minimum_wallet_amount); die();
    if ($cash_on_hand >= $grand_total && $rider_wallet_balance >= $minimum_wallet_amount) {
      $res = $this->cart_model->acceptBookingCMS($cart_id, $rider_id,$isSchedule,$dateTimeShed);
    }else if($cash_on_hand < $grand_total && $rider_wallet_balance >= $minimum_wallet_amount && $cart->cart_type_id == 3 )
    {
      $res = 'not_enough_cash_on_hand';
    }else if(($cart->cart_type_id == 4 || $cart->cart_type_id == 1)  && $rider_wallet_balance >= $minimum_wallet_amount)
    {
      $res = $this->cart_model->acceptBookingCMS($cart_id, $rider_id,$isSchedule,$dateTimeShed);
    }

    

    if ($cart->status == 'cancelled') {
      $res = "cancelled";
    }

    if ($cart->status == 'accepted') {
      $res = 'accepted_na';
    }

    if ($res === 'accepted_na') {
      $message = "Order has already been accepted by another rider";
      $code = "order_already_accepted";
      $status = 400;
    } else if ($res === "cancelled") {
      $message = "Booking was cancelled. Failed to accept.";
      $code = "booking_cancelled";
      $status = 400;
    } else if ($res === 'order_not_yet_finalized') {
      $message = "Order not yet finalized! Please try again later.";
      $code = "order_not_yet_finalized";
      $status = 400;
    } else if ($res === 'minimum_wallet_amount_not_met') {
      $message = "Minimum wallet amount of $minimum_wallet_amount not met";
      $code = "minimum_wallet_amount_not_met";
      $status = 400;
    } else if ($res === "not_enough_cash_on_hand") {
      $message = "Not enough cash on hand to accept this order.";
      $code = "not_enough_cash_on_hand";
      $status = 400;
    } else {
      $res = "ok";
      $message = "Status changed.";
      $code = "ok";
      $status = 200;
    }

    $response = (object)[];
    $response->data = $res == "ok"?true: false;
    $response->meta = (object)[
      'message' => $message,
      'code' => $code,
      'status' => $status
    ];

    return $response;
  }

  //init delivery
  public function initialize_cart()
  {
    try {
    $data = json_decode(file_get_contents("php://input"), true);
   //print_r(json_encode($data));
   //die();
    $pickup = json_encode($data['pickup_location']);
    $delivery = json_encode($data["delivery_location"]);
    $assigned_location = $data['assigned_location'];
    $adminFee = $this->admin_model->getMeta('assisted_booking_fee',$assigned_location);
    $forInsertData = [
                      'cart_type_id'=>$data['cart_type_id'],
                      "customer_id"=>$data['customer_id'],
                      "vehicle_id"=>$data['vehicle_id'],
                      'delivery_location' => $delivery,
                      'pickup_location'   => $pickup,
                      'assigned_location' => $assigned_location,
                      'munzoneid' => '0',
                      'zoneid' => '0',
                      'admin_fee' => $adminFee,
                      'payment_method'=> 'COD'
                    ];
           $data = [
                      'cart_type_id'=>$data['cart_type_id'],
                      "customer_id"=>$data['customer_id'],
                      "vehicle_id"=>$data['vehicle_id'],
                      'delivery_location' => $delivery,
                      'pickup_location'   => $pickup,
                      'assigned_location' => $assigned_location,
                      "category" =>$data['category'],
                      "notes" => $data['notes'],
                      'admin_fee' => $adminFee,
                      'payment_method'=> 'COD'
                    ];

   // $data["delivery_location"] =  $delivery;
   // $data["pickup_location"]   =  $pickup;          
    $last_id = 0;
    //unset($_POST["packageoption"]);
    $cart = $this->booking_model->isCartExists($data);
    //$last_id = $cart->id;
    $hasId = false;
      
   if($cart !=null){
      $last_id = $cart->id;
      $this->db->where('id', $last_id);
      $updateres = $this->db->update('cart',$forInsertData);

      $hasId = true;
 
    }else
    {
      $last_id = $this->booking_model->initializeCart($forInsertData,$hasId,$last_id);
    }

    
    $this->booking_model->tomtomsetDistanceByMatrix($last_id);
    
    $this->booking_model->webtransactDelivery($data,$last_id,$assigned_location);

    $trans =  $this->cart_model->get($last_id);

    $response = (object)[];
    $response->cartid = $last_id;
    $response->data =  unserialize($trans->payload);
    
    print_r(json_encode($response));
      
    } catch (Exception $e) {
       print_r(json_encode($e));
    }
  }
  
  // init update delivery
  public function initialize_cartUpdate($cartId)
  {
    try {
    $data = json_decode(file_get_contents("php://input"), true);
   //print_r(json_encode($data));
   //die();
    $pickup = json_encode($data['pickup_location']);
    $delivery = json_encode($data["delivery_location"]);
    $assigned_location = $data['assigned_location'];
    $adminFee = $this->admin_model->getMeta('assisted_booking_fee',$assigned_location);
    $forUpdateData = [
                      'cart_type_id'=>$data['cart_type_id'],
                      "customer_id"=>$data['customer_id'],
                      "vehicle_id"=>$data['vehicle_id'],
                      'delivery_location' => $delivery,
                      'pickup_location'   => $pickup,
                      'assigned_location' => $assigned_location,
                      'munzoneid' => '0',
                      'admin_fee' => $adminFee,
                      'zoneid' => '0',
                      'payment_method'=> 'COD'
                    ];
           $data = [
                      'cart_type_id'=>$data['cart_type_id'],
                      "customer_id"=>$data['customer_id'],
                      "vehicle_id"=>$data['vehicle_id'],
                      'delivery_location' => $delivery,
                      'pickup_location'   => $pickup,
                      'assigned_location' => $assigned_location,
                      "category" =>$data['category'],
                      'admin_fee' => $adminFee,
                      "notes" => $data['notes'],
                      'payment_method'=> 'COD'
                    ];
      

      $last_id = $cartId;
      $this->db->where('id', $last_id);
      $updateres = $this->db->update('cart',$forUpdateData); 

    $this->booking_model->tomtomsetDistanceByMatrix($last_id);
    //print_r(json_encode($response));
   // die();
    $this->booking_model->webtransactDelivery($data,$last_id,$assigned_location);

    $trans =  $this->cart_model->get($last_id);

    $response = (object)[];
    $response->cartid = $last_id;
    $response->data =  unserialize($trans->payload);
    
    print_r(json_encode($response));
      
    } catch (Exception $e) {
       print_r(json_encode($e));
    }
  }
  


  //init pabili
  public function initialize_cart_pabili()
  {
    try {
    $data = json_decode(file_get_contents("php://input"), true);
    //$data = $this->input->post();
    $pickup = json_encode($data['pickup_location']);
    $delivery = json_encode($data["delivery_location"]);
    $assigned_location = $data['assigned_location'];
    $adminFee = $this->admin_model->getMeta('assisted_booking_fee',$assigned_location);
    $forInsertData = [
                      'cart_type_id'=>$data['cart_type_id'],
                      "customer_id"=>$data['customer_id'],
                      "vehicle_id"=>$data['vehicle_id'],
                      'delivery_location' => $delivery,
                      'pickup_location'   => $pickup,
                      'assigned_location' => $assigned_location,
                      'admin_fee' => $adminFee,
                      'payment_method'=> 'COD'
                    ];
            $dataNew   = [
                      'cart_type_id'=>$data['cart_type_id'],
                      "customer_id"=>$data['customer_id'],
                      "vehicle_id"=>$data['vehicle_id'],
                      'delivery_location' => $delivery,
                      'pickup_location'   => $pickup,
                      'assigned_location' => $assigned_location,
                      "notes" => $data['notes'],
                      'payment_method'=> 'COD',
                      'admin_fee' => $adminFee,
                      "items_name"=>$data["items_name"],
                      "estimate_total_amount"=>$data["estimate_total_amount"]
                    ];

    
    $last_id = 0;
    $cart = $this->booking_model->isCartExists($dataNew);
    $hasId = false;
      
   if($cart !=null){
      $last_id = $cart->id;
      $this->db->where('id', $last_id);
      $updateres = $this->db->update('cart',$forInsertData);
      $hasId = true;
    }else
    {
     $last_id = $this->booking_model->initializeCart($forInsertData,$hasId,$last_id);
    }

    $this->booking_model->webtransactPabili($dataNew,$last_id,$assigned_location);
    $this->cart_model->setDistanceByMatrix($last_id);
    //$resp =  $this->cart_model->get($last_id);
    $this->booking_model->webreInitPabili($last_id,$assigned_location);

    $trans =  $this->cart_model->get($last_id);

    $response = (object)[];
    $response->cartid = $last_id;
    $response->data =  unserialize($trans->payload);
    
    print_r(json_encode($response));
      
    } catch (Exception $e) {
       print_r(json_encode($e));
    }
    
  }

public function initialize_cart_pabiliUpdate($cartId)
  {
    try {
    $data = json_decode(file_get_contents("php://input"), true);
    //$data = $this->input->post();
    $pickup = json_encode($data['pickup_location']);
    $delivery = json_encode($data["delivery_location"]);
    $assigned_location = $data['assigned_location'];
    $adminFee = $this->admin_model->getMeta('assisted_booking_fee',$assigned_location);
    $forUpdateData = [
                      'cart_type_id'=>$data['cart_type_id'],
                      "customer_id"=>$data['customer_id'],
                      "vehicle_id"=>$data['vehicle_id'],
                      'delivery_location' => $delivery,
                      'pickup_location'   => $pickup,
                      'admin_fee' => $adminFee,
                      'assigned_location' => $assigned_location,
                      'payment_method'=> 'COD'
                    ];
            $dataNew   = [
                      'cart_type_id'=>$data['cart_type_id'],
                      "customer_id"=>$data['customer_id'],
                      "vehicle_id"=>$data['vehicle_id'],
                      'delivery_location' => $delivery,
                      'pickup_location'   => $pickup,
                      'assigned_location' => $assigned_location,
                      "notes" => $data['notes'],
                      'payment_method'=> 'COD',
                      'admin_fee' => $adminFee,
                      "items_name"=>$data["items_name"],
                      "estimate_total_amount"=>$data["estimate_total_amount"]
                    ];

    
    $last_id = $cartId;
    
    $this->db->where('id', $last_id);
    $updateres = $this->db->update('cart',$forUpdateData);
    
    $this->booking_model->webtransactPabili($dataNew,$last_id,$assigned_location);
    $this->cart_model->setDistanceByMatrix($last_id);
    $this->booking_model->webreInitPabili($last_id,$assigned_location);

    $trans =  $this->cart_model->get($last_id);

    $response = (object)[];
    $response->cartid = $last_id;
    $response->data =  unserialize($trans->payload);
    
    print_r(json_encode($response));
      
    } catch (Exception $e) {
       print_r(json_encode($e));
    }
    
  }


  public function monitorActiveBooking()
  {
    $response = [];
    $this->db->select('assigned_location, COUNT(id) as total');
    $this->db->where("status","pending");
    $this->db->group_by('assigned_location'); 
    $db_results = $this->db->get("cart");

    $results = $db_results->result();
    $num_rows = $db_results->num_rows();
    
    $response["data"] = $results;
    $response["totalrows"] = $num_rows;
  
   
   print_r(json_encode($response));
  }

  public function checkScheduledBooking()
  {
    $response = [];
    $status = "";
    $this->db->select('*');
    $this->db->where("status","scheduled_delivery");
    $this->db->where("is_scheduled","1");  
    $db_results = $this->db->get("cart");


    $results = $db_results->result();
    $num_rows = $db_results->num_rows();

    $response["data"] = $results;

   if($num_rows > 0)
    {

      foreach($results as $row)
      {
        $riderid = $row->rider_id;
        $cart_id = $row->id;
        $status = "pending";
       
        $deliverySchedDate =strtotime($row->scheduled_at);
        $serverDate = strtotime($this->getServerDate());
        
        if($riderid > 0){
          $status = "accepted";
        } 

        if($serverDate >= $deliverySchedDate)
        {
          $this->cart_model->updateSchedBooking($cart_id,$status);
        }
      }

    }
    print_r(json_encode($response));
  }

  public function getServerDate()
  {
    $manilatimezone = new DateTimeZone('Asia/Manila');
    $dt = new DateTime('now', $manilatimezone);
    $currentdate = $dt->format("Y-m-d");
    $currenttime = $dt->format("H:i:s") ;
    return $currentdatetime = $currentdate ." ".$currenttime;
  }

  public function applyVoucher()
  {
   $dataPost = json_decode(file_get_contents("php://input"), true);
   $cart_Id     = $dataPost['cart_id'];
   $custmers_Id = $dataPost['customer_id'];
   $vouchercode = $dataPost['vouchercode'];
   $assignedlocation = $this->session->userdata('location');
   $message = "Voucher code is not applicable in the area.";
   $available = false;
   $used = 0;
   $voucherAmount = 0.00;
   $voucherCode = "";
   $isCustomerAvailVoucher = $this->checkifUserAvailtheVoucherCMS($custmers_Id,$assignedlocation,$vouchercode);
 
   $res = $this->booking_model->voucherCodeCMS($vouchercode,$assignedlocation);
   
    if($res){
       foreach ($res as $key => $value) {
        $expiredate = $this->compareDates($value->expired_at);
          if($expiredate == true){
            $message = "Expired Voucher Code";
          }else if($value->status == 0)
          {
           $message = "Inactive Voucher Code.";
          }else if($value->status == 0)
          {
           $message = "Inactive Voucher Code.";
          }else if($value->used  >= $value->limit)
          {
           $message = "Voucher Code had been reach its limit.";

          }else{
                $available = true;
                $used = $value->used;
                $voucherAmount = $value->amount;
                $voucherCode = $value->voucher_code;
          }
       }
    }

    if($available == true && $isCustomerAvailVoucher == false ){
       $used = $used + 1;
       $this->cart_model->updatevoucherCode($vouchercode,$assignedlocation,$used);
       $this->db->where('id',$cart_Id);
       $this->db->update('cart',array('voucher_amount'=>$voucherAmount,"voucher_code"=>$vouchercode));
       $message = "Voucher Code successfully applied";
    }else if($available == true && $isCustomerAvailVoucher == true){
      $res = [];
       $message = "You already avail the Voucher Code, try next time.";
    }else{
      $res = [];
    }
   
    $response = (object)[];
    $response->data = $res;
    $response->meta = (object)[
      'message' =>$message,
      'code' => 'ok',
      'status' => 200
    ];

   print_r(json_encode($response));
  }

  public function checkifUserAvailtheVoucherCMS($custmers_Id,$assignedlocation,
    $vouchercode)
  {
    $dateToday = date('Y-m-d');
    $response = $this->booking_model->userAvailVoucherCMS($custmers_Id,$assignedlocation,
      $vouchercode,$dateToday);
    if($response != null || $response > 0)
      {
       return true;
      }else
      {
        return false; 
      }
    //return $response;
  }

  public function compareDates($d1)
  {
    $date1 = date("Y-m-d",strtotime($d1));
    $date2 = date("Y-m-d",strtotime(date('Y-m-d')));

    $d1 = strtotime($date1);
    $d2 = strtotime($date2);

    if($d2 <= $d1)
    {
      return false;
    }else{
      return true;
    }
  }
  
  
}


