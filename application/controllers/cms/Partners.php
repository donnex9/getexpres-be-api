<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partners extends Admin_core_controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->model('cms/partners_model');
    $this->load->model('cms/services_model');

  }

  public function index()
  {
    $this->db->order_by("name", "asc");
    $res = $this->partners_model->all();

    $data['res'] = $res;
    $data['allLocation'] = $this->session->userdata('allLocation');
    $data['services'] = $this->services_model->allHaveCMSAccess();
    // var_dump($data['services']); die();
    $data['startingPK'] = $this->input->get('page') && $this->input->get('page') != 1  ? (($this->partners_model->per_page) * ($this->input->get('page') - 1)) + 1: 1;

    $this->db->where('assigned_location', $this->session->userdata('location'));
    $this->partners_model->squery(['name']);
    $data['total_pages'] = $this->partners_model->getTotalPages();
   $userlevel =  $this->session->userdata('userlevel'); 
    $data["accesslevel"] = $this->admin_model->getuseraccess('25',$userlevel);

    $this->wrapper('cms/partners', $data);
  }


  public function invoices()
  {
    // $this->db->order_by('name', 'asc');
    $res = $this->partners_model->all_partners();
    // var_dump($res); die();
    $res2 = $this->partners_model->getPartnerSales();

    $data['res'] = $res;
    $data['res2'] = $res2;

    $this->wrapper('cms/partner_invoice', $data);
  }


  public function generateInvoices()
  {
   
    $res2 = $this->partners_model->genetateInvoices();
    $data['res'] = $res2;
    print_r(json_encode($data));

    //$this->wrapper('cms/partner_invoice', $data);
  }

  public function export_menu()
  {
    // $this->db->order_by('name', 'asc');
    $res = $this->partners_model->all_partners();
    // var_dump($res); die();
    $res2 = $this->partners_model->getPartnerSales();

    $data['res'] = $res;
    $data['res2'] = $res2;

    $this->wrapper('cms/partners_export_menu', $data);
  }

  public function export_to_csv()
  {
    // var_dump($this->session->role); die();

    // output headers so that the file is downloaded rather than displayed
    header('Content-type: text/csv');
    header('Content-Disposition: attachment; filename="' . date('Y-m-d') . '_merchant_sales.csv"');
    // do not cache the file
    header('Pragma: no-cache');
    header('Expires: 0');
    // create a file pointer connected to the output stream

    $file = fopen('php://output', 'w');
    // send the column headers
    fputcsv($file, array('Order ID', 'Merchant Name', 'GetApp Commission %', 'GetApp Commission Value (in PHP)', 'Merchant Total Sales (in PHP)'));

    $res = $this->partners_model->getPartnerSales();
    $new_res = [];

    foreach ($res as $key => $value) {
      $new_res[] = array(
        $value->order_id,
        $value->name,
        $value->getapp_comission_percentage,
        $value->commission_value,
        $value->total_price,
        // $value->partner_total_sales_minus_commission,
      );
    }
    $data = $new_res;

    foreach ($data as $row)
    {
      fputcsv($file, $row);
    }
    exit();
  }

  public function profile()
  {
    $res = $this->partners_model->get($this->session->id);
    $data['user'] = $res;

    $this->wrapper('cms/partner_profile', $data);
  }

 public function updateStoreAvailability()
  {
    $id = $this->input->post('id');
    $storeopen = "";
    unset($_POST['id']);
    
    $data = array("isopen" => $this->input->post('isitopen'));
   // print_r($data);
   // die();
    if($this->partners_model->updateStoreAvail($id,$data)){
      if($this->input->post('isitopen') == 'Y'){
         $this->session->set_flashdata('flash_msg2', ['message' => 'Store is now open ', 'color' => 'green']);
      }else{
         $this->session->set_flashdata('flash_msg2', ['message' => 'Store is Closed', 'color' => 'red']);
      }
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'Error updating item', 'color' => 'red']);
    }
      redirect('cms/partners/profile');
   // die();
  }

public function update()
  {
    $id = $this->input->post('id');
    unset($_POST['id']);

    $data = array_merge($this->input->post(), $this->partners_model->upload('image'));
    $data = array_merge($data, $this->partners_model->upload('banner'));

    $isImageUploaded = $this->partners_model->upload('image');
    $isBannerUploaded = $this->partners_model->upload('image');
    
    //array getimagesize( $filename, $image_info );

//   if(!empty($isImageUploaded))
  // {
    if($this->partners_model->update($id, $data)){
      $this->session->set_flashdata('flash_msg', ['message' => 'Item updated successfully', 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'Error updating item', 'color' => 'red']);
    }

 // }else{
  //  $this->session->set_flashdata('flash_msg', ['message' => 'Invalid image (file size must be at least 960 x 720 and less than or equal 2MB)', 'color' => 'red']);
 // }
   
    
    if ($this->session->role == 'partner') {
      redirect('cms/partners/profile');
    } else {
      redirect('cms/partners');
    }

    die();
  }

  public function add()
  {
    $data = array_merge($this->input->post(), $this->partners_model->upload('image'));
    $data = array_merge($data, $this->partners_model->upload('banner'));

    $isImageUploaded = $this->partners_model->upload('image');
   
    //array getimagesize( $filename, $image_info );

   //if(!empty($isImageUploaded))
  // {

    $last_id = $this->partners_model->add($data);
    if($last_id){
      $this->session->set_flashdata('flash_msg', ['message' => 'Item added successfully', 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'Error adding item', 'color' => 'red']);
    }

 // }else{
   // $this->session->set_flashdata('flash_msg', ['message' => 'Invalid image (file size must be at least 960 x 720 and less than or equal 2MB)', 'color' => 'red']);
 // }
    redirect('cms/partners');
  }

  public function delete()
  {
    if($this->partners_model->delete($this->input->post('id'))){
      $this->session->set_flashdata('flash_msg', ['message' => 'Item deleted successfully', 'color' => 'green']);
    } else {
      $this->session->set_flashdata('flash_msg', ['message' => 'Error deleting item', 'color' => 'red']);
    }
    redirect('cms/partners');
  }
}
