<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History extends Admin_core_controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('cms/admin_model', 'admin_model');
    $this->load->model('api/customers_model');
    $this->load->model('api/cart_model');
    $this->load->model('api/riders_model');
    // $this->load->model('api/vehicles_model');
    //
  }

  public function delete($cart_id, $from)
  {
    $this->db->where('cart_id', $cart_id);
    $this->db->delete('cart_items');

    $this->db->where('id', $cart_id);
    $this->db->delete('cart');

    $this->session->set_flashdata('flash_msg', ['message' => 'Order deleted permanently', 'color' => 'green']);
    if ($this->input->get('customer_id')) {
      $from = $from . "?customer_id=" . $this->input->get('customer_id');
    }
    if ($this->input->get('rider_id')) {
      $from = $from . "?rider_id=" . $this->input->get('rider_id');
    }

    redirect('cms/history/' . $from);
  }

  public function forcecomplete($cart_id, $from)
  {
    
   $iscompleted = $this->completed($cart_id);
   if($iscompleted == true)
    {
      $this->session->set_flashdata('flash_msg', ['message' => 'Order completed successfully.', 'color' => 'green']);
    }else
    {
     $this->session->set_flashdata('flash_msg', ['message' => 'Error Occured', 'color' => 'red']);
    }
    
    if ($this->input->get('customer_id')) {
      $from = $from . "?customer_id=" . $this->input->get('customer_id');
    }
    if ($this->input->get('rider_id')) {
      $from = $from . "?rider_id=" . $this->input->get('rider_id');
    }

    redirect('cms/history/' . $from);
  }

  public function rebookorder($cart_id, $from)
  {
    
   $isrebooked = $this->cart_model->updateSchedBooking($cart_id,"pending");

   if($isrebooked)
    {
      $this->session->set_flashdata('flash_msg', ['message' => 'Order rebooked successfully.', 'color' => 'green']);
    }else
    {
     $this->session->set_flashdata('flash_msg', ['message' => 'Error Occured', 'color' => 'red']);
    }
    
    redirect('cms/history/' . $from);
  }

  public function completed($cart_id)
  {
   
    $res = $this->cart_model->get($cart_id);
    $grandtotal = unserialize($res->payload);

    $datasave = ["payment_status"=> "verified","total_price"=>$grandtotal["grand_total"]];
   
    // var_dump($res->basket); die();
    if ($res->status != 'completed' && count($res)>0 ) {
      if (in_array($res->cart_type_id, [4,1])) {
        // code...
        $deducted_amount = $res->basket['getapp_commission'];
      } else {
        $deducted_amount = $res->basket->getapp_commission;
      }
      $this->riders_model->deductCommission($deducted_amount, $res->rider_id);
    }
   $message = "" ;

   if ($res->status != 'completed' && count($res) >0) {
       $message = $this->cart_model->CMScompleteBooking($datasave, $cart_id);
    }

    $this->cart_model->setTotalPrice($cart_id);

  //  $message = $this->cart_model->CMScompleteBooking($datasave, $cart_id);

    $res = $this->cart_model->get($cart_id);

   if($res->voucher_code != null)
   {
    $completedBookingWithVoucherCode = [
      'booking_num'=>$res->id,
      'booking_date' =>$res->completed_at ,
      'rider_id' =>$res->rider_id ,
      'customer_id' =>$res->customer_id,
      'service_type' =>$res->cart_type_id,
      'voucher_code'=>$res->voucher_code,
      'voucher_amount'=>$res->voucher_amount,
      'status' =>'unpaid' ,
      'assigned_location' =>$res->assigned_location];

    $insertLastId = $this->cart_model->completedBookingwithVoucher($completedBookingWithVoucherCode);
   }
   
    if($message){
      return true;
    }else{
      return false;
    }
    
  }

  public function cancelorder($cart_id, $from)
  {

   $cart = $this->cart_model->get($cart_id);
   $this->returnTheVoucherCode($cart);
  
    $this->db->where('id', $cart_id);
    $this->db->update('cart',array('status' => "cancelled",'rider_id'=>0));

    $this->session->set_flashdata('flash_msg', ['message' => 'Order has been cancelled', 'color' => 'green']);
    if ($this->input->get('customer_id')) {
      $from = $from . "?customer_id=" . $this->input->get('customer_id');
    }
    if ($this->input->get('rider_id')) {
      $from = $from . "?rider_id=" . $this->input->get('rider_id');
    }

    redirect('cms/history/' . $from);
  }

public function compareDates($d1)
  {
    $date1 = date("Y-m-d",strtotime($d1));
    $date2 = date("Y-m-d",strtotime(date('Y-m-d')));

    $d1 = strtotime($date1);
    $d2 = strtotime($date2);

    if($d2 <= $d1)
    {
      return false;
    }else{
      return true;
    }
  }  

function  returnTheVoucherCode($cart)
{
 $available = false;
 $used = 0;
$assignedlocation = $this->session->userdata('location');
$vouchercode = $cart->voucher_code;
if ($vouchercode != null || $vouchercode != "") {
  $used = $this->cart_model->countVoucher($vouchercode,$assignedlocation);
  $this->cart_model->updatevoucherCode($vouchercode,$assignedlocation,$used);
}


 /*if($cart->voucher_code != NULL || $cart->voucher_code != ""){
  $res = $this->cart_model->voucherCode($vouchercode,$assignedlocation);
    if($res){
       foreach ($res as $key => $value) {
        $expiredate = $this->compareDates($value->expired_at);
          if($expiredate == true){
             $available = false;
          }else if($value->status == 0)
          {
            $available = false;
          }else if($value->used  >= $value->limit)
          {
            $available = false;

          }else{
                $available = true;
                $used = $value->used;
          }
       }
    }
}*/

 /*if($available == true){
       $used = $used - 1;
       $this->cart_model->updatevoucherCode($vouchercode,$assignedlocation,$used);
    }*/
}


  public function orders()
  {
    if (isset($_GET['rider_id']) && !$_GET['rider_id'])
    redirect('cms/history/orders');

    if (isset($_GET['customer_id']) && !$_GET['customer_id'])
    redirect('cms/history/orders');

    $this->db->where('assigned_location', $this->session->userdata('location'));
    $this->db->order_by('lower(full_name)', 'asc');
    $this->db->limit(9999999);
    $customers = $this->customers_model->all();
    $this->db->order_by('lower(full_name)', 'asc');
    $this->db->limit(9999999);
    $riders = $this->riders_model->all();

    // var_dump($customers); die();

    $res = [];

    if ($this->input->get('rider_id')) {

      $this->db->order_by('updated_at', 'desc');
      $this->db->where('assigned_location', $this->session->userdata('location'));
      $res = $this->cart_model->history('rider_id', $this->input->get('rider_id'));
      $data['history_of'] = $this->riders_model->get($this->input->get('rider_id'))->full_name;
    } else if($this->input->get('customer_id')) {
      $this->db->where('assigned_location', $this->session->userdata('location'));
      $this->db->order_by('updated_at', 'desc');
      $res = $this->cart_model->history('customer_id', $this->input->get('customer_id'));
      $data['history_of'] = $this->customers_model->get($this->input->get('customer_id'))->full_name;
    }

    if ($res) {
      foreach ($res as $key => &$value) {
        $value->cart = $this->cart_model->get($value->cart_id);
      }
    }



    $data['customers'] = $customers;
    $data['riders'] = $riders;
    $data['res'] = $res;

    // var_dump($res); die();
    $this->wrapper('cms/order_history', $data);
  }

  public function all_orders()
  {
    # Fix page 2 error
    $data['orders_daily'] = $this->cart_model->getCartStats('orders_daily');
    $data['orders_weekly'] = $this->cart_model->getCartStats('orders_weekly');
    $data['orders_monthly'] = $this->cart_model->getCartStats('orders_monthly');
    $data['orders_all_time'] = $this->cart_model->getCartStats('orders_all_time');
    
    $data['cancellation_order_daily'] = $this->cart_model->getCartStats('cancel_orders_daily');
    $data['cancellation_orders_weekly'] = $this->cart_model->getCartStats('cancel_orders_weekly');
    $data['cancellation_orders_monthly'] = $this->cart_model->getCartStats('cancel_orders_monthly');

    $data['car_all_time'] = $this->cart_model->getCartStats('car_all_time');
    $data['food_all_time'] = $this->cart_model->getCartStats('food_all_time');
    $data['pabili_all_time'] = $this->cart_model->getCartStats('pabili_all_time');
    $data['delivery_all_time'] = $this->cart_model->getCartStats('delivery_all_time');
    $data['grocery_all_time'] = $this->cart_model->getCartStats('grocery_all_time');

    $data['total_pages'] = $this->cart_model->getTotalPagesHist();

   
    $res = $this->cart_model->getCompleteHistory();
    if ($res) {
      foreach ($res as $key => &$value) {
        $value->cart = $this->cart_model->get($value->cart_id);
      }
    }

    $userlevel =  $this->session->userdata('userlevel'); 
    $data["accesslevel"] = $this->admin_model->getuseraccess('8',$userlevel);

    $data['res'] = $res;
    $this->wrapper('cms/all_orders', @$data);
  }

  public function active_orders()
  {
    # Fix page 2 error
    $data['orders_daily'] = $this->cart_model->getCartStats('orders_daily');
    $data['orders_weekly'] = $this->cart_model->getCartStats('orders_weekly');
    $data['orders_monthly'] = $this->cart_model->getCartStats('orders_monthly');
    $data['orders_all_time'] = $this->cart_model->getCartStats('orders_all_time');

    $data['cancellation_order_daily'] = $this->cart_model->getCartStats('cancel_orders_daily');
    $data['cancellation_orders_weekly'] = $this->cart_model->getCartStats('cancel_orders_weekly');
    $data['cancellation_orders_monthly'] = $this->cart_model->getCartStats('cancel_orders_monthly');

    $data['car_all_time'] = $this->cart_model->getCartStats('car_all_time');
    $data['food_all_time'] = $this->cart_model->getCartStats('food_all_time');
    $data['pabili_all_time'] = $this->cart_model->getCartStats('pabili_all_time');
    $data['delivery_all_time'] = $this->cart_model->getCartStats('delivery_all_time');
    $data['grocery_all_time'] = $this->cart_model->getCartStats('grocery_all_time');

    $_GET['active_only'] = 1;

    $data['total_pages'] = $this->cart_model->getTotalPagesHist();
    $userlevel =  $this->session->userdata('userlevel'); 
    $data["accesslevel"] = $this->admin_model->getuseraccess('8',$userlevel);

    $res = $this->cart_model->getCompleteHistory();
    if ($res) {
      foreach ($res as $key => &$value) {
        $value->cart = $this->cart_model->get($value->cart_id);
      }
    }

    $data['res'] = $res;
    $this->wrapper('cms/all_orders', @$data);
  }


  public function cancelled_orders()
  {
    # Fix page 2 error
    $data['orders_daily'] = $this->cart_model->getCartStats('orders_daily');
    $data['orders_weekly'] = $this->cart_model->getCartStats('orders_weekly');
    $data['orders_monthly'] = $this->cart_model->getCartStats('orders_monthly');
    $data['orders_all_time'] = $this->cart_model->getCartStats('orders_all_time');

    $data['cancellation_order_daily'] = $this->cart_model->getCartStats('cancel_orders_daily');
    $data['cancellation_orders_weekly'] = $this->cart_model->getCartStats('cancel_orders_weekly');
    $data['cancellation_orders_monthly'] = $this->cart_model->getCartStats('cancel_orders_monthly');

    $data['car_all_time'] = $this->cart_model->getCartStats('car_all_time');
    $data['food_all_time'] = $this->cart_model->getCartStats('food_all_time');
    $data['pabili_all_time'] = $this->cart_model->getCartStats('pabili_all_time');
    $data['delivery_all_time'] = $this->cart_model->getCartStats('delivery_all_time');
    $data['grocery_all_time'] = $this->cart_model->getCartStats('grocery_all_time');

    $_GET['cancelled_only'] = 1;

    $data['total_pages'] = $this->cart_model->getTotalPagesHist();
    $userlevel =  $this->session->userdata('userlevel'); 
    $data["accesslevel"] = $this->admin_model->getuseraccess('8',$userlevel);

    $res = $this->cart_model->getCompleteHistory();
    if ($res) {
      foreach ($res as $key => &$value) {
        $value->cart = $this->cart_model->get($value->cart_id);
      }
    }

    $data['res'] = $res;
    $this->wrapper('cms/all_orders', @$data);
  }


  public function active_orders_partner()
  {
    $usertype = $this->session->role;
    $id = $this->session->id;
    $customers = $this->getCustomerids($usertype,$id,$this->session->userdata('location'));

    $_GET['active_only'] = 1;

    $data['total_pages'] = $this->cart_model->getTotalPagesHistPartner($customers);


    $res = $this->cart_model->getCompleteHistoryPartner($customers);
    if ($res) {
      foreach ($res as $key => &$value) {
        $value->cart = $this->cart_model->get($value->cart_id);
      }
    }

    $data['res'] = $res;
    $this->wrapper('cms/activepartnerbooking', @$data);
  }

  public function cancelorderPartner($cart_id)
  {

   $cart = $this->cart_model->get($cart_id);
   $this->returnTheVoucherCode($cart);
  
    $this->db->where('id', $cart_id);
    $this->db->update('cart',array('status' => "cancelled",'rider_id'=>0));

    $this->session->set_flashdata('flash_msg', ['message' => 'Order has been cancelled', 'color' => 'green']);
    if ($this->input->get('customer_id')) {
      $from = $from . "?customer_id=" . $this->input->get('customer_id');
    }
    if ($this->input->get('rider_id')) {
      $from = $from . "?rider_id=" . $this->input->get('rider_id');
    }

   $this->wrapper('cms/partnerbookinghistory');
  }


  public function all_orders_partner()
  {
    $usertype = $this->session->role;
    $id = $this->session->id;

   $customers = $this->getCustomerids($usertype,$id,$this->session->userdata('location'));
   //print_r($customers);
   //die;
    $data['total_pages'] = $this->cart_model->getTotalPagesHistPartner($customers);


    $res = $this->cart_model->getCompleteHistoryPartner($customers);
    if ($res) {
      foreach ($res as $key => &$value) {
        $value->cart = $this->cart_model->get($value->cart_id);
      }
    }

    $data['res'] = $res;
    $this->wrapper('cms/partnerbookinghistory', @$data);
  }

  public function getCustomerids($usertype,$id,$al)
  {
    $arrCustomerids = [];
    $resp = $this->customers_model->selectedcustomers($usertype,$id,$al);
    if($resp)
    {
      foreach ($resp as $key => $value)
      {
        array_push($arrCustomerids,$value->id);
      }
    }
    
    return $arrCustomerids;

  }


  public function export()
  {

    $completedOrderDaily = $this->cart_model->getCartStats('completed_orders_daily');
    $cancellationOrderDaily = $this->cart_model->getCartStats('cancel_orders_daily');

    # //output headers so that the file is downloaded rather than displayed
    header('Content-type: text/csv');
    header('Content-Disposition: attachment; filename="' . date('Y-m-d') . '_sales.csv"');
    // do not cache the file
    header('Pragma: no-cache');
    header('Expires: 0');
    // create a file pointer connected to the output stream

    $file = fopen('php://output', 'w');
    // send the column headers

    fputcsv($file, 
      array('COMPLETED | SUCCESSFUL DELIVERY', $completedOrderDaily)
    );
    fputcsv($file, 
      array('Cancelled', $cancellationOrderDaily)
    );

    fputcsv($file, 
      array('')
    );

    fputcsv($file, 
      array('Merchant | Customer Name', 'Time of Booking', 'Rider Name', 'Delivery Status', 'Vertical','App Booking')
    );

    $this->db->where('assigned_location', $this->session->userdata('location'));
    $this->db->where('DAY(updated_at) = DAY(NOW()) && MONTH(updated_at) = MONTH(NOW()) && YEAR(updated_at) = YEAR(NOW())');
    $this->db->where_in('status',['completed','cancelled']);
    $this->db->order_by('updated_at', 'desc');
    $res = $this->db->get('cart')->result();

    //var_dump($res);
   // die();

    if (!$res) {
        return [];
    }
    foreach ($res as &$value) {
      $value = $this->cart_model->formatRes($value);
      //$value = $this->cart_model->formatHistory($value);
    }

    // if ($res) {
    //   foreach ($res as $key => &$value) {
    //     $value->cart = $this->cart_model->get($value->cart_id);
    //   }
    // }

    // var_dump($res); die();

    $new_res = [];
    foreach ($res as $key => $value) {
     // print_r($value->status);
      $pickUpLocatonName = $value->pickup_location->label;
      $deliveryLocatonName = $value->delivery_location->label;
      $new_res[] = array(
        $pickUpLocatonName."|".$deliveryLocatonName,
        $value->status == 'completed' ? date('F j, Y g:i a', strtotime($value->completed_at)) : date('F j, Y g:i a', strtotime($value->updated_at)),
        $value->rider->full_name,
        ucwords($value->status),
        "Get " . ucwords($value->service_type),
        "Yes"
      );
    }
    $data = $new_res;

     //var_dump($data); die();

    foreach ($data as $row)
    {
      fputcsv($file, $row);
    }
    exit();
  }


}

