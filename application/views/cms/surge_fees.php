<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/admin/') ?>assets/jquery-multi-select/css/multi-select.css" />
<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            <h4>Note: Only one Surge Rate could be applied at a time</h4>
          </header>
        </section>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-5">
        <section class="panel">
          <header class="panel-heading">
            Scheduled Surge
            <?php if ($flash_msg  && @$_GET['meta_key'] == 'scheduled_surge'): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
          </header>
          <div class="panel-body">
            <form action="<?php echo base_url('cms/dashboard/update_scheduled_surge') ?>" method="post">
              <input type="hidden" name="from" value="cms/dashboard/surge_fees">
              <input type="hidden" name="meta_key" value="scheduled_surge">
              <label>
                Rate Multiplier (in percent %) E.G 1.2 = 20%, etc.
                <br>
                <sub>The final delivery price will be multiplied with this value</sub>
              </label>
              <input class="form-control" type="text" name="scheduled_surge_rate" value="<?php echo @$scheduled_surge_rate ?>">
              <label>
                Scheduled Surge Label
                <br>
                <sub>This field gives information on why is there a surge</sub>
              </label>
              <input class="form-control" type="text" name="scheduled_surge_label" value="<?php echo @$scheduled_surge_label ?>">
              <br>
              <label>
                Scheduled Surge Days
                <br>
                <sub>The days when the scheduled surge will be triggered</sub>
              </label>
              <select multiple="multiple" class="multi-select" name="scheduled_surge_days[]">
                <option <?php echo in_array(1, $scheduled_surge_days) ? 'selected' : '' ?> value="1">Mondays</option>
                <option <?php echo in_array(2, $scheduled_surge_days) ? 'selected' : '' ?> value="2">Tuesdays</option>
                <option <?php echo in_array(3, $scheduled_surge_days) ? 'selected' : '' ?> value="3">Wednesdays</option>
                <option <?php echo in_array(4, $scheduled_surge_days) ? 'selected' : '' ?> value="4">Thursdays</option>
                <option <?php echo in_array(5, $scheduled_surge_days) ? 'selected' : '' ?> value="5">Fridays</option>
                <option <?php echo in_array(6, $scheduled_surge_days) ? 'selected' : '' ?> value="6">Saturdays</option>
                <option <?php echo in_array(7, $scheduled_surge_days) ? 'selected' : '' ?> value="7">Sundays</option>
              </select>
              <br>
              <label>
                Scheduled Surge Time
                <br>
                <sub>Time range when the surge will be triggered</sub>
              </label>
              <div class="row">
                <div class="col-md-6">
                  <div class="input-group bootstrap-timepicker">
                    <input value="<?php echo $scheduled_surge_time[0] ?>" id="from_hour" value="" type="text"  class="form-control timepicker-24" name="from_hour" placeholder="Pick a time">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                    </span>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="input-group bootstrap-timepicker">
                    <input value="<?php echo $scheduled_surge_time[1] ?>" id="to_hour" value="" type="text"  class="form-control timepicker-24" name="to_hour" placeholder="Pick a time">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                    </span>
                  </div>
                </div>
              </div>
              <br>
              <label>
                Is active?
                <br>
                <sub>Tick this box to enable surge</sub>
              </label>
              <br>
              <input type="checkbox" name="scheduled_surge_is_active" value="1" <?php echo ($scheduled_surge_is_active) ? 'checked': "" ?>>
              <br>
              <br>
              <input class="btn btn-info btn-md" type="submit" value="Update Options">
            </form>
            </div>
          </section>
        </div>

      <div class="col-lg-5">
        <section class="panel">
          <header class="panel-heading">
            Rainy Days Surge ☂️
            <?php if ($flash_msg  && @$_GET['meta_key'] == 'rainy_days_surge'): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php  endif; ?>
          </header>
          <div class="panel-body">
            <form action="<?php echo base_url('cms/dashboard/update_rainy_day_surge') ?>" method="post">
              <input type="hidden" name="meta_key" value="rainy_days_surge">
              <input type="hidden" name="from" value="cms/dashboard/surge_fees">
              <label>
                Rate Multiplier (in percent %) E.G 1.2 = 20%, etc.
                <br>
                <sub>The final delivery price will be multiplied with this value</sub>
              </label>
              <input class="form-control" type="text" name="rainy_day_surge_rate" value="<?php echo @$rainy_day_surge_rate ?>">
              <label>
                Rainy Days Surge Label
                <br>
                <sub>This field gives information on why is there a surge</sub>
              </label>
              <input class="form-control" type="text" name="rainy_day_surge_label" value="<?php echo @$rainy_day_surge_label ?>">
              <br>
              <label>
                Is active?
                <br>
                <sub>Tick this box to enable surge</sub>
              </label>
              <br>
              <input type="checkbox" name="rainy_day_surge_is_active" value="1" <?php echo ($rainy_day_surge_is_active) ? 'checked': "" ?>>
              <br>
              <br>
              <input class="btn btn-info btn-md" type="submit" value="Update Options">
            </form>
            </div>
          </section>
        </div>
      </div>




      <!-- page end-->
    </section>
  </section>


<script type="text/javascript">
  $(document).ready(function() {


      $('.multi-select').multiSelect({
          selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='Search Days...'>",
          selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='Search Days...'>",
          afterInit: function (ms) {
              var that = this,
                  $selectableSearch = that.$selectableUl.prev(),
                  $selectionSearch = that.$selectionUl.prev(),
                  selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                  selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

              that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                  .on('keydown', function (e) {
                      if (e.which === 40) {
                          that.$selectableUl.focus();
                          return false;
                      }
                  });

              that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                  .on('keydown', function (e) {
                      if (e.which == 40) {
                          that.$selectionUl.focus();
                          return false;
                      }
                  });
          },
          afterSelect: function () {
              this.qs1.cache();
              this.qs2.cache();
          },
          afterDeselect: function () {
              this.qs1.cache();
              this.qs2.cache();
          }
      });

    $('#from_hour').timepicker({
      autoclose: true,
      minuteStep: 1,
      showSeconds: true,
      showMeridian: false,
      defaultTime: 'value'
    });
    $('#to_hour').timepicker({
      autoclose: true,
      minuteStep: 1,
      showSeconds: true,
      showMeridian: false,
      defaultTime: 'value'
    });

  });
</script>

<script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/admin/') ?>assets/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/admin/') ?>assets/jquery-multi-select/js/jquery.quicksearch.js"></script>
