      <?php 
          $canedit = $accesslevel->can_edit;
          $candelete = $accesslevel->can_delete;
          $canadd = $accesslevel->can_add;
      ?>
<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    


    <div class="row">
      <div class="col-lg-12">
        <?php if($canadd == 1):  ?>
         <p>
          <button type="button" class="add-new btn btn-success btn-sm">Add new</button>
        </p>
        <?php endif; ?>
        <section class="panel">
          <header class="panel-heading" style="padding:15px">
            Voucher Code
            <?php if ($flash_msg = $this->session->flash_msg): ?>
              <br><sub style="color: <?php echo $flash_msg['color']; ?>"><?php echo $flash_msg['message']; ?></sub>
            <?php endif; ?>
            <div style="float:right">
              <form method="GET">
                <input type="text" name="squery" value="<?php echo @$_GET['squery'] ?>" placeholder="Search by Voucher Code">
                <input type="submit">
              </form>
            </div>
          </header>

          <div class="panel-body">
            <p>
              <!-- <button type="button" class="add-new btn btn-success btn-sm">Add new</button> -->
            </p>
            <div class="table-responsive" style="overflow: hidden; outline: none;" tabindex="1">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Voucher Code</th>
                    <th>Expiry Date</th>
                    <th>Amount</th>
                    <th>Voucher limit</th>
                    <th>Used Voucher</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if (count($res) > 0 ): ?>

                    <?php 
                    // var_dump($allLocation);
                    foreach ($res as $key => $value): ?>
                      <tr>
                        <th scope="row"><?php echo $startingPK++ ?></th>
                        <td><?php echo $value->voucher_code ?></td>
                        <td><?php echo date('F j, Y', strtotime($value->expired_at))  ?></td>
                        <td><?php echo $value->amount ?></td>
                        <td><?php echo $value->limit ?></td>
                        <td><?php echo $value->used ?></td>
                        <td><?php echo $value->status == 1?"Active":"Inactive"; ?></td>
                        <td>
                           <?php if($canedit == 1):  ?>
                          <button type="button"
                          data-payload='<?php echo json_encode(['id' => $value->id, 'voucher_code' => $value->voucher_code,'expired_at' => $value->expired_at, 'amount' => $value->amount, 'limit' => $value->limit,  'used' => $value->used , 'status' => $value->status, 'assigned_location'=>$value->assigned_location,'created_at'=>$value->created_at,'created_by'=>$value->created_by,'allLocation'=>$allLocation], JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE)?>'
                          class="edit-row btn btn-info btn-xs">Edit</button>
                        <?php endif; ?>
                        <?php if($candelete == 1): ?>
                          <button type="button" data-id='<?php echo $value->id; ?>'
                            class="btn btn-delete btn-danger btn-xs">Delete</button>
                         <?php endif; ?>
                          </td>
                        </tr>
                      <?php endforeach; ?>


                    <?php else: ?>
                      <tr>
                        <td colspan="8" style="text-align:center"><?= ($this->input->get('squery')) ? 'Empty Search Results':'Empty table data' ?></td>
                      </tr>
                    <?php endif; ?>
                  </tbody>
                </table>
              </div>

              <style>
              .active_lg {
                background: lightgray !important
              }
              </style>
              <ul class="pagination">
                <ul class='pagination'>
                  <?php $page = ($this->input->get('page')) ?: 1; ?>
                  <li><a href="<?php echo base_url('cms/voucher/') . "?page=1&squery=" . @$_GET['squery'];?>">&laquo;</a></li>

                  <!-- loop for desc -->
                  <?php for ($i = $page - 2; $i < ($page) ; $i++):
                    if ($i == -1 || $i == 0) {
                      continue;
                    }
                   ?>
                  <li><a href="<?php echo base_url('cms/voucher/') . "?page=" . $i . "&squery=" . @$_GET['squery'];?>"><?= $i ?></a></li>
                  <?php endfor; ?>
                  <!-- / loop for desc -->

                  <li><a href="<?php echo base_url('cms/voucher/') . "?page=" . $page . "&squery=" . @$_GET['squery'];?>"><?= $page ?></a></li>

                  <!-- loop for asc -->
                  <?php for ($i = $page + 1; $i < ($page + 3) ; $i++):
                  if ($i == $total_pages + 1 || $i == $total_pages + 2 || $total_pages == 0) {
                      continue;
                  }
                  ?>
                  <li><a href="<?php echo base_url('cms/voucher/') . "?page=" . $i . "&squery=" . @$_GET['squery'];?>"><?= $i ?></a></li>
                  <?php endfor; ?>
                  <!-- / loop for asc -->


                <li><a href="<?php echo base_url('cms/voucher/') . "?page=" . $total_pages . "&squery=" . @$_GET['squery'];?>">&raquo;</a></li>
                </ul>
              </ul>

            </div>
          </section>
        </div>
      </div>
      <!-- page end-->
    </section>
  </section>

  

  <!-- Modal -->
  <div class="modal fade update-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Manage</h4>
        </div>
        <div class="modal-body row">

          <div class="col-md-12">
            <h3>Voucher Details</h3>
            <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('cms/voucher/update') ?>">
              
              <div class="form-group">
                <label >Voucher Code</label>
                <input type="text" class="form-control" name="voucher_code"  id="up_voucher_code" required="required">
              </div>
              <div class="form-group">
                <label >Amount</label>
                <input type="text" class="form-control" name="amount" id="up_amount" required="required">
              </div>
              <div class="form-group">
                <label >Voucher limit</label>
                <input type="text" class="form-control" name="limit" id="up_limit" required="required">
              </div>
              <div class="form-group">
                <label >Expiry Date</label>
                <input type="date" class="form-control" name="expired_at" id="up_expired_at" required="required">
              </div>
              
              <div class="form-group">
                <label >Assigment Area</label>
                <select name="assigned_location" id="up_assigmentArea" class="form-control" >
                 
                 </select>
              </div>
              <div class="form-group">
                <label >Status</label>
               <select name="status" id="up_status" class="form-control" >
                 <option value='0'>Inactive</option>
                  <option value='1'>Active</option>
                </select>
              </div>
              <div class="form-group">
                <label >Created by</label>
                <input type="text" class="form-control"  id="up_created_by" disabled="true">
              </div>

              <div class="form-group">
                <label >Date & Time created</label>
                <input type="text" class="form-control"  id="up_created_at" disabled="true">
              </div>
  

        </div><!--  end div column 8 -->

        <!-- <div class="col-md-4">


        </div>  -->
        <!-- end div col md 4 -->

      </div>

      <div class="modal-footer">
        <input type="hidden" name="id" id="up_id">
        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
        <input class="btn btn-info" type="submit" value="Save changes">
      </div>
    </form>

    </div>
  </div>
</div>
  <!-- modal -->

<!-- Modal add new -->
  <div class="modal fade" id="addnewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Add New Voucher</h4>
        </div>
        <div class="modal-body row">

          <div class="col-md-12">
            <h3>Voucher Details</h3>
            <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('cms/voucher/addNewVoucher') ?>">
              <div class="form-group">
                <label >Voucher Code</label>
                <input type="text" class="form-control" name="voucher_code"  id="voucher_code" required="required">
              </div>
              <div class="form-group">
                <label >Amount</label>
                <input type="text" class="form-control" name="amount" id="amount" required="required">
              </div>
              <div class="form-group">
                <label >Expiry Date</label>
                <input type="date" class="form-control" name="expired_at" id="expired_at" required="required">
              </div>
              <div class="form-group">
                <label >Voucher limit</label>
                <input type="text" class="form-control" name="limit" id="limit" required="required">
              </div>
              <div class="form-group">
                <label >Assigment Area</label>
                <select name="assigned_location" id="assigned_location" class="form-control" >
                  <?php 

                    foreach($allLocation as $key => $value){
                      $brgy = $value["barangay"] != null?$value["barangay"].", ":"";
                      $city = $value["city"] != null?$value["city"].", ":"";
                      echo "<option value='".$value["id"]."'>".$brgy.$city.$value["province"]."</option>";
                    }
                  ?>
                 </select>
              </div>
            
             
             <div class="form-group">
           
                <label >Status</label>
                
                <select name="status" id="status" class="form-control" >
                  <option value='0'>Inactive</option>
                  <option value='1'>Active</option>
                 </select>
              </div>

        </div>

      <div class="modal-footer">
        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
        <input class="btn btn-info" type="submit" value="Save">
      </div>
    </form>

    </div>
  </div>
</div>
  <!-- modal addnew-->

  <script type="text/javascript">
    $(document).ready(function() {
     
      $('.add-new').on('click', function(){
        $('#addnewModal').modal()
      })

      $('.edit-row').on('click', function(){
      
       let All_location = $(this).data('payload').allLocation

        let id = $(this).data('payload').id
        let voucher = $(this).data('payload').voucher_code
        let expired_at = $(this).data('payload').expired_at
        let amount = $(this).data('payload').amount
        let status = $(this).data('payload').status
        let limit  = $(this).data('payload').limit
        let created_by = $(this).data('payload').created_by
        let created_at = $(this).data('payload').created_at
        
        let assigned_location = $(this).data('payload').assigned_location

        $.each(All_location, function(i,val){
         // console.log(vald+","+val.province)
          $('#up_assigmentArea').append(`<option value="${val.id}">
                                       ${val.barangay !=null ?val.barangay +",":""} ${val.city != null?val.city+",":""}  ${val.province}
                                  </option>`);
        })

        $("#up_assigmentArea option[value="+assigned_location+"]").prop("selected", "selected")
        
        $('#up_voucher_code').val(voucher)
        $('#up_expired_at').val(expired_at)
        $('#up_amount').val(amount)
        $("#up_status option[value="+status+"]").prop("selected", "selected")
        $('#up_created_by').val(created_by)
        $('#up_created_at').val(created_at)
        $('#up_id').val(id)
        $('#up_limit').val(limit)
       /* let vehicles = $(this).data('vehicles')
        let unverified_vehicles = $(this).data('unverified_vehicles')
        let active_vehicle = $(this).data('active_vehicle')


        
        $('#active_vehicle').empty();
        $('#active_vehicle').append("<li style='font-weight:bold'>" + active_vehicle.vehicle_model + " — " + active_vehicle.plate_number + "</li>")
        // console.log(active_vehicle);

        $('#up_full_name').val(full_name)
        $('#up_email').val(email)

        $('#up_is_admin_verified').removeAttr('checked')
        if (parseInt(is_admin_verified)) {
          $('#up_is_admin_verified').prop('checked', true)
        }

        $('#up_is_email_verified').removeAttr('checked')
        if (parseInt(is_email_verified)) {
          $('#up_is_email_verified').prop('checked', true)
        }

        $('#up_birthdate').val(birthdate)
        $('#up_location').val(location)
        $('#up_mobile_num').val(mobile_num)

        $('#booking_available_until').empty();
        if (booking_available_until == null) {
          $('#booking_available_until').html('This rider currently doesn\'t have any registered promos')
        } else {
          $('#booking_available_until').html(booking_available_until)
        }
      
        if (identification_document.includes('placeholder-logo')) {
          $('#identification_document').html('Empty')
        } else {
          $('#identification_document').html('<a target="_blank" href="' + identification_document + '">'+ identification_document_name +'</a>')
        }

        $('#up_id').val(id)
        $('#up_image').attr('src', image)*/
        $('.update-modal').modal()
      })

      $('html').on('click', '.btn-delete', function(e) {
        if (prompt('Are you sure you want to DELETE this? Type DELETE to continue') == 'DELETE') {
          invokeForm(base_url + 'cms/voucher/delete', {id: $(this).data('id')});
        }
      })

     
    });
  </script>

  <script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>

