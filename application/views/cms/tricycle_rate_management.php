<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            Tricycle Rates Management
            <?php if ($flash_msg && @$_GET['meta_key'] == 'tricycle_rates_management'): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
          </header>
          <div class="panel-body">
            <!--tab nav start-->
            <section class="panel">
                <header class="panel-heading tab-bg-dark-navy-blue ">
                    <ul class="nav nav-tabs">
                        <?php
                         $i=1;
                         //print_r($services);
                         foreach ($services as $key => $value): ?>
                          <li class="<?php echo $i == 1? 'active': ''; $i++;?>">
                              <a data-toggle="tab" href="#<?php echo str_replace(' ', '_', strtolower($value->type)) ?>"><?php 
                                         if(strtolower($value->type) == "car")
                                         {
                                           echo "Get Tricycle"; 
                                         }
                                      /*else
                                         {
                                           echo "Get ".ucfirst($value->type) ;
                                         }*/
                                        
                                        ?></a>
                          </li>
                        <?php endforeach; ?>
                    </ul>
                </header>
                <div class="panel-body">
                    <div class="tab-content">
                      <?php $i=1; foreach ($services as $key => $services): ?>
                        <div id="<?php echo str_replace(' ', '_', strtolower($services->type)) ?>" class="tab-pane <?php echo $i == 1? 'active': ''; $i++;?>">
                          

                      <div class="row">
                        <div class="col-md-4"> 
                         <div class="panel panel-info">
                        <div class="panel-heading">
                          <h3 class="panel-title">Cities/Municipalities</h3>
                        </div>
                        <div class="panel-body">
                          <ul class="nav nav-pills nav-stacked" id="navcity">
                           <?php foreach ($cities as $key => $val): ?> 
                            <!-- inner foreach -->
                            <li>
                            <form action="<?php echo base_url('cms/dashboard/update_rate') ?>" method="post">
                              
                                <div class="form-check">
                                  <input class="form-check-input flexcheck<?php echo str_replace(' ', '_', strtolower($services->type)) ?>" type="checkbox" value="" 
                                  data-payload='<?php echo json_encode(['rates'=>$rates,'city'=>$val->id,
                                  'allzone'=>$zone,'serviceid'=>$services->id,'servicetype'=>$services->type,'iszone'=>$val->is_zone], JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE)?>' >
                                  <label class="form-check-label" >
                                   <?php echo $val->municipality_city; ?>
                                  </label>
                                </div>
                              
                            </form>
                            </li>
                            <!-- / inner foreach -->
                          <?php endforeach; ?>
                         
                          </ul>
                        </div>
                      </div>
                       </div>
                        <div class="col-md-4">
                            <div class="panel panel-info">
                        <div class="panel-heading">
                          <h3 class="panel-title">Toda Zone</h3>
                        </div>
                        <div class="panel-body">
                          
                            <form action="<?php echo base_url('cms/dashboard/update_rate') ?>" method="post">
                              
                               <ul class="nav nav-pills nav-stacked" id="ulCheck<?php echo str_replace(' ', '_', strtolower($services->type)) ?>"> 
                               

                              </ul>
                              
                            </form>

                          
                        </div>
                      </div>
                        </div>
                        <div class="col-md-4">
                            <div class="panel panel-info">
                        <div class="panel-heading">
                          <h3 class="panel-title">Toda Rates</h3>
                        </div>
                        <div class="panel-body">
                            
                            <form action="<?php echo base_url('cms/dashboard/tricycle_rate_update') ?>" method="post">

                              <table class="table border-collapsed">
                                <tr>
                                  <th> Base fare</th>
                                  <td><input type="text"  name="basefare" id="basefare<?php echo str_replace(' ', '_', strtolower($services->type)) ?>" class="form-control" value="0.00" ></td>
                                </tr>

                                <tr>
                                  <th> Per KM fare</th>
                                  <td><input type="text" name="perkmfare" id="perkmfare<?php echo str_replace(' ', '_', strtolower($services->type)) ?>" class="form-control" value="0.00" ></td>
                                </tr>

                                 <tr>
                                  <th>Passenger limit</th>
                                  <td><input type="text" name="passlimit" id="passlimit<?php echo str_replace(' ', '_', strtolower($services->type)) ?>" class="form-control" value="0.00" ></td>
                                </tr>
                                <tr>
                                  <th>Admin fee</th>
                                  <td><input type="text" name="adminfee" id="adminfee<?php echo str_replace(' ', '_', strtolower($services->type)) ?>" class="form-control" value="0.00" ></td>
                                </tr>
                                <tr>
                                  <th>Getapp Commission</th>
                                  <td><input type="text" name="getappcommission" id="getappcommission<?php echo str_replace(' ', '_', strtolower($services->type))?>" class="form-control" value="0.00" ></td>
                                </tr>
                                <tr  >
                                  <th>Get Pabili Fee</th>
                                  <td><input type="text" name="pabilifee" id="pabilifee<?php echo str_replace(' ', '_', strtolower($services->type))?>" class="form-control" value="0.00" ></td>
                                </tr>
                              </table>

                              <input type="text" name="id"  id="ratesId<?php echo str_replace(' ', '_', strtolower($services->type))?>"  hidden="true" />
                              <input type="text" name="zoneId"  id="textZoneId" hidden="true" />
                              <input type="text" name="munId"  id="textmunId" hidden="true" >
                              <input type="text" name="isZone"  id="is_zone"  hidden="true">
                              
                              <input 
                                 class="btn btn-info btn-md btnsavechanges"   
                                 type="submit" value="Save Changes">

                            </form><br>
                              
                            <!-- / inner foreach -->
                         
                        </div>
                      </div>
                        </div>
                      </div>

 
                         
                        </div>
                      <?php endforeach; ?>
                        <!-- <div id="about" class="tab-pane">About</div>
                        <div id="profile" class="tab-pane">Profile</div>
                        <div id="contact" class="tab-pane">Contact</div> -->
                    </div>
                </div>
            </section>

          
            </div>
          </section>
        </div>

          


      </div>




      <!-- page end-->
    </section>
  </section>


<script type="text/javascript">
  $(document).ready(function() {

   var $checks = $('#navcity input[type="checkbox"]');
                $checks.click(function() {
                $checks.not(this).prop("checked", false);
     });

  //city trycicle
  $('.flexcheckcar').on('click', function(){
   
      let cityid  = $(this).data('payload').city
      let AllZone = $(this).data('payload').allzone
      let rates   = $(this).data('payload').rates
      let iszone  = $(this).data('payload').iszone
      let serviceType  = $(this).data('payload').serviceid
      let serivice = $(this).data('payload').servicetype
      let ischecked = $(this).prop('checked')
      console.log(serivice)
     
      //let
       $('#basefare'+serivice).val("")
       $('#perkmfare'+serivice).val("")
       $('#passlimit'+serivice).val("")
       $('#adminfee'+serivice).val("")
       $('#pabilifee'+serivice).val("0.00")
       $('#pabilifee'+serivice).attr("disabled",true)
       $('#getappcommission'+serivice).val("")
       $("#textmunId"+serivice).val(cityid)
       $(".btnsavechanges").attr("disabled", true)

     
        $("#ulCheck"+serivice).html("")
        
        $.each(AllZone, function(i,val){
          //console.log(cityid + ","+val.mun_cities)
         if(cityid == val.mun_cities && ischecked == true ){
          
          $("#ulCheck"+serivice).append(`<li><div class="form-check divcheckzone">
                                  <input class="form-check-input" data-zoneid = "${val.id}" 
                                   data-cityid = "${cityid}" data-serviceid = "${serviceType}" type="checkbox" >
                                  <label class="form-check-label" for="flexCheckDefault">
                                    ${val.zone_name}
                                  </label>
                                </div></li>`);
          $("#ulCheck"+serivice+"  .divcheckzone .form-check-input").data("rates",{'rate':rates,'zoneid':val.id,'assignedLocatiserviceTypeon':val.assigned_location,'cityid':cityid,'service':serivice});
          }
           $("#is_zone").val(iszone)
        })

        
         if(iszone == 0 && ischecked == true){
         
          $("#ulCheck"+serivice).append(`<li><div class="form-check divcheckzone">
                                  <input class="form-check-input" 
                                   type="checkbox"  checked="checked" disabled="true">
                                  <label class="form-check-label" for="flexCheckDefault">
                                    No TODA Zone
                                  </label>
                                </div></li>`);

            $.each(rates, function(index, item) {
             
              if(item.zone_id == 0 && item.mun_cities_id == cityid && item.service_id == serviceType)
              {
                 console.log(item)
                $(".btnsavechanges").attr("disabled", false);
                hasvalue = true
                
                 $('#basefare'+serivice).val(item.base_fare)
                 $('#perkmfare'+serivice).val(item.per_km_fare)
                 $('#passlimit'+serivice).val(item.passenger_limit)
                 $('#adminfee'+serivice).val(item.admin_fee)
                 $('#getappcommission'+serivice).val(item.getapp_commission_percentage)   
                 $('#pabilifee'+serivice).val(item.pabili_fee)
                 $("#textZoneId").val(item.zone_id)
                 $("#ratesId"+serivice).val(item.id)
              } 
              
           });

         }else
         {
           $('#ratesId'+serivice).val("")
           $('#textZoneId').val("")
           $('#textmunId').val("")
           $('#textmunId').val("")
           $(":submit").attr("disabled", true)
         }
    });

 //grocery
  $('.flexcheckgrocery').on('click', function(){
   
      let cityid  = $(this).data('payload').city
      let AllZone = $(this).data('payload').allzone
      let rates   = $(this).data('payload').rates
      let iszone  = $(this).data('payload').iszone
      let serviceType  = $(this).data('payload').serviceid
      let serivice = $(this).data('payload').servicetype
      let ischecked = $(this).prop('checked')
      //console.log(serivice)
     
      //let
       $('#basefare'+serivice).val("")
       $('#perkmfare'+serivice).val("")
       $('#passlimit'+serivice).val("")
       $('#adminfee'+serivice).val("")
       $('#pabilifee'+serivice).attr("disabled",true)
       $('#pabilifee'+serivice).val("0.00")
       $('#getappcommission'+serivice).val("")
       $("#textmunId").val(cityid)
       $(".btnsavechanges").attr("disabled", true)
     
       $("#ulCheck"+serivice).html("")
        
      
         if(ischecked == true){
         
          $("#ulCheck"+serivice).append(`<li><div class="form-check divcheckzone">
                                  <input class="form-check-input" 
                                   type="checkbox"  checked="checked" disabled="true">
                                  <label class="form-check-label" for="flexCheckDefault">
                                    No TODA Zone
                                  </label>
                                </div></li>`);

            $.each(rates, function(index, item) {

              if(item.zone_id == 0 && item.mun_cities_id == cityid && item.service_id == serviceType)
              {
                
                $(".btnsavechanges").attr("disabled", false);
                //hasvalue = true
                 $("#textZoneId").val(item.zone_id)
                 $('#basefare'+serivice).val(item.base_fare)
                 $('#perkmfare'+serivice).val(item.per_km_fare)
                 $('#passlimit'+serivice).val(item.passenger_limit)
                 $('#adminfee'+serivice).val(item.admin_fee)
                 $('#getappcommission'+serivice).val(item.getapp_commission_percentage)   
                 $('#pabilifee'+serivice).val(item.pabili_fee)
                 //$("#btnsavechanges").data("zoneid",item.id);
                 $("#ratesId"+serivice).val(item.id)
              } 
              
           });

         }else
         {
           $('#ratesId'+serivice).val("")
           $('#textZoneId').val("")
           $('#textmunId').val("")
           $('#textmunId').val("")
           $(":submit").attr("disabled", true)
         }
    });

  //city pabili
  $('.flexcheckpabili').on('click', function(){
   
      let cityid  = $(this).data('payload').city
      let AllZone = $(this).data('payload').allzone
      let rates   = $(this).data('payload').rates
      let iszone  = $(this).data('payload').iszone
      let serviceType  = $(this).data('payload').serviceid
      let serivice = $(this).data('payload').servicetype
      let ischecked = $(this).prop('checked')
      console.log(serivice)
     
      //let
       $('#basefare'+serivice).val("")
       $('#perkmfare'+serivice).val("")
       $('#passlimit'+serivice).val("")
       $('#pabilifee'+serivice).val("")
       $('#adminfee'+serivice).val("")
       $('#getappcommission'+serivice).val("")
       $("#textmunId"+serivice).val(cityid)
       $(".btnsavechanges").attr("disabled", true)
     
        $("#ulCheck"+serivice).html("")
        
        
         if(ischecked == true){
         
          $("#ulCheck"+serivice).append(`<li><div class="form-check divcheckzone">
                                  <input class="form-check-input" 
                                   type="checkbox"  checked="checked" disabled="true">
                                  <label class="form-check-label" for="flexCheckDefault">
                                    No TODA Zone
                                  </label>
                                </div></li>`);

            $.each(rates, function(index, item) {

              if(item.zone_id == 0 && item.mun_cities_id == cityid && item.service_id == serviceType)
              {
                $(".btnsavechanges").attr("disabled", false);
                hasvalue = true
                 $("#textZoneId"+serivice).val(item.zone_id)
                 $('#basefare'+serivice).val(item.base_fare)
                 $('#perkmfare'+serivice).val(item.per_km_fare)
                 $('#pabilifee'+serivice).val(item.pabili_fee)
                 $('#passlimit'+serivice).val(item.passenger_limit)
                 $('#adminfee'+serivice).val(item.admin_fee)
                 $('#getappcommission'+serivice).val(item.getapp_commission_percentage)   
                 $("#ratesId"+serivice).val(item.id)
              } 
              
           });

         }else
         {
           $('#ratesId'+serivice).val("")
           $('#textZoneId'+serivice).val("")
           $('#textmunId'+serivice).val("")
           $('#textmunId'+serivice).val("")
           $(":submit").attr("disabled", true)
         }
    });
  
  //city delivery
  $('.flexcheckdelivery').on('click', function(){
   
      let cityid  = $(this).data('payload').city
      let AllZone = $(this).data('payload').allzone
      let rates   = $(this).data('payload').rates
      let iszone  = $(this).data('payload').iszone
      let serviceType  = $(this).data('payload').serviceid
      let serivice = $(this).data('payload').servicetype
      let ischecked = $(this).prop('checked')
      console.log(serivice)
     
      //let
       $('#basefare'+serivice).val("")
       $('#perkmfare'+serivice).val("")
       $('#passlimit'+serivice).val("")
       $('#adminfee'+serivice).val("0.00")
       $('#pabilifee'+serivice).attr("disabled",true)
       $('#getappcommission'+serivice).val("")
       $("#textmunId"+serivice).val(cityid)
       $(".btnsavechanges").attr("disabled", true)
       $('#pabilifee'+serivice).val("")
     
        $("#ulCheck"+serivice).html("")
        
       
         if(ischecked == true){
         
          $("#ulCheck"+serivice).append(`<li><div class="form-check divcheckzone">
                                  <input class="form-check-input" 
                                   type="checkbox"  checked="checked" disabled="true">
                                  <label class="form-check-label" for="flexCheckDefault">
                                    No TODA Zone
                                  </label>
                                </div></li>`);

            $.each(rates, function(index, item) {

              if(item.zone_id == 0 && item.mun_cities_id == cityid && item.service_id == serviceType)
              {
                $(".btnsavechanges").attr("disabled", false);
                hasvalue = true
                 $("#textZoneId"+serivice).val(item.zone_id)
                 $('#basefare'+serivice).val(item.base_fare)
                 $('#perkmfare'+serivice).val(item.per_km_fare)
                 $('#passlimit'+serivice).val(item.passenger_limit)
                 $('#adminfee'+serivice).val(item.admin_fee)
                 $('#pabilifee'+serivice).val(item.pabili_fee)
                 $('#getappcommission'+serivice).val(item.getapp_commission_percentage)   
                 //$("#btnsavechanges").data("zoneid",item.id);
                 $("#ratesId"+serivice).val(item.id)
              } 
              
           });

         }else
         {
           $('#ratesId'+serivice).val("")
           $('#textZoneId'+serivice).val("")
           $('#textmunId'+serivice).val("")
           $('#textmunId'+serivice).val("")
           $(":submit").attr("disabled", true)
         }
    });

  //city food 

  $('.flexcheckfood').on('click', function(){
   
      let cityid  = $(this).data('payload').city
      let AllZone = $(this).data('payload').allzone
      let rates   = $(this).data('payload').rates
      let iszone  = $(this).data('payload').iszone
      let serviceType  = $(this).data('payload').serviceid
      let serivice = $(this).data('payload').servicetype
      let ischecked = $(this).prop('checked')
      console.log(serivice)
     
      //let
       $('#basefare'+serivice).val("")
       $('#perkmfare'+serivice).val("")
       $('#passlimit'+serivice).val("")
       $('#adminfee'+serivice).val("")
       $('#getappcommission'+serivice).val("")
       $('#pabilifee'+serivice).val("0.00")
       $('#pabilifee'+serivice).attr("disabled",true)
       $("#textmunId").val(cityid)
       $(".btnsavechanges").attr("disabled", true)
     
        $("#ulCheck"+serivice).html("")
        
        
         if(ischecked == true){
         
          $("#ulCheck"+serivice).append(`<li><div class="form-check divcheckzone">
                                  <input class="form-check-input" 
                                   type="checkbox"  checked="checked" disabled="true">
                                  <label class="form-check-label" for="flexCheckDefault">
                                    No TODA Zone
                                  </label>
                                </div></li>`);

            $.each(rates, function(index, item) {

              if(item.zone_id == 0 && item.mun_cities_id == cityid && item.service_id == serviceType)
              {
                $(".btnsavechanges").attr("disabled", false);
                hasvalue = true
                 $("#textZoneId").val(item.zone_id)
                 $('#basefare'+serivice).val(item.base_fare)
                 $('#perkmfare'+serivice).val(item.per_km_fare)
                 $('#passlimit'+serivice).val(item.passenger_limit)
                 $('#adminfee'+serivice).val(item.admin_fee)
                 $('#pabilifee'+serivice).val(item.pabili_fee)
                 $('#getappcommission'+serivice).val(item.getapp_commission_percentage)   
                 //$("#btnsavechanges").data("zoneid",item.id);
                 $("#ratesId"+serivice).val(item.id)
              } 
              
           });

         }else
         {
           $('#ratesId'+serivice).val("")
           $('#textZoneId'+serivice).val("")
           $('#textmunId'+serivice).val("")
           $('#textmunId'+serivice).val("")
           $(":submit").attr("disabled", true)
         }
    });


     //trycycle
    $('#ulCheckcar').on('click', '.divcheckzone .form-check-input', function() {
         let rates   = $(this).data('rates').rate
         let zoneid  =  $(this).data('zoneid')
         let servicetype = $(this).data('serviceid')
         let cityid  = $(this).data('cityid')
         let assignedLocation =  $(this).data('rates').assignedLocation
         let serivice =  $(this).data('rates').service
         $(".btnsavechanges").attr("disabled", true);
         let hasvalue = false
         $('#pabilifee'+serivice).val("0.00")

         if($(this).prop('checked'))
         {
          $.each(rates, function(index, item) {

            if(item.zone_id == zoneid && item.mun_cities_id == cityid && item.service_id == servicetype)
            {
              hasvalue = true
        
               $(".btnsavechanges").attr("disabled", false);
               $("#textZoneId"+serivice).val(item.zone_id)
               $('#basefare'+serivice).val(item.base_fare)
               //$('#pabilifee'+serivice).val(item.pabili_fee)
               $('#perkmfare'+serivice).val(item.per_km_fare)
               $('#passlimit'+serivice).val(item.passenger_limit)
               $('#adminfee'+serivice).val(item.admin_fee)
               $('#getappcommission'+serivice).val(item.getapp_commission_percentage)   
               $("#ratesId"+serivice).val(item.id)
            } 
         });

         }else{
           $('#basefare'+serivice).val("0.00")
             $('#perkmfare'+serivice).val("0.00")
             $('#pabilifee'+serivice).val("0.00")
             $('#passlimit'+serivice).val("0.00")
             $('#adminfee'+serivice).val("0.00")
             $('#getappcommission'+serivice).val("0.00")   
             $("#ratesId"+serivice).val("")
             $("#textZoneId"+serivice).val("")
             
         }

         var $checks = $('#ulCheck'+serivice+' input[type="checkbox"]');
             $checks.click(function() {
                $checks.not(this).prop("checked", false);
             });

         if(hasvalue == false)
         {
            $('#basefare'+serivice).val("0.00")
            $('#perkmfare'+serivice).val("0.00")
            $('#pabilifee'+serivice).val("0.00")
            $('#passlimit'+serivice).val("0.00")
            $('#adminfee'+serivice).val("0.00")
            $('#getappcommission'+serivice).val("0.00")
            $("#ratesId"+serivice).val("")
            $("#textZoneId"+serivice).val("")  
         }         
         
     });

    //grocery 
    $('#ulCheckgrocery').on('click', '.divcheckzone .form-check-input', function() {
         let rates   = $(this).data('rates').rate
         let zoneid  =  $(this).data('zoneid')
         let servicetype = $(this).data('serviceid')
         let cityid  = $(this).data('cityid')
         let assignedLocation =  $(this).data('rates').assignedLocation
         $(".btnsavechanges").attr("disabled", true);
         let hasvalue = false
          $('#boundaries').val("")

         if($(this).prop('checked'))
         {
          $.each(rates, function(index, item) {

            if(item.zone_id == zoneid && item.mun_cities_id == cityid && item.service_id == servicetype)
            {
              hasvalue = true
        
               $(".btnsavechanges"+serivice).attr("disabled", false);
               $("#textZoneId"+serivice).val(item.zone_id)
               $('#basefare'+serivice).val(item.base_fare)
               $('#perkmfare'+serivice).val(item.per_km_fare)
               $('#passlimit'+serivice).val(item.passenger_limit)
               $('#adminfee'+serivice).val(item.admin_fee)
               $('#getappcommission'+serivice).val(item.getapp_commission_percentage)   
               $("#ratesId"+serivice).val(item.id)
            } 
         });

         }else{
           $('#basefare'+serivice).val("0.00")
             $('#perkmfare'+serivice).val("0.00")
             $('#passlimit'+serivice).val("0.00")
             $('#adminfee'+serivice).val("0.00")
             $('#getappcommission'+serivice).val("0.00")   
             $("#ratesId"+serivice).val("")
             $("#textZoneId"+serivice).val("")
             
         }

         var $checks = $('#ulCheck input[type="checkbox"]');
             $checks.click(function() {
                $checks.not(this).prop("checked", false);
             });

         if(hasvalue == false)
         {
            $('#basefare'+serivice).val("0.00")
            $('#perkmfare'+serivice).val("0.00")
            $('#passlimit'+serivice).val("0.00")
            $('#adminfee'+serivice).val("0.00")
            $('#getappcommission'+serivice).val("0.00")
            $("#ratesId"+serivice).val("")
            $("#textZoneId"+serivice).val("")  
         }         
         
     });

    //pabili
    $('#ulCheckpabili').on('click', '.divcheckzone .form-check-input', function() {
         let rates   = $(this).data('rates').rate
         let zoneid  =  $(this).data('zoneid')
         let servicetype = $(this).data('serviceid')
         let cityid  = $(this).data('cityid')
         let assignedLocation =  $(this).data('rates').assignedLocation
         $(".btnsavechanges").attr("disabled", true);
         let hasvalue = false
          $('#boundaries').val("")

         if($(this).prop('checked'))
         {
          $.each(rates, function(index, item) {

            if(item.zone_id == zoneid && item.mun_cities_id == cityid && item.service_id == servicetype)
            {
              hasvalue = true
        
               $(".btnsavechanges"+serivice).attr("disabled", false);
               $("#textZoneId"+serivice).val(item.zone_id)
               $('#basefare'+serivice).val(item.base_fare)
               $('#perkmfare'+serivice).val(item.per_km_fare)
               $('#passlimit'+serivice).val(item.passenger_limit)
               $('#adminfee'+serivice).val(item.admin_fee)
               $('#getappcommission'+serivice).val(item.getapp_commission_percentage)   
               $("#ratesId"+serivice).val(item.id)
            } 
         });

         }else{
           $('#basefare'+serivice).val("0.00")
             $('#perkmfare'+serivice).val("0.00")
             $('#passlimit'+serivice).val("0.00")
             $('#adminfee'+serivice).val("0.00")
             $('#getappcommission'+serivice).val("0.00")   
             $("#ratesId"+serivice).val("")
             $("#textZoneId"+serivice).val("")
             
         }

         var $checks = $('#ulCheck input[type="checkbox"]');
             $checks.click(function() {
                $checks.not(this).prop("checked", false);
             });

         if(hasvalue == false)
         {
            $('#basefare'+serivice).val("0.00")
            $('#perkmfare'+serivice).val("0.00")
            $('#passlimit'+serivice).val("0.00")
            $('#adminfee'+serivice).val("0.00")
            $('#getappcommission'+serivice).val("0.00")
            $("#ratesId"+serivice).val("")
            $("#textZoneId"+serivice).val("")  
         }         
         
     });

   //delivery
    $('#ulCheckdelivery').on('click', '.divcheckzone .form-check-input', function() {
         let rates   = $(this).data('rates').rate
         let zoneid  =  $(this).data('zoneid')
         let servicetype = $(this).data('serviceid')
         let cityid  = $(this).data('cityid')
         let assignedLocation =  $(this).data('rates').assignedLocation
         $(".btnsavechanges").attr("disabled", true);
         let hasvalue = false
        

         if($(this).prop('checked'))
         {
          $.each(rates, function(index, item) {

            if(item.zone_id == zoneid && item.mun_cities_id == cityid && item.service_id == servicetype)
            {
              hasvalue = true
        
               $(".btnsavechanges").attr("disabled", false);
               $("#textZoneId"+serivice).val(item.zone_id)
               $('#basefare'+serivice).val(item.base_fare)
               $('#perkmfare'+serivice).val(item.per_km_fare)
               $('#passlimit'+serivice).val(item.passenger_limit)
               $('#adminfee'+serivice).val(item.admin_fee)
               $('#getappcommission'+serivice).val(item.getapp_commission_percentage)   
               $("#ratesId"+serivice).val(item.id)
            } 
         });

         }else{
           $('#basefare'+serivice).val("0.00")
             $('#perkmfare'+serivice).val("0.00")
             $('#passlimit'+serivice).val("0.00")
             $('#adminfee'+serivice).val("0.00")
             $('#getappcommission'+serivice).val("0.00")   
             $("#ratesId"+serivice).val("")
             $("#textZoneId"+serivice).val("")
             
         }

         var $checks = $('#ulCheck input[type="checkbox"]');
             $checks.click(function() {
                $checks.not(this).prop("checked", false);
             });

         if(hasvalue == false)
         {
            $('#basefare'+serivice).val("0.00")
            $('#perkmfare'+serivice).val("0.00")
            $('#passlimit'+serivice).val("0.00")
            $('#adminfee'+serivice).val("0.00")
            $('#getappcommission'+serivice).val("0.00")
            $("#ratesId"+serivice).val("")
            $("#textZoneId"+serivice).val("")  
         }         
         
     });
   
   //food
   $('#ulCheckfood').on('click', '.divcheckzone .form-check-input', function() {
         let rates   = $(this).data('rates').rate
         let zoneid  =  $(this).data('zoneid')
         let servicetype = $(this).data('serviceid')
         let cityid  = $(this).data('cityid')
         let assignedLocation =  $(this).data('rates').assignedLocation
         $(".btnsavechanges").attr("disabled", true);
         let hasvalue = false
         
         if($(this).prop('checked'))
         {
          $.each(rates, function(index, item) {

            if(item.zone_id == zoneid && item.mun_cities_id == cityid && item.service_id == servicetype)
            {
              hasvalue = true
        
               $(".btnsavechanges").attr("disabled", false);
               $("#textZoneId"+serivice).val(item.zone_id)
               $('#basefare'+serivice).val(item.base_fare)
               $('#perkmfare'+serivice).val(item.per_km_fare)
               $('#passlimit'+serivice).val(item.passenger_limit)
               $('#adminfee'+serivice).val(item.admin_fee)
               $('#getappcommission'+serivice).val(item.getapp_commission_percentage)   
               $("#ratesId"+serivice).val(item.id)
            } 
         });

         }else{
           $('#basefare'+serivice).val("0.00")
             $('#perkmfare'+serivice).val("0.00")
             $('#passlimit'+serivice).val("0.00")
             $('#adminfee'+serivice).val("0.00")
             $('#getappcommission'+serivice).val("0.00")   
             $("#ratesId"+serivice).val("")
             $("#textZoneId"+serivice).val("")
             
         }

         var $checks = $('#ulCheck input[type="checkbox"]');
             $checks.click(function() {
                $checks.not(this).prop("checked", false);
             });

         if(hasvalue == false)
         {
            $('#basefare'+serivice).val("0.00")
            $('#perkmfare'+serivice).val("0.00")
            $('#passlimit'+serivice).val("0.00")
            $('#adminfee'+serivice).val("0.00")
            $('#getappcommission'+serivice).val("0.00")
            $("#ratesId"+serivice).val("")
            $("#textZoneId"+serivice).val("")  
         }         
         
     });

  });

    
</script>

<script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>

