<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">

            <div class="row">
              <div class="col-md-2">
                GET24 All-time Purchases: <span style="font-weight:bold"><?php echo $get24_purchases ?></span>
              </div>

              <div class="col-md-2">
                GET6 All-time Purchases: <span style="font-weight:bold"><?php echo $get6_purchases ?></span>
              </div>
              <div class="col-md-2">
                GET3 All-time Purchases: <span style="font-weight:bold"><?php echo $get3_purchases ?></span>
              </div>
            </div>
<hr>
            <div class="row">
              <div class="col-md-2">
                Active Riders Daily: <span style="font-weight:bold"><?php echo $active_riders_daily ?></span>
              </div>

              <div class="col-md-2">
                Active Riders this Week: <span style="font-weight:bold"><?php echo $active_riders_weekly ?></span>
              </div>
              <div class="col-md-2">
                Active Riders this Month: <span style="font-weight:bold"><?php echo $active_riders_monthly ?></span>
              </div>
            </div>
          </header>
        </section>
      </div>
    </div>


    <div class="row">
      <div class="col-lg-12">
       <p>
          <button type="button" class="add-new btn btn-success btn-sm">Add new</button>
        </p>
        <section class="panel">
          <header class="panel-heading" style="padding:15px">
            Riders
            <?php if ($flash_msg = $this->session->flash_msg): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
            <div style="float:right">
              <form method="GET">
                <input type="text" name="squery" value="<?php echo @$_GET['squery'] ?>" placeholder="Search by full name">
                <input type="submit">
              </form>
            </div>
          </header>
          <div class="panel-body">
            <p>
              <!-- <button type="button" class="add-new btn btn-success btn-sm">Add new</button> -->
            </p>
            <div class="table-responsive" style="overflow: hidden; outline: none;" tabindex="1">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Full name</th>
                    <th>Email</th>
                    <th>Mobile number</th>
                    <th>Profile picture</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if (count($res) > 0 ): ?>

                    <?php 
                    // var_dump($allLocation);
                    foreach ($res as $key => $value): ?>
                      <tr>
                        <th scope="row"><?php echo $startingPK++ ?></th>
                        <td><?php echo $value->full_name ?></td>
                        <td><?php echo $value->email ?></td>
                        <td><?php echo $value->mobile_num ?></td>
                        <td><img style="max-width:150px" src="<?php echo $value->profile_picture ?>"></td>
                        <td>
                          <button type="button"
                          data-payload='<?php echo json_encode(['id' => $value->id, 'full_name' => $value->full_name, 'location' => $value->location, 'birthdate' => $value->birthdate, 'is_admin_verified' => $value->is_admin_verified, 'is_email_verified' => $value->is_email_verified,  'email' => $value->email , 'mobile_num' => $value->mobile_num, 'booking_available_until' => ($value->booking_available_until) ? date('F j, Y g:i:sA', strtotime($value->booking_available_until)) : null, 'image' => $value->profile_picture, 'identification_document' => $value->identification_document, 'identification_document_name' => $value->identification_document_name,'assigned_location'=>$value->assigned_location, 'allLocation'=>$allLocation, ], JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE)?>'
                          data-vehicles='<?php echo json_encode($value->vehicles, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE)?>'
                          data-unverified_vehicles='<?php echo json_encode($value->unverified_vehicles, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE)?>'
                          data-active_vehicle='<?php echo json_encode($value->active_vehicle, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE)?>'
                          class="edit-row btn btn-info btn-xs">Edit</button>
                          <button type="button" data-id='<?php echo $value->id; ?>'
                            class="btn btn-delete btn-danger btn-xs">Delete</button>
                            <?php if ($value->banned_at): ?>
                              <button type="button" data-id='<?php echo $value->id; ?>'
                                class="btn btn-unban btn-success btn-xs">Unban</button>
                            <?php else: ?>
                              <button type="button" data-id='<?php echo $value->id; ?>'
                                class="btn btn-ban btn-danger btn-xs">Ban</button>
                            <?php endif; ?>
                            <?php if (count($value->unverified_vehicles)): ?>
                              <button type="button" data-id='<?php echo $value->id; ?>'
                                class="btn btn-success btn-xs" title="This rider has pending vehicle registrations"><i class="fa fa-exclamation-circle"></i> Has Pending Vehicles</i></button>
                            <?php endif; ?>
                            <?php if (!$value->is_admin_verified): ?>
                              <button type="button" data-id='<?php echo $value->id; ?>'
                                class="btn btn-info btn-xs" title="This rider needs to be approved by admin"><i class="fa fa-exclamation-circle"></i> Waiting for Admin Approval</i></button>
                            <?php endif; ?>
                            <?php if (!$value->is_email_verified): ?>
                              <button type="button" data-id='<?php echo $value->id; ?>'
                                class="btn btn-warning btn-xs" title="This rider needs to be verify their email"><i class="fa fa-exclamation-circle"></i> Needs email verification</i></button>
                            <?php endif; ?>
                          </td>
                        </tr>
                      <?php endforeach; ?>


                    <?php else: ?>
                      <tr>
                        <td colspan="6" style="text-align:center"><?= ($this->input->get('squery')) ? 'Empty Search Results':'Empty table data' ?></td>
                      </tr>
                    <?php endif; ?>
                  </tbody>
                </table>
              </div>

              <style>
              .active_lg {
                background: lightgray !important
              }
              </style>
              <ul class="pagination">
                <ul class='pagination'>
                  <?php $page = ($this->input->get('page')) ?: 1; ?>
                  <li><a href="<?php echo base_url('cms/riders/') . "?page=1&squery=" . @$_GET['squery'];?>">&laquo;</a></li>

                  <!-- loop for desc -->
                  <?php for ($i = $page - 2; $i < ($page) ; $i++):
                    if ($i == -1 || $i == 0) {
                      continue;
                    }
                   ?>
                  <li class='<?php echo  @$_GET['page'] == $i ? 'active': ''; ?>' > <a href="<?php echo base_url('cms/riders/') . "?page=" . $i . "&squery=" . @$_GET['squery'];?>"><?= $i ?></a></li>
                  <?php endfor; ?>
                  <!-- / loop for desc -->

                  <li class='<?php echo  @$_GET['page'] == $i ? 'active': ''; ?>' ><a href="<?php echo base_url('cms/riders/') . "?page=" . $page . "&squery=" . @$_GET['squery'];?>"><?= $page ?></a></li>

                  <!-- loop for asc -->
                  <?php for ($i = $page + 1; $i < ($page + 3) ; $i++):
                  if ($i == $total_pages + 1 || $i == $total_pages + 2 || $total_pages == 0) {
                      continue;
                  }
                  ?>
                  <li class='<?php echo  @$_GET['page'] == $i ? 'active': ''; ?>' ><a href="<?php echo base_url('cms/riders/') . "?page=" . $i . "&squery=" . @$_GET['squery'];?>"><?= $i ?></a></li>
                  <?php endfor; ?>
                  <!-- / loop for asc -->


                <li class='<?php echo  $_GET['page'] == $i ? 'active': ''; ?>' ><a href="<?php echo base_url('cms/riders/') . "?page=" . $total_pages . "&squery=" . @$_GET['squery'];?>">&raquo;</a></li>
                </ul>
              </ul>

            </div>
          </section>
        </div>
      </div>
      <!-- page end-->
    </section>
  </section>

  <!-- Modal -->
  <!-- <div class="modal fade add-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Manage</h4>
        </div>
        <div class="modal-body">

          <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('cms/riders/add') ?>">
            <div class="form-group">
              <label >Title</label>
              <input type="text" class="form-control" name="title"  required="required">
            </div>
            <div class="form-group">
              <label >Category</label>
              <input type="text" class="form-control" name="category"  required="required">
            </div>
            <div class="form-group">
              <label >Body</label>
              <textarea name="body" class="form-control" required="required"></textarea>
            </div>
            <div class="form-group">
              <label >Banner Image</label>
              <input type="file" name="banner_image" class="form-control">
            </div>
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <input class="btn btn-info" type="submit" value="Save changes">
          </div>
        </form>
      </div>
    </div>
  </div> -->
  <!-- modal -->

  <!-- Modal -->
  <div class="modal fade update-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Manage</h4>
        </div>
        <div class="modal-body row">

          <div class="col-md-12">
            <h3>Rider Information</h3>
            <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('cms/riders/update') ?>">
              <div class="form-group">
                <label >Full name</label>
                <input type="text" class="form-control" name="full_name"  id="up_full_name" required="required">
              </div>
              <div class="form-group">
                <label >Email</label>
                <input type="email" class="form-control" name="email" id="up_email" required="required">
              </div>
              <div class="form-group">
                <label >Mobile number</label>
                <input type="text" class="form-control" name="mobile_num" id="up_mobile_num" required="required">
              </div>
              <div class="form-group">
                <label >Birth date</label>
                <input type="date" class="form-control" name="birthdate" id="up_birthdate" required="required">
              </div>
              <div class="form-group">
                <label >Assigment Area</label>
                <select name="assigned_location" id="up_assigmentArea" class="form-control" >
                 
                 </select>
              </div>
              <div class="form-group">
                <label >Location</label>
                <textarea name="location" class="form-control" id="up_location"></textarea>
              </div>
              <div class="form-group">
                <label >Is email verified?</label>
                <input type="checkbox" id="up_is_email_verified" name="is_email_verified" value="1">
              </div>
              <div class="form-group">
                <label >Verify this Rider?</label>
                <input type="checkbox" id="up_is_admin_verified" name="is_admin_verified" value="1">
              </div>

              <div class="form-group">
                <label >Profile picture</label>
                <br>
                <img src="" id="up_image" style="max-width:150px">
                <br>
                <br>
                <input type="file" name="profile_picture" class="form-control">
              </div>
              <h3>Vehicles</h3>
              <h4>Active Vehicle</h4>
              <ol id="active_vehicle">
              </ol>
              <h4>All vehicles</h4>
              <ol id="vehicles">
              </ol>
              <h4>Unverified Vehicles</h4>
              <ol id="unverified_vehicles">
              </ol>

              <hr>

              <h4>Identification Document</h4>
              <ol>
                <li id="identification_document"></li>
              </ol>
              <div class="form-group">
                <label >Upload Identification Document</label>
                <input type="file" name="identification_document" class="form-control">
              </div>
              <h4>Booking Available Until</h4>
              <ol>
                <li id="booking_available_until"></li>
              </ol>


        </div><!--  end div column 8 -->

        <!-- <div class="col-md-4">


        </div>  -->
        <!-- end div col md 4 -->

      </div>

      <div class="modal-footer">
        <input type="hidden" name="id" id="up_id">
        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
        <input class="btn btn-info" type="submit" value="Save changes">
      </div>
    </form>

    </div>
  </div>
</div>
  <!-- modal -->

<!-- Modal add new -->
  <div class="modal fade" id="addnewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Add New Rider</h4>
        </div>
        <div class="modal-body row">

          <div class="col-md-12">
            <h3>Rider Information</h3>
            <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('cms/riders/addNewRider') ?>">
              <div class="form-group">
                <label >Full name</label>
                <input type="text" class="form-control" name="full_name"  id="full_name" required="required">
              </div>
              <div class="form-group">
                <label >Email</label>
                <input type="email" class="form-control" name="email" id="email" required="required">
              </div>
              <div class="form-group">
                <label >Password</label>
                <input type="password" class="form-control" name="password" id="password" required="required">
              </div>
              <div class="form-group">
                <label >Mobile number (auto add dash)</label>
                <input type="text" class="form-control" name="mobile_num" id="mobile_num" required="required">
              </div>
              <div class="form-group">
                <label >Birth date</label>
                <input type="date" class="form-control" name="birthdate" id="birthdate" required="required">
              </div>
              <div class="form-group">
                <label >Assigment Area</label>
                <select name="assigned_location" id="assigmentArea" class="form-control" >
                  <?php 

                    foreach($allLocation as $key => $value){
                      $brgy = $value["barangay"] != null?$value["barangay"].", ":"";
                      $city = $value["city"] != null?$value["city"].", ":"";
                      echo "<option value='".$value["id"]."'>".$brgy.$city.$value["province"]."</option>";
                    }
                  ?>
                 </select>
              </div>
              <div class="form-group">
                <label >Location</label>
                <textarea name="location" class="form-control" id="location"></textarea>
              </div>
              <div class="form-group">
                <label >Is email verified?</label>
                <input type="checkbox" id="is_email_verified" name="is_email_verified" value="1">
              </div>
              <div class="form-group">
                <label >Verify this Rider?</label>
                <input type="checkbox" id="is_admin_verified" name="is_admin_verified" value="1">
              </div>

              <div class="form-group">
                <label >Profile picture</label>
                
                <input type="file" name="profile_picture" class="form-control">
              </div>
             
             <div class="form-group">
           
                <label >Vehicles Type</label>
                
                <select name="vehicle_id" id="vehicle_id" class="form-control" >
                  <?php 
                
                    foreach($vehicles as $key => $value){
                     //echo $value->id;
                     echo "<option value='".$value->id."'>".$value->name."</option>";
                    }
                  ?>
                 </select>
              </div>
              <div class="form-group">
                <label >Vehicle Model</label>
                <input type="text" class="form-control" name="vehicle_model" id="vehicle_model" required="required">
              </div>
              <div class="form-group">
                <label >Vehicle Plate Number</label>
                <input type="text" class="form-control" name="plate_number" id="plate_number" required="required">
              </div>
              
              <hr>

              <div class="form-group">
                <label >Identification Document</label>
                <input type="file" name="identification_document" class="form-control">
              </div>

             


        </div><!--  end div column 8 -->

        <!-- <div class="col-md-4">


        </div>  -->
        <!-- end div col md 4 -->

      </div>

      <div class="modal-footer">
        <input type="hidden" name="id" id="up_id">
        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
        <input class="btn btn-info" type="submit" value="Save">
      </div>
    </form>

    </div>
  </div>
</div>
  <!-- modal addnew-->

  <script type="text/javascript">
    $(document).ready(function() {
     $('#mobile_num').keyup(function(){
          $(this).val($(this).val().match(/\d+/g).join("").replace(/(\d{4})\-?(\d{3})\-?(\d{4})/,'$1-$2-$3'))
     });

      $('.add-new').on('click', function(){
        $('#addnewModal').modal()
      })

      $('.edit-row').on('click', function(){

       // assigned_location'=>$value->assigned_location, 'allLocation'

        let id = $(this).data('payload').id
        let full_name = $(this).data('payload').full_name
        let email = $(this).data('payload').email
        let birthdate = $(this).data('payload').birthdate
        let location = $(this).data('payload').location
        let mobile_num = $(this).data('payload').mobile_num
        let image = $(this).data('payload').image
        let identification_document = $(this).data('payload').identification_document
        let booking_available_until = $(this).data('payload').booking_available_until
        let identification_document_name = $(this).data('payload').identification_document_name
        let is_admin_verified = $(this).data('payload').is_admin_verified
        let is_email_verified = $(this).data('payload').is_email_verified
        let allLocation = $(this).data('payload').allLocation
        let assigned_location = $(this).data('payload').assigned_location

        $.each(allLocation, function(i,val){
         // console.log(vald+","+val.province)
          $('#up_assigmentArea').append(`<option value="${val.id}">
                                       ${val.barangay !=null ?val.barangay +",":""} ${val.city != null?val.city+",":""}  ${val.province}
                                  </option>`);
        })

        $("#up_assigmentArea option[value="+assigned_location+"]").prop("selected", "selected")

        let vehicles = $(this).data('vehicles')
        let unverified_vehicles = $(this).data('unverified_vehicles')
        let active_vehicle = $(this).data('active_vehicle')

        $('#vehicles').empty();
        for (var i = 0; i < vehicles.length; i++) {
          $('#vehicles').append("<li> <input type='text' value ='" +vehicles[i].vehicle_model  + "'  id='vehiclemodel' /> <input type='text' value ='" + vehicles[i].plate_number + "'  id='vehicleplatenum' /> <button data-vehicle_id='"+ vehicles[i].id + "' type='button' class='btn-update_vehicle btn btn-success btn-xs'> update vehicle</button></li>")
        }

        $('#unverified_vehicles').empty();
        for (var i = 0; i < unverified_vehicles.length; i++) {
          $('#unverified_vehicles').append("<li> <input type='text' value ='" +unverified_vehicles[i].vehicle_model  + "'  id='vehiclemodel' /> <input type='text' value ='" + unverified_vehicles[i].plate_number + "'  id='vehicleplatenum' /> <button data-vehicle_id='"+ unverified_vehicles[i].id + "' type='button' class='btn-verify_vehicle btn btn-success btn-xs'>Click to verify the vehicle</button></li>")
        }

        $('#active_vehicle').empty();
        $('#active_vehicle').append("<li style='font-weight:bold'>" + active_vehicle.vehicle_model + " — " + active_vehicle.plate_number + "</li>")
        // console.log(active_vehicle);

        $('#up_full_name').val(full_name)
        $('#up_email').val(email)

        $('#up_is_admin_verified').removeAttr('checked')
        if (parseInt(is_admin_verified)) {
          $('#up_is_admin_verified').prop('checked', true)
        }

        $('#up_is_email_verified').removeAttr('checked')
        if (parseInt(is_email_verified)) {
          $('#up_is_email_verified').prop('checked', true)
        }

        $('#up_birthdate').val(birthdate)
        $('#up_location').val(location)
        $('#up_mobile_num').val(mobile_num)

        $('#booking_available_until').empty();
        if (booking_available_until == null) {
          $('#booking_available_until').html('This rider currently doesn\'t have any registered promos')
        } else {
          $('#booking_available_until').html(booking_available_until)
        }
        console.log(identification_document);
        if (identification_document.includes('placeholder-logo')) {
          $('#identification_document').html('Empty')
        } else {
          $('#identification_document').html('<a target="_blank" href="' + identification_document + '">'+ identification_document_name +'</a>')
        }

        $('#up_id').val(id)
        $('#up_image').attr('src', image)
        $('.update-modal').modal()
      })

      $('html').on('click', '.btn-delete', function(e) {
        if (prompt('Are you sure you want to DELETE this? Type DELETE to continue') == 'DELETE') {
          invokeForm(base_url + 'cms/riders/delete', {id: $(this).data('id')});
        }
      })

      $('html').on('click', '.btn-verify_vehicle', function(e) {
        if (confirm('Are you sure you want to VERIFY this vehicle?')) {

           var plate_number = $(this).closest('li').find('#vehicleplatenum').val();
           var vehicleModel = $(this).closest('li ').find('#vehiclemodel').val();
          invokeForm(base_url + 'cms/riders/verify_vehicle', {id: $(this).data('vehicle_id'),plateNumber: plate_number,vehiclemodel:vehicleModel});
        }
      })

      $('html').on('click', '.btn-update_vehicle', function(e) {
        if (confirm('Are you sure you want to UPDATE this vehicle?')) {

           var plate_number = $(this).closest('li').find('#vehicleplatenum').val();
           var vehicleModel = $(this).closest('li ').find('#vehiclemodel').val();
          invokeForm(base_url + 'cms/riders/verify_vehicle', {id: $(this).data('vehicle_id'),plateNumber: plate_number,vehiclemodel:vehicleModel});
        }
      })

      $('html').on('click', '.btn-ban', function(e) {
        if (confirm('Are you sure you want to BAN this user?', 'Yes, BAN the user', 'No, go back')) {
          invokeForm(base_url + 'cms/riders/banning/ban', {id: $(this).data('id')});
        }
      })

      $('html').on('click', '.btn-unban', function(e) {
        if (confirm('Are you sure you want to UNBAN this user?', 'Yes, UNBAN the user', 'No, go back')) {
          invokeForm(base_url + 'cms/riders/banning/unban', {id: $(this).data('id')});
        }
      })

    });
  </script>

  <script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>
