<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-4">
        <section class="panel">
          <header class="panel-heading">
            Rider radius for <span style="font-weight:bold">Nearby Orders Notif</span> and <span style="font-weight:bold">Order Searching Radius Limit</span>
            <?php if ($flash_msg  && @$_GET['meta_key'] == 'rider_radius_in_m'): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
          </header>
          <div class="panel-body">
            
            <form action="<?php echo base_url('cms/dashboard/update_option') ?>" method="post">
              <input type="hidden" name="meta_key" value="rider_radius_in_m">
              <input type="hidden" name="from" value="cms/dashboard/rider_radius_editor">
              <label>
                Maximum radius range in Meters (M) 
              </label>
              <input class="form-control" type="number" step="1" name="meta_value" value="<?php echo $rider_radius_in_m ?>">
              <br>
              <input class="btn btn-info btn-md" type="submit" value="Update">
            </form>
            </div>
          </section>
        </div>
      </div>

      <!-- page end-->
    </section>
  </section>


<script type="text/javascript">
  $(document).ready(function() {


  });
</script>

<script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>
<script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>

