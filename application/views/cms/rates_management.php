<section id="main-content">
  <section class="wrapper">
    <!-- page start-->


           <?php 
                 $canedit = $accesslevel->can_edit;
             ?>

    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            Rates Management
            <?php if ($flash_msg && @$_GET['meta_key'] == 'rates_m'): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
          </header>
          <div class="panel-body">
            <!--tab nav start-->
            <section class="panel">
                <header class="panel-heading tab-bg-dark-navy-blue ">
                    <ul class="nav nav-tabs">
                       
                        <?php
                         $i=1;
              
                         foreach ($vehicles as $key => $value): ?>
                          <?php if($value->id ): ?>
                          <li class="<?php echo $i == 1? 'active': ''; $i++;?>">
                              <a data-toggle="tab" href="#<?php echo str_replace(' ', '_', strtolower($value->name)) ?>"><?php echo $value->name ?></a>
                          </li>
                          <?php  endif; ?>
                        <?php  endforeach; ?>
                    </ul>
                </header>
                <div class="panel-body">
                    <div class="tab-content">
                      <?php $i=1; foreach ($vehicles as $key => $vehicle): ?>
                        <div id="<?php echo str_replace(' ', '_', strtolower($vehicle->name)) ?>" class="tab-pane <?php echo $i == 1? 'active': ''; $i++;?>">
                          <h4><?php echo $vehicle->name ?> Rates</h4>

                          <form class="" action="<?php echo base_url('cms/dashboard/update_vehicle_availability') ?>" method="post">
                            <hr>
                            <label for="">ON
                              <input type="radio" name="is_active" value="1" <?php echo $vehicle->is_active == 1 ? 'checked':'' ?>>
                            </label> &nbsp;&nbsp;
                            <label for="">OFF
                              <input type="radio" name="is_active" value="0" <?php echo $vehicle->is_active == 0 ? 'checked':'' ?>>
                            </label><br><br>
                            <input type="hidden" name="meta_key" value="rates_m">
                            <input type="hidden" name="id" value="<?php echo $vehicle->id ?>">

                            <?php if($canedit == 1):  ?>
                             <input class="btn btn-info btn-md" type="submit" value="Update Vehicle Availability">
                            <?php endif; ?>
                            <br>
                            <hr>
                          </form>
                          
                           

                          <table class="table table-bordered">
                            <thead>
                              <th>Service name</th>
                              <th>Base fare</th>
                              <th>Per KM fare</th>
                              <th>Getapp Commission %</th>
                              <th>Surge Multiplier</th>
                              <th>Per Minute Fee</th>
                              <th></th>
                            </thead>
                          <?php foreach ($services as $key => $service): ?>
                            <!-- inner foreach -->
                            
                            <?php if ($service->id !=1 && $service->id != 6 && $vehicle->id !="11" ): ?>
                            <form action="<?php echo base_url('cms/dashboard/update_rate') ?>" method="post">
                                <tr>
                                  <td>
                                    <?php echo $service->label ?>
                                  </td>
                                  <td>
                                    <input type="hidden" name="meta_key" value="rates_m">
                                    <input type="hidden" name="vehicle_id" value="<?php echo $vehicle->id ?>">
                                    <input type="hidden" name="service_id" value="<?php echo $service->id ?>">
                                    <input type="number" step="0.01" class="form-control" name="base_fare" placeholder="base_fare" value="<?php echo $rates[$vehicle->id][$service->id]->base_fare ?>">
                                  </td>
                                  <td>
                                    <input type="number" step="0.01" class="form-control" name="per_km_fare" placeholder="per_km_fare" value="<?php echo $rates[$vehicle->id][$service->id]->per_km_fare ?>">
                                   
                                  </td>
                                  <td>
                                    <input type="number" step="0.01" class="form-control" name="getapp_commission_percentage" placeholder="getapp_commission_percentage" value="<?php echo $rates[$vehicle->id][$service->id]->getapp_commission_percentage ?>">
                                  </td>
                                  <td>
                                    <input type="number" step="0.01" class="form-control" name="surge_multiplier" placeholder="surge_multiplier" value="<?php echo $rates[$vehicle->id][$service->id]->surge_multiplier ?>">
                                  </td>
                                  <td>
                                    <input type="number" step="0.01" class="form-control" name="per_minute_fee" placeholder="per_minute_fee" value="<?php echo $rates[$vehicle->id][$service->id]->per_minute_fee ?>">
                                  </td>
                                  <td>
                                    <?php if($canedit == 1):  ?>
                                    <input class="btn btn-info btn-md" type="submit" value="Update Row">
                                  <?php endif; ?>
                                  </td>
                                </tr>
                            </form>
                          <?php endif; ?>
                            <!-- / inner foreach -->
                          <?php endforeach; ?>
                        </table>
                        </div>
                      <?php endforeach; ?>
                        <!-- <div id="about" class="tab-pane">About</div>
                        <div id="profile" class="tab-pane">Profile</div>
                        <div id="contact" class="tab-pane">Contact</div> -->
                    </div>
                </div>
            </section>


            </div>
          </section>
        </div>

          <div class="col-lg-4">
            <section class="panel">
              <header class="panel-heading">
                Get Pabili Additional Fee
                <?php if ($flash_msg && @$_GET['meta_key'] == 'pabili_fee_additional'): ?>
                  <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
                <?php endif; ?>
              </header>
              <div class="panel-body">
                <form action="<?php echo base_url('cms/dashboard/update_option') ?>" method="post">
                  <input type="hidden" name="meta_key" value="pabili_fee_additional">
                  <input type="hidden" name="from" value="cms/dashboard/rates_management">
                  <label>
                    PHP
                  </label>
                  <input class="form-control" type="number" step="0.01" name="meta_value" value="<?php echo $pabili_fee_additional ?>">
                  <br>
                  <?php if($canedit == 1):  ?>
                  <input class="btn btn-info btn-md" type="submit" value="Update fee">
                <?php endif; ?>
                </form>
                </div>
              </section>
            </div>

            <div class="col-lg-4">
            <section class="panel">
              <header class="panel-heading">
                Assisted Booking Charge Fee
                <?php if ($flash_msg && @$_GET['meta_key'] == 'assisted_booking_fee'): ?>
                  <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
                <?php endif; ?>
              </header>
              <div class="panel-body">
                <form action="<?php echo base_url('cms/dashboard/update_option') ?>" method="post">
                  <input type="hidden" name="meta_key" value="assisted_booking_fee">
                  <input type="hidden" name="from" value="cms/dashboard/rates_management">
                  <label>
                    PHP
                  </label>
                  <input class="form-control" type="number" step="0.01" name="meta_value" value="<?php echo $assisted_booking_fee ?>">
                  <br>
                  <?php if($canedit == 1):  ?>
                  <input class="btn btn-info btn-md" type="submit" value="Update fee">
                <?php endif; ?>
                </form>
                </div>
              </section>
            </div>

             <div class="col-lg-4">
            <section class="panel">
              <header class="panel-heading">
                Laundry Service Fee
                <?php if ($flash_msg && @$_GET['meta_key'] == 'laundry_service_fee'): ?>
                  <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
                <?php endif; ?>
              </header>
              <div class="panel-body">
                <form action="<?php echo base_url('cms/dashboard/update_option') ?>" method="post">
                  <input type="hidden" name="meta_key" value="laundry_service_fee">
                  <input type="hidden" name="from" value="cms/dashboard/rates_management">
                  <label>
                    PHP
                  </label>
                  <input class="form-control" type="number" step="0.01" name="meta_value" value="<?php echo $laundry_service_fee ?>">
                  <br>
                  <?php if($canedit == 1):  ?>
                  <input class="btn btn-info btn-md" type="submit" value="Update fee">
                <?php endif; ?>
                </form>
                </div>
              </section>
            </div>


      </div>




      <!-- page end-->
    </section>
  </section>


<script type="text/javascript">
  $(document).ready(function() {

   $('.table').find('td:contains("Get Car")').text("Get Ride");
  });
</script>

<script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>

