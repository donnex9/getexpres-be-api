<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-4">
        <section class="panel">
          <header class="panel-heading">
            Package Options
            <?php if ($flash_msg  && @$_GET['meta_key'] == 'package_options'): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
          </header>
          <div class="panel-body">
            <form action="<?php echo base_url('cms/dashboard/update_option') ?>" method="post">
              <input type="hidden" name="meta_key" value="package_options">
              <input type="hidden" name="from" value="cms/dashboard/delivery_management">
              <label>
                Package Options
                <br>
                <small>The allowed delivery types separated by comma and a space</small>
              </label>
              <input class="form-control" type="text" name="meta_value" value="<?php echo $package_options ?>">
              <br>
              <input class="btn btn-info btn-md" type="submit" value="Update Options">
            </form>
            </div>
          </section>
        </div>
      </div>




      <!-- page end-->
    </section>
  </section>


<script type="text/javascript">
  $(document).ready(function() {


  });
</script>

<script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>
