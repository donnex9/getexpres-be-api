<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Mosaddek">
  <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <!-- <link rel="shortcut icon" href="img/favicon.png"> -->

  <title>CMS Login</title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url('public/admin/') ?>css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url('public/admin/') ?>css/bootstrap-reset.css" rel="stylesheet">
  <!--external css-->
  <link href="<?php echo base_url('public/admin/') ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="<?php echo base_url('public/admin/') ?>css/style.css" rel="stylesheet">
  <link href="<?php echo base_url('public/admin/') ?>css/style-responsive.css" rel="stylesheet" />

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
  <!--[if lt IE 9]>
  <script src="<?php echo base_url('public/admin/') ?>js/html5shiv.js"></script>
  <script src="<?php echo base_url('public/admin/') ?>js/respond.min.js"></script>
  <![endif]-->
  <style media="screen">
  html {
    height: 100%;
  }
  body {
    margin: 0;
    padding: 0;
    height: 100%;
    background-color: #434343;
    background-image:linear-gradient(#434343, #282828);
  }
  #content{
    background-color: transparent;
    background-image:       linear-gradient(0deg, transparent 24%, rgba(255, 255, 255, .05) 25%, rgba(255, 255, 255, .05) 26%, transparent 27%, transparent 74%, rgba(255, 255, 255, .05) 75%, rgba(255, 255, 255, .05) 76%, transparent 77%, transparent), linear-gradient(90deg, transparent 24%, rgba(255, 255, 255, .05) 25%, rgba(255, 255, 255, .05) 26%, transparent 27%, transparent 74%, rgba(255, 255, 255, .05) 75%, rgba(255, 255, 255, .05) 76%, transparent 77%, transparent);
    height:100%;
    background-size:50px 50px;
  }

  .panel-heading .nav > li > a {
    color: dimgrey;
}


  .follow-me {
    position:absolute;
    bottom:10px;
    right:10px;
    text-decoration: none;
    color: #FFFFFF;
  }
  </style>
  <script type="text/javascript">
  const base_url = '<?php echo base_url(); ?>';
  </script>
</head>


<body class="login-body">
  <div class="container">

      <div class="login-wrap" style="margin: 0 auto;margin-top: 100px;
    width: 400px;">
<?php

$admin_mess = $this->session->login_msg;
$partner_mess = $this->session->login_msg_partner;
$both_miss = false;

if (!$admin_mess && !$partner_mess) {
  $both_miss = true;
}

?>
  <section class="panel">
      <header class="panel-heading tab-bg-dark-navy-blue" style="background:#F9F075">
              <h4 style="color:white;text-align: center;color:dimgrey"> <img style="max-width:50px" src="<?php echo base_url('public/admin/img/splashlogo.png') ?>"><br>Admin Login</h4><br>
          <ul class="nav nav-tabs">
              <li class="<?php echo ($both_miss || $partner_mess)? 'active':'' ?>">
                  <a data-toggle="tab" href="#home-2">
                      <i class="fa fa-user"></i>
                      Partner
                  </a>
              </li>
              <li class="<?php echo ($admin_mess)?'active': '' ?>">
                  <a data-toggle="tab" href="#about-2">
                      <i class="fa fa-user"></i>
                      Admin
                  </a>
              </li>
              <li class="">
                  <a data-toggle="tab" href="#forgot">
                      <i class="fa fa-key"></i>
                      Forgot Password
                  </a>
              </li>
          </ul>
      </header>
      <div class="panel-body">
          <div class="tab-content">
            <div id="home-2" class="tab-pane <?php echo ($both_miss || $partner_mess)? 'active':'' ?>">

              <!-- PARTNER LOGIN -->
                   <form class="form-signin" method="post" action="<?php echo base_url('cms/login/attempt_partner') ?>" style="margin-top:0px">
                      <input type="text" name="email" class="form-control" placeholder="Email" autofocus>
                      <input type="password" name="password" class="form-control" placeholder="Password">
                      <?php if ($partner_mess): ?>
                        <p style="color: <?php echo $partner_mess['color'] ?>"><?php echo $partner_mess['message'] ?></p>
                      <?php endif; ?>
                      <button style="background: dimgray; box-shadow: 0px 4px #908f8f;background:#F9F075;color:dimgrey;font-weight:bold"
                      class="btn btn-lg btn-login btn-block" type="submit">Partner Sign in</button>
                    </form>
              <!-- PARTNER LOGIN -->

            </div>
            <div id="about-2" class="tab-pane <?php echo ($admin_mess)?'active': '' ?>">

              <!-- ADMIN LOGIN -->
                   <form class="form-signin" method="post" action="<?php echo base_url('cms/login/attempt') ?>" style="margin-top:0px">
                      <input type="text" name="email" class="form-control" placeholder="Email" autofocus>
                      <input type="password" name="password" class="form-control" placeholder="Password">
                      <?php if ($admin_mess): ?>
                        <p style="color: <?php echo $admin_mess['color'] ?>"><?php echo $admin_mess['message'] ?></p>
                      <?php endif; ?>
                      <button style="background: dimgray; box-shadow: 0px 4px #908f8f;background:#F9F075;color:dimgrey;font-weight:bold"
                      class="btn btn-lg btn-login btn-block" type="submit">Admin Sign in</button>
                    </form>
              <!-- ADMIN LOGIN -->
             </div>
            <div id="forgot" class="tab-pane">

              <!-- ADMIN LOGIN -->
                   <form class="form-signin" method="post" action="<?php echo base_url('cms/login/forgot_password') ?>" style="margin-top:0px">
                      <input type="text" name="email" class="form-control" placeholder="Email" autofocus required>
                      <p style="color: grey; font-size: 15px;">
                        I am a
                        <label>
                          <input type="radio" name="type" value="admin" required> Administrator
                        </label>
                        <label>
                          <input type="radio" name="type" value="partners" required> Partner
                        </label>
                      </p>
                      <button style="background: dimgray; box-shadow: 0px 4px #908f8f; background:#F9F075;color:dimgrey;font-weight:bold"
                      class="btn btn-lg btn-login btn-block" type="submit">Reset Password</button>
                    </form>
              <!-- ADMIN LOGIN -->
             </div>
          </div>
      </div>
  </section>

</div>


<!-- <body class="login-body">
  <div class="container">
    <form class="form-signin" method="post" action="<?php echo base_url('cms/login/attempt') ?>">
      <h2 class="form-signin-heading" style="background: dimgray;"> CMS LOGIN -->
        <!-- <img src="" alt=""
        style="max-height:80px;"> -->
      <!-- </h2>
      <div class="login-wrap">
        <input type="text" name="email" class="form-control" placeholder="Email" autofocus>
        <input type="password" name="password" class="form-control" placeholder="Password">
        <?php if ($login_msg = $this->session->login_msg): ?>
          <p style="color: <?php echo $login_msg['color'] ?>"><?php echo $login_msg['message'] ?></p>
        <?php endif; ?>
        <button style="background: dimgray; box-shadow: 0px 4px #908f8f"
        class="btn btn-lg btn-login btn-block" type="submit">Sign in</button>
      </div>
    </form>
  </div> -->
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="<?php echo base_url('public/admin/') ?>js/jquery.js"></script>
  <script src="<?php echo base_url('public/admin/') ?>js/bootstrap.min.js"></script>
  <!-- <script src="<?php echo base_url('public/admin/js/custom/') ?>login.js"></script> -->

</body>
</html>
