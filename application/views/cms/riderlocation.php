<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            Riders Location

            <input hidden="true" type="text" id="base_url" value="<?php echo base_url();?>">
             <input hidden="true" type="text" id="al" value="<?php echo $assigned_location;?>">
            
          </header>
          <div class="panel-body">
            <!--tab nav start-->

            <div id="ridermap">
              
                <?php echo $map["js"] ?>
              <?php echo $map["html"] ?>
            </div>
           
            </div>
          </section>
        </div>
 
      </div>

      <!-- page end-->
    </section>
  </section>

  




