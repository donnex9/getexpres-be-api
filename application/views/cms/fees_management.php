<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-4">
        <section class="panel">
          <header class="panel-heading">
            Get Food
            <?php if ($flash_msg && @$_GET['meta_key'] == 'food_fee'): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
          </header>
          <div class="panel-body">
            <form action="<?php echo base_url('cms/dashboard/update_option') ?>" method="post">
              <input type="hidden" name="meta_key" value="food_fee">
              <input type="hidden" name="from" value="cms/dashboard/fees_management">
              <label>
                PHP
              </label>
              <input class="form-control" type="number" step="0.01" name="meta_value" value="<?php echo $food_fee ?>">
              <br>
              <input class="btn btn-info btn-md" type="submit" value="Update fee">
            </form>
            </div>
          </section>
        </div>
      <div class="col-lg-4">
        <section class="panel">
          <header class="panel-heading">
            Get Food per KM
            <?php if ($flash_msg && @$_GET['meta_key'] == 'food_per_km'): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
          </header>
          <div class="panel-body">
            <form action="<?php echo base_url('cms/dashboard/update_option') ?>" method="post">
              <input type="hidden" name="meta_key" value="food_per_km">
              <input type="hidden" name="from" value="cms/dashboard/fees_management">
              <label>
                PHP
              </label>
              <input class="form-control" type="number" step="0.01" name="meta_value" value="<?php echo $food_per_km ?>">
              <br>
              <input class="btn btn-info btn-md" type="submit" value="Update fee">
            </form>
            </div>
          </section>
        </div>
      </div>
    <div class="row">
      <div class="col-lg-4">
        <section class="panel">
          <header class="panel-heading">
            Get Grocery
            <?php if ($flash_msg && @$_GET['meta_key'] == 'grocery_fee'): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
          </header>
          <div class="panel-body">
            <form action="<?php echo base_url('cms/dashboard/update_option') ?>" method="post">
              <input type="hidden" name="meta_key" value="grocery_fee">
              <input type="hidden" name="from" value="cms/dashboard/fees_management">
              <label>
                PHP
              </label>
              <input class="form-control" type="number" step="0.01" name="meta_value" value="<?php echo $grocery_fee ?>">
              <br>
              <input class="btn btn-info btn-md" type="submit" value="Update fee">
            </form>
            </div>
          </section>
        </div>
      <div class="col-lg-4">
        <section class="panel">
          <header class="panel-heading">
            Get Grocery per KM
            <?php if ($flash_msg && @$_GET['meta_key'] == 'grocery_per_km'): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
          </header>
          <div class="panel-body">
            <form action="<?php echo base_url('cms/dashboard/update_option') ?>" method="post">
              <input type="hidden" name="meta_key" value="grocery_per_km">
              <input type="hidden" name="from" value="cms/dashboard/fees_management">
              <label>
                PHP
              </label>
              <input class="form-control" type="number" step="0.01" name="meta_value" value="<?php echo $grocery_per_km ?>">
              <br>
              <input class="btn btn-info btn-md" type="submit" value="Update fee">
            </form>
            </div>
          </section>
        </div>
      </div>
    <div class="row">
      <div class="col-lg-4">
        <section class="panel">
          <header class="panel-heading">
            Get Pabili
            <?php if ($flash_msg && @$_GET['meta_key'] == 'pabili_fee'): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
          </header>
          <div class="panel-body">
            <form action="<?php echo base_url('cms/dashboard/update_option') ?>" method="post">
              <input type="hidden" name="meta_key" value="pabili_fee">
              <input type="hidden" name="from" value="cms/dashboard/fees_management">
              <label>
                PHP
              </label>
              <input class="form-control" type="number" step="0.01" name="meta_value" value="<?php echo $pabili_fee ?>">
              <br>
              <input class="btn btn-info btn-md" type="submit" value="Update fee">
            </form>
            </div>
          </section>
        </div>
        <div class="col-lg-4">
          <section class="panel">
            <header class="panel-heading">
              Get Pabili per KM
              <?php if ($flash_msg && @$_GET['meta_key'] == 'pabili_per_km'): ?>
                <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
              <?php endif; ?>
            </header>
            <div class="panel-body">
              <form action="<?php echo base_url('cms/dashboard/update_option') ?>" method="post">
                <input type="hidden" name="meta_key" value="pabili_per_km">
                <input type="hidden" name="from" value="cms/dashboard/fees_management">
                <label>
                  PHP
                </label>
                <input class="form-control" type="number" step="0.01" name="meta_value" value="<?php echo $pabili_per_km ?>">
                <br>
                <input class="btn btn-info btn-md" type="submit" value="Update fee">
              </form>
              </div>
            </section>
          </div>
      </div>






      <!-- page end-->
    </section>
  </section>


<script type="text/javascript">
  $(document).ready(function() {


  });
</script>

<script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>
