<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/admin/') ?>assets/jquery-multi-select/css/multi-select.css" />
<style >
  .ms-container{
    width:100%;
  }
</style>
<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading" style="padding:15px">
            Inventory
            <?php if ($flash_msg = $this->session->flash_msg): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
            <div style="float:right">
              <form method="GET">
                <input type="text" name="squery" value="<?php echo @$_GET['squery'] ?>" placeholder="Search Product name or Add-ons">
                <input type="submit">
              </form>
            </div>
          </header>
          <div class="panel-body">
            <p>
              <button type="button" class="add-new btn btn-success btn-sm">Add new</button>
            </p>
            <div class="table-responsive" style="overflow: hidden; outline: none;" tabindex="1">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Product name</th>
                    <th>Category</th>
                    <th>Base price</th>
                    <th>Product image</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if (count($res) > 0 ): ?>

                    <?php foreach ($res as $key => $value): ?>
                      <tr>
                        <th scope="row"><?php echo $startingPK++ ?></th>
                        <td><?php echo $value->product_name ?>
                          <?php if ($value->is_addon): ?>
                            <button type="button" class="btn btn-xs btn-success">addon</button>
                          <?php endif; ?>
                        </td>
                        <td><?php echo $value->category ?></td>
                        <td><?php echo number_format($value->base_price, 2) ?> PHP</td>
                        <td><img style="max-width:80px" src="<?php echo $value->image_f ?>"></td>
                        <td>
                          <button type="button"
                          data-payload='<?php echo json_encode(['id' => $value->id, 'in_stock' => $value->in_stock,'is_addon' => $value->is_addon, 'addon_children_ids' => unserialize($value->addon_children_ids) ?: [], 'description' => $value->description, 'product_name' => $value->product_name, 'category' => $value->category , 'base_price' => $value->base_price , 'image' => $value->image_f], JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE)?>'
                          class="edit-row btn btn-info btn-xs">Edit</button>
                          <!-- <button type="button" data-id='<?php echo $value->id; ?>'
                            class="btn btn-delete btn-danger btn-xs">Delete</button> -->
                            <?php if ($value->is_hidden): ?>
                              <button type="button" data-id='<?php echo $value->id; ?>'
                                class="btn btn-unhide btn-success btn-xs">Unhide</button>
                            <?php else: ?>
                              <button type="button" data-id='<?php echo $value->id; ?>'
                                class="btn btn-hide btn-warning btn-xs">Hide</button>
                            <?php endif; ?>
                          <button type="button" data-id='<?php echo $value->id; ?>'
                                class="btn btn-delete btn-danger btn-xs">delete</button>   
                          </td>
                        </tr>
                      <?php endforeach; ?>


                    <?php else: ?>
                      <tr>
                        <td colspan="6" style="text-align:center"><?= ($this->input->get('squery')) ? 'Empty Search Results':'Empty table data' ?></td>
                      </tr>
                    <?php endif; ?>
                  </tbody>
                </table>
              </div>

              <style>
              .active_lg {
                background: lightgray !important
              }
              </style>
              <ul class="pagination">
                <ul class='pagination'>
                  <?php $page = ($this->input->get('page')) ?: 1; ?>
                  <li><a href="<?php echo base_url('cms/inventory/') . "?page=1&squery=" . @$_GET['squery'];?>">&laquo;</a></li>

                  <!-- loop for desc -->
                  <?php for ($i = $page - 2; $i < ($page) ; $i++):
                    if ($i == -1 || $i == 0) {
                      continue;
                    }
                   ?>
                  <li><a href="<?php echo base_url('cms/inventory/') . "?page=" . $i . "&squery=" . @$_GET['squery'];?>"><?= $i ?></a></li>
                  <?php endfor; ?>
                  <!-- / loop for desc -->

                  <li><a href="<?php echo base_url('cms/inventory/') . "?page=" . $page . "&squery=" . @$_GET['squery'];?>"><?= $page ?></a></li>

                  <!-- loop for asc -->
                  <?php for ($i = $page + 1; $i < ($page + 3) ; $i++):
                  if ($i == $total_pages + 1 || $i == $total_pages + 2 || $total_pages == 0) {
                      continue;
                  }
                  ?>
                  <li><a href="<?php echo base_url('cms/inventory/') . "?page=" . $i . "&squery=" . @$_GET['squery'];?>"><?= $i ?></a></li>
                  <?php endfor; ?>
                  <!-- / loop for asc -->


                <li><a href="<?php echo base_url('cms/inventory/') . "?page=" . $total_pages . "&squery=" . @$_GET['squery'];?>">&raquo;</a></li>
                </ul>
              </ul>

            </div>
          </section>
        </div>
      </div>
      <!-- page end-->
    </section>
  </section>

  <!-- Modal -->
  <div class="modal fade add-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Manage</h4>
        </div>
        <div class="modal-body">

          <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('cms/inventory/add') ?>" autocomplete="off">
            <div class="form-group">
              <label >Product name</label>
              <input type="text" class="form-control" name="product_name"  required="required">
            </div>
            <div class="form-group">
              <label >Category</label>
              <input list="categories" type="text" class="form-control" name="category"  required="required">
              <datalist id="categories">
                <?php foreach ($unique_categories as $value): ?>
                  <option><?php echo $value ?></option>
                <?php endforeach; ?>
              </datalist>
            </div>
            <div class="form-group">
              <label >Base price (in PHP)</label>
              <input type="number" min="0" step="0.01" class="form-control" name="base_price"  required="required">
            </div>
            <div class="form-group">
              <label >Description</label>
              <textarea name="description" class="form-control" required="required"></textarea>
            </div>
            <div class="row">

              <div class="form-group col-md-6">
                <label >Product image</label>
                <input type="file" name="image" class="form-control">
              </div>
              <div class="form-group col-md-6">
                <label >In stock?</label><br>
                <input type="checkbox" name="in_stock" value="1" checked>
              </div>
              <div class="col-md-12">
                <hr>
                  *Assign addon items to the right. Leave empty if no add-on.
                  <br>
                  <br>

              </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <select multiple="multiple" class="multi-select" id="addon_children_ids" name="addon_children_ids[]">
                      <?php foreach ($parent_products as $key => $value): ?>
                        <option value="<?php echo $value->id ?>"><?php echo $value->product_name ?></option>
                      <?php endforeach; ?>
                    </select>
                </div>
            </div>
          </div>
          <input type="hidden" name="partner_id" value="<?php echo $this->session->id ?>">
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <input class="btn btn-info" type="submit" value="Save changes">
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- modal -->

  <!-- Modal -->
  <div class="modal fade update-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Manage</h4>
        </div>
        <div class="modal-body">

          <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('cms/inventory/update') ?>" autocomplete="off">
              <div class="form-group">
                <label >Product name</label>
                <input type="text" class="form-control" name="product_name" id="up_product_name"  required="required">
              </div>
              <div class="form-group">
                <label >Category</label>
                <input list="categories" type="text" class="form-control" id="up_category" name="category"  required="required">
                <datalist id="categories">
                  <?php foreach ($unique_categories as $value): ?>
                    <option><?php echo $value ?></option>
                  <?php endforeach; ?>
                </datalist>
              </div>
              <div class="form-group">
                <label >Base price (in PHP)</label>
                <input type="number" min="0" step="0.01" class="form-control" id="up_base_price" name="base_price"  required="required">
              </div>
              <div class="form-group">
                <label >Description</label>
                <textarea name="description" id="up_description" class="form-control" required="required"></textarea>
              </div>
              <div class="row">

                <div class="form-group col-md-6">
                  <label >Product image</label><br>
                  <img src="" alt="" id="up_image" style="max-width:150px"><br><br>
                  <input type="file" name="image"  class="form-control">
                </div>
                <div class="form-group col-md-6">
                  <label >In stock?</label><br>
                  <input type="checkbox" name="in_stock"  id="up_in_stock" value="1">
                </div>
                <div class="col-md-12">
                  <hr>
                    *Assign addon items to the right. Leave empty if no add-on.
                    <br>
                    <br>

                </div>
              </div>
              <div class="form-group row" id="addon_children_ids2_row">
                  <div class="col-md-12">
                      <select multiple="multiple" class="multi-select" id="addon_children_ids2" name="addon_children_ids[]">
                        <?php foreach ($parent_products as $key => $value): ?>
                          <option value="<?php echo $value->id ?>"><?php echo $value->product_name ?></option>
                        <?php endforeach; ?>
                      </select>
                  </div>
              </div>
            </div>
            <input type="hidden" name="partner_id" value="<?php echo $this->session->id ?>">
            <input type="hidden" name="id" id="up_id">
            <div class="modal-footer">
              <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
              <input class="btn btn-info" type="submit" value="Save changes">
            </div>
          </form>
      </div>
    </div>
  </div>
</div>
  <!-- modal -->

  <script type="text/javascript">
    $(document).ready(function() {
      $('.add-new').on('click', function(){
        $('.add-modal').modal()
      })

      $('.edit-row').on('click', function(){
        let id = $(this).data('payload').id
        let product_name = $(this).data('payload').product_name
        let description = $(this).data('payload').description
        let category = $(this).data('payload').category
        let is_addon = $(this).data('payload').is_addon
        let base_price = $(this).data('payload').base_price
        let in_stock = $(this).data('payload').in_stock
        let image = $(this).data('payload').image
        let addon_children_ids = $(this).data('payload').addon_children_ids
        console.log(is_addon);
        $('#up_product_name').val(product_name)

        $('#up_in_stock').prop('checked', in_stock == 1);
        $('#up_description').val(description)
        $('#up_base_price').val(base_price)
        $('#up_category').val(category)
        $('#up_id').val(id)
        $('#addon_children_ids2').val(addon_children_ids)
        $('#addon_children_ids2').multiSelect('refresh')

        $('#up_image').attr('src', image)
        $('.update-modal').modal()
      })

       $('html').on('click', '.btn-delete', function(e) {
         if (confirm('Are you sure you want to delete this?', 'Yes, delete it', 'No, go back')) {
           invokeForm(base_url + 'cms/inventory/delete', {id: $(this).data('id')});
         }
       })

      $('html').on('click', '.btn-hide', function(e) {
        if (confirm('Are you sure you want to hide this?', 'Yes, hide it', 'No, go back')) {
          invokeForm(base_url + 'cms/inventory/hide', {id: $(this).data('id')});
        }
      })

      $('html').on('click', '.btn-unhide', function(e) {
        if (confirm('Are you sure you want to unhide this?', 'Yes, unhide it', 'No, go back')) {
          invokeForm(base_url + 'cms/inventory/unhide', {id: $(this).data('id')});
        }
      })

      $('#addon_children_ids, #addon_children_ids2').multiSelect({
          selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='Search Product name or Addons...'>",
          selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='Search Product name or Addons...'>",
          afterInit: function (ms) {
              var that = this,
                  $selectableSearch = that.$selectableUl.prev(),
                  $selectionSearch = that.$selectionUl.prev(),
                  selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                  selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

              that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                  .on('keydown', function (e) {
                      if (e.which === 40) {
                          that.$selectableUl.focus();
                          return false;
                      }
                  });

              that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                  .on('keydown', function (e) {
                      if (e.which == 40) {
                          that.$selectionUl.focus();
                          return false;
                      }
                  });
          },
          afterSelect: function () {
              this.qs1.cache();
              this.qs2.cache();
          },
          afterDeselect: function () {
              this.qs1.cache();
              this.qs2.cache();
          }
      });

    });
  </script>

  <script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>
  <script type="text/javascript" src="<?php echo base_url('public/admin/') ?>assets/jquery-multi-select/js/jquery.multi-select.js"></script>
  <script type="text/javascript" src="<?php echo base_url('public/admin/') ?>assets/jquery-multi-select/js/jquery.quicksearch.js"></script>
