<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Mosaddek">
  <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <!-- <link rel="shortcut icon" href="img/favicon.png"> -->

  <title>Get Express</title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url('public/admin/'); ?>css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url('public/admin/'); ?>css/bootstrap-reset.css" rel="stylesheet">

  <!--external css-->
  <link href="<?php echo base_url('public/admin/'); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

  <!--right slidebar-->
  <link href="<?php echo base_url('public/admin/'); ?>css/slidebars.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="<?php echo base_url('public/admin/'); ?>css/style.css" rel="stylesheet">
  <link href="<?php echo base_url('public/admin/'); ?>css/style-responsive.css" rel="stylesheet" />
  <link rel="stylesheet" href="<?php echo base_url('public/admin/'); ?>assets/data-tables/DT_bootstrap.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/admin/'); ?>assets/bootstrap-timepicker/compiled/timepicker.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

 <link href="<?php echo base_url('public/admin/'); ?>css/toggleswitch.css" rel="stylesheet">

 <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
  <!--[if lt IE 9]>
  <script src="<?php echo base_url('public/admin/'); ?>js/html5shiv.js"></script>
  <script src="<?php echo base_url('public/admin/'); ?>js/respond.min.js"></script>
  <![endif]-->
  <script src="<?php echo base_url('public/admin/'); ?>js/jquery.js"></script>
  <script type="text/javascript" src="<?php echo base_url('public/admin/'); ?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
  <script type="text/javascript" src="<?php echo base_url('public/admin/js/axios/dist/') ?>axios.min.js"></script>
 <script type="text/javascript" src="<?php echo base_url('public/admin/'); ?>js/ordermonitoring.js"> </script>
  <script type="text/javascript">
    const base_url = '<?php echo base_url(); ?>';
  </script>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/admin/'); ?>css/inputsearcstyle.css">
  <link rel='stylesheet' type='text/css' href='https://api.tomtom.com/maps-sdk-for-web/cdn/plugins/SearchBox/3.1.3-public-preview.0/SearchBox.css'/>
    <script src="https://api.tomtom.com/maps-sdk-for-web/cdn/6.x/6.1.2-public-preview.15/services/services-web.min.js"></script>
    <script src="https://api.tomtom.com/maps-sdk-for-web/cdn/plugins/SearchBox/3.1.3-public-preview.0/SearchBox-web.js"></script>

    <link rel='stylesheet' type='text/css' href='https://api.tomtom.com/maps-sdk-for-web/cdn/plugins/SearchBox/3.1.3-public-preview.0/SearchBox.css'>

    <link rel='stylesheet' type='text/css' href='https://api.tomtom.com/maps-sdk-for-web/cdn/6.x/6.15.0/maps/maps.css'>
   <link rel="stylesheet" type="text/css" href="https://api.tomtom.com/maps-sdk-for-web/6.x/6.15.0//examples/pages/examples/assets/ui-library/index.css">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
  <style>


  /* Make the image fully responsive */
   .tableroute, th,td {
    border-collapse: collapse;
    margin: 0;
    z-index: 1;

   }
   [contenteditable] {
     outline: 0px solid transparent;padding: 0
   }
   .form-control:focus {
      border-color: inherit;
      -webkit-box-shadow: none;
      box-shadow: none;

      }
    #pickupAutocompleteResult{
      z-index:1;
      position: absolute;
      top: 200px

    }  
    .containerPickup{
      background: #e0e0d7;
      color: #3d3d3b;
      margin-bottom:2px;
      width: 580px
    }

    .map {
      background: blue
    width: 100%;
    z-index: 1
    }

    .sb-title {
    position: relative;
    font-family: Roboto, sans-serif;
    font-weight: 500;
    }
    .sb-title-icon {
    position: relative;
    
    }

  
    h2 {
    margin: 0;
    font-family: Roboto, sans-serif;
    color: white
    }
    
    #divrecipientInformation
    {
       background-color:#FFFFFF;
      display: none;
    }

    #divSenderInformation
    {
       background-color:#FFFFFF;
      display: none;
       text-align: left;
       margin:auto;
    }
    #divmodal{  
    background-color:#FFFFFF;
    position:fixed;
    width:30%;
    height:50%;
    left: 55%;
    top:20%;
    z-index:2000;
    margin: auto;
   display: none
   }
    #divOrderSummary{display: none}
    #divAssignaRider{display: none}
    #divDeliveryDetails{display: none;background: #FFFFFF}
    #divPabili{display: none;background: #FFFFFF}
    .asterisk{color: red}
    .servicesicon
    {
      width:50px;
      height: 50px;
      border-radius: 5px

    }
    .divtotal
    {
      width: 100%;
      height: 50px;
      padding: 5px;
      
    }
    .divtotal label{
      float: right;
      font-size: 30px;
      text-align:center;

    }

    #loading{
    opacity:0.8;
    background-color:#ccc;
    position:fixed;
    width:100%;
    height:100%;
    top:0px;
    left:0px;
    z-index:2000;
    text-align: center
   }
   #bellicon{width: 30px; height:35px;margin-right: 10%}
   #bellicon2{width: 30px; height:35px;display: none;margin-right:10%}
   #bellbadge
   {
    position: absolute;top: 3px;left: 15px;
    background-color: #fccd4c;color: #0808c4;font-size: 10px}
   .badgebreak{background-color: #fccd4c;color: #0808c4;font-size: 9px} 
   .divpadding{padding:5px;}
   .bgcolor1{background-color: #f5edb0;border-radius: 5px}
   .bgcolor2{background-color: #f5f5f5;margin-bottom: 2px;border-radius: 5px}
   #ridermap{z-index: 5}
   #mapresult
   {
    position: fixed;
    top: 120px;
    left: 880px;
    width: 500px;
    height: auto;
    z-index: 5;
    border:1px solid black;
    padding-top: -20px
  }
  .map{width: 48%;height: 90%;}
  .img_24 {width: 20px;height: 20px}
  .grid-container {
  display: grid;
  grid-template-columns: auto auto auto auto;
  grid-gap: 10px;
  padding: 10px;
}

.grid-container > div {
  
  border: 1px solid black;
  text-align: center;
  font-size: 30px;
}

.card-native {
  border-radius: 10px;
   display: inline-block;
  box-shadow: 0 4px 8px 0 rgba(1,1,1,0.2);
  transition: 0.3s;
  width: 250px;
  left:10px;
  text-align:left;
  margin: 3px

}

.card-native:hover {
  box-shadow: 0 8px 16px 0 rgba(0,0,0,0.3);
}

.container-native {
  padding: 20px;
  
}

.container-naive ,p {
  padding-top: 20px;
  font-family: 'arial';
  font-size: 20px;
}

.customize-button{
  border-radius: 50px;
  display: inline;
  border:0.5px solid #d5dce6;
  width: 150px;
  text-align:center;
  padding: 5px;
}
.color-blue{color:#0991e6}
.color-red{color: #de1600}
.color-grey{color: #787b80}
.color-green{color: #20f719} 
.ripple {
  background-position: center;
  transition: background 0.8s;
}
.ripple:hover {
  background: #d7e6fc radial-gradient(circle, transparent 1%, #d7e6fc 1%) center/15000%;
}
.ripple:active {
  background-color: #6eb9f7;
  background-size: 100%;
  transition: background 0s;
}

.bg-blue{background-color: #06a6cf;color: white}
.btn-fr{float: right;}

.buttonload {
  background-color: #04AA6D; /* Green background */
  border: none; /* Remove borders */
  color: white; /* White text */
  padding: 12px 24px; /* Some padding */
  font-size: 16px; /* Set a font-size */
}

/* Add a right margin to each icon */
.fa {
  margin-left: -12px;
  margin-right: 8px;
}

  </style>

</head>

<body >
<input hidden="true" type="text" id="base_url" value="<?php echo base_url();?>">
