<section id="container" class="">
  <!--header start-->
  <header class="header white-bg">
    <div class="sidebar-toggle-box">
      <i class="fa fa-bars"></i>
    </div>
    <!--logo start-->
    <a href="#" class="logo" >GET<span>EXPRESS</span></a>
    <!--logo end-->

    <?php 
       $currentPageStr =  str_replace(base_url(), "", base_url(uri_string()));
       $currentPage = str_replace("/", "-", $currentPageStr);
    ?>

    <div class="top-nav " >
      <ul class="nav pull-right top-menu">
         <?php if ($this->session->role == 'administrator'){ ?>
     <li>
      <img data-toggle="tooltip" data-placement="right" title="Active Booking" id="bellicon"src="<?php echo base_url('public/admin/assets/svg/') ?>bell-icon.png">
      <img data-toggle="tooltip" data-placement="right" title="Active Booking" id="bellicon2"src="<?php echo base_url('public/admin/assets/svg/') ?>bell-icon.png">
      <span id="bellbadge" class="badge">0</span>
    </li>
    <li>&nbsp;&nbsp;</li>
        <li class="dropdown">
          <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <!-- <img alt="" src="img/avatar1_small.jpg"> -->
            <span class="username">Selected Location: 
              <?php 
                    $areaAssigned = $this->session->userdata('areaAssigned');
                     $areaArr = [];
                     if($areaAssigned !="" || $areaAssigned != null){
                        $areaArr = explode(",",$areaAssigned);
                     } 
                     $allLocation = $this->session->userdata('allLocation');
                     $selectedloc =  $this->session->userdata('location'); 
                     $useraccess =  $this->session->userdata('useraccess'); 
                     $userlevel =  $this->session->userdata('userlevel'); 
                    
                // print_r($useraccess);
                //echo $userlevel;
                      foreach ($allLocation as $key => $value) 
                      {
                            if($value["id"] == $selectedloc)
                            {
                               $city =  $value["city"] != null ? $value["city"].', ':"";
                               $brgy =  $value["barangay"] != null ? $value["barangay"].', ':"";
                               
                               echo $brgy .$city. $value["province"];
                            }
                      }

                ?><span class='badge badgebreak' id='areabadge<?php echo $selectedloc ?>'>0</span></span>
            <b class="caret"></b>
          </a>

         <?php if($userlevel ==1):?> 
          <ul class="dropdown-menu ">

            <?php  
              $arr = [];
              foreach ($allLocation as $key => $value) {
               
                if($value["id"] != $selectedloc)
                {
                  $city =  $value["city"] != null ? $value["city"].', ':"";
                               $brgy =  $value["barangay"] != null ? $value["barangay"].', ':"";

                   $placeValue =  $brgy .$city. $value["province"];            

                  $arr = ["id"=>$value["id"],"currentPage"=>$currentPage];

                   echo '<li><a class="dropdown-item" href="'.base_url('cms/login/updatesession/'.$value["id"].'/'.$currentPage).'">'.$placeValue.'&nbsp;<span class="badge badgebreak" id="areabadge'.$value["id"].'">0</span></a></li>';
                }
                
              }

            ?>
          </ul>
          <?php elseif($userlevel !=1 && count($areaArr) > 0):?> 
             <ul class="dropdown-menu ">

            <?php  
              $arr = [];
              foreach ($allLocation as $key => $value) 
              {
                foreach ($areaArr as  $val) {

                if($value["id"] != $selectedloc && $value["id"] == $val )
                {
                  $city =  $value["city"] != null ? $value["city"].', ':"";
                               $brgy =  $value["barangay"] != null ? $value["barangay"].', ':"";

                   $placeValue =  $brgy .$city. $value["province"];            

                  $arr = ["id"=>$value["id"],"currentPage"=>$currentPage];

                   echo '<li><a class="dropdown-item" href="'.base_url('cms/login/updatesession/'.$value["id"].'/'.$currentPage).'">'.$placeValue.'&nbsp;<span class="badge badgebreak" id="areabadge'.$value["id"].'">0</span></a></li>';
                } 
              }
            }

            ?>
          </ul>
           <?php endif;?> 
        </li>

         <?php } ?>



        <li class="dropdown">
          <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <!-- <img alt="" src="img/avatar1_small.jpg"> -->
            <span class="username">Welcome back, <?php echo $this->session->userdata('name'); ?></span>
            <b class="caret"></b>
          </a>
          <ul class="dropdown-menu extended logout">
            <li><a href="<?php echo base_url('cms/login/logout') ?>"><i class="fa fa-key"></i> Log Out</a></li>
          </ul>
        </li>

      </ul>
    </div>

    <audio id="audioNotif">
        <source src="<?php echo base_url('public/admin/assets/audio/') ?>cartoon-laugh.wav"></source>
    </audio>
    

  </header>
  <!--header end-->
  <!--sidebar start-->
  <aside>
    <div id="sidebar"  class="nav-collapse ">
      <!-- sidebar menu start-->
      <ul class="sidebar-menu" id="nav-accordion">
      <?php if ($this->session->role == 'partner'): ?>
        <li>
          <a href="<?php echo base_url('cms/partners/profile') ?>"
            class="<?php echo $this->uri->segment(1) === 'cms' && ($this->uri->segment(3) === 'profile') ? 'active': ''; ?>">
            <i class="fa fa-user"></i>
            <span>Partner Profile</span>
          </a>
        </li>
        <hr>
        <li>
          <a href="<?php echo base_url('cms/inventory') ?>"
            class="<?php echo $this->uri->segment(1) === 'cms' && ($this->uri->segment(2) === 'inventory') ? 'active': ''; ?>">
            <i class="fa fa-shopping-cart"></i>
            <span>Partner Inventory</span>
          </a>
        </li>
        <?php if ($this->session->hasbackendbooking == 'Y'): ?>
        <li>
          <a href="<?php echo base_url('cms/booking/partnerbooking') ?>"
            class="<?php echo  $this->uri->segment(3) === 'partnerbooking' ? 'active': ''; ?>">
            <i class="fa fa-motorcycle" ></i>
            <span>Place Delivery</span>
          </a>
        </li>

        <li>
          <a href="<?php echo base_url('cms/history/active_orders_partner') ?>"
            class="<?php echo  $this->uri->segment(3) === 'active_orders_partner' ? 'active': ''; ?>">
           <i class="fa fa-desktop" ></i>
            <span>Ongoing Booking</span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url('cms/history/all_orders_partner') ?>"
            class="<?php echo  $this->uri->segment(3) === 'all_orders_partner' ? 'active': ''; ?>">
            <i class="fa fa-area-chart"></i>
            <span>Booking History</span>
          </a>
        </li>
        <?php endif; ?>
      <?php endif; ?>
      <?php if ($this->session->role == 'administrator'): ?>
        <?php if(in_array(22,$useraccess) == 1):?>
          <li >
            <a href="<?php echo base_url('cms') ?>"
              class="<?php echo $this->uri->segment(1) === 'cms' && ($this->uri->segment(2) == 'dashboard' || $this->uri->segment(2) == null) && !@$this->uri->segment(3) ? 'active': ''; ?>">
              <i class="fa fa-list-alt"></i>
              <span>Admin Management</span>
            </a>
          </li>
         <?php endif;?>

         <?php if(in_array(23,$useraccess) == 1):?> 
        <li>
          <a href="<?php echo base_url('cms/dashboard/accessmanagement') ?>"
            class="<?php echo $this->uri->segment(1) === 'cms' && ($this->uri->segment(2) == 'dashboard') && $this->uri->segment(3) =='accessmanagement' ? 'active': ''; ?>">
            <i class="fa fa-lock"></i>
            <span>Access Management</span>
          </a>
        </li>
        <?php endif; ?>
        
        <?php if(in_array(24,$useraccess) == 1):?>
        <li>
          <a href="<?php echo base_url('cms/news') ?>"
            class="<?php echo $this->uri->segment(1) === 'cms' && $this->uri->segment(2) === 'news' ? 'active': ''; ?>">
            <i class="fa fa-dedent"></i>
            <span>News</span>
          </a>
        </li>
      <?php endif; ?>
      <?php if(in_array(1,$useraccess) == 1):?>
        <li>
          <a href="<?php echo base_url('cms/customers') ?>"
            class="<?php echo $this->uri->segment(1) === 'cms' && $this->uri->segment(2) === 'customers' ? 'active': ''; ?>">
            <i class="fa fa-users"></i>
            <span>Customers</span>
          </a>
        </li>
        <?php endif;?>

        <?php if(in_array(2,$useraccess) == 1):?>
        <li>
          <a href="<?php echo base_url('cms/riders') ?>"
            class="<?php echo $this->uri->segment(1) === 'cms' && $this->uri->segment(2) === 'riders' && $this->uri->segment(3) == ''? 'active': ''; ?>">
            <i class="fa fa-truck"></i>
            <span>Riders</span>
          </a>
        </li>
        <?php endif; ?>
        <?php if(in_array(3,$useraccess) == 1):?>
        <li>
          <a href="<?php echo base_url('cms/riders/add_rider_balance') ?>"
            class="<?php echo $this->uri->segment(1) === 'cms' && $this->uri->segment(2) === 'riders' && $this->uri->segment(3) == 'add_rider_balance'? 'active': ''; ?>">
            <i class="fa fa-plus"></i>
            <span>Add Rider Balance</span>
          </a>
        </li>
       <?php endif; ?>
       <?php if(in_array(4,$useraccess) == 1):?>
          <li>
            <a href="<?php echo base_url('cms/riderlocation/getriderslocation') ?>"
              class="<?php echo $this->uri->segment(1) === 'cms' && $this->uri->segment(2) === 'riders' && $this->uri->segment(3) == 'riderlocation'? 'active': ''; ?>">
              <i class="fa fa-map-marker"></i>
              <span>Riders Location</span>
            </a>
          </li>
        <?php endif; ?>
        <?php if(in_array(5,$useraccess) == 1):?>  
        <li>
          <a href="<?php echo base_url('cms/riders/riderTopup') ?>"
            class="<?php echo $this->uri->segment(1) === 'cms' && $this->uri->segment(2) === 'riders' && $this->uri->segment(3) === 'riderTopup' ? 'active': ''; ?>">
            <i class="fa fa-book"></i>
            <span>Rider Topup Transactions</span>
          </a>
        </li>
      <?php endif; ?>
      <?php if(in_array(6,$useraccess) == 1):?>
        <li>
          <a href="<?php echo base_url('cms/riders/transactions') ?>"
            class="<?php echo $this->uri->segment(1) === 'cms' && $this->uri->segment(2) === 'riders' && $this->uri->segment(3) === 'transactions' ? 'active': ''; ?>">
            <i class="fa fa-book"></i>
            <span>Rider Transactions</span>
          </a>
        </li>
      <?php endif; ?>  
        
     <?php if(in_array(7,$useraccess) == 1):?>   
        <li>
          <a href="<?php echo base_url('cms/booking/manualbooking') ?>"
            class="<?php echo  $this->uri->segment(3) === 'manualbooking' ? 'active': ''; ?>">
            <i class="fa fa-shopping-cart"></i>
            <span>Assisted Booking</span>
          </a>
        </li>
    <?php endif; ?>
    <?php if(in_array(8,$useraccess) == 1):?>
        <li>
          <a href="<?php echo base_url('cms/history/all_orders') ?>"
            class="<?php echo $this->uri->segment(1) === 'cms' && $this->uri->segment(2) === 'history' && $this->uri->segment(3) === 'all_orders' ? 'active': ''; ?>">
            <i class="fa fa-book"></i>
            <span>All Orders</span>
          </a>
        </li>
    <?php endif; ?>
    <?php if(in_array(8,$useraccess) == 1):?>    
        <li>
          <a href="<?php echo base_url('cms/history/active_orders') ?>"
            class="<?php echo $this->uri->segment(1) === 'cms' && $this->uri->segment(2) === 'history' && $this->uri->segment(3) === 'active_orders' ? 'active': ''; ?>">
            <i class="fa fa-book"></i>
            <span>Active Orders</span>
          </a>
        </li>
    <?php endif; ?>
    <?php if(in_array(8,$useraccess) == 1):?>    
        <li>
          <a href="<?php echo base_url('cms/history/cancelled_orders') ?>"
            class="<?php echo $this->uri->segment(1) === 'cms' && $this->uri->segment(2) === 'history' && $this->uri->segment(3) === 'cancelled_orders' ? 'active': ''; ?>">
            <i class="fa fa-times"></i>
            <span>Cancelled Orders</span>
          </a>
        </li>
    <?php endif; ?>
    <?php if(in_array(25,$useraccess) == 1):?>
        <li>
          <a href="<?php echo base_url('cms/partners') ?>"
            class="<?php echo $this->uri->segment(1) === 'cms' && $this->uri->segment(2) === 'partners' && $this->uri->segment(3) === null ? 'active': ''; ?>">
            <i class="fa fa-shopping-cart"></i>
            <span>Partners</span>
          </a>
        </li>

    <?php endif; ?>  
    <?php if(in_array(10,$useraccess) == 1):?>
        <li>
          <a href="<?php echo base_url('cms/partners/export_menu') ?>"
            class="<?php echo $this->uri->segment(1) === 'cms' && $this->uri->segment(3) === 'export_menu' ? 'active': ''; ?>">
            <i class="fa fa-exchange"></i>
            <span>Partners Reports</span>
          </a>
        </li>
    <?php endif; ?> 

   <!-- <?php// if(in_array(22,$useraccess) == 1):?>
        <li>
          <a href="<?php //echo base_url('cms/partners/invoices') ?>"
            class="<?php //echo $this->uri->segment(1) === 'cms' && $this->uri->segment(3) === 'invoices' ? 'active': ''; ?>">
            <i class="fa fa-exchange"></i>
            <span>Partners Invoices</span>
          </a>
        </li>
    <?php// endif; ?>  -->
       
    <?php if(in_array(11,$useraccess) == 1):?>    
        <li>
          <a href="<?php echo base_url('cms/dashboard/delivery_management') ?>"
            class="<?php echo $this->uri->segment(3) === 'delivery_management' ? 'active': ''; ?>">
            <i class="fa fa-truck"></i>
            <span>Get Delivery (Options)</span>
          </a>
        </li>
     <?php endif; ?>
     <?php if(in_array(12,$useraccess) == 1):?>   
        <li>
          <a href="<?php echo base_url('cms/dashboard/surge_fees') ?>"
            class="<?php echo $this->uri->segment(3) === 'surge_fees' ? 'active': ''; ?>">
            <i class="fa fa-umbrella"></i>
            <span>Surge Fees</span>
          </a>
        </li>
    <?php endif; ?>
    <?php if(in_array(13,$useraccess) == 1):?>    
        <li>
          <a href="<?php echo base_url('cms/dashboard/rates_management') ?>"
            class="<?php echo $this->uri->segment(3) === 'rates_management' ? 'active': ''; ?>">
            <i class="fa fa-dollar"></i>
            <span>Rates Management</span>
          </a>
        </li>
     <?php endif; ?>
     <?php if(in_array(14,$useraccess) == 1):?>   
        <li>
          <a href="<?php echo base_url('cms/dashboard/tricycle_rates_management') ?>"
            class="<?php echo $this->uri->segment(3) === 'tricycle_rates_management' ? 'active': ''; ?>">
            <i class="fa fa-dollar"></i>
            <span>Tricycle Rates Management</span>
          </a>
        </li>
      <?php endif; ?>
      <?php if(in_array(15,$useraccess) == 1):?>  
        <li>
          <a href="<?php echo base_url('cms/dashboard/vehicle_per_service') ?>"
            class="<?php echo $this->uri->segment(3) === 'vehicle_per_service' ? 'active': ''; ?>">
            <i class="fa fa-truck"></i>
            <span>Assign Vehicle per Service</span>
          </a>
        </li>
      <?php endif; ?>
      <?php if(in_array(16,$useraccess) == 1):?>  
        <li>
          <a href="<?php echo base_url('cms/dashboard/vehicles_management') ?>"
            class="<?php echo $this->uri->segment(3) === 'vehicles_management' ? 'active': ''; ?>">
            <i class="fa fa-truck"></i>
            <span>Get Delivery Vehicle Fees</span>
          </a>
        </li>
      <?php endif; ?>  
        <!-- <li class="sub-menu">

          <a href="javascript:;" class="<?php echo (in_array($this->uri->segment(3), ['fees_management', 'fare_management']))  ? 'active': ''; ?>">
            <i class="fa fa-tasks"></i>
            <span>Fees management</span>
          </a>
          <ul class="sub" >
            <li><a <?php echo $this->uri->segment(3) === 'fare_management' ? 'style="color:#ff6c60"': ''; ?> href="<?php echo base_url('cms/dashboard/fare_management') ?>"><i class="fa fa-dollar"></i>Base Fare/Per KM</a></li>
            <li><a <?php echo $this->uri->segment(3) === 'fees_management' ? 'style="color:#ff6c60"': ''; ?> href="<?php echo base_url('cms/dashboard/fees_management') ?>"><i class="fa fa-dollar"></i>Delivery Fee</a></li>
          </ul>
        </li> -->
        <?php if ($this->session->id == 1): ?>
          <!-- <li>
            <a href="<?php echo base_url('cms/dashboard/rider_wallet_management') ?>"
              class="<?php echo $this->uri->segment(3) === 'rider_wallet_management' ? 'active': ''; ?>">
              <i class="fa fa-money"></i>
              <span>Rider Wallet Rates/Offers</span>
            </a>
          </li> -->
        <?php endif; ?>
         <?php if(in_array(17,$useraccess) == 1):?>
        <li>
          <a href="<?php echo base_url('cms/dashboard/radius_editor') ?>"
            class="<?php echo $this->uri->segment(3) === 'radius_editor' ? 'active': ''; ?>">
            <i class="fa fa-dot-circle-o"></i>
            <span>Center Radius Editor</span>
          </a>
        </li>
      <?php endif; ?>
      <?php if(in_array(18,$useraccess) == 1):?>
        <li>
          <a href="<?php echo base_url('cms/dashboard/rider_radius_editor') ?>"
            class="<?php echo $this->uri->segment(3) === 'rider_radius_editor' ? 'active': ''; ?>">
            <i class="fa fa-dot-circle-o"></i>
            <span>Rider Radius Editor</span>
          </a>
        </li>
      <?php endif; ?>
     
      <?php if(in_array(19,$useraccess) == 1):?>  
        <hr>
        <li>
          <a href="<?php echo base_url('cms/voucher') ?>"
            class="<?php echo $this->uri->segment(2) === 'voucher' && $this->uri->segment(3) === null ? 'active': ''; ?>">
            <i class="fa fa-gift"></i>
            <span>Voucher Code</span>
          </a>
        </li>
      <?php endif; ?>
     <?php if(in_array(19,$useraccess) == 1):?>  
        
        <li>
          <a href="<?php echo base_url('cms/voucher/serial') ?>"
            class="<?php echo $this->uri->segment(3) === 'serial' ? 'active': ''; ?>">
            <i class="fa fa-barcode"></i>
            <span>Voucher Serial</span>
          </a>
        </li>
      <?php endif; ?>    

   
   <?php if(in_array(20,$useraccess) == 1):?>  
         <li>
          <a href="<?php echo base_url('cms/vouchersummary') ?>"
            class="<?php echo $this->uri->segment(3) === 'vouchersummary' ? 'active': ''; ?>">
            <i class="fa fa-list-alt"></i>
            <span>Voucher Summary</span>
          </a>
        </li>
       <?php endif; ?>
       <?php if(in_array(21,$useraccess) == 1):?> 
        <hr>
        <li>
          <a href="<?php echo base_url('cms/dashboard/maintenance_mode') ?>"
            class="<?php echo $this->uri->segment(3) === 'maintenance_mode' ? 'active': ''; ?>">
            <i class="fa fa-exclamation-triangle"></i>
            <span>MAINTENANCE MODE</span>
          </a>
        </li>
      <?php endif; ?>
      <?php endif; ?>
        <!-- <li class="sub-menu">

          <a href="javascript:;" class="<?php echo (in_array($this->uri->segment(2), ['news', 'events', 'about']))  ? 'active': ''; ?>">
            <i class="fa fa-tasks"></i>
            <span>Portal Management</span>
          </a>
          <ul class="sub" >
            <li><a <?php echo $this->uri->segment(2) === 'news' ? 'style="color:#ff6c60"': ''; ?> href="<?php echo base_url('admin/news') ?>">News</a></li>
            <li><a <?php echo $this->uri->segment(2) === 'events' ? 'style="color:#ff6c60"': ''; ?> href="<?php echo base_url('admin/events') ?>">Events</a></li>
            <li><a <?php echo $this->uri->segment(2) === 'about' ? 'style="color:#ff6c60"': ''; ?> href="<?php echo base_url('admin/about') ?>">About</a></li>
          </ul>
        </li> -->
      </ul>
      <!-- sidebar menu end-->
    </div>
  </aside>
  <!--sidebar end-->

