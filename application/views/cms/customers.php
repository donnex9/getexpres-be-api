<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading" style="padding:15px">
            Customers
            <?php if ($flash_msg = $this->session->flash_msg): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
            <div style="float:right">
              <form method="GET">
                <input type="text" name="squery" value="<?php echo @$_GET['squery'] ?>" placeholder="Search by full name">
                <input type="submit">
              </form>
            </div>
          </header>
          <div class="panel-body">
            <p>
              <!-- <button type="button" class="add-new btn btn-success btn-sm">Add new</button> -->
            </p>
            <div class="table-responsive" style="overflow: hidden; outline: none;" tabindex="1">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Full name</th>
                    <th>Email</th>
                    <th>Mobile number</th>
                    <th>Profile picture</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if (count($res) > 0 ): ?>

                    <?php foreach ($res as $key => $value): ?>
                      <tr>
                        <th scope="row"><?php echo $startingPK++ ?></th>
                        <td><?php echo $value->full_name ?></td>
                        <td><?php echo $value->email ?></td>
                        <td><?php echo $value->mobile_num ?></td>
                        <td><img style="max-width:150px" src="<?php echo $value->profile_picture_f ?>"></td>
                        <td>
                          <button type="button"
                          data-payload='<?php echo json_encode(['id' => $value->id, 'full_name' => $value->full_name, 'location' => $value->location, 'birthdate' => $value->birthdate,  'email' => $value->email , 'mobile_num' => $value->mobile_num, 'image' => $value->profile_picture_f], JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE)?>'
                          class="edit-row btn btn-info btn-xs">Edit</button>
                          <button type="button" data-id='<?php echo $value->id; ?>'
                            class="btn btn-delete btn-danger btn-xs">Delete</button>
                            <?php if ($value->banned_at): ?>
                              <button type="button" data-id='<?php echo $value->id; ?>'
                                class="btn btn-unban btn-success btn-xs">Unban</button>
                              </td>
                            <?php else: ?>
                              <button type="button" data-id='<?php echo $value->id; ?>'
                                class="btn btn-ban btn-danger btn-xs">Ban</button>
                            <?php endif; ?>
                          </td>
                        </tr>
                      <?php endforeach; ?>


                    <?php else: ?>
                      <tr>
                        <td colspan="6" style="text-align:center"><?= ($this->input->get('squery')) ? 'Empty Search Results':'Empty table data' ?></td>
                      </tr>
                    <?php endif; ?>
                  </tbody>
                </table>
              </div>

              <style>
              .active_lg {
                background: lightgray !important
              }
              </style>
              <ul class="pagination">
                <ul class='pagination'>
                  <?php $page = ($this->input->get('page')) ?: 1; ?>
                  <li><a href="<?php echo base_url('cms/customers/') . "?page=1&squery=" . @$_GET['squery'];?>">&laquo;</a></li>

                  <!-- loop for desc -->
                  <?php for ($i = $page - 2; $i < ($page) ; $i++):
                    if ($i == -1 || $i == 0) {
                      continue;
                    }
                   ?>
                  <li><a href="<?php echo base_url('cms/customers/') . "?page=" . $i . "&squery=" . @$_GET['squery'];?>"><?= $i ?></a></li>
                  <?php endfor; ?>
                  <!-- / loop for desc -->

                  <li><a href="<?php echo base_url('cms/customers/') . "?page=" . $page . "&squery=" . @$_GET['squery'];?>"><?= $page ?></a></li>

                  <!-- loop for asc -->
                  <?php for ($i = $page + 1; $i < ($page + 3) ; $i++):
                  if ($i == $total_pages + 1 || $i == $total_pages + 2 || $total_pages == 0) {
                      continue;
                  }
                  ?>
                  <li><a href="<?php echo base_url('cms/customers/') . "?page=" . $i . "&squery=" . @$_GET['squery'];?>"><?= $i ?></a></li>
                  <?php endfor; ?>
                  <!-- / loop for asc -->


                <li><a href="<?php echo base_url('cms/customers/') . "?page=" . $total_pages . "&squery=" . @$_GET['squery'];?>">&raquo;</a></li>
                </ul>
              </ul>

            </div>
          </section>
        </div>
      </div>
      <!-- page end-->
    </section>
  </section>

  <!-- Modal -->
  <!-- <div class="modal fade add-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Manage</h4>
        </div>
        <div class="modal-body">

          <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('cms/customers/add') ?>">
            <div class="form-group">
              <label >Title</label>
              <input type="text" class="form-control" name="title"  required="required">
            </div>
            <div class="form-group">
              <label >Category</label>
              <input type="text" class="form-control" name="category"  required="required">
            </div>
            <div class="form-group">
              <label >Body</label>
              <textarea name="body" class="form-control" required="required"></textarea>
            </div>
            <div class="form-group">
              <label >Banner Image</label>
              <input type="file" name="banner_image" class="form-control">
            </div>
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <input class="btn btn-info" type="submit" value="Save changes">
          </div>
        </form>
      </div>
    </div>
  </div> -->
  <!-- modal -->

  <!-- Modal -->
  <div class="modal fade update-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Manage</h4>
        </div>
        <div class="modal-body">

          <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('cms/customers/update') ?>">
            <div class="form-group">
              <label >Full name</label>
              <input type="text" class="form-control" name="full_name"  id="up_full_name" required="required">
            </div>
            <div class="form-group">
              <label >Email</label>
              <input type="email" class="form-control" name="email" id="up_email" required="required">
            </div>
            <div class="form-group">
              <label >Mobile number</label>
              <input type="text" class="form-control" name="mobile_num" id="up_mobile_num" required="required">
            </div>
            <div class="form-group">
              <label >Birth date</label>
              <input type="date" class="form-control" name="birthdate" id="up_birthdate" required="required">
            </div>
            <div class="form-group">
              <label >Location</label>
              <textarea name="location" class="form-control" id="up_location"></textarea>
            </div>
            <div class="form-group">
              <label >Profile picture</label>
              <br>
              <img src="" id="up_image" style="max-width:150px">
              <br>
              <br>
              <input type="file" name="profile_picture" class="form-control">
            </div>
          <div class="modal-footer">
            <input type="hidden" name="id" id="up_id">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <input class="btn btn-info" type="submit" value="Save changes">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
  <!-- modal -->

  <script type="text/javascript">
    $(document).ready(function() {
      $('.add-new').on('click', function(){
        $('.add-modal').modal()
      })

      $('.edit-row').on('click', function(){
        let id = $(this).data('payload').id
        let full_name = $(this).data('payload').full_name
        let email = $(this).data('payload').email
        let birthdate = $(this).data('payload').birthdate
        let location = $(this).data('payload').location
        let mobile_num = $(this).data('payload').mobile_num
        let image = $(this).data('payload').image

        $('#up_full_name').val(full_name)
        $('#up_email').val(email)
        $('#up_birthdate').val(birthdate)
        $('#up_location').val(location)
        $('#up_mobile_num').val(mobile_num)
        $('#up_id').val(id)
        $('#up_image').attr('src', image)
        $('.update-modal').modal()
      })

      $('html').on('click', '.btn-delete', function(e) {
        if (prompt('Are you sure you want to DELETE this? Type DELETE to continue') == 'DELETE') {
          invokeForm(base_url + 'cms/customers/delete', {id: $(this).data('id')});
        }
      })

      $('html').on('click', '.btn-ban', function(e) {
        if (confirm('Are you sure you want to BAN this user?', 'Yes, BAN the user', 'No, go back')) {
          invokeForm(base_url + 'cms/customers/banning/ban', {id: $(this).data('id')});
        }
      })

      $('html').on('click', '.btn-unban', function(e) {
        if (confirm('Are you sure you want to UNBAN this user?', 'Yes, UNBAN the user', 'No, go back')) {
          invokeForm(base_url + 'cms/customers/banning/unban', {id: $(this).data('id')});
        }
      })

    });
  </script>

  <script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>
