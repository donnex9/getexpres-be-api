
<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            Manual Booking

            <input hidden="true" type="text" id="base_url" value="<?php echo base_url();?>">
            <input hidden="true" type="text" id="al" value="<?php echo $assigned_location;?>">
            
          </header>
          <div class="panel-body">
            <!--tab nav start-->
            <section class="panel">
              <div class="w3-row">
                <div class="w3-col m6 w3-card divpadding">
                  <div>
                    <img class="sb-title-icon img_24" src="<?php echo base_url('public/admin/assets/svg/') ?>delivery-time.png" alt="">
                     <span class="sb-title">PICK UP TIME</span>

                    <!-- Split button -->
                  <div class="btn-group">
                    
                    <button type="button" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="caret"></span>
                      <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu">
                      <li id="pickUpAsap"><a>ASAP</a></li>
                      <li id="pickUpLater"><a >LATER</a></li>
                    </ul>

                  </div>
                  
                  <div class="row" id="divDatePicker" hidden="true">
                      <div class='col-sm-6'>
                         <div class="form-group">
                            <div class='input-group date' id='datetimepicker'>
                               <input type='text' id="dateTimeSched" class="form-control" />
                               <span class="input-group-addon">
                               <span class="glyphicon glyphicon-calendar"></span>
                               </span>
                            </div>
                         </div>

                      </div>
                   </div>

                  <input type="text" id="pickupTimeVal" hidden="true" value="0"/>
                  <br><br>
                  </div>
                  <div>
                    <img class="sb-title-icon" src="https://fonts.gstatic.com/s/i/googlematerialicons/location_pin/v5/24px.svg" alt="">
                  <span class="sb-title">ROUTE</span>
                  
                  <div class="w3-panel w3-round " style="background: #fafffe">
                    
                    <table class="w3-table tableroute">
                      <tr>
                        <td >
                          <div id="tdPickUpLocationdDiv" class="w3-input w3-animate-input" type="text"  />
                           <span id="tdPickUpLabel"></span>
                           <span hidden= "true"  id="tdPickUpLabelspan"></span>
                           <input type="text" hidden="true" id="tdPickUpLocation">
                           <input type="text" hidden="true" id="senderlatitude">
                           <input type="text" hidden="true" id="senderlongitude">
                        </td>
                        
                     </tr>
                     
                     <tr>
                       <td>
                         <!-- <input id="tdDestination" class="w3-input w3-animate-input" type="text"  placeholder="Destination"> -->
                         <div id="tdDestinationDiv" class="w3-input w3-animate-input" type="text"  />
                           <span id="tdDestinationLabel"></span>
                           <span hidden="true" id="tdDestinationLabelspan"></span>
                           <input type="text" hidden="true" id="tdDestination">
                           <input type="text" hidden="true" id="recipientlatitude">
                           <input type="text" hidden="true" id="recipientlongitude">
                        </td>
                     </tr>
                    </table>
                  </div>
                  </div>
                  
                <div id="customer">
                    <img class="sb-title-icon" src="<?php echo base_url('public/admin/assets/svg/') ?>people.svg" alt=""/>
                  <span class="sb-title">CUSTOMER'S NAME</span>
                  <div class="form-group">
                      <br>
                     
                      <select class="form-control" id="selectedcustomer">
                        <?php $cnt=0; foreach($customers as $key=>$value): ?>
                        
                        <option value="<?php echo $value->id?>">
                         <?php echo $value->full_name; $cnt++;?>
                        </option>

                        <?php endforeach; ?>
                        
                      </select>
                    <?php if($cnt <= 0): ?>
                      <span class="asterisk"> Please inform administrator to bind customer account.</span>
                    <?php endif; ?>  
                    <input type="text" id="cusmeraccount" hidden="true" value="<?php echo $cnt ?>">
                  </div>

                </div>  
                  
                <div id="services">
                   <img class="sb-title-icon" src="<?php echo base_url('public/admin/assets/svg/') ?>delivery_dining_black_24dp.svg" alt=""/>
                  <span class="sb-title">SERVICES</span>
                  <input type="text" hidden="true" id="selectedservices">
                 <br>  <br>
                 <div>
                      <div class="checkbox-inline " id="divdelivery">
                        <button id="btndelivery" class="btn btn-primary" data-toggle="tooltip" 
                         data-vehicles='<?php echo json_encode(["vehicles"=>$availableVehicles], JSON_HEX_QUOT|JSON_HEX_APOS) ?>'
                          data-placement="right" title="Get Delivery">
                            <img  class="servicesicon" src="<?php echo base_url('public/admin/assets/svg/') ?>delivery-icon.png" alt="Delivery"/>
                        </button>
                         
                      </div>
                      <div class="checkbox-inline" id="divpabili">
                          <button id="btnpabili" class="btn btn-primary" data-toggle="tooltip" 
                          data-vehicles='<?php echo json_encode(["vehicles"=>$availableVehicles], JSON_HEX_QUOT|JSON_HEX_APOS) ?>' data-placement="right" title="Get Pabili">
                            <img  class="servicesicon" src="<?php echo base_url('public/admin/assets/svg/') ?>pabili-icon.png" alt=""/>
                        </button>
                      </div>
                  </div>
                  <br>
                </div>

                <div id="vehicles">
                   <img class="sb-title-icon" src="<?php echo base_url('public/admin/assets/svg/') ?>two_wheeler_black_24dp.svg" alt=""/>
                  <span class="sb-title">VEHICLES</span>
                  <input type="text"  hidden="true" id="selectedvehicle" name="">
                  <br><br>
                  <div id="activeVehicles">
                      
                  </div>
                 </div>   
                <br><br>
                <div > 
                    <img class="sb-title-icon" src="<?php echo base_url('public/admin/assets/svg/') ?>distance-icon.png" alt=""/>
                  <span class="sb-title">DISTANCE COMPUTATION</span>
                  <br><br>
                  <div class="col-sm-12 bgcolor2">
                       <span class="pull-left" id="distance">Distance - 0 KM</span>
                  </div><br>
                  <div  class="col-sm-12 bgcolor2">
                    <span class="pull-left">Base Fare</span>
                    <label class="pull-right" id="basefare">₱0</label>
                  </div><br>

                  <div class="col-sm-12 bgcolor2" >
                    <span class="pull-left">KM charge</span>
                    <label class="pull-right" id="kmcharge">₱0</label>
                  </div><br><br>
                  <div class="col-sm-12 bgcolor2" >
                    <span class="pull-left">Admin fee</span>
                    <label class="pull-right" id="adminFee">₱0</label>
                  </div><br><br>
                  <div class="col-sm-12 bgcolor2" >
                    <button id="btnVoucher" class="btn btn-primary">Voucher</button>
                    <span class="pull-left" id="vouchercode"></span>
                    <label class="pull-right" id="voucheramount">₱0</label>
                    <input type="text" id="vouchercodetxt" hidden="true" >
                    <input type="text" id="voucherAmttxt" hidden="true" >
                  </div>
                  <br><br>

                  <div class="divtotal bgcolor1 col-sm-12" >
                    <input type="text" id="totaltxt" hidden="true" class="pull-left" name="">
                      <label  id="labeltotal">₱0</label>
                  </div>
                 
                 <br> <br><br>
                 <input type="text" id="cartidtxt" hidden="true" name="">
                  <div class="divtotal">
                      <button id="btnreview"  class="btn btn-info pull-right hidden" >Review Order</button>
                  </div>
               
                </div>
          
           </div>

                <div class="w3-col m6 divpadding">

                  <div id="map" class="map"></div>

                  
                </div>

            </section>

          
            </div>
          </section>
        </div>

      </div>

    <div class="modal fade" id="modalAlert" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><span id="headertitlemodal"></span></h4>
        </div>
        <div class="modal-body">
          <p id="contentitlemodal"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>


  <div class="modal fade" id="modalRider" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><span >Assign Rider</span></h4>
        </div>
        <div class="modal-body">
          <div class="w3-card-2 ">
                     
                      <form class="w3-container" >
                        <p>      
                         <label class="w3-text-brown"><b>Search Rider </b></label>
                        
                        <div class="input-group">
                            
                            <input id="searchrider" type="text" class="form-control" placeholder="Ex. Juan dela Cruz">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        </div><br>
                        <div id="riderresult">
                            
                        </div>
                        <br>
                        
                      </form>
                      
                </div>
        </div>
        <div class="modal-footer">
          
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

  <div class="modal fade" id="modalVoucher" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><span >Enter Voucher Code<span class="asterisk">*</span></span></h4>
        </div>
        <div class="modal-body">
          <p >
              <input type="text"  class="w3-input w3-animate-input" id="txtinputVoucherCode" placeholder="Enter Voucher Code....">
          </p>
          <p>
            <span id="voucherError" class="text-danger"></span>
          </p>
        </div>
        <div class="modal-footer">
           <button type="button" id="btnApply" class="btn btn-info">
             Apply 
           </button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

  <div class="modal fade" id="modalrderSummary" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><span >Order Summary</span></h4>
        </div>
        <div class="modal-body">
          <p >
              
                   <div class="w3-card-2 ">
                      
                      <div class="w3-container table-responsive" ><br>
                        <table class="table">

                          <tr>
                            <td>
                              Pick Up Time
                            </td>
                            <td>
                               <span id="spanPickUpTime">ASAP</span>
                            </td>
                          </tr>
                            
                            <tr>
                                <td>Pick Up Address</td>
                                <td>
                                    <input id="tdPickUpLocationSummary" class="w3-input w3-animate-input " type="text" readonly="true">
                           <span id="tdPickUpLabelSummary"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Destination Address</td>
                                <td>
                                  <input id="tdDestinationSummary" class="w3-input w3-animate-input" type="text"  readonly="true">
                           <span id="tdDestinationLabelSummary"></span>
                                </td>
                            </tr>

                            <tr>
                                <td>Customer's Name</td>
                                <td>
                                  <span id="customernameSummary"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Service Type</td>
                                <td>
                                  <span id="servicesSummary"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Vehicle</td>
                                <td>
                                  <span id="vehicleSummary"></span>
                                </td>
                            </tr>
                            <tr>
                                <td><span id="itemtitleSummary"></span></td>
                                <td>
                                  <span id="itemSummary"></span>
                                </td>
                            </tr>
                            <tr id="tresmitatedpabiliamount">
                                <td>Estimated Amount</td>
                                <td><span id="estimatedSummary"></span></td>
                            </tr>
                            <tr>
                                <td>Assigned Rider</td>
                                <td>
                                  <span id="assignedriderSummary"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Instruction to rider</td>
                                <td><span id="instructionSummary"></span></td>
                             </tr>
                             <tr>
                                <td><span id="smVoucherCode"></span></td>
                                <td><span id="smVoucherAmount"></span></td>
                             </tr>
                             <tr>
                                <td>Delivery Fee</td>
                                <td><span id="deliveryfeeSummary"></span></td>
                             </tr>
                        </table>
                           <div class="text-center"> ***************************************************</div>
                        <p>
                            <button id="btncheckout"  class="btn btn-info pull-right" >Check Out</button>
                        </p>
                        <br><br>
                        
                      </div>
                    </div>
                 
          </p>
          <p>
            <span id="voucherError" class="text-danger"></span>
          </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

 <div id="divmodal" >
   
   <div id="divSenderInformation">
                   <div class="w3-card-2 ">
                      <div class="w3-container w3-cyan ">
                        <h2>Sender  Information</h2>
                      </div>
                      <div class="w3-container" >
                        <p>      
                        <label class="w3-text-brown"><b>Name <span class="asterisk">*</span></b></label>
                        <input class="w3-input w3-border w3-sand" id="sendername" type="text">
                        <span id="sendernameerror" class="asterisk" ></span>
                       </p>
                        <p>      
                        <label class="w3-text-brown"><b>Contact Number<span class="asterisk">*</span></b></label>
                        <input class="w3-input w3-border w3-sand" id="sendercontact" type="text">
                        <span id="senderconerror" class="asterisk" ></span>
                       </p>
                        <p>      
                        <label class="w3-text-brown"><b>Block/Floor/Room</b></label>
                        <input class="w3-input w3-border w3-sand" id="senderblock" type="text"></p>
                        <p>
                            <button id="btnSenderSave" class="btn btn-primary" >Save</button>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div id="divrecipientInformation">
                   <div class="w3-card-2 ">
                      <div class="w3-container w3-cyan ">
                        <h2>Recipient  Information</h2>
                      </div>
                      <form class="w3-container" >
                        <p>      
                         <label class="w3-text-brown"><b>Name <span class="asterisk">*</span></b></label>
                        <input class="w3-input w3-border w3-sand" id="recipientname" type="text">
                        <span id="recipientnameerror" class="asterisk" ></span>
                       </p>
                        <p>      
                        <label class="w3-text-brown"><b>Contact Number<span class="asterisk">*</span></b></label>
                        <input class="w3-input w3-border w3-sand" id="recipientcontact" type="text">
                        <span id="recipientconerror" class="asterisk" ></span>
                        </p>
                        <p>      
                        <label class="w3-text-brown"><b>Block/Floor/Room</b></label>
                        <input class="w3-input w3-border w3-sand" id="recipientblock" type="text"></p>
                        <p>
                        <button  type="button" id="btnRecipientSave" class="btn btn-primary" >Save</button></p>
                      </form>
                    </div>
                  </div>


                   <div id="divDeliveryDetails">
                   <div class="w3-card-2 ">
                      <div class="w3-container w3-cyan ">
                        <h2>Delivery Details</h2>
                      </div>
                      <form class="w3-container" >
                        <p>      
                         <label class="w3-text-brown"><b>Package Options <span class="asterisk">*</span></b></label>
                        
                        <?php $arrpackageOptionns = explode(",",$package_options) ?>
                          <?php foreach ($arrpackageOptionns as $keyval):?> 

                          <div class="form-check">
                          <input class="form-check-input" type="radio" name="packageoption"  
                          value="<?php echo $keyval; ?>" >
                          <label class="form-check-label" >
                            <?php echo $keyval; ?>
                          </label>
                           </div>

                          <?php endforeach; ?>
                         
                        <p>      
                         <label class="w3-text-brown"><b>Delivery Instructions </b></label>
                         <div class="mb-3">
                          <textarea class="form-control" id="deliveryinstruction" rows="3"></textarea>
                        </div>

                        <span id="recipientnameerror" class="asterisk" ></span>
                       </p>
                       <p><button  type="button" id="btnDL" class="btn btn-primary" >Save</button></p>
                      </form>

                      
                    </div>
                  </div>


                  <div id="divPabili">
                   <div class="w3-card-2 ">
                      <div class="w3-container w3-cyan ">
                        <h2>Add Pabili Items</h2>
                      </div>
                      <div class="w3-container" >
                        <p>      
                         <label class="w3-text-brown"><b>Pabili Items <span class="asterisk">*</span> </b></label>
                        
                        <div class="input-group">
                            
                            <input id="pabilitxt" type="text" class="form-control" placeholder="Ex. Pork BBQ 10 pcs.">
                            <span id="btnaddpabili" class="input-group-addon"><i class="glyphicon glyphicon-plus"></i></span>
                        </div><br>
                        <div class="w3-container">
                         
                          <ul class="w3-ul w3-card-4" id="pabilItems">
                            
                            
                          </ul>
                        </div>
                        <br>
                        <p>      
                         <label class="w3-text-brown"><b>Estimated total amount <span class="asterisk">*</span></b></label>
                         <div class="mb-3">
                          <input class="form-control" id="estimatedamount" placeholder="2000" >
                        </div>

                       </p>
                        <p>      
                         <label class="w3-text-brown"><b>Pabili Instructions </b></label>
                         <div class="mb-3">
                          <textarea class="form-control" id="pabiliinstruction" rows="3"></textarea>
                        </div>

                       </p>

                       <p><button  type="button" id="btnPab" class="btn btn-primary" >Save</button></p>
                        
                      </div>
                    </div>
                    
                  </div>

            
 </div>

 <input type="text" id="minWallet" hidden="true" value="<?php echo $minimum_wallet; ?>">

 <div id="loading" style="display:none"><img src="<?php echo base_url('public/admin/assets/svg/') ?>loadingdots.gif"></div>



      <!-- page end-->
    </section>
  </section>


<script type="text/javascript">

  $(document).ready(function() {
    let pabiliItems = []
    let customerlen = $("#cusmeraccount").val()
    $('[data-toggle="tooltip"]').tooltip();
    $("#btnVoucher").attr('disabled',true)
    
    if($("#divSenderInformation").is(':visible')){ $("#sendername").focus();}
    if($("#divrecipientInformation").is(':visible')){ $("#recipientname").focus();}


    if(customerlen <=0)
    {
     $("#btndelivery").addClass('hidden');
     $("#btnpabili").addClass('hidden');
    }
    

    $("#sendername").on('keyup',function(){
        $(this).spanclear("sendernameerror")
    })

    $("#sendercontact").on('keyup',function(){
        $(this).spanclear("senderconerror")
    })

    $("#recipientname").on('keyup',function(){
        $(this).spanclear("recipientnameerror")
    })

    $("#recipientcontact").on('keyup',function(){
        $(this).spanclear("recipientconerror")
    })

    $("#selectedcustomer").on('change',function()
    {
        var optionSelected = $("option:selected", this);
        let name = optionSelected.text()
        $("#customernameSummary").html(name)

    })

    
    $("#btnDL").on('click',function()
    {
      $("#divDeliveryDetail").hide()
      $("#divmodal").hide()
      //$("#divPabili").hide()
    })

    $("#btnPab").on('click',function()
    {
      $("#divmodal").hide()
      $("#divPabili").hide()
    })
    
    $("#pickUpAsap").on('click',function()
    {
      $("#divDatePicker").hide()
      $("#pickupTimeVal").val("0")
    })

    $("#pickUpLater").on('click',function()
    {
      $("#divDatePicker").show()
      $("#pickupTimeVal").val("1")
    })

    $('#datetimepicker').datetimepicker()

    
    $("#btnRider").on('click',function()
    {
      $("#divmodal").hide()
      $("#divAssignaRider").hide()
    })

    $("#btnVoucher").on('click',function()
    {

      $("#modalVoucher").modal('show');
    })

    $("#btnApply").on('click',function()
    {
      let voucherCode = $("#txtinputVoucherCode").val();
      let cartid = $("#cartidtxt").val();
      let customerid = $("#selectedcustomer").val()

      let dataPost = {
                      "cart_id":cartid,
                      "customer_id":customerid,
                      "vouchercode":voucherCode,
                     } 
      if(voucherCode != null || voucherCode !="")
      {
        $(this).attr("disabled",true)
        $(this).html('Loading...')
        $(this).applyvoucherrequest("cms/booking/applyVoucher",dataPost);
      }else
      {
       $("#voucherError").html("This field is required.")
      }
      
    })

    $("#btnpabili").on('click',function(){

        let vehicleservice = $(this).data("vehicles").vehicles
        let allActiveVehicles = []
        let pickupLocation =  $(this).deliverlocation()
        let destinationLocation = $(this).destination()

        $("#selectedservices").val("3")
        $("#activeVehicles").html("")
        $("#deliveryinstruction").text("")
        $("#btnreview").addClass("hidden")
        $("#servicesSummary").html("")

        $("#btndelivery").removeClass("btn-warning")
        $("#btndelivery").addClass("btn-primary")
        $("#divDeliveryDetails").hide()
        $("#divmodal").show()
        $("#divAssignaRider").hide()
        $("#divOrderSummary").hide()
        
      if($.isNumeric(pickupLocation.length) == false && $.isNumeric(destinationLocation.length) == false){
        
            $(this).removeClass("btn-primary")
            $(this).addClass("btn-warning")
            $("#servicesSummary").html("Get Pabili")
            $("#divPabili").show()

            $.each(vehicleservice,function(i,val){
            if(val.service_id == 3)
            {
                allActiveVehicles.push(val.vehicle_id)

             $("#activeVehicles").append(`<div class="checkbox-inline "><button class="btn btn-primary" id="vehi${val.vehicle_id}" data-vehicleid ="${val.vehicle_id}"  class="btn btn-primary" data-vehiclename="${val.name}" data-toggle="tooltip" data-placement="right" title="${val.name}"><img src="${val.image}" class='servicesicon' ></button> </div>`)
            }  
          })

        }
        
        $('[data-toggle="tooltip"]').tooltip();
    })

    
 $('#pabilitxt').keyup(function(event){
        
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        //event.preventDefault();
         let pabili = $(this).val();
         if(pabili != ""){
            pabiliItems.push(pabili)
              $("#pabilItems").html("")

              $.each(pabiliItems, function(i,item){
                $("#pabilItems").append(`<li class="w3-display-container">${item} <span  class="w3-button w3-transparent w3-display-right" data-itemid="${i}">&times;</span></li>`);
              })
         }

         $("#pabilitxt").val("");
         $("#pabilitxt").focus();
    }
  });

    $("#btnaddpabili").on('click',function()
    {
     
      let pabili = $("#pabilitxt").val();
     if(pabili != ""){
        pabiliItems.push(pabili)
          $("#pabilItems").html("")

          $.each(pabiliItems, function(i,item){
            $("#pabilItems").append(`<li class="w3-display-container">${item} <span  class="w3-button w3-transparent w3-display-right" data-itemid="${i}">&times;</span></li>`);
          })
     }
      
      $("#pabilitxt").val("");
      $("#pabilitxt").focus();
    })

    $("#pabilItems").on("click","span", function(){
        let itemid =  $(this).attr("data-itemid");
      //  array.splice(pabiliItems, itemid);
        pabiliItems.splice(itemid,1);
        $("#pabilitxt").focus();
        $("#pabilItems").html("")
        $.each(pabiliItems, function(i,item){
        $("#pabilItems").append(`<li class="w3-display-container">${item} <span  class="w3-button w3-transparent w3-display-right" data-itemid="${i}">&times;</span></li>`);
      })
    })

    $("#btndelivery").on('click',function(){

        let vehicleservice = $(this).data("vehicles").vehicles
        let allActiveVehicles = []
        let pickupLocation =  $(this).deliverlocation()
        let destinationLocation = $(this).destination()

        $("#servicesSummary").html("")
        $("#selectedservices").val("4")
        $("#deliveryinstruction").val("")
        
        $("#activeVehicles").html("")
        $("#btnpabili").removeClass("btn-warning")
        $("#btnpabili").addClass("btn-primary")
        
        $("#divPabili").hide()
        $("#btnreview").addClass("hidden")
        $("#divAssignaRider").hide()
        $("#divOrderSummary").hide()


      if($.isNumeric(pickupLocation.length) == false && $.isNumeric(destinationLocation.length) == false){
        $("#divmodal").show()
        $("#divDeliveryDetails").show()
        $("#servicesSummary").html("Get Delivery")
        $(this).removeClass("btn-primary")
        $(this).addClass("btn-warning")

        $.each(vehicleservice,function(i,val){
            
            if(val.service_id == 4)
            {
                allActiveVehicles.push(val.vehicle_id)

             $("#activeVehicles").append(`<div class="checkbox-inline "><button class="btn btn-primary" id="vehi${val.vehicle_id}" data-vehicleid ="${val.vehicle_id}"  class="btn btn-primary" data-vehiclename="${val.name}" data-toggle="tooltip" data-placement="right" title="${val.name}"><img src="${val.image}" class='servicesicon' ></button> </div>`)
            }  
        })

        $('[data-toggle="tooltip"]').tooltip();

      }
        
    })

    $('#btncheckout').on('click', function() {
      let cartid = $("#cartidtxt").val();
      let customerid = $("#selectedcustomer").val()
      let rider  = $('input[name="rider"]:checked').val()?$('input[name="rider"]:checked').val():0;
      let total  = $.trim($("#totaltxt").val());
      let vcode  =  $("#vouchercodetxt").val()
      let vamount = $("#voucherAmttxt").val()
      let isScheduled   = $("#pickupTimeVal").val()
      let dateTimeSched = $("#dateTimeSched").val()
      let data = []

      if(isScheduled == 0){
        dateTimeSched = null
      }

      data = {'cartid':cartid,"riderid":rider,"total":total,"customer_id":customerid,
              'vcode':vcode,'vamount':vamount,'isScheduled':isScheduled,'dateTimeSched':dateTimeSched}
      $("#loading").show()
      //$(this).addClass('disabled')
     $(this).ajaxpostrequestbooknow("cms/booking/booknow",data)

    })

    $("#btnreview").on('click',function()
    {
        let pickUpSched = $("#pickupTimeVal").val();
       // console.log(pickUpSched)
        if (pickUpSched == 1) 
        {
           $("#spanPickUpTime").html($("#dateTimeSched").val());
        }else{
          $("#spanPickUpTime").html("ASAP");
        }
        
        let ridername  = $('input[name="rider"]:checked').data("name")?$('input[name="rider"]:checked').data("name"):"No rider assigned";
        $("#assignedriderSummary").html(ridername)
        $("#modalrderSummary").modal('show')
        $("#divDeliveryDetails").hide();
        $("#divAssignaRider").hide();
        $("#divPabili").hide()
        $(this).addClass("hidden");

    })
    
    $('#activeVehicles').on('click', 'button', function() {
       
       let btnid = $(this).attr("id");
       let numberOfVehicles = 20 
       let i=0
       let serviceType = $("#selectedservices").val()
       let al = $("#al").val()
       let tdDestination =  $(this).destination()
       let tdPickUpLocation = $(this).deliverlocation()
      
       let customersname = $( "#selectedcustomer option:selected" ).text();
       let packageOption = $('input[name="packageoption"]:checked').val()?$('input[name="packageoption"]:checked').val():"";
       //let isCheckPackage = $('input[name="packageoption"]:checked');
       let data = []
       let vehicleid = $(this).data("vehicleid")
       let vehiclename = $(this).data("vehiclename")


       $("#vehicleSummary").html(vehiclename)
       $("#selectedvehicle").val(vehicleid)
       let customers = $("#selectedcustomer").val()
       let notes = $("#deliveryinstruction").val()
       let estimatedamount = $("#estimatedamount").val()?$("#estimatedamount").val():0
       $("#customernameSummary").html(customersname)
       $("#estimatedSummary").html(estimatedamount)
       $("#instructionSummary").html(notes)
       $("#divOrderSummary").hide()

       data = {'cart_type_id':serviceType,"customer_id":customers,"vehicle_id":vehicleid,"delivery_location":tdDestination,"pickup_location":tdPickUpLocation,
            "assigned_location":al,"notes":notes,"category":packageOption}
       
       $(this).removeClass("btn-primary")
       $(this).addClass("btn-warning")
      //  $(this).trigger('click')
       $(this).notselectedVehicles(vehicleid)
        
       if(packageOption != "" && serviceType == 4)
       {
        $("#itemtitleSummary").html("Package for Delivery")
        $("#itemSummary").html(packageOption)
        $("#loading").show()
        $(this).ajaxpostrequest("cms/booking/initialize_cart",data)
       }else if(pabiliItems.length > 0 && serviceType == 3 && estimatedamount >0)
       {
        let itemslist = "";
        notes = $("#pabiliinstruction").val()
        $("#instructionSummary").html(notes)
        $("#itemtitleSummary").html("Pabili Items")
        $("#itemSummary").html(packageOption)

        data = {'cart_type_id':serviceType,"customer_id":customers,"vehicle_id":vehicleid,"delivery_location":tdDestination,"pickup_location":tdPickUpLocation,
            "assigned_location":al,"notes":notes,"items_name":pabiliItems,
            "estimate_total_amount":estimatedamount}
        
  
        itemslist = "<ol>"
        $.each(pabiliItems,function(i,item)
        {
          itemslist +=`<li>${item}</li>`
        })

        itemslist += "</ol>"
        $("#itemSummary").html(itemslist)

        $("#loading").show()
        $(this).ajaxpostrequestpabili("cms/booking/initialize_cart_pabili",data)
           //console.log(pabiliItems)
       }else
       {
         
         if(serviceType == 4)
         {
            $("#headertitlemodal").html("Package Option");
            $("#contentitlemodal").html("Please select Package Option.");
         }else
         {
           if(estimatedamount <=0)
           {
            $("#headertitlemodal").html("Estimated Amount");
            $("#contentitlemodal").html("Please enter estimated amount."); 
            $("#estimatedamount").focus()
           }else
           {
            $("#pabilitxt").focus();
            $("#headertitlemodal").html("Pabili Items");
            $("#contentitlemodal").html("Please add pabili item"); 
           }
            
         }
        
        $("#modalAlert").modal("show");
        $(this).removeClass("btn-warning")
        $(this).addClass("btn-primary")
       }
       
      // console.log(packageOption)
  
    })


   // let ridername  = $('input[name="rider"]:checked').data("name")?$('input[name="rider"]:checked').data("name"):"No rider assigned";

       


   
    $("#btnSenderSave").on('click',function(){
        let hasError = false;
        let name = $("#sendername").val()
        let contactNum = $("#sendercontact").val()
        let blockAddress = $("#senderblock").val()

        let validNumber = $("#sendercontact").phonenumber(contactNum)
        if(name == "" || name == null)
        {
            $("#sendernameerror").html("This is required.")
            hasError = true;
        }
       
        if(contactNum  == "" || contactNum  == null)
        {
            $("#senderconerror").html("This is required.")
            hasError = true;
        }

        if(validNumber == false && contactNum !="")
        {
            $("#senderconerror").html("Invalid contact number.")
            hasError = true;
        }

        if(hasError == false)
        {
          $("#tdPickUpLabel").html(name+"|"+contactNum+"|"+blockAddress);
          $("#divSenderInformation").hide(); 
          $("#divmodal").hide()
          $("#tdPickUpLocationSummary").val($("#tdPickUpLocation").val())
          $("#tdPickUpLabelSummary").html(name+"|"+contactNum+"|"+blockAddress)
        }
    })


    $("#btnRecipientSave").on('click',function(){
        let hasError = false;
        let name = $("#recipientname").val()
        let contactNum = $("#recipientcontact").val()
        let blockAddress = $("#recipientblock").val()

        let validNumber = $("#recipientcontact").phonenumber(contactNum)
        if(name == "" || name == null)
        {
            $("#recipientnameerror").html("This is required.")
            hasError = true;
        }
       
        if(contactNum  == "" || contactNum  == null)
        {
            $("#recipientconerror").html("This is required.")
            hasError = true;
        }

        if(validNumber == false && contactNum !="")
        {
            $("#recipientconerror").html("Invalid contact number.")
            hasError = true;
        }

        if(hasError == false)
        {
          $("#tdDestinationLabel").html(name+"|"+contactNum+"|"+blockAddress);
          $("#divrecipientInformation").hide(); 
          $("#divmodal").hide()
          $("#tdDestinationSummary").val($("#tdDestination").val())
          $("#tdDestinationLabelSummary").html(name+"|"+contactNum+"|"+blockAddress)
        }
    })

  $.fn.deliverlocation = function()
  {
    let dl = []
    let senderLat = $("#senderlatitude").val()
    let senderLon = $("#senderlongitude").val()
    let senderName =  $("#sendername").val()
    let senderAddress = $("#senderblock").val()+", "+$("#tdPickUpLocation").val() 
    let contactNum = $("#sendercontact").val()
    let labelvalue = $("#tdPickUpLabel").html()
    let address = $("#tdPickUpLocation").val() 
    
    if(address == "" )
    {
        $("#headertitlemodal").html("Invalid Pick Up Address");
        $("#contentitlemodal").html("Please select from the auto suggested address."); 
        $("#tdPickUpLocation").focus()
        $("#modalAlert").modal("show");

    }
    else if(senderLat == "" && senderLon == "")
    {
        $("#headertitlemodal").html("Invalid Pick Up Address");
        $("#contentitlemodal").html("Please select from the auto suggested address."); 
        $("#tdPickUpLocation").focus()
        $("#modalAlert").modal("show");

    }else if(senderName == "")
    {
       $("#headertitlemodal").html("Sender Information");
        $("#contentitlemodal").html("Please fill out required field."); 
        $("#tdPickUpLocation").focus()
        $("#modalAlert").modal("show");
        $("#divSenderInformation").show()
        $("#divrecipientInformation").hide()

    }else if (labelvalue == "")
    {
        $("#headertitlemodal").html("Sender Information");
        $("#contentitlemodal").html("Please save sender information"); 
        $("#tdPickUpLocation").focus()
        $("#modalAlert").modal("show");
        $("#divSenderInformation").show()
        $("#divrecipientInformation").hide()

    }else
    {
        dl = {
            "label":senderName,
            "address_text":senderAddress,
            "latitude":senderLat,
            "longitude":senderLon,
            "landmark":"",
            "contact_number":contactNum
        }
    }
    
        /*dl = {"label":"5","address_text":"25 T. Evangelista, Lungsod Quezon, Kalakhang Maynila, Philippines","latitude":"14.6340026","longitude":"121.0650074","landmark":"","contact_number":"2"}*/

    return dl
  } 

  $.fn.destination = function()
  {
    let recl = []
    let recLat = $("#recipientlatitude").val()
    let recLon = $("#recipientlongitude").val()
    let recName =  $("#recipientname").val()
    let recAddress = $("#recipientblock").val()+", "+$("#tdDestination").val() 
    let contactNum = $("#recipientcontact").val()
    let address = $("#tdDestination").val() 
    let labelvalue = $("#tdDestinationLabel").html()
    
    if(address == "")
    {
     $("#headertitlemodal").html("Invalid Destination Address");
        $("#contentitlemodal").html("Please select from the auto suggested address."); 
        $("#tdDestination").focus()
        $("#modalAlert").modal("show");
    }
    else if(recLat == "" && recLon == "")
    {
        $("#headertitlemodal").html("Invalid Destination Address");
        $("#contentitlemodal").html("Please select from the auto suggested address."); 
        $("#tdDestination").focus()
        $("#modalAlert").modal("show");

    }else if(recName == "")
    {
       $("#headertitlemodal").html("Recipient Information");
        $("#contentitlemodal").html("Please fill out required field."); 
        $("#tdDestination").focus()
        $("#modalAlert").modal("show");
        $("#divmodal").show()
        $("#divSenderInformation").hide()
        $("#divrecipientInformation").show()

    }else if (labelvalue == "")
    {
        $("#headertitlemodal").html("Recipient Information");
        $("#contentitlemodal").html("Please save recipient information"); 
        $("#tdDestination").focus()
        $("#modalAlert").modal("show");
        $("#divSenderInformation").hide()
        $("#divmodal").show()
        $("#divrecipientInformation").show()

    }else
    {
        recl = {
            "label":recName,
            "address_text":recAddress,
            "latitude":recLat,
            "longitude":recLon,
            "landmark":"",
            "contact_number":contactNum
        }
    }
    
   /* recl = {"label":"5","address_text":"Daang Reyna, Vista Avenue, Vista Alabang Portofino South, Almanza Dos, Las Pinas, Metro Manila, Philippines","latitude":"14.3684577","longitude":"121.0099434","landmark":"","contact_number":"2"}  
   */

    return recl
  }     

 $.fn.applyvoucherrequest = function(url,dataPost)
 {

   let baseurl = $(this).baseurl()
   let totalDF = parseInt($("#totaltxt").val())
   let newDF = 0
   $("#smVoucherCode").html("Voucher")
   $("#smVoucherAmount").html("₱0")
   $("#vouchercodetxt").html("")
   $("#voucherAmttxt").html("0.00")
   
     axios.post(base_url + url,dataPost, {
        headers: {
          'Content-Type': 'application/json; charset=UTF-8'
        }
        // other configuration there
      })
      .then(function (response) {

        let respData = response.data.data
        let vAmnt = response.data.data[0].amount;
       // console.log(respData.length);
        $("#btnApply").attr("disabled",false)
        $("#btnApply").html('Apply')
       
        if(respData.length > 0){
          $("#btnVoucher").hide()
          $("#vouchercode").html(`Voucher (${response.data.data[0].voucher_code})`)
          $("#voucheramount").html(`- ₱${parseInt(vAmnt)}`)
          $("#smVoucherCode").html(`Voucher (${response.data.data[0].voucher_code})`)
          $("#smVoucherAmount").html(`- ₱${parseInt(vAmnt)}`)
          $("#modalVoucher").modal('hide')
           newDF = totalDF - parseInt(vAmnt)
          $("#labeltotal").html("₱"+newDF)
          $("#deliveryfeeSummary").html("₱"+newDF)
          $("#vouchercodetxt").val(response.data.data[0].voucher_code)
          $("#voucherAmttxt").val(vAmnt)
           
         }else{
           $("#voucherError").html(response.data.meta.message)
         }

      })
      .catch(function (error) {
        alert('oops');
        console.log(error);
      })

     return false; 
 }
  

  $.fn.ajaxpostrequest = function(url, dataPost)
  {
     let baseurl = $(this).baseurl()
     axios.post(base_url + url,dataPost, {
        headers: {
          'Content-Type': 'application/json; charset=UTF-8'
        }
        // other configuration there
      })
      .then(function (response) {
      //  alert('yeah!');
        $("#loading").hide()
        let resp = response.data
      //  console.log(resp);
        $("#labeltotal").html("₱"+resp.data.grand_total)
        $("#deliveryfeeSummary").html("₱"+resp.data.grand_total)
                if(resp.data.grand_total > 0)
                {
                    let distance = Math.floor(resp.data.distance_in_km)
                    let kmcharge = parseInt(distance) * parseInt(resp.data.price_per_km)
                    $("#divmodal").hide()
                    $("#divDeliveryDetails").hide();
                     $("#btnVoucher").attr('disabled',false)
                    $("#modalRider").modal('show')
                    $("#cartidtxt").val(resp.cartid)
                    $("#totaltxt").val(resp.data.grand_total)
                    $("#btnreview").removeClass("hidden")
                    $("#btnreview").show()
                    $("#basefare").html(`₱ ${resp.data.basefare_fee}`)
                    $("#distance").html(`Distance (${distance} KM)`)
                    $("#kmcharge").html(`₱ ${kmcharge}`)
                    $("#adminFee").html(`₱ ${resp.data.admin_fee}`)
                }
                $("#loading").hide()
      })
      .catch(function (error) {
        alert('oops');
        console.log(error);
      })

     
     return false; 
  }


  $.fn.ajaxpostrequestpabili = function(url, dataPost)
  {
     let baseurl = $(this).baseurl()


     axios.post(base_url + url,dataPost, {
        headers: {
          'Content-Type': 'application/json; charset=UTF-8'
        }
        // other configuration there
      })
      .then(function (response) {
       // alert('yeah!');

        $("#loading").hide()
        let resp = response.data
        console.log(resp);
        $("#labeltotal").html("₱"+resp.data.grand_total)
        $("#deliveryfeeSummary").html("₱"+resp.data.grand_total)
                if(resp.data.grand_total > 0)
                {
                  $("#btnVoucher").attr('disabled',false)
                    let distance = Math.floor(resp.data.distance_in_km)
                    let kmcharge = parseInt(distance) * parseInt(resp.data.price_per_km)
                    $("#divmodal").hide()
                    $("#divPabili").hide()
                    $("#modalRider").modal('show')
                    $("#cartidtxt").val(resp.cartid)
                    $("#totaltxt").val(resp.data.grand_total)
                    $("#btnreview").removeClass("hidden")
                    $("#btnreview").show()
                    
                    $("#basefare").html(`₱ ${resp.data.basefare_fee}`)
                    $("#adminfee").html(`₱ ${resp.data.admin_fee}`)
                    $("#distance").html(`Distance (${distance} KM)`)
                    $("#kmcharge").html(`₱ ${kmcharge}`)
                    $("#adminFee").html(`₱ ${resp.data.admin_fee}`)
                }
                $("#loading").hide()
      })
      .catch(function (error) {
        alert('oops');
        console.log(error);
      })

     return false; 
  }

  $.fn.ajaxpostrequestbooknow = function(url, dataPost)
  {
     let baseurl = $(this).baseurl()

     console.log("dataToPost:"+dataPost)

     axios.post(base_url + url,dataPost, {
        headers: {
          'Content-Type': 'application/json; charset=UTF-8'
        }
        // other configuration there
      })
      .then(function (response) {
       // alert('yeah!');
        
        //console.log(response.data);
        let resp = response.data;
           $("#loading").hide()
        console.log(resp.meta.message)
        if(resp.data == true)
        {
             /*$("#headertitlemodal").html("Response");
             $("#contentitlemodal").html("Successfully Booked.");
             $("#modalAlert").modal("show");*/
             if (confirm('Successfully Booked.') ) {
                       location.reload();
                  }
             location.reload();
        }else{
            alert(resp.meta.message)
        }
        //console.log(resp)
        
      })
      .catch(function (error) {
         $("#loading").hide()
        alert('oops');
        console.log(error);
      })
     return false; 
  }


  $("#riderresult").on('click',function(){
    
    let riderWalletBalance =  parseInt($('input[name="rider"]:checked').data("wb"))
    let minWalletBalance = $("#minWallet").val()?parseInt($("#minWallet").val()):0
    console.log("min wallet balance:"+riderWalletBalance + "-"+minWalletBalance)
    
    if(riderWalletBalance < minWalletBalance){
       alert("Insufficient rider wallet balance.")
        $('input[name="rider"]').removeAttr('checked');
    }else{
      $("#modalRider").modal('hide')
    }
    
  })


  $.fn.ajaxpostrequestrider = function(url, dataPost)
  {
     let baseurl = $(this).baseurl()
     
    $.ajax({
         type: "POST",
         url: base_url + url, 
         data: dataPost,
         dataType: "json",  
         cache:false,
         success: 
              function(data){
                let datatoappend = "";
               // console.log(data);
                if(data.length > 0)
                {
                    $.each(data,function(i,val){
                       datatoappend +=`<div class="form-check">
                         <input class="form-check-input" type="radio" name="rider"  
                          value="${val.id}" data-name="${val.full_name}" data-wb="${val.wb}">
                          <label class="form-check-label" >
                             ${val.full_name} -  Wallet Balance: ₱${val.wb}
                          </label></div>`
                    })
                    $("#riderresult").html(datatoappend)
                }else{
                     $("#riderresult").html("")
                }
                
                
              }
          });// you have missed this bracket
     return false; 
  }

  $.fn.unserialize = function(data) {
    data = data.split(';');
    var response = {};
    for (var k in data){
        var newData = data[k].split(':');
        response[newData[0]] = newData[1];
    }
    return response;
}

  $.fn.phonenumber = function(contactNum) {
    var a = contactNum;
    var filter = /^\d*(?:\.\d{1,2})?$/;
    var f2digits = a.substring(0,2)
    if (filter.test(a) && a.length == 11 && f2digits == "09") {
        return true;
    }
    else {
        return false;
    }
 }

 $.fn.baseurl = function(){
    let baseUrl = $("#base_url").val();
    return baseUrl
 }
 
 $.fn.notselectedVehicles = function(vehicleid)
 {
    let numberOfVehicles = 20

    for(i=0;i < numberOfVehicles;i++)
       {
         
        if(i != vehicleid )
        {
            $("#vehi"+i).addClass("btn-primary")
            $("#vehi"+i).removeClass("btn-warning")   
        }
       }
 }

 $.fn.spanclear = function(divname){$("#"+divname).html("")}

 

$("#searchrider").keyup(function(){
       let senderLat = $("#senderlatitude").val()
       let senderLon = $("#senderlongitude").val()
       let value = $.trim($(this).val());
       let pickuplat = senderLat
       let pickuplng = senderLon
       let vehicleid = $("#selectedvehicle").val()
       let al        = $("#al").val()
       let data = []
       data = {
               "ridername":value,
               "pickuplat":pickuplat,
               "pickuplng" :pickuplng,
               "vehicleid" :vehicleid,
               "al":al
              }
       $(this).ajaxpostrequestrider("cms/booking/searchrider",data)
    }) 
  });
</script>
 <script src='https://api.tomtom.com/maps-sdk-for-web/cdn/6.x/6.15.0/maps/maps-web.min.js'></script>

<script src='https://api.tomtom.com/maps-sdk-for-web/6.x/6.15.0/examples/pages/examples/assets/js/mobile-or-tablet.js'></script>

<script src="https://api.tomtom.com/maps-sdk-for-web/cdn/plugins/SearchBox/3.1.3-public-preview.0/SearchBox-web.js"></script>
<script src="https://api.tomtom.com/maps-sdk-for-web/cdn/6.x/6.1.2-public-preview.15/services/services-web.min.js"></script>

<script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/admin/js/') ?>tomtoAutocomplete.js"></script> 


<!--<script type="text/javascript" src="<?php //echo base_url('public/admin/js/') ?>autocompletemap.js"></script> -->

 <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>





