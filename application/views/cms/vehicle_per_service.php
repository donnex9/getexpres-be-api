<style>
  .ms-container {
    width: 100% !important;
  }
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/admin/') ?>assets/jquery-multi-select/css/multi-select.css" />
<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            <h4><i class="fa fa-plus"></i> Choose Vehicle to Assign per Service</h4>
            <?php if ($flash_msg = $this->session->flash_msg): ?>
              <h5 style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></h5>
            <?php endif; ?>
          </header>
        </section>
      </div>
    </div>

    <?php 
        $canedit = $accesslevel->can_edit;         
      ?>

    <div class="row">
    <?php $i=1; foreach ($services as $key => $value):
      $ressy = 'res_' . $i;
      ?>
        <div class="col-lg-6">
          <section class="panel">
            <header class="panel-heading">
              <!-- Scheduled Surge -->
              <h7 style="font-weight:bold">
                <?php echo $value->label ?>
              </h7><br>
              <h7 >Note: INACTIVE vehicles are on the LEFT, and ACTIVE vehicles are on the RIGHT</h7>

            </header>
            <div class="panel-body">
              <form action="<?php echo base_url('cms/dashboard/update_services_vehicles') ?>" method="post">
                <input type="hidden" name="service_id" value="<?php echo $value->id ?>">
                <select multiple="multiple" class="multi-select" name="vehicle_ids[]">

                    <?php
                      $selected_vehicles_arr = [];
                       foreach ($$ressy as $key3 => $value3):
                         $selected_vehicles_arr[] = $value3->vehicle_id;
                         ?>
                         <option selected="selected" value="<?php echo $value3->vehicle_id ?>"><?php echo $value3->name ?></option>
                      <?php endforeach; ?>

                  <?php
                  // var_dump($selected_vehicles_arr); die();
                  foreach ($vehicles as $key2 => $value2):
                     ?>
                    <?php if (!in_array($value2->id, $selected_vehicles_arr)): ?>
                      <option value="<?php echo $value2->id ?>"><?php echo $value2->name ?></option>
                    <?php endif; ?>
                  <?php endforeach; ?>

                </select>
                <br>
                 <?php if($canedit == 1):  ?>
                <input class="btn btn-info btn-md" type="submit" value="Update">
              <?php endif; ?>
              </form>
              </div>
            </section>
          </div>
    <?php $i++; endforeach; ?>
  </div>




    </section>
  </section>


<script type="text/javascript">
  $(document).ready(function() {

    $('form').on('submit',function(event) {
      if (confirm('Are you sure you want to change the available vehicles for this service?')) {
        return true
        $('input[type=submit]').prop('disabled', true)
      } else {
        event.preventDefault();
        return false;
      }

      /* Act on the event */
    });

    $('.multi-select').multiSelect({
        selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='Search Vehicles...'>",
        selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='Search Vehicles...'>",
        afterInit: function (ms) {
            var that = this,
                $selectableSearch = that.$selectableUl.prev(),
                $selectionSearch = that.$selectionUl.prev(),
                selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function (e) {
                    if (e.which === 40) {
                        that.$selectableUl.focus();
                        return false;
                    }
                });

            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function (e) {
                    if (e.which == 40) {
                        that.$selectionUl.focus();
                        return false;
                    }
                });
        },
        afterSelect: function () {
            this.qs1.cache();
            this.qs2.cache();
        },
        afterDeselect: function () {
            this.qs1.cache();
            this.qs2.cache();
        }
    });


  });
</script>

<script type="text/javascript">
  $(document).ready(function() {

   $('.panel-heading').find('h4:contains("Get Car")').text("Get Ride");
  });
</script>

<script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/admin/') ?>assets/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/admin/') ?>assets/jquery-multi-select/js/jquery.quicksearch.js"></script>

