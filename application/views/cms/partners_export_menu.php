<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading" style="padding:15px">
            Partners Data Exporter
            <?php if ($flash_msg = $this->session->flash_msg): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>

          </header>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-2">
              <form method="GET" >
                <select class="form-control" name="partner_id">
                  <option value="">Choose a Merchant/Partner</option>
                  <?php foreach ($res as $key => $value): ?>
                    <option value="<?php echo $value->id ?>" <?php echo (@$_GET['partner_id'] == $value->id) ? 'selected="selected"' : '' ?>><?php echo $value->name ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="col-md-2">
                <input type="date" name="from" class="form-control" value="<?php echo @$_GET['from'] ?>">
              </div>
              <div class="col-md-1">
                <label for="" style="margin-top:8px">
                  Start to End date
                </label>
              </div>
              <div class="col-md-2">
<input type="date" name="to" class="form-control" value="<?php echo @$_GET['to'] ?>">
              </div>
              <div class="col-md-1">
                <button type="submit" class="btn btn-info" >Go Filter <i class="fa fa-filter"></i></button>
              </div>
              <div class="col-md-1">
                <a href="<?php echo base_url('cms/partners/export_menu') ?>" class="btn btn-success">Clear <i class="fa fa-filter"></i></a>
              </div>

            </form>
              <div class="col-md-1">
                <a href="<?php echo base_url('cms/partners/export_to_csv') ?>?from=<?php echo @$_GET['from'] ?>&to=<?php echo @$_GET['to'] ?>&partner_id=<?php echo @$_GET['partner_id'] ?>" class="btn btn-warning">Export to CSV with Filters <i class="fa fa-download"></i></a>
              </div>
            </div>
<br>
<br>
            <div class="table-responsive" style="overflow: hidden; outline: none;" tabindex="1">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <!-- <th>Merchand ID#</th> -->
                    <th>Transaction ID#</th>
                    <th>Merchant Name</th>
                    <th>GetApp Commission %</th>
                    <th>GetApp Commission Value</th>
                    <th>Merchant Total Sales</th>
                    <!-- <th>Merchant Total Sales (No Commission)</th> -->
                  </tr>
                </thead>
                <tbody>
                  <?php if (count($res2) > 0 ): ?>

                    <?php foreach ($res2 as $key => $value): ?>
                      <tr>
                        <!-- <th scope="row"><?php echo $value->cart_id ?></th> -->
                        <td> <a target="_blank" href="<?php echo base_url('cms/history/all_orders?cart_id=') . $value->cart_id . "&_=" . time()?>"><?php echo $value->order_id ?> (<?php echo $value->cart_id ?>) <i class="fa fa-link"></i></a></td>
                        <td><?php echo $value->name ?></td>
                        <td><?php echo $value->getapp_comission_percentage ?> %</td>
                        <td><?php echo $value->commission_value ?> php</td>
                        <td><?php echo $value->total_price ?> php</td>
                        <!-- <td><?php echo $value->partner_total_sales_minus_commission ?> php</td> -->
                      </tr>
                      <?php endforeach; ?>


                    <?php else: ?>
                      <tr>
                        <td colspan="6" style="text-align:center">No filters applied or empty result set.</td>
                      </tr>
                    <?php endif; ?>
                  </tbody>
                </table>

            </div>
          </section>
        </div>
      </div>
      <!-- page end-->
    </section>
  </section>



  <script type="text/javascript">
    $(document).ready(function() {

    });
  </script>

  <script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>
