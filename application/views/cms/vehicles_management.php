<section id="main-content">
  <section class="wrapper">
    <!-- page start-->

    <?php if ($flash_msg): ?>
      <p style="color: <?php echo $flash_msg['color'] ?>;font-weight:bold"><?php echo $flash_msg['message'] ?></p>
    <?php endif; ?>
    <div class="row">

      <?php foreach ($res as $key => $value): ?>

      <div class="col-lg-2">
        <section class="panel">
          <header class="panel-heading">
            <?php echo $value->name ?>
          </header>
          <div class="panel-body">
            <img src="<?php echo $value->image ?>" alt="" style="max-width:150px">
            <form action="<?php echo base_url('cms/dashboard/update_vehicle') ?>" method="post">
              <input type="hidden" name="id" value="<?php echo $value->id ?>">
              <br>
              <label>
                Weight Capacity
              </label>
              <input class="form-control" type="text" name="weight_capacity" value="<?php echo $value->weight_capacity ?>">
              <br>
              <!--
              <label>
                Delivery Price (for this vehicle)
              </label>
              <input class="form-control" type="text" name="delivery_price" value="<?php echo $value->delivery_price ?>">
              <br> -->
              <input class="btn btn-info btn-md" type="submit" value="Update Options">
            </form>
            </div>
          </section>
      </div>
      <?php endforeach; ?>


      </div>




      <!-- page end-->
    </section>
  </section>


<script type="text/javascript">
  $(document).ready(function() {


  });
</script>

<script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>
