<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    


    <div class="row">
      <div class="col-lg-12">
       
        <section class="panel">
          <header class="panel-heading" style="padding:15px">
            Voucher Summary
            <?php if ($flash_msg = $this->session->flash_msg): ?>
              <br><sub style="color: <?php echo $flash_msg['color']; ?>"><?php print_r($flash_msg['message']); ?></sub>
            <?php endif; ?>
            <div style="float:right">
              <form method="GET">
                <input type="text" name="squery" value="<?php echo @$_GET['squery'] ?>" placeholder="Search by Voucher Code">
                <input type="submit">
              </form>
            </div>
          </header>

          <div class="panel-body">
            <p>
              <!-- <button type="button" class="add-new btn btn-success btn-sm">Add new</button> -->
            </p>
            <div class="table-responsive" style="overflow: hidden; outline: none;" tabindex="1">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>Item No.</th>
                    <th>Booking No.</th>
                    <th>Booking Date and Time</th>
                    <th>Customer's Name</th>
                    <th>Service Type</th>
                    <th>Voucher Code</th>
                    <th>Voucher Amount</th>
                    <th>Delivery Fee</th>
                    <th>Rider's Name</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if (count($res) > 0 ): ?>

                    <?php 
                    // var_dump($allLocation);
                    $page = isset($_GET["page"])?$_GET["page"]:1;
                    if($page >= 2)
                    {
                      $row = ((INT) $_GET["page"] - 1) * 30;
                      $rowcount = $row + 1;
                    }else
                    {
                      $rowcount = 1;
                    }

                    foreach ($res as $key => $value): ?>
                      <tr>
                        <th scope="row"><?php echo $rowcount++ ?></th>
                        <td><?php echo $value->booking_num ?></td>
                        <td><?php echo date('F j, Y g:i a', strtotime($value->booking_date)) ?></td>
                        <td><?php echo $value->customer_id  ?></td>
                        <td><?php echo ucwords($value->service_type) ?></td>
                        <td><?php echo $value->voucher_code ?></td>
                        <td><?php echo $value->voucher_amount ?></td>
                        <td><?php echo $value->delivery_fee ?></td>
                        <td><?php echo $value->rider_id ?></td>
                        <td><?php echo $value->status ?></td>
                        <td>
                          <button type="button"
                          data-payload='<?php echo json_encode(['id' => $value->booking_num,'bookingDateTime' =>$value->booking_date,'voucher_code' => $value->voucher_code,'amount' => $value->voucher_amount, 'serviceType' => $value->service_type,  'customer' => $value->customer_id , 'status' => $value->status, 'assigned_location'=>$value->assigned_location,'updated_by'=>$value->updated_by,'updated_at'=>$value->updated_at,'allLocation'=>$allLocation], JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE)?>'
                          class="edit-row btn btn-info btn-xs">update</button>
                          
                          </td>
                        </tr>
                      <?php endforeach; ?>


                    <?php else: ?>
                      <tr>
                        <td colspan="8" style="text-align:center"><?= ($this->input->get('squery')) ? 'Empty Search Results':'Empty table data' ?></td>
                      </tr>
                    <?php endif; ?>
                  </tbody>
                </table>
              </div>

              <style>
              .active_lg {
                background: lightgray !important
              }
              </style>
              <ul class="pagination">
                <ul class='pagination'>
                  <?php $page = ($this->input->get('page')) ?: 1; ?>
                  <li><a href="<?php echo base_url('cms/vouchersummary/') . "?page=1&squery=" . @$_GET['squery'];?>">&laquo;</a></li>

                  <!-- loop for desc -->
                  <?php for ($i = $page - 2; $i < ($page) ; $i++):
                    if ($i == -1 || $i == 0) {
                      continue;
                    }
                   ?>
                  <li><a href="<?php echo base_url('cms/vouchersummary/') . "?page=" . $i . "&squery=" . @$_GET['squery'];?>"><?= $i ?></a></li>
                  <?php endfor; ?>
                  <!-- / loop for desc -->

                  <li><a href="<?php echo base_url('cms/vouchersummary/') . "?page=" . $page . "&squery=" . @$_GET['squery'];?>"><?= $page ?></a></li>

                  <!-- loop for asc -->
                  <?php for ($i = $page + 1; $i < ($page + 3) ; $i++):
                  if ($i == $total_pages + 1 || $i == $total_pages + 2 || $total_pages == 0) {
                      continue;
                  }
                  ?>
                  <li><a href="<?php echo base_url('cms/vouchersummary/') . "?page=" . $i . "&squery=" . @$_GET['squery'];?>"><?= $i ?></a></li>
                  <?php endfor; ?>
                  <!-- / loop for asc -->


                <li><a href="<?php echo base_url('cms/vouchersummary/') . "?page=" . $total_pages . "&squery=" . @$_GET['squery'];?>">&raquo;</a></li>
                </ul>
              </ul>

            </div>
          </section>
        </div>
      </div>
      <!-- page end-->
    </section>
  </section>

  

  <!-- Modal -->
  <div class="modal fade update-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Manage</h4>
        </div>
        <div class="modal-body row">

          <div class="col-md-12">
            <h3>Voucher Details</h3>
            <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('cms/vouchersummary/update') ?>">
              
              <div class="form-group">
                <label >Booking Number</label>
                <input type="text" class="form-control"  name="id"  id="up_id" readonly="true">
              </div>
              <div class="form-group">
                <label >Booking Date and Time</label>
                <input type="text" class="form-control"   id="up_bookindDateTime" readonly="true">
              </div>

              <div class="form-group">
                <label >Voucher Code</label>
                <input type="text" class="form-control"   id="up_voucher_code" readonly="true">
              </div>
              <div class="form-group">
                <label >Amount</label>
                <input type="text" class="form-control"  id="up_amount" readonly="true">
              </div>
              <div class="form-group">
                <label >Service Type</label>
                <input type="text" class="form-control"  id="up_serviceType" readonly="true">
              </div>
             
              <!--<div class="form-group">
                <label >Assigment Area</label>
                <select name="assigned_location" id="up_assigmentArea" class="form-control" >
                 
                 </select>
              </div>-->
              <div class="form-group">
                <label >Status</label>
               <select name="status" id="up_status" class="form-control" >
                 <option value='unpaid'>Unpaid</option>
                  <option value='paid'>Paid</option>
                </select>
              </div>
              <div class="form-group">
                <label >Updated by</label>
                <input type="text" class="form-control"  id="up_updated_by" disabled="true">
              </div>

              <div class="form-group">
                <label >Date & Time updated</label>
                <input type="text" class="form-control"  id="up_updated_at" disabled="true">
              </div>
  

        </div><!--  end div column 8 -->

        <!-- <div class="col-md-4">


        </div>  -->
        <!-- end div col md 4 -->

      </div>

      <div class="modal-footer">
       
        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
        <input class="btn btn-info" type="submit" value="Save changes">
      </div>
    </form>

    </div>
  </div>
</div>
  <!-- modal -->



  <script type="text/javascript">
    $(document).ready(function() {
     
     
      $('.edit-row').on('click', function(){
      
       let All_location = $(this).data('payload').allLocation
      console.log($(this).data('payload'))
  

        let id = $(this).data('payload').id
        let bookingDateTime = $(this).data('payload').bookingDateTime
        let voucher = $(this).data('payload').voucher_code
        let amount = $(this).data('payload').amount
        let status = $(this).data('payload').status
        let serviceType  = $(this).data('payload').serviceType
        let updated_by = $(this).data('payload').updated_by
        let updated_at = $(this).data('payload').updated_at
        let assigned_location = $(this).data('payload').assigned_location

       /* $.each(All_location, function(i,val){
         // console.log(vald+","+val.province)
          $('#up_assigmentArea').append(`<option value="${val.id}">
                                       ${val.barangay !=null ?val.barangay +",":""} ${val.city != null?val.city+",":""}  ${val.province}
                                  </option>`);
        })*/

       $("#up_status option[value="+status+"]").prop("selected", "selected")
        
        $('#up_voucher_code').val(voucher)
        $('#up_bookindDateTime').val(bookingDateTime)
        $('#up_amount').val(amount)
        $("#up_status option[value="+status+"]").prop("selected", "selected")
        $('#up_updated_by').val(updated_by)
        $('#up_updated_at').val(updated_at)
        $('#up_id').val(id)
        $('#up_serviceType').val(serviceType)
       
        $('.update-modal').modal()
      })

      

     
    });
  </script>

  <script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>

