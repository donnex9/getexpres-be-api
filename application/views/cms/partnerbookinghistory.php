<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">

            Booking History
            <hr>

          </header>

        </div>

      <div class="col-lg-12">
        <section class="panel">

          <?php if (@$_GET['customer_id'] || @$_GET['rider_id']): ?>
            <header class="panel-heading">
              <h4><?php echo @$history_of ?>'s History</h4>
            </header>
          <?php endif; ?>

          <?php if ($flash_msg = $this->session->flash_msg): ?>
            <br><h4 style="color: <?php echo $flash_msg['color'] ?>;     margin: 0px 20px -30px;"><?php echo $flash_msg['message'] ?></h4>
          <?php endif; ?>

          <div class="panel-body">
            <?php if (@$res): ?>

          <style>
          .active_lg {
            background: lightgray !important
          }
          </style>
          <ul class="pagination">
            <ul class='pagination'>
              <?php $page = ($this->input->get('page')) ?: 1; ?>
              <li><a href="<?php echo base_url('cms/history/all_orders_partner') . "?page=1&cart_type_id=" . @$_GET['cart_type_id'] . "&from=" . @$_GET['from'] . "&to=" . @$_GET['to'];?>">&laquo;</a></li>

              <!-- loop for desc -->
              <?php for ($i = $page - 2; $i < ($page) ; $i++):
                if ($i == -1 || $i == 0) {
                  continue;
                }
               ?>
              <li><a href="<?php echo base_url('cms/history/all_orders_partner') . "?page=" . $i . "&cart_type_id=" . @$_GET['cart_type_id']. "&from=" . @$_GET['from'] . "&to=" . @$_GET['to'];?>"><?= $i ?></a></li>
              <?php endfor; ?>
              <!-- / loop for desc -->

              <li><a href="<?php echo base_url('cms/history/all_orders_partner') . "?page=" . $page . "&cart_type_id=" . @$_GET['cart_type_id'] . "&from=" . @$_GET['from'] . "&to=" . @$_GET['to'];?>"><?= $page ?></a></li>

              <!-- loop for asc -->
              <?php for ($i = $page + 1; $i < ($page + 3) ; $i++):
              if ($i == $total_pages + 1 || $i == $total_pages + 2 || $total_pages == 0) {
                  continue;
              }
              ?>
              <li><a href="<?php echo base_url('cms/history/all_orders_partner') . "?page=" . $i . "&cart_type_id=" . @$_GET['cart_type_id']  . "&from=" . @$_GET['from'] . "&to=" . @$_GET['to'];?>"><?= $i ?></a></li>
              <?php endfor; ?>
              <!-- / loop for asc -->


            <li><a href="<?php echo base_url('cms/history/all_orders_partner') . "?page=" . $total_pages . "&cart_type_id=" . @$_GET['cart_type_id']  . "&from=" . @$_GET['from'] . "&to=" . @$_GET['to'];?>">&raquo;</a></li>
            </ul>
          </ul>


              <div class="table-responsive" style="overflow: hidden; outline: none;" tabindex="1">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Order ID (#db identifier)</th>
                      <th>Service Type</th>
                      <th>Status</th>
                      <th>Customer name</th>
                      <th>Order/Booking</th>
                      <th>Date</th>
                      <th>Price</th>
                      <th> </th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php $i = 1; foreach ($res as $key => $value): ?>
                        <tr>
                          <th scope="row"><?php echo $value->cart->order_id ?> (#<?php echo $value->cart->id?>)</th>
                          <td><img src="<?php echo $value->icon ?>" style="max-width:50px"><br>Get <?php echo ucwords($value->service_type) ?></td>
                          <td><?php echo str_replace("_"," ", ucwords($value->cart->status));  ?></td>
                          <td><?php echo ucwords(@$value->cart->customer->customer->full_name) ?></td>
                          <td><?php echo @$value->location->label ?> - <?php echo @$value->location->address_text ?></td>
                          <td><?php 
                          if($value->cart->status=="completed")
                            {
                             echo date('F j, Y g:i a', strtotime($value->cart->completed_at)) ;
                            }else{
                              echo date('F j, Y g:i a', strtotime($value->cart->updated_at)) ;
                            }
                          ?></td>
                          <td>PHP<?php 
                                      if($value->cart->cart_type_id == 3 || $value->cart->cart_type_id == 5)
                                        {
                                          echo number_format(@$value->cart->basket->delivery_fee,2);
                                        }else
                                        {
                                          echo number_format($value->total_price, 2);
                                        }
                                                               
                                  ?> </td>
                          <td><button class="btn btn-xs btn-detail btn-success"
                            data-delivery_location='<?php echo json_encode($value->cart->delivery_location, JSON_HEX_QUOT|JSON_HEX_APOS) ?>'
                            data-pickup_location='<?php echo json_encode($value->cart->pickup_location, JSON_HEX_QUOT|JSON_HEX_APOS) ?>'
                            data-service_type='<?php echo $value->service_type ?>'
                            data-voucher='<?php echo json_encode(['vouchercode'=>$value->cart->voucher_code,'amount'=>$value->cart->voucher_amount], JSON_HEX_QUOT|JSON_HEX_APOS) ?>'
                            data-payment_method='<?php echo $value->cart->payment_method ?>'
                            data-rider='<?php echo json_encode($value->cart->rider, JSON_HEX_QUOT|JSON_HEX_APOS) ?>'
                            data-customer='<?php echo json_encode($value->cart->customer, JSON_HEX_QUOT|JSON_HEX_APOS) ?>'
                            data-last_location='<?php echo $value->cart->last_location ?>'
                            data-basket='<?php echo json_encode($value->cart->basket, JSON_HEX_QUOT|JSON_HEX_APOS) ?>' ><i class="fa fa-angle-double-down"></i> Details</button>

                          <?php if($value->cart->status =="pending"): ?>
                             <a class="btn btn-danger btn-cancel btn-xs" href="<?php echo base_url('cms/history/cancelorder/' . $value->cart_id . "/all_orders") ?>">Cancel</a>
                            <?php endif; ?>
                          </td>

                          </tr>
                        <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
            <?php else: ?>
              <center><h5>No ongoing booking.</h5></center>
            <?php endif; ?>

            </div>
          </section>
        </div>
      </div>


      <!-- Modal -->
      <div class="modal fade details-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Order details</h4>
            </div>
            <div class="modal-body">
             <h3>Order Information</h3>
             <hr>

             <h4>Customer Information</h4>
             <pre>Full name: <span class="clearme" id="customer_full_name"></span></pre>
             <pre>Location: <span class="clearme" id="customer_location"></span></pre>
             <pre>Mobile number: <span class="clearme" id="customer_mobile_num"></span></pre>
             <hr>

             <h4>Customer's Last Location Information</h4>
             <pre>Latitude: <span class="clearme" id="customer_last_lat"></span></pre>
             <pre>Longitude: <span class="clearme" id="customer_last_long"></span></pre>

             <hr>
             <h4>Rider Information</h4>
              <pre>Full name: <span class="clearme" id="rider_full_name"></span></pre>
              <pre>Vehicle model: <span class="clearme" id="rider_vehicle_model"></span></pre>
              <pre>Plate number: <span class="clearme" id="rider_plate_number"></span></pre>
              <pre>Mobile number: <span class="clearme" id="rider_mobile_number"></span></pre>

             <hr>
             <h4>Delivery Information</h4>
              <pre>Pickup location: <span class="clearme" id="pickup_location"></span></pre>
              <pre>Delivery location: <span class="clearme" id="delivery_location"></span></pre>
              <pre>Payment method: <span class="clearme" id="payment_method"></span></pre>


            <hr>
            <div class="basket-info">
               <hr>
               <h4>Basket Information</h4>
               <h5>Items</h5>
               <pre><span class="clearme" id="pre_items"></span></pre>
               <hr>
               <h5>All addons</h5>
               <pre><span class="clearme" id="pre_addons"></span></pre>
             </div>
            <div class="delivery-info">
               <hr>
               <h4>Get Delivery details</h4>
               <pre>Weight Capacity: <span class="clearme" id="delivery_weight_capacity"></span></pre>
               <pre>Item Category: <span class="clearme" id="delivery_item_category"></span></pre>
               <pre>Notes: <span class="clearme" id="delivery_notes"></span></pre>

             </div>

               <hr>
               <h4>Summary</h4>
               <pre>Estimate total amount — in PHP: <span class="clearme" id="pre_estimate_total_amount"></span></pre>
               <pre>Sub total (w/ addons) — in PHP: <span class="clearme" id="pre_sub_total_with_addons"></span></pre>
               <pre>Delivery price — in PHP: <span class="clearme" id="pre_delivery_price"></span></pre>
               <pre>Delivery fee — in PHP: <span class="clearme" id="pre_delivery_fee"></span></pre>
               <pre><label id="label_voucher">Voucher - in PHP:</label> <span class="clearme" id="pre_voucher_amount"></span></pre>
               <pre>Pabili fee — in PHP: <span class="clearme" id="pabili_fee"></span></pre>
               <pre>Grand total — in PHP: <span class="clearme" id="pre_grand_total"></span></pre>
          </div>
        </div>
      </div>
      <!-- modal -->



      <!-- page end-->
    </section>
  </section>


<script type="text/javascript">
  $(document).ready(function() {

    $('.btn-del').on('click', function(e){
      if (confirm('Are you sure you want to PERMANENTLY DELETE this order? This action is irreversible and cannot be undone, and may result in affecting order analytics.', 'Yes', 'No')) {
        return true;
      } else {
        e.preventDefault()
        return false;
      }
    })

    $('.btn-detail').on('click', function() {
      $('.modal').modal()


      var pickup_location = $(this).data('pickup_location')
      var delivery_location = $(this).data('delivery_location')
      var payment_method = $(this).data('payment_method')
      var basket = $(this).data('basket')
      var customer = $(this).data('customer')
      var rider = $(this).data('rider')
      var service_type = $(this).data('service_type')
      var last_lat = $(this).data('last_location').latitude
      var last_long = $(this).data('last_location').longitude
      var voucherdetails =  $(this).data('voucher')

      var grand_total = parseFloat(basket.grand_total) - parseFloat(voucherdetails.amount)

      var items = ""
      var addons = ""
      $('.clearme').text('')

      if ('items' in basket && service_type == 'pabili') {
        for (var i = 0; i < basket.items.length; i++) {
          items += basket.items[i].item_name + " " + basket.items[i].quantity + "x"

        }
        items = items.replace(/(^\s*,)|(,\s*$)/g, ''); // remove stray commas
      }

      if ('items' in basket && service_type == 'food' || service_type == 'grocery') {

        for (var i = 0; i < basket.items.length; i++) {
          items += basket.items[i].product_name + " " + basket.items[i].quantity + "x — PHP " + (basket.items[i].computed_price ? basket.items[i].computed_price.toFixed(2) : '' ) + ", "

          if ('addons' in basket.items[i]) {
            for (var g = 0; g < basket.items[i].addons.length; g++) {
              addons += basket.items[i].addons[g].product_name + " — PHP " + basket.items[i].addons[g].base_price.toFixed(2) + ", "
            }
            addons = addons.replace(/(^\s*,)|(,\s*$)/g, '');
          }
        }
        items = items.replace(/(^\s*,)|(,\s*$)/g, ''); // remove stray commas
      }

      $('#customer_full_name').text(customer.customer.full_name ? customer.customer.full_name : 'N/A')
      $('#customer_location').text(customer.order_info.pickup_location_label ? customer.order_info.pickup_location_label : 'N/A')
      $('#customer_mobile_num').text(customer.customer.mobile_num ? customer.customer.mobile_num : 'N/A')

      $('#customer_last_lat').text(last_lat ? last_lat  : 'N/A')
      $('#customer_last_long').text(last_long ? last_long  : 'N/A')

      $('#rider_full_name').text(rider.full_name ? rider.full_name : 'N/A')
      $('#rider_vehicle_model').text(rider.vehicle_model ? rider.vehicle_model : 'N/A')
      $('#rider_plate_number').text(rider.plate_number ? rider.plate_number : 'N/A')
      $('#rider_mobile_number').text(rider.mobile_num ? rider.mobile_num : 'N/A')

      $('#pickup_location').text(pickup_location ? pickup_location.label + " — " + pickup_location.address_text : 'N/A')
      $('#delivery_location').text(delivery_location ? delivery_location.label + " — " + delivery_location.address_text : 'N/A')
      $('#payment_method').text(payment_method ? payment_method : 'N/A')

      $('#pre_items').text(items? items : 'N/A')
      $('#pre_addons').text(addons? addons : 'N/A')
      $('#pre_delivery_fee').text(basket.delivery_fee ? basket.delivery_fee.toFixed(2) : 'N/A')
      $('#pre_delivery_price').text(basket.delivery_price ? basket.delivery_price.toFixed(2) : 'N/A')
      $('#pre_grand_total').text(grand_total ? grand_total.toFixed(2) : 'N/A')
      $('#pre_sub_total_with_addons').text(basket.sub_total_with_addons ? basket.sub_total_with_addons.toFixed(2) : 'N/A')
      $('#pre_estimate_total_amount').text(basket.estimate_total_amount ? basket.estimate_total_amount.toFixed(2) : 'N/A')
      $('#pabili_fee').text(basket.pabili_fee_additional ? basket.pabili_fee_additional.toFixed(2) : 'N/A')
      
      $('#label_voucher').text(voucherdetails.vouchercode ? "Voucher Code("+voucherdetails.vouchercode+") - in PHP: ": 'Voucher - in PHP:')
      $('#pre_voucher_amount').text(voucherdetails.amount ? voucherdetails.amount.toFixed(2) : '0.00')

      $('#delivery_notes').text(basket.notes ? basket.notes : 'N/A')
      $('#delivery_weight_capacity').text(basket.weight_capacity ? basket.weight_capacity : 'N/A')
      $('#delivery_item_category').text(basket.category ? basket.category : 'N/A')

      

      if (service_type == 'food' || service_type == 'grocery') {
        $('.basket-info').show()
        $('.delivery-info').hide();

      } else if (service_type == 'delivery') {
        $('.basket-info').hide()
        $('.delivery-info').show();
      } else if (service_type == 'pabili') {
        // $('.basket-info').hide()
        $('.delivery-info').hide();
      }
      // $('#prepre').text(JSON.stringify($(this).data('basket')))
    })

  });
</script>

<script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>

