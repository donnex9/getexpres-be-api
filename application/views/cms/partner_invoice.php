<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading" style="padding:15px">
            Partners Invoices
            <?php if ($flash_msg = $this->session->flash_msg): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>

            <?php
  
?>

          <button class="ripple customize-button bg-blue btn-fr" id="generate">
              <i  id="spinner" class="fa  fa-spin"></i>Generate
          </button>


          </header>
          <div class="panel-body">

          <div class="">
            <div class="card-native inline-layout">
              <div class="container-native">
                <b>Overdue</b>
                <p>₱0.00</p> 
              </div>
            </div>
            <div class="card-native inline-layout">
              <div class="container-native ">
               <b>Revenue this week</b>
                <p>₱0.00</p> 
              </div>
            </div>

            <div class="card-native inline-layout">
              <div class="container-native ">
               <b>Revenue last week</b>
                <p>₱0.00</p> 
              </div>
            </div>
              </div>

            <div class="row">

            
<br>
<div style="padding:10px;width:100%;">
  Search Invoices
</div>

<div style="padding:10px;width:100%;">
  <form class="form-inline">
  <div class="form-group">
    <input type="text" class="form-control" readonly="false" id="partners" placeholder="By Partner">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail2">From</label>
    <input type="date" class="form-control" id="from_date" >
  </div>
  <div class="form-group">
    <label for="exampleInputEmail2">To</label>
    <input type="date" class="form-control" id="from_date" >
  </div>
  <button type="submit" class="btn btn-primary ripple">Search</button>
</form>
</div>

<div style="padding:10px;width:100%;">

  <button class="customize-button color-grey ripple">ALL <span class="badge">0</span></button>
  <button class="customize-button color-green ripple">PAID <span class="badge">0</span></button>
   <button class="customize-button color-blue ripple">UNPAID <span class="badge">0</span></button>
    <button class="customize-button color-red ripple">OVERDUE <span class="badge">0</span></button>
  
</div>


<br>
            <div class="table-responsive" style="overflow: hidden; outline: none;" tabindex="1">
              <table class="table table-bordered" >
                <thead style="text-align: center">
                  <tr>
                    <td>Status</td>
                    <td>Due In</td>
                    <td>Merchant</td>
                    <td>Invoice Amount</td>
                    <td>Total Sales</td>
                    <td>Invoice No.</td>
                    <!-- <th>Merchant Total Sales (No Commission)</th> -->
                  </tr>
                </thead>
                <tbody>
                  <?php if (count($res2) > 0 ): ?>

                    <?php foreach ($res2 as $key => $value): ?>
                      <tr>
                        <!-- <th scope="row"><?php echo $value->cart_id ?></th> -->
                        <td><?php echo $value->status ?></td>
                         
                        <td><?php echo date('F j, Y ', strtotime($value->due_date)) ?> </td>
                        <td><?php echo $value->name ?> </td>
                        <td><?php echo "₱" .$value->invoice_amount ?></td>
                        <td><?php echo "₱" .$value->total_sales ?></td>
                        <td><?php echo $value->invoice_no  ?></td>
                      </tr>
                      <?php endforeach; ?>


                    <?php else: ?>
                      <tr>
                        <td colspan="6" style="text-align:center">No filters applied or empty result set.</td>
                      </tr>
                    <?php endif; ?>
                  </tbody>
                </table>

            </div>
          </section>
        </div>
      </div>
      <!-- page end-->
    </section>
  </section>



  <script type="text/javascript">
    $(document).ready(function() {

    $.fn.baseurl = function()
    {
      let baseUrl = $("#base_url").val();
       return baseUrl
    }

      $("#generate").on('click',function(){
          // $("#spinner").addClass("fa-spinner")
           $(this).prop('disabled', true);

         //  alert("hello")
      data = {}
      
      $(this).ajaxPostGenerate("cms/partners/generateInvoices",data)

      })


 $.fn.ajaxPostGenerate = function(url,dataPost)
 {
   
   let baseurl = $(this).baseurl()
    $.ajax({
    type: "POST",
    url: baseurl + url,
    data:dataPost,
    beforeSend: function() {
         $("#spinner").addClass("fa-spinner")
    },
    success: function(data) {
       console.log(data);
        $("#spinner").removeClass("fa-spinner")
        $("#generate").prop('disabled', false);
    }
    });

     return false; 
 }

    });
  </script>

  <script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>
