<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-4">
        <section class="panel">
          <header class="panel-heading">
            Stores/Customers radius
            <?php if ($flash_msg  && @$_GET['meta_key'] == 'store_radius'): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
          </header>
          <div class="panel-body">
            <form action="<?php echo base_url('cms/dashboard/updateStoreRadius_editor') ?>" method="post">
              <input type="hidden" name="meta_key" value="store_radius">
              <input type="hidden" name="from" value="cms/dashboard/radius_editor">
              <label>
                Maximum radius range in Meters (M)
              </label>
              <input class="form-control" type="number" step="0.01" name="meta_value" value="<?php echo $stores_radius ?>">
              <br>
              <input class="btn btn-info btn-md" type="submit" value="Update">
            </form>
            </div>
          </section>
        </div>
      </div>
    <div class="row">
      <div class="col-lg-4">
        <section class="panel">
          <header class="panel-heading">
            Map center
            <?php if ($flash_msg  && @$_GET['meta_key'] == 'map_center'): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
          </header>
          <div class="panel-body">
            <form action="<?php echo base_url('cms/dashboard/updateStoreRadius_editor') ?>" method="post">
              <input type="hidden" name="meta_key" value="map_center">
              <input type="hidden" name="from" value="cms/dashboard/radius_editor">
              <label>
                Map Center (latitude,longitude)
              </label>
              <input class="form-control" type="text" name="meta_value" value="<?php echo $map_center ?>">
              <br>
              <input class="btn btn-info btn-md" type="submit" value="Update">
            </form>
            </div>
          </section>
        </div>
      </div>


      <!-- page end-->
    </section>
  </section>


<script type="text/javascript">
  $(document).ready(function() {


  });
</script>

<script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>
