<style>
  .ms-container {
    width: 100% !important;
  }
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/admin/') ?>assets/jquery-multi-select/css/multi-select.css" />
<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            <h4><i class="fa fa-plus"></i> Manually Add Wallet Balance To Rider</h4>
          </header>
        </section>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6">
        <section class="panel">
          <header class="panel-heading">
            <!-- Scheduled Surge -->
            <?php if ($flash_msg = $this->session->flash_msg): ?>
              <h4 style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></h4>
            <?php endif; ?>
          </header>
          <div class="panel-body">
            <form action="<?php echo base_url('cms/riders/add_rider_balance_post') ?>" method="post" id="riderswallet">

              <label>
                Riders
                <br>
                <sub>Choose riders to give wallet balance (Multiple selections are ALLOWED)</sub>
              </label>
              <select multiple="multiple" class="multi-select" name="rider_ids[]">
                <?php foreach ($res as $key => $value): ?>
                    <option value="<?php echo $value->id ?>"><?php echo $value->full_name . " @ " . "($value->balance PHP)"  ?></option>
                <?php endforeach; ?>
              </select>
              <br>
              <label>Amount to Add to Wallet
                <br>
                <sub>Enter numeric value to add to Rider's wallet (in Philippine Peso) Example: 200</sub>
              </label>
              <br>
              <input class="form-control" type="number" name="amount" step="0.01" value="" >
              <br>
              <input class="btn btn-info btn-md" type="submit" value="Add Balance">
            </form>
            </div>
          </section>
        </div>

        <div class="row">
      <div class="col-lg-4">
        <section class="panel">
          <header class="panel-heading">
            Balance Management
            <?php if ($flash_msg  && @$_GET['meta_key'] == 'minimum_wallet'): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
          </header>
          <div class="panel-body">
            <form action="<?php echo base_url('cms/dashboard/update_option') ?>" method="post" id="minwall">
              <input type="hidden" name="meta_key" value="minimum_wallet">
              <input type="hidden" name="from" value="cms/riders/add_rider_balance">
              <label>
                Balance Options
                <br>
                <small>Set minimum balance for Riders.</small>
              </label>
              <input class="form-control" type="text" name="meta_value" value="<?php echo $minimum_wallet ?>">
              <br>
              <input class="btn btn-info btn-md" type="submit" value="Update Minimum Wallet">
            </form>
            </div>
          </section>
        </div>
      </div>


    </section>
  </section>


<script type="text/javascript">
  $(document).ready(function() {

    $('#riderswallet').on('submit',function(event) {
      if (confirm('Are you sure you want to add wallet balance to these riders?')) {
        return true
        $('input[type=submit]').prop('disabled', true)
      } else {
        event.preventDefault();
        return false;
      }

      /* Act on the event */
    });

    $('#minwall').on('submit',function(event) {
      if (confirm('Confirm update of Minimum Balance?')) {
        return true
        $('input[type=submit]').prop('disabled', true)
      } else {
        event.preventDefault();
        return false;
      }

      /* Act on the event */
    });

    $('.multi-select').multiSelect({
        selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='Search Rider...'>",
        selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='Search Rider...'>",
        afterInit: function (ms) {
            var that = this,
                $selectableSearch = that.$selectableUl.prev(),
                $selectionSearch = that.$selectionUl.prev(),
                selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function (e) {
                    if (e.which === 40) {
                        that.$selectableUl.focus();
                        return false;
                    }
                });

            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function (e) {
                    if (e.which == 40) {
                        that.$selectionUl.focus();
                        return false;
                    }
                });
        },
        afterSelect: function () {
            this.qs1.cache();
            this.qs2.cache();
        },
        afterDeselect: function () {
            this.qs1.cache();
            this.qs2.cache();
        }
    });


  });
</script>

<script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/admin/') ?>assets/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/admin/') ?>assets/jquery-multi-select/js/jquery.quicksearch.js"></script>
