<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-4">
        <section class="panel">
          <header class="panel-heading">
            Get Car
            <?php if ($flash_msg  && @$_GET['meta_key'] == 'car_fare'): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
          </header>
          <div class="panel-body">
            <form action="<?php echo base_url('cms/dashboard/update_option') ?>" method="post">
              <input type="hidden" name="meta_key" value="car_fare">
              <input type="hidden" name="from" value="cms/dashboard/fare_management">
              <label>
                PHP
              </label>
              <input class="form-control" type="number" step="0.01" name="meta_value" value="<?php echo $car_fare ?>">
              <br>
              <input class="btn btn-info btn-md" type="submit" value="Update fee">
            </form>
            </div>
          </section>
        </div>
      </div>
    <div class="row">
      <div class="col-lg-4">
        <section class="panel">
          <header class="panel-heading">
            Get Delivery
            <?php if ($flash_msg  && @$_GET['meta_key'] == 'delivery_fare'): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
          </header>
          <div class="panel-body">
            <form action="<?php echo base_url('cms/dashboard/update_option') ?>" method="post">
              <input type="hidden" name="meta_key" value="delivery_fare ">
              <input type="hidden" name="from" value="cms/dashboard/fare_management">
              <label>
                PHP
              </label>
              <input class="form-control" type="number" step="0.01" name="meta_value" value="<?php echo $delivery_fare ?>">
              <br>
              <input class="btn btn-info btn-md" type="submit" value="Update fee">
            </form>
            </div>
          </section>
        </div>
      <div class="col-lg-4">
        <section class="panel">
          <header class="panel-heading">
            Get Delivery per KM
            <?php if ($flash_msg  && @$_GET['meta_key'] == 'delivery_per_km'): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
          </header>
          <div class="panel-body">
            <form action="<?php echo base_url('cms/dashboard/update_option') ?>" method="post">
              <input type="hidden" name="meta_key" value="delivery_per_km ">
              <input type="hidden" name="from" value="cms/dashboard/fare_management">
              <label>
                PHP
              </label>
              <input class="form-control" type="number" step="0.01" name="meta_value" value="<?php echo $delivery_per_km ?>">
              <br>
              <input class="btn btn-info btn-md" type="submit" value="Update fee">
            </form>
            </div>
          </section>
        </div>
      </div>







      <!-- page end-->
    </section>
  </section>


<script type="text/javascript">
  $(document).ready(function() {


  });
</script>

<script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>
