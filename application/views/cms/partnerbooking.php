<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            Manual Booking

            <input hidden="true" type="text" id="base_url" value="<?php echo base_url();?>">
             <input hidden="true" type="text" id="al" value="<?php echo $assigned_location;?>">
            
          </header>
          <div class="panel-body">
            <!--tab nav start-->
            <section class="panel">
              <div class="w3-row">
                <div class="w3-col m6 ">
                  <img class="sb-title-icon" src="https://fonts.gstatic.com/s/i/googlematerialicons/location_pin/v5/24px.svg" alt="">
                  <span class="sb-title">ROUTE</span>
                  
                  <div class="w3-panel w3-round " style="background: #fafffe">
                    
                    <table class="w3-table tableroute">
                      <tr>
                        <td >
                          <input id="tdPickUpLocation" class="w3-input w3-animate-input" type="text"  placeholder="Pick Up" >
                           <span id="tdPickUpLabel"></span>
                           <input type="text" hidden="true" id="senderlatitude">
                           <input type="text" hidden="true" id="senderlongitude">
                        </td>
                        
                     </tr>
                     
                     <tr>
                       <td>
                          <input id="tdDestination" class="w3-input w3-animate-input" type="text"  placeholder="Destination">
                           <span id="tdDestinationLabel"></span>
                           <input type="text" hidden="true" id="recipientlatitude">
                           <input type="text" hidden="true" id="recipientlongitude">
                        </td>
                     </tr>
                    </table>
                  </div>
                <div id="customer">
                    <img class="sb-title-icon" src="<?php echo base_url('public/admin/assets/svg/') ?>people.svg" alt=""/>
                  <span class="sb-title">CUSTOMER'S NAME</span>
                  <div class="form-group">
                      <br>
                     
                      <select class="form-control" id="selectedcustomer">
                        <?php $cnt=0; foreach($customers as $key=>$value): ?>
                        
                        <option value="<?php echo $value->id?>">
                         <?php echo $value->full_name; $cnt++;?>
                        </option>

                        <?php endforeach; ?>
                        
                      </select>
                    <?php if($cnt <= 0): ?>
                      <span class="asterisk"> Please inform administrator to bind customer account.</span>
                    <?php endif; ?>  
                    <input type="text" id="cusmeraccount" hidden="true" value="<?php echo $cnt ?>">
                  </div>

                </div>  
                  
                <div id="services">
                   <img class="sb-title-icon" src="<?php echo base_url('public/admin/assets/svg/') ?>delivery_dining_black_24dp.svg" alt=""/>
                  <span class="sb-title">SERVICES</span>
                  <input type="text" hidden="true" id="selectedservices">
                 <br>  <br>
                 <div>
                      <div class="checkbox-inline " id="divdelivery">
                        <button id="btndelivery" class="btn btn-primary" data-toggle="tooltip" 
                         data-vehicles='<?php echo json_encode(["vehicles"=>$availableVehicles], JSON_HEX_QUOT|JSON_HEX_APOS) ?>'
                          data-placement="right" title="Get Delivery">
                            <img  class="servicesicon" src="<?php echo base_url('public/admin/assets/svg/') ?>delivery-icon.png" alt="Delivery"/>
                        </button>
                         
                      </div>
                      <div class="checkbox-inline" id="divpabili">
                          <button id="btnpabili" class="btn btn-primary" data-toggle="tooltip" 
                          data-vehicles='<?php echo json_encode(["vehicles"=>$availableVehicles], JSON_HEX_QUOT|JSON_HEX_APOS) ?>' data-placement="right" title="Get Pabili">
                            <img  class="servicesicon" src="<?php echo base_url('public/admin/assets/svg/') ?>pabili-icon.png" alt=""/>
                        </button>
                      </div>
                  </div>
                  <br>
                </div>

                <div id="vehicles">
                   <img class="sb-title-icon" src="<?php echo base_url('public/admin/assets/svg/') ?>two_wheeler_black_24dp.svg" alt=""/>
                  <span class="sb-title">VEHICLES</span>
                  <input type="text"  hidden="true" id="selectedvehicle" name="">
                  <br><br>
                  <div id="activeVehicles">
                      
                  </div>
                <br><br>
                  <div class="divtotal alert alert-warning" >
                    <input type="text" id="totaltxt" hidden="true" class="pull-left" name="">
                      <label  id="labeltotal">₱0</label>
                  </div>
                 
                 <br> <input type="text" id="cartidtxt" hidden="true" name="">
                  <div class="divtotal">
                      <button id="btnreview"  class="btn btn-info pull-right hidden" >Review Order</button>
                  </div>
                </div> 
                  
                  
                </div>

                <div class="w3-col m6">
                  <div id="divSenderInformation">
                   <div class="w3-card-4 ">
                      <div class="w3-container w3-cyan ">
                        <h2>Sender  Information</h2>
                      </div>
                      <div class="w3-container" >
                        <p>      
                        <label class="w3-text-brown"><b>Name <span class="asterisk">*</span></b></label>
                        <input class="w3-input w3-border w3-sand" id="sendername" type="text">
                        <span id="sendernameerror" class="asterisk" ></span>
                       </p>
                        <p>      
                        <label class="w3-text-brown"><b>Contact Number<span class="asterisk">*</span></b></label>
                        <input class="w3-input w3-border w3-sand" id="sendercontact" type="text">
                        <span id="senderconerror" class="asterisk" ></span>
                       </p>
                        <p>      
                        <label class="w3-text-brown"><b>Block/Floor/Room</b></label>
                        <input class="w3-input w3-border w3-sand" id="senderblock" type="text"></p>
                        <p>
                            <button id="btnSenderSave" class="btn btn-primary" >Save</button>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div id="divrecipientInformation">
                   <div class="w3-card-4 ">
                      <div class="w3-container w3-cyan ">
                        <h2>Recipient  Information</h2>
                      </div>
                      <form class="w3-container" >
                        <p>      
                         <label class="w3-text-brown"><b>Name <span class="asterisk">*</span></b></label>
                        <input class="w3-input w3-border w3-sand" id="recipientname" type="text">
                        <span id="recipientnameerror" class="asterisk" ></span>
                       </p>
                        <p>      
                        <label class="w3-text-brown"><b>Contact Number<span class="asterisk">*</span></b></label>
                        <input class="w3-input w3-border w3-sand" id="recipientcontact" type="text">
                        <span id="recipientconerror" class="asterisk" ></span>
                        </p>
                        <p>      
                        <label class="w3-text-brown"><b>Block/Floor/Room</b></label>
                        <input class="w3-input w3-border w3-sand" id="recipientblock" type="text"></p>
                        <p>
                        <button  type="button" id="btnRecipientSave" class="btn btn-primary" >Save</button></p>
                      </form>
                    </div>
                  </div>

                  <div id="divDeliveryDetails">
                   <div class="w3-card-4 ">
                      <div class="w3-container w3-cyan ">
                        <h2>Delivery Details</h2>
                      </div>
                      <form class="w3-container" >
                        <p>      
                         <label class="w3-text-brown"><b>Package Options <span class="asterisk">*</span></b></label>
                        
                        <?php $arrpackageOptionns = explode(",",$package_options) ?>
                          <?php foreach ($arrpackageOptionns as $keyval):?> 

                          <div class="form-check">
                          <input class="form-check-input" type="radio" name="packageoption"  
                          value="<?php echo $keyval; ?>" >
                          <label class="form-check-label" >
                            <?php echo $keyval; ?>
                          </label>
                           </div>

                          <?php endforeach; ?>
                         
                        <p>      
                         <label class="w3-text-brown"><b>Delivery Instructions </b></label>
                         <div class="mb-3">
                          <textarea class="form-control" id="deliveryinstruction" rows="3"></textarea>
                        </div>

                        <span id="recipientnameerror" class="asterisk" ></span>
                       </p>
                      </form>
                    </div>
                  </div>

                  <div id="divAssignaRider">
                   <div class="w3-card-4 ">
                      <div class="w3-container w3-cyan ">
                        <h2>Assign Rider</h2>
                      </div>
                      <form class="w3-container" >
                        <p>      
                         <label class="w3-text-brown"><b>Search Rider </b></label>
                        
                        <div class="input-group">
                            
                            <input id="searchrider" type="text" class="form-control" placeholder="Ex. Juan dela Cruz">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        </div><br>
                        <div id="riderresult">
                            
                        </div>
                        <br>
                        
                      </form>
                    </div>
                  </div>

                  <div id="divPabili">
                   <div class="w3-card-4 ">
                      <div class="w3-container w3-cyan ">
                        <h2>Add Pabili Items</h2>
                      </div>
                      <div class="w3-container" >
                        <p>      
                         <label class="w3-text-brown"><b>Pabili Items <span class="asterisk">*</span> </b></label>
                        
                        <div class="input-group">
                            
                            <input id="pabilitxt" type="text" class="form-control" placeholder="Ex. Pork BBQ 10 pcs.">
                            <span id="btnaddpabili" class="input-group-addon"><i class="glyphicon glyphicon-plus"></i></span>
                        </div><br>
                        <div class="w3-container">
                         
                          <ul class="w3-ul w3-card-4" id="pabilItems">
                            
                            
                          </ul>
                        </div>
                        <br>
                        <p>      
                         <label class="w3-text-brown"><b>Estimated total amount <span class="asterisk">*</span></b></label>
                         <div class="mb-3">
                          <input class="form-control" id="estimatedamount" placeholder="2000" >
                        </div>

                       </p>
                        <p>      
                         <label class="w3-text-brown"><b>Pabili Instructions </b></label>
                         <div class="mb-3">
                          <textarea class="form-control" id="pabiliinstruction" rows="3"></textarea>
                        </div>

                       </p>
                        
                      </div>
                    </div>
                  </div>

                  <div id="divOrderSummary">
                   <div class="w3-card-4 ">
                      <div class="w3-container w3-cyan ">
                        <h2>Order Summary</h2>
                      </div>
                      <div class="w3-container table-responsive" ><br>
                        <table class="table">
                            
                            <tr>
                                <td>Pick Up Address</td>
                                <td>
                                    <input id="tdPickUpLocationSummary" class="w3-input w3-animate-input " type="text" readonly="true">
                           <span id="tdPickUpLabelSummary"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Destination Address</td>
                                <td>
                                  <input id="tdDestinationSummary" class="w3-input w3-animate-input" type="text"  readonly="true">
                           <span id="tdDestinationLabelSummary"></span>
                                </td>
                            </tr>

                            <tr>
                                <td>Customer's Name</td>
                                <td>
                                  <span id="customernameSummary"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Service Type</td>
                                <td>
                                  <span id="servicesSummary"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Vehicle</td>
                                <td>
                                  <span id="vehicleSummary"></span>
                                </td>
                            </tr>
                            <tr>
                                <td><span id="itemtitleSummary"></span></td>
                                <td>
                                  <span id="itemSummary"></span>
                                </td>
                            </tr>
                            <tr id="tresmitatedpabiliamount">
                                <td>Estimated Amount</td>
                                <td><span id="estimatedSummary"></span></td>
                            </tr>
                            <tr>
                                <td>Assigned Rider</td>
                                <td>
                                  <span id="assignedriderSummary"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Instruction to rider</td>
                                <td><span id="instructionSummary"></span></td>
                             </tr>
                             <tr>
                                <td>Delivery Fee</td>
                                <td><span id="deliveryfeeSummary"></span></td>
                             </tr>
                        </table>
                           <div class="text-center"> ***************************************************</div>
                        <p>
                            <button id="btncheckout"  class="btn btn-info pull-right" >Check Out</button>
                        </p>
                        <br><br>
                        
                      </div>
                    </div>
                  </div>
                  
  
                </div>

            </section>

          
            </div>
          </section>
        </div>

        <div id=map></div>
          
        
          


      </div>

    <div class="modal fade" id="modalAlert" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><span id="headertitlemodal"></span></h4>
        </div>
        <div class="modal-body">
          <p id="contentitlemodal"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
 <div id="loading" style="display:none"><img src="<?php echo base_url('public/admin/assets/svg/') ?>loadingdots.gif"></div>



      <!-- page end-->
    </section>
  </section>


<script type="text/javascript">

  $(document).ready(function() {
    let pabiliItems = []
    let customerlen = $("#cusmeraccount").val()
    $('[data-toggle="tooltip"]').tooltip();
    
    if($("#divSenderInformation").is(':visible')){ $("#sendername").focus();}
    if($("#divrecipientInformation").is(':visible')){ $("#recipientname").focus();}

    

    if(customerlen <=0)
    {
     $("#btndelivery").addClass('hidden');
     $("#btnpabili").addClass('hidden');
    }
    

    $("#sendername").on('keyup',function(){
        $(this).spanclear("sendernameerror")
    })

    $("#sendercontact").on('keyup',function(){
        $(this).spanclear("senderconerror")
    })

    $("#recipientname").on('keyup',function(){
        $(this).spanclear("recipientnameerror")
    })

    $("#recipientcontact").on('keyup',function(){
        $(this).spanclear("recipientconerror")
    })

    $("#selectedcustomer").on('change',function()
    {
        var optionSelected = $("option:selected", this);
        let name = optionSelected.text()
        $("#customernameSummary").html(name)

    })

    $("#btnpabili").on('click',function(){

        let vehicleservice = $(this).data("vehicles").vehicles
        let allActiveVehicles = []
        let pickupLocation =  $(this).deliverlocation()
        let destinationLocation = $(this).destination()

        $("#selectedservices").val("3")
        $("#activeVehicles").html("")
        $("#deliveryinstruction").text("")
        $("#btnreview").addClass("hidden")
        $("#servicesSummary").html("")

        $("#btndelivery").removeClass("btn-warning")
        $("#btndelivery").addClass("btn-primary")
        $("#divDeliveryDetails").hide()
        $("#divAssignaRider").hide()
        $("#divOrderSummary").hide()
        
  if($.isNumeric(pickupLocation.length) == false && $.isNumeric(destinationLocation.length) == false){
        
            $(this).removeClass("btn-primary")
            $(this).addClass("btn-warning")
            $("#servicesSummary").html("Get Pabili")
            $("#divPabili").show()

            $.each(vehicleservice,function(i,val){
            if(val.service_id == 3)
            {
                allActiveVehicles.push(val.vehicle_id)

             $("#activeVehicles").append(`<div class="checkbox-inline "><button class="btn btn-primary" id="vehi${val.vehicle_id}" data-vehicleid ="${val.vehicle_id}"  class="btn btn-primary" data-vehiclename="${val.name}" data-toggle="tooltip" data-placement="right" title="${val.name}"><img src="${val.image}" class='servicesicon' ></button> </div>`)
            }  
          })

        }
        
        $('[data-toggle="tooltip"]').tooltip();
    })

    
 $('#pabilitxt').keyup(function(event){
        
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        //event.preventDefault();
         let pabili = $(this).val();
         if(pabili != ""){
            pabiliItems.push(pabili)
              $("#pabilItems").html("")

              $.each(pabiliItems, function(i,item){
                $("#pabilItems").append(`<li class="w3-display-container">${item} <span  class="w3-button w3-transparent w3-display-right" data-itemid="${i}">&times;</span></li>`);
              })
         }

         $("#pabilitxt").val("");
         $("#pabilitxt").focus();
    }
  });

    $("#btnaddpabili").on('click',function()
    {
     
      let pabili = $("#pabilitxt").val();
     if(pabili != ""){
        pabiliItems.push(pabili)
          $("#pabilItems").html("")

          $.each(pabiliItems, function(i,item){
            $("#pabilItems").append(`<li class="w3-display-container">${item} <span  class="w3-button w3-transparent w3-display-right" data-itemid="${i}">&times;</span></li>`);
          })
     }
      
      $("#pabilitxt").val("");
      $("#pabilitxt").focus();
    })

    $("#pabilItems").on("click","span", function(){
        let itemid =  $(this).attr("data-itemid");
      //  array.splice(pabiliItems, itemid);
        pabiliItems.splice(itemid,1);
        $("#pabilitxt").focus();
        $("#pabilItems").html("")
        $.each(pabiliItems, function(i,item){
        $("#pabilItems").append(`<li class="w3-display-container">${item} <span  class="w3-button w3-transparent w3-display-right" data-itemid="${i}">&times;</span></li>`);
      })
    })

    $("#btndelivery").on('click',function(){

        let vehicleservice = $(this).data("vehicles").vehicles
        let allActiveVehicles = []
        let pickupLocation =  $(this).deliverlocation()
        let destinationLocation = $(this).destination()

        $("#servicesSummary").html("")
        $("#selectedservices").val("4")
        $("#deliveryinstruction").val("")
        
        $("#activeVehicles").html("")
        $("#btnpabili").removeClass("btn-warning")
        $("#btnpabili").addClass("btn-primary")
        
        $("#divPabili").hide()
        $("#btnreview").addClass("hidden")
        $("#divAssignaRider").hide()
        $("#divOrderSummary").hide()


      if($.isNumeric(pickupLocation.length) == false && $.isNumeric(destinationLocation.length) == false){
        $("#divDeliveryDetails").show()
        $("#servicesSummary").html("Get Delivery")
        $(this).removeClass("btn-primary")
        $(this).addClass("btn-warning")

        $.each(vehicleservice,function(i,val){
            
            if(val.service_id == 4)
            {
                allActiveVehicles.push(val.vehicle_id)

             $("#activeVehicles").append(`<div class="checkbox-inline "><button class="btn btn-primary" id="vehi${val.vehicle_id}" data-vehicleid ="${val.vehicle_id}"  class="btn btn-primary" data-vehiclename="${val.name}" data-toggle="tooltip" data-placement="right" title="${val.name}"><img src="${val.image}" class='servicesicon' ></button> </div>`)
            }  
        })

        $('[data-toggle="tooltip"]').tooltip();

      }
        
    })

    $('#btncheckout').on('click', function() {
      let cartid = $("#cartidtxt").val();
      let customerid = $("#selectedcustomer").val()
      let rider  = $('input[name="rider"]:checked').val()?$('input[name="rider"]:checked').val():0;
      let total  = $.trim($("#totaltxt").val());
      let data = []
      data = {'cartid':cartid,"riderid":rider,"total":total,"customer_id":customerid}

     $(this).ajaxpostrequestbooknow("cms/booking/booknow",data)

    })

    $("#btnreview").on('click',function()
    {
        let ridername  = $('input[name="rider"]:checked').data("name")?$('input[name="rider"]:checked').data("name"):"No rider assigned";
        $("#assignedriderSummary").html(ridername)
        $("#divOrderSummary").show();
        $("#divDeliveryDetails").hide();
        $("#divAssignaRider").hide();
        $("#divPabili").hide()
        $(this).addClass("hidden");

    })
    
    $('#activeVehicles').on('click', 'button', function() {
       
       let btnid = $(this).attr("id");
       let numberOfVehicles = 20 
       let i=0
       let serviceType = $("#selectedservices").val()
       let al = $("#al").val()
       let tdDestination =  $(this).destination()
       let tdPickUpLocation = $(this).deliverlocation()
       let customersname = $( "#selectedcustomer option:selected" ).text();
       let packageOption = $('input[name="packageoption"]:checked').val()?$('input[name="packageoption"]:checked').val():"";
       //let isCheckPackage = $('input[name="packageoption"]:checked');
       let data = []
       let vehicleid = $(this).data("vehicleid")
       let vehiclename = $(this).data("vehiclename")

       $("#vehicleSummary").html(vehiclename)
       $("#selectedvehicle").val(vehicleid)
       let customers = $("#selectedcustomer").val()
       let notes = $("#deliveryinstruction").val()
       let estimatedamount = $("#estimatedamount").val()?$("#estimatedamount").val():0
       $("#customernameSummary").html(customersname)
       $("#estimatedSummary").html(estimatedamount)
       $("#instructionSummary").html(notes)
       $("#divOrderSummary").hide()

       data = {'cart_type_id':serviceType,"customer_id":customers,"vehicle_id":vehicleid,"delivery_location":tdDestination,"pickup_location":tdPickUpLocation,
            "assigned_location":al,"notes":notes,"category":packageOption}
       
       $(this).removeClass("btn-primary")
       $(this).addClass("btn-warning")
      //  $(this).trigger('click')
       $(this).notselectedVehicles(vehicleid)
        
       if(packageOption != "" && serviceType == 4)
       {
        $("#itemtitleSummary").html("Package for Delivery")
        $("#itemSummary").html(packageOption)
        $("#loading").show()
        $(this).ajaxpostrequest("cms/booking/initialize_cart",data)
       }else if(pabiliItems.length > 0 && serviceType == 3 && estimatedamount >0)
       {
        let itemslist = "";
        notes = $("#pabiliinstruction").val()
        $("#instructionSummary").html(notes)
        $("#itemtitleSummary").html("Pabili Items")
        $("#itemSummary").html(packageOption)

        data = {'cart_type_id':serviceType,"customer_id":customers,"vehicle_id":vehicleid,"delivery_location":tdDestination,"pickup_location":tdPickUpLocation,
            "assigned_location":al,"notes":notes,"items_name":pabiliItems,
            "estimate_total_amount":estimatedamount}
        
  
        itemslist = "<ol>"
        $.each(pabiliItems,function(i,item)
        {
          itemslist +=`<li>${item}</li>`
        })

        itemslist += "</ol>"
        $("#itemSummary").html(itemslist)

        $("#loading").show()
        $(this).ajaxpostrequestpabili("cms/booking/initialize_cart_pabili",data)
           //console.log(pabiliItems)
       }else
       {
         
         if(serviceType == 4)
         {
            $("#headertitlemodal").html("Package Option");
            $("#contentitlemodal").html("Please select Package Option.");
         }else
         {
           if(estimatedamount <=0)
           {
            $("#headertitlemodal").html("Estimated Amount");
            $("#contentitlemodal").html("Please enter estimated amount."); 
            $("#estimatedamount").focus()
           }else
           {
            $("#pabilitxt").focus();
            $("#headertitlemodal").html("Pabili Items");
            $("#contentitlemodal").html("Please add pabili item"); 
           }
            
         }
        
        $("#modalAlert").modal("show");
        $(this).removeClass("btn-warning")
        $(this).addClass("btn-primary")
       }
       
      // console.log(packageOption)
    
       
      
    })
   
    $("#btnSenderSave").on('click',function(){
        let hasError = false;
        let name = $("#sendername").val()
        let contactNum = $("#sendercontact").val()
        let blockAddress = $("#senderblock").val()

        let validNumber = $("#sendercontact").phonenumber(contactNum)
        if(name == "" || name == null)
        {
            $("#sendernameerror").html("This is required.")
            hasError = true;
        }
       
        if(contactNum  == "" || contactNum  == null)
        {
            $("#senderconerror").html("This is required.")
            hasError = true;
        }

        if(validNumber == false && contactNum !="")
        {
            $("#senderconerror").html("Invalid contact number.")
            hasError = true;
        }

        if(hasError == false)
        {
          $("#tdPickUpLabel").html(name+"|"+contactNum+"|"+blockAddress);
          $("#divSenderInformation").hide(); 
          $("#tdPickUpLocationSummary").val($("#tdPickUpLocation").val())
          $("#tdPickUpLabelSummary").html(name+"|"+contactNum+"|"+blockAddress)
        }
    })


    $("#btnRecipientSave").on('click',function(){
        let hasError = false;
        let name = $("#recipientname").val()
        let contactNum = $("#recipientcontact").val()
        let blockAddress = $("#recipientblock").val()

        let validNumber = $("#recipientcontact").phonenumber(contactNum)
        if(name == "" || name == null)
        {
            $("#recipientnameerror").html("This is required.")
            hasError = true;
        }
       
        if(contactNum  == "" || contactNum  == null)
        {
            $("#recipientconerror").html("This is required.")
            hasError = true;
        }

        if(validNumber == false && contactNum !="")
        {
            $("#recipientconerror").html("Invalid contact number.")
            hasError = true;
        }

        if(hasError == false)
        {
          $("#tdDestinationLabel").html(name+"|"+contactNum+"|"+blockAddress);
          $("#divrecipientInformation").hide(); 
          $("#tdDestinationSummary").val($("#tdDestination").val())
          $("#tdDestinationLabelSummary").html(name+"|"+contactNum+"|"+blockAddress)
        }
    })

  $.fn.deliverlocation = function()
  {
    let dl = []
    let senderLat = $("#senderlatitude").val()
    let senderLon = $("#senderlongitude").val()
    let senderName =  $("#sendername").val()
    let senderAddress = $("#senderblock").val()+", "+$("#tdPickUpLocation").val() 
    let contactNum = $("#sendercontact").val()
    let labelvalue = $("#tdPickUpLabel").html()
    let address = $("#tdPickUpLocation").val() 
    
    if(address == "" )
    {
        $("#headertitlemodal").html("Invalid Pick Up Address");
        $("#contentitlemodal").html("Please select from the auto suggested address."); 
        $("#tdPickUpLocation").focus()
        $("#modalAlert").modal("show");

    }
    else if(senderLat == "" && senderLon == "")
    {
        $("#headertitlemodal").html("Invalid Pick Up Address");
        $("#contentitlemodal").html("Please select from the auto suggested address."); 
        $("#tdPickUpLocation").focus()
        $("#modalAlert").modal("show");

    }else if(senderName == "")
    {
       $("#headertitlemodal").html("Sender Information");
        $("#contentitlemodal").html("Please fill out required field."); 
        $("#tdPickUpLocation").focus()
        $("#modalAlert").modal("show");
        $("#divSenderInformation").show()
        $("#divrecipientInformation").hide()

    }else if (labelvalue == "")
    {
        $("#headertitlemodal").html("Sender Information");
        $("#contentitlemodal").html("Please save sender information"); 
        $("#tdPickUpLocation").focus()
        $("#modalAlert").modal("show");
        $("#divSenderInformation").show()
        $("#divrecipientInformation").hide()

    }else
    {
        dl = {
            "label":senderName,
            "address_text":senderAddress,
            "latitude":senderLat,
            "longitude":senderLon,
            "landmark":"",
            "contact_number":contactNum
        }
    }
    
        /*dl = {"label":"5","address_text":"25 T. Evangelista, Lungsod Quezon, Kalakhang Maynila, Philippines","latitude":"14.6340026","longitude":"121.0650074","landmark":"","contact_number":"2"}*/

    return dl
  } 

  $.fn.destination = function()
  {
    let recl = []
    let recLat = $("#recipientlatitude").val()
    let recLon = $("#recipientlongitude").val()
    let recName =  $("#recipientname").val()
    let recAddress = $("#recipientblock").val()+", "+$("#tdDestination").val() 
    let contactNum = $("#recipientcontact").val()
    let address = $("#tdDestination").val() 
    let labelvalue = $("#tdDestinationLabel").html()
    
    if(address == "")
    {
     $("#headertitlemodal").html("Invalid Destination Address");
        $("#contentitlemodal").html("Please select from the auto suggested address."); 
        $("#tdDestination").focus()
        $("#modalAlert").modal("show");
    }
    else if(recLat == "" && recLon == "")
    {
        $("#headertitlemodal").html("Invalid Destination Address");
        $("#contentitlemodal").html("Please select from the auto suggested address."); 
        $("#tdDestination").focus()
        $("#modalAlert").modal("show");

    }else if(recName == "")
    {
       $("#headertitlemodal").html("Recipient Information");
        $("#contentitlemodal").html("Please fill out required field."); 
        $("#tdDestination").focus()
        $("#modalAlert").modal("show");
        $("#divSenderInformation").hide()
        $("#divrecipientInformation").show()

    }else if (labelvalue == "")
    {
        $("#headertitlemodal").html("Recipient Information");
        $("#contentitlemodal").html("Please save recipient information"); 
        $("#tdDestination").focus()
        $("#modalAlert").modal("show");
        $("#divSenderInformation").hide()
        $("#divrecipientInformation").show()

    }else
    {
        recl = {
            "label":recName,
            "address_text":recAddress,
            "latitude":recLat,
            "longitude":recLon,
            "landmark":"",
            "contact_number":contactNum
        }
    }
    
    
   /* recl = {"label":"5","address_text":"Daang Reyna, Vista Avenue, Vista Alabang Portofino South, Almanza Dos, Las Pinas, Metro Manila, Philippines","latitude":"14.3684577","longitude":"121.0099434","landmark":"","contact_number":"2"}  
   */

    return recl
  }     

  $.fn.ajaxpostrequest = function(url, dataPost)
  {
     let baseurl = $(this).baseurl()

    
     axios.post(base_url + url,dataPost, {
        headers: {
          'Content-Type': 'application/json; charset=UTF-8'
        }
        // other configuration there
      })
      .then(function (response) {
       // alert('yeah!');
        $("#loading").hide()
        let resp = response.data
        console.log(resp);
        $("#labeltotal").html("₱"+resp.data.delivery_fee)
        $("#deliveryfeeSummary").html("₱"+resp.data.delivery_fee)
                if(resp.data.delivery_fee > 0)
                {
                    $("#divDeliveryDetails").hide();
                    $("#divAssignaRider").hide();
                    $("#cartidtxt").val(resp.cartid)
                    $("#totaltxt").val(resp.data.delivery_fee)
                     $("#btnreview").removeClass("hidden")
                }
                $("#loading").hide()
      })
      .catch(function (error) {
        alert('oops');
        console.log(error);
      })

     
     return false; 
  }


  $.fn.ajaxpostrequestpabili = function(url, dataPost)
  {
     let baseurl = $(this).baseurl()


     axios.post(base_url + url,dataPost, {
        headers: {
          'Content-Type': 'application/json; charset=UTF-8'
        }
        // other configuration there
      })
      .then(function (response) {
       // alert('yeah!');

        $("#loading").hide()
        let resp = response.data
        console.log(resp);
        $("#labeltotal").html("₱"+resp.data.delivery_fee)
        $("#deliveryfeeSummary").html("₱"+resp.data.delivery_fee)
                if(resp.data.delivery_fee > 0)
                {
                    $("#divPabili").hide()
                    $("#divAssignaRider").hide();
                    $("#cartidtxt").val(resp.cartid)
                    $("#totaltxt").val(resp.data.delivery_fee)
                    $("#btnreview").removeClass("hidden")
                }
                $("#loading").hide()
      })
      .catch(function (error) {
        alert('oops');
        console.log(error);
      })

     
     return false; 
  }

  $.fn.ajaxpostrequestbooknow = function(url, dataPost)
  {
     let baseurl = $(this).baseurl()

     axios.post(base_url + url,dataPost, {
        headers: {
          'Content-Type': 'application/json; charset=UTF-8'
        }
        // other configuration there
      })
      .then(function (response) {
       // alert('yeah!');
        
        console.log(response);
        let resp = response.data;

        
        if(resp == true)
        {
             /*$("#headertitlemodal").html("Response");
             $("#contentitlemodal").html("Successfully Booked.");
             $("#modalAlert").modal("show");*/
             if (confirm('Successfully Booked.') ) {
                       location.reload();
                  }
             location.reload();
        }else{
            alert("Failed to book.")
        }
        //console.log(resp)
        
      })
      .catch(function (error) {
        alert('oops');
        console.log(error);
      })

    
     return false; 
  }


  $.fn.ajaxpostrequestrider = function(url, dataPost)
  {
     let baseurl = $(this).baseurl()
     
    $.ajax({
         type: "POST",
         url: base_url + url, 
         data: dataPost,
         dataType: "json",  
         cache:false,
         success: 
              function(data){
                let datatoappend = "";
                console.log(data);
                if(data.length > 0)
                {
                    $.each(data,function(i,val){
                       datatoappend +=`<div class="form-check">
                         <input class="form-check-input" type="radio" name="rider"  
                          value="${val.id}" data-name="${val.full_name}">
                          <label class="form-check-label" >
                             ${val.full_name}
                          </label></div>`
                    })
                    $("#riderresult").html(datatoappend)
                }else{
                     $("#riderresult").html("")
                }
                
                
              }
          });// you have missed this bracket
     return false; 
  }

  $.fn.unserialize = function(data) {
    data = data.split(';');
    var response = {};
    for (var k in data){
        var newData = data[k].split(':');
        response[newData[0]] = newData[1];
    }
    return response;
}

  $.fn.phonenumber = function(contactNum) {
    var a = contactNum;
    var filter = /^\d*(?:\.\d{1,2})?$/;
    var f2digits = a.substring(0,2)
    if (filter.test(a) && a.length == 11 && f2digits == "09") {
        return true;
    }
    else {
        return false;
    }
    
 }
 $.fn.baseurl = function(){
    let baseUrl = $("#base_url").val();
    return baseUrl
 }
 
 $.fn.notselectedVehicles = function(vehicleid)
 {
    let numberOfVehicles = 20

    for(i=0;i < numberOfVehicles;i++)
       {
         
        if(i != vehicleid )
        {
            $("#vehi"+i).addClass("btn-primary")
            $("#vehi"+i).removeClass("btn-warning")   
        }
       }
 }

 $.fn.spanclear = function(divname){$("#"+divname).html("")}

 

$("#searchrider").keyup(function(){
       let senderLat = $("#senderlatitude").val()
       let senderLon = $("#senderlongitude").val()
       let value = $.trim($(this).val());
       let pickuplat = senderLat
       let pickuplng = senderLon
       let vehicleid = $("#selectedvehicle").val()
       let al        = $("#al").val()
       let data = []
       data = {
               "ridername":value,
               "pickuplat":pickuplat,
               "pickuplng" :pickuplng,
               "vehicleid" :vehicleid,
               "al":al
              }
       $(this).ajaxpostrequestrider("cms/booking/searchrider",data)
    }) 
  });
</script>

<script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>
<script type="text/javascript" src="<?php echo base_url('public/admin/js/') ?>autocompletemap.js"></script>

 <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
<script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCvRNfdwl_oCDWhFneA8qaulprqFPT4hz0&callback=initMap&libraries=places&v=weekly"
      async
    ></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCvRNfdwl_oCDWhFneA8qaulprqFPT4hz0&libraries=places&callback=initMap&channel=GMPSB_addressselection_v1_cABC" async defer></script>-->




