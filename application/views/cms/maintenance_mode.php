<section id="main-content">
  <section class="wrapper">
    <!-- page start-->

    <?php if ($flash_msg): ?>
      <p style="color: <?php echo $flash_msg['color'] ?>;font-weight:bold"><?php echo $flash_msg['message'] ?></p>
    <?php endif; ?>
    <div class="row">

      <?php 
          $canedit = $accesslevel->can_edit;
      ?>

      <?php foreach ($maintenance as $key => $value): ?>

      <div class="col-lg-2">
        <section class="panel">
          <header class="panel-heading">
            <?php echo $key ?> Maintenance
          </header>
          <div class="panel-body">
            <img src="<?php echo $value['icon'] ?>" alt="" style="max-width:90px">
            <form action="<?php echo base_url('cms/dashboard/update_option') ?>" method="post">
              <input type="hidden" name="meta_key" value="<?php echo $value['key'] ?>">
              <input type="hidden" name="from" value="<?php echo base_url('cms/dashboard/maintenance_mode') ?>">
              <hr>
              <label>
                <h4>Maintenance Mode</h4>
              </label><br>
              <label for="">ON
                <input type="radio" name="meta_value" value="1" <?php echo $value['mm'] == 1 ? 'checked':'' ?>>
              </label> &nbsp;&nbsp;
              <label for="">OFF
                <input type="radio" name="meta_value" value="0" <?php echo $value['mm'] == 0 ? 'checked':'' ?>>
              </label><br>
              <hr>
              <?php if($canedit == 1):  ?>
              <input class="btn btn-info btn-md" type="submit" value="Submit">
            <?php endif; ?>
            </form>
            </div>
          </section>
      </div>
      <?php endforeach; ?>


      </div>




      <!-- page end-->
    </section>
  </section>


<script type="text/javascript">
  $(document).ready(function() {

   $('.panel').find('.panel-heading:contains("Get Car")').text("Get Ride Maintenance");
  });
</script>

<script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>

