<section id="main-content">
  <section class="wrapper">

   <input hidden="true" type="text" id="base_url" value="<?php echo base_url();?>">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            Administrators
            <?php if ($flash_msg = $this->session->flash_msg): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
          </header>
          <div class="panel-body">
            <?php 
                 $canedit = $accesslevel->can_edit;
                 $candelete = $accesslevel->can_delete;
                 $canadd = $accesslevel->can_add;
             ?>

            <p>
              <?php if($canadd == 1):  ?>
              <button type="button" class="add-new btn btn-success btn-sm">Add new</button>
             <?php endif; ?>
            </p>
            <div class="table-responsive" style="overflow: hidden; outline: none;" tabindex="1">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if (count($res) > 0 ): ?>
                    
                    <?php $i = 1; foreach ($res as $key => $value): ?>
                      <tr>
                        <th scope="row"><?php echo $i++ ?></th>
                        <td><?php echo $value->name ?></td>
                        <td><?php echo $value->email ?></td>
                        <td>
                          <?php if($canedit == 1):  ?>
                          <button type="button"
                          data-payload='<?php echo json_encode(['id' => $value->id, 'name' => $value->name, 'email' => $value->email,'userlevel' =>$userLevel,
                          'useridlevel'=>$value->userlevel,'allLocation'=>$allLocation,"areaAssigned"=>$value->area_assigned])?>'
                          class="edit-row btn btn-info btn-xs">Edit</button>
                          <?php endif;?>
                          
                          <?php if($candelete == 1): ?>
                          <button type="button" data-id='<?php echo $value->id; ?>'
                            class="btn btn-delete btn-danger btn-xs">Delete</button>
                           <?php endif; ?>  

                          </td>
                        </tr>
                      <?php endforeach; ?>


                    <?php else: ?>
                      <tr>
                        <td colspan="4" style="text-align:center">Empty table data</td>
                      </tr>
                    <?php endif; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </section>
        </div>
      </div>
      <!-- page end-->
    </section>
  </section>

    <!-- Modal -->
    <div class="modal fade add-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Manage</h4>
          </div>
          <div class="modal-body">

            <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('cms/dashboard/add') ?>">
            <div>
              <label >Name</label>
              <input type="text" class="form-control" name="name"  >
            </div>
            <div class="form-group">
              <label >Email address</label>
              <input type="email" class="form-control" name="email"  >
            </div>
            <div class="form-group">
              <label >Password</label>
              <input type="password" class="form-control" name="password" id="" >
            </div>
            <div class="form-group">
                <label >User Level</label>
                <select name="userlevel"  class="form-control" >
                  <?php 
                    foreach($userLevel as $key => $val){
                      echo "<option value='".$val->id."'>".$val->user_level."</option>";
                    }
                  ?>
                 </select>
              </div>
            </div>
            <div class="modal-footer">
              <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
              <input class="btn btn-info" type="submit" value="Save changes">
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- modal -->

    <!-- Modal -->
    <div class="modal fade update-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Manage</h4>
          </div>
          <div class="modal-body">
            <form role="form" >
            <div >
              <label >Name</label>
              <input type="text" class="form-control" name="name" id="up_name" >
            </div>
            <div class="form-group">
              <label >Email address</label>
              <input type="email" class="form-control" name="email" id="up_email" >
            </div>
            <div class="form-group">
              <label >New Password</label>
              <input type="password" class="form-control" name="password"  id="up_password">
            </div>
            <div class="form-group">
                <label >User Level</label>
                <select name="userlevel"  id="up_userlevel" class="form-control" >
                 
                 </select>
              </div>
           
            <div class="form-group"  id="formgrouparea" hidden="true">
                <label >Area Assigned</label>
                <div class="grid-container">
                  
                </div>
 
               <span id="spanerr" class="asterisk"></span>
              </div>

            <div class="modal-footer">
              <input type="hidden" name="id" id="up_id">
              <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
              <input class="btn btn-info" id="btnsave" type="button" value="Save changes">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
    <!-- modal -->

<script type="text/javascript">
  $(document).ready(function() {
    $('.add-new').on('click', function(){
      $('.add-modal').modal()
    })

    $('.edit-row').on('click', function(){
      let id = $(this).data('payload').id
      let name = $(this).data('payload').name
      let email = $(this).data('payload').email
      let userlevel = $(this).data('payload').userlevel
      let useridlevel =  $(this).data('payload').useridlevel
      let allLocation = $(this).data('payload').allLocation
      let areaAssigned = $.trim($(this).data('payload').areaAssigned)
      $(".grid-container").html("");
      console.log(areaAssigned)

      $("#up_userlevel").html("")

       $.each(userlevel, function(i,val){
         // console.log(vald+","+val.province)
          $("#up_userlevel").append(`<option value="${val.id}">
                                       ${val.user_level}  
                                  </option>`);
        })
       
       
       $.each(allLocation, function(i,val){
         // console.log(vald+","+val.province)
          let city =  val.city != null ? val.city+', ':"";
          let brgy =  val.barangay  != null ? val.barangay+', ':"";
          let isCheck = "";               
          let area =  brgy+city+val.province;
          
          if(areaAssigned != "" || areaAssigned != null) 
          {
            $.each(areaAssigned.split(','), function(ii,s){
                  if(s == val.id){
                     isCheck = "checked"
                     console.log(s)
                  }
            })
          }
          
          // console.log(isCheck)
          $(".grid-container").append(`<input class="areaCheckBox" ${isCheck} type="checkbox" value="${val.id}" />${area}`);

        })


      $("#up_userlevel option[value="+useridlevel+"]").prop("selected", "selected")
      $('#up_id').val(id)
      $('#up_name').val(name)
      $('#up_email').val(email)
      $('.update-modal').modal()

      if(useridlevel == 2){
         $("#formgrouparea").show()
      }else{
         $("#formgrouparea").hide()
      }

    })


   $('#up_userlevel').on('change', function(){
     
    let selectedVal = $(this).val() 
    if(selectedVal == 2){
       $("#formgrouparea").show()
    }else{
       $("#formgrouparea").hide()
    }

   })


    $('html').on('click', '.btn-delete', function(e) {
      if (confirm('Are you sure you want to delete this?', 'Yes, delete it', 'No, go back')) {
        invokeForm(base_url + 'cms/dashboard/delete', {id: $(this).data('id')});
      }
    })

    $('#btnsave').on('click', function(){

       let dataToPost = []
       let hasError = false
       let id = $('#up_id').val()
       let name = $("#up_name").val()
       let username = $("#up_email").val()
       let newPassword = $("#up_password").val()
       let userLevel = $("#up_userlevel").find('option:selected').val()
       let areaAssigned = $('.areaCheckBox:checked').map(function () {
         return this.value;
          }).get();

       let defaultArea = 0
       
       if(areaAssigned.length > 0 && userLevel == 2){
          defaultArea = areaAssigned[0]
          hasError = false
       }else if(areaAssigned.length < 0 && userLevel == 2){
        $("#spanerr").html("Please assign area")
        hasError = true
       }else{
        areaAssigned = null
       }

       if(newPassword == null || newPassword == "" ){
        dataToPost = {'data':{'name': name, 'email':username,
                     'userlevel':userLevel,'area_assigned':areaAssigned,'default_location':defaultArea },"id":id}

       }else{
        dataToPost = {'data':{'name': name, 'email':username,'password':newPassword,
                     'userlevel':userLevel,'area_assigned':areaAssigned,'default_location':defaultArea },"id":id}
       }
      
       
     if(hasError == false){
       $(this).ajaxpostrequest('cms/dashboard/update',dataToPost)
       $(this).prop('disabled', false);
     }
     
    })

   


$.fn.ajaxpostrequest = function(url, dataPost)
  {
     let baseurl = $(this).baseurl()

     //console.log("dataToPost:"+dataPost)

     axios.post(base_url + url,dataPost, {
        headers: {
          'Content-Type': 'application/json; charset=UTF-8'
        }
        // other configuration there
      })
      .then(function (response) {
        
      if(response.data == true){
        alert('Successfully updated')
        location.reload();
      }
        
      })
      .catch(function (error) {
         $("#loading").hide()
           alert('Oops, an error occurred during process.');
      //  console.log(error);
      })
     return false; 
  }

$.fn.baseurl = function()
{
    let baseUrl = $("#base_url").val();
    return baseUrl
}

  });
</script>

<script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>

