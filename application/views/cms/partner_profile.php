<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-10">
        <section class="panel">
          <header class="panel-heading">
            Partner Profile
            <?php if ($flash_msg = $this->session->flash_msg): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
          </header>
          <div class="panel-body">
              <form enctype="multipart/form-data" method="post" role="form" action="<?php echo base_url('cms/partners/update/') ?>">
                <input type="hidden" name="id" value="<?php echo $this->session->id ?>">
                <div class="row">
           <div class="form-group col-md-6">
                <label >Name</label>
                <input type="text" class="form-control" name="name"   value="<?php echo $user->name ?>">
              </div>
              <div class="form-group col-md-6">
                <label >Email</label>
                <input type="email" class="form-control" name="email"   value="<?php echo $user->email ?>">
              </div>
              <div class="form-group col-md-6">
                <label >Category</label>
                <input type="text" class="form-control" name="category"   value="<?php echo $user->category ?>">
              </div>
              <div class="form-group col-md-6">
                <label >Address</label>
                <input type="text" class="form-control" name="location_text"  value="<?php echo $user->location_text ?>">
              </div>
              <div class="form-group col-md-6">
                <label >Partner logo</label>
                <input type="file" class="form-control" name="image">
              </div>
              <div class="form-group col-md-6">
                <label>Current partner logo</label><br>
                <img src="<?php echo $user->image ?>" id="image" style="width: 100px">
              </div>
              <div class="form-group col-md-12">
                <hr>
              </div>
              <div class="form-group col-md-6">
                <label >Banner</label>
                <input type="file" class="form-control" name="banner">
              </div>
              <div class="form-group col-md-6">
                <label>Current banner</label><br>
                <img src="<?php echo $user->banner ?>" id="banner" style="width: 100px">
              </div>
              <div class="form-group col-md-12">
                <hr>
              </div>
              <div class="col-md-12">
                <h4 style="margin-top:-10px">Store Coordinates</h4>
              </div>
              <div class="form-group">
                <div class="col-md-6">
                  <label >Latitude</label>
                  <input type="text" name="latitude" value="<?php echo $user->latitude ?>" class="form-control">
                </div>
                <div class="col-md-6">
                  <label >Longitude</label>
                  <input type="text" name="longitude"  value="<?php echo $user->longitude ?>"class="form-control">
                </div>
              </div>

               <div class="form-group">
                <div class="col-md-6">
                  <hr>
                    <h4>Store Information</h4>
                </div>
                <div class="col-md-6">
                  <hr>
                   <h4>24 hour format</h4>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-3">
                  <label >Start Day</label>
                  <select class="form-control" name="from_day">
                    <option <?php echo ($user->from_day) == '1' ? 'selected' : '' ?> value="1">Mondays</option>
                    <option <?php echo ($user->from_day) == '2' ? 'selected' : '' ?> value="2">Tuesdays</option>
                    <option <?php echo ($user->from_day) == '3' ? 'selected' : '' ?> value="3">Wednesdays</option>
                    <option <?php echo ($user->from_day) == '4' ? 'selected' : '' ?> value="4">Thursdays</option>
                    <option <?php echo ($user->from_day) == '5' ? 'selected' : '' ?> value="5">Fridays</option>
                    <option <?php echo ($user->from_day) == '6' ? 'selected' : '' ?> value="6">Saturdays</option>
                    <option <?php echo ($user->from_day) == '7' ? 'selected' : '' ?> value="7">Sundays</option>
                  </select>
                </div>
                <div class="col-md-3">
                  <label >End day</label>
                  <select class="form-control" name="to_day">
                    <option <?php echo ($user->to_day) == '1' ? 'selected' : '' ?> value="1">Mondays</option>
                    <option <?php echo ($user->to_day) == '2' ? 'selected' : '' ?> value="2">Tuesdays</option>
                    <option <?php echo ($user->to_day) == '3' ? 'selected' : '' ?> value="3">Wednesdays</option>
                    <option <?php echo ($user->to_day) == '4' ? 'selected' : '' ?> value="4">Thursdays</option>
                    <option <?php echo ($user->to_day) == '5' ? 'selected' : '' ?> value="5">Fridays</option>
                    <option <?php echo ($user->to_day) == '6' ? 'selected' : '' ?> value="6">Saturdays</option>
                    <option <?php echo ($user->to_day) == '7' ? 'selected' : '' ?> value="7">Sundays</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-3">
                  <label >Opening Hour</label>
                  <!-- <input type="text" name="latitude" value="<?php echo $user->latitude ?>" class="form-control"> -->
                  <div class="input-group bootstrap-timepicker">
                    <input id="from_hour" value="<?php echo $user->from_hour ?>" type="text"  class="form-control timepicker-24" name="from_hour" placeholder="Pick a time">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                    </span>
                  </div>

                </div>
                <div class="col-md-3">
                  <label >Closing Hour</label>
                  <!-- <input type="text" name="longitude"  value="<?php echo $user->longitude ?>"class="form-control"> -->
                  <div class="input-group bootstrap-timepicker">
                    <input id="to_hour" value="<?php echo $user->to_hour ?>" type="text"  class="form-control timepicker-24" name="to_hour" placeholder="Pick a time">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                    </span>
                  </div>

                </div>
                <div class="col-md-6">
                  <br>
                  <label >Contact information</label>

                  <textarea name="contact_numbers" rows="8" cols="80" class="form-control" placeholder="Enter your numbers here separated by a new line..."><?php echo $user->contact_numbers ?></textarea>

                </div>
                <div class="col-md-6">
                  <br>
                  <label >About</label>

                  <textarea name="about" rows="8" cols="80" class="form-control" placeholder=""><?php echo $user->about ?></textarea>

                </div>
                <div class="col-md-6">
                  <br>
                  <label >Keywords for searching (Separated by commas)</label>

                  <textarea name="keywords" rows="8" cols="80" class="form-control" placeholder="Keyword1, Keyword2, Keyword3..."><?php echo $user->keywords ?></textarea>

                </div>
              </div>


              <div class="col-md-12">
                <h4>Password<br><small>Leave blank if you do not wish to change password</small></h4>
              </div>
              <div class="form-group col-md-6">
                <label>New Password</label>
                <input type="password" class="form-control" name="password" autocomplete="off">
              </div>
              <div class="form-group col-md-6">
                <label>Confirm Password</label>
                <input type="password" class="form-control" id="confirm_password">
              </div>

             <div class="form-group col-md-12">
                 <h4>Store Availability</h4>
                 <input type="text" name="" hidden="true" id="storeavail" value="<?php echo $user->isopen .";;&".$user->id ?>">
                <label class="switch23">
                   <input type="checkbox" id='checkboxstoreAvail'>
                  <div class="slider round">
                  </div>
                </label>

               <p>
                 <?php if ($flash_msg = $this->session->flash_msg2): ?>
                <sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
               <?php endif; ?>
               </p>

              </div>

              <div class="form-group col-md-6">
                <br>
                <button class="btn btn-info" type="submit">Save changes</button>
              </div>


                  </div>
                </form>
              </div>
            </div>
            <!-- page end-->
          </section>
        </section>


  <script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>

  <script>
    $(document).ready(function($) {

    let inputxt = $("#storeavail").val().split(";;&");
        let isopen  =  inputxt[0];
        let id2      = inputxt[1];
        if(isopen == 'Y')
        {
           $('#checkboxstoreAvail').prop('checked', true);
          
        }else
        {
           $('#checkboxstoreAvail').removeAttr('checked');  
        } 
    

    $('#checkboxstoreAvail').on('change',function(){
      let isOpen = $(this).is(":checked");
      //alert(isOpen);
      if(isOpen == true)
        {
           invokeForm(base_url + 'cms/partners/updateStoreAvailability', {id:id2,isitopen:'Y'});
        }else
        {
           invokeForm(base_url + 'cms/partners/updateStoreAvailability', {id:id2,isitopen:'N'});
        } 

    });

     $('form').on('submit', function (){

        let p = $('input[name=password]').val()
        let cp = $('input[id=confirm_password]').val()

      if (!(p === cp)) {
        swal("Passwords don't match", "Please try again or leave them blank", {
          icon : "error",
          buttons: {
            confirm: {
              className : 'btn btn-danger'
            }
          },
        });
          return false
        }

      })
      //
      //
      // $('.timepicker-24').timepicker({
      //     autoclose: true,
      //     minuteStep: 1,
      //     showSeconds: true,
      //     showMeridian: false
      // });

      $('#from_hour').timepicker({
        autoclose: true,
        minuteStep: 1,
        showSeconds: true,
        showMeridian: false,
        defaultTime: 'value'
      });
      $('#to_hour').timepicker({
        autoclose: true,
        minuteStep: 1,
        showSeconds: true,
        showMeridian: false,
        defaultTime: 'value'
      });

    });
  </script>
