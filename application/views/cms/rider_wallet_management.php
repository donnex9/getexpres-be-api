<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <?php if ($rider_wallet_offers): ?>

      <?php foreach ($rider_wallet_offers as $key => $value): ?>

      <div class="col-lg-4">
        <section class="panel">
          <form action="<?php echo base_url('cms/dashboard/update_option_serialized') ?>" method="post">
          <header class="panel-heading">
            <?php if (@$flash_msg && @$_GET['offer_id'] == $value->id): ?>
              <h5 style="color: <?php echo @$flash_msg['color'] ?>"><?php echo @$flash_msg['message'] ?></h5>
            <?php endif; ?>
            <input class="form-control" type="text" required name="name" value="<?php echo $value->name  ?>" placeholder="Offer name (e.g. Get-all-you-want)">
          </header>
          <div class="panel-body">
              <input type="hidden" name="from" value="cms/dashboard/rider_wallet_management">
              <input type="hidden" name="id" value="<?php echo $value->id ?>">
              <label>
                Price (in PHP)
              </label>
              <input class="form-control" required type="number" min="0" step="0.01" name="price" value="<?php echo $value->price ?>">
              <label>
                Duration (in Hours)
              </label>
              <input class="form-control" required type="number" min="0" step="1" name="duration_in_hours" value="<?php echo $value->duration_in_hours ?>">
              <label>
                Custom Duration Label (optional)
              </label>
              <input class="form-control" type="text" placeholder="e.g. 1 week, 1 day, etc." name="duration_label" value="<?php echo $value->duration_label ?>">
              <br>
              <input class="btn btn-info btn-md" type="submit" value="Update offer">
            </div>
          </section>
          </form>
        </div>

      <?php endforeach; ?>

    <?php else: ?>
      <h1>Looks like this place is empty</h1>
    <?php endif; ?>


    <div class="col-lg-4">
      <section class="panel">
        <form action="<?php echo base_url('cms/dashboard/update_option') ?>" method="post">
        <header class="panel-heading">
          <?php if (@$flash_msg && @$_GET['topup_description'] == $value->id): ?>
            <h5 style="color: <?php echo @$flash_msg['color'] ?>"><?php echo @$flash_msg['message'] ?></h5>
          <?php endif; ?>
          Top-up Description
        </header>
        <div class="panel-body">
            <input type="hidden" name="from" value="cms/dashboard/rider_wallet_management">
            <input type="hidden" name="id" value="<?php echo $value->id ?>">
            <!-- <label>
              Custom Duration Label (optional)
            </label> -->
            <input class="form-control" type="text" placeholder="Description here" name="topup_description" value="<?php echo $topup_description ?>">
            <br>
            <input class="btn btn-info btn-md" type="submit" value="Update">
          </div>
        </section>
        </form>
      </div>



      <!-- page end-->
    </section>
  </section>


<script type="text/javascript">
  $(document).ready(function() {


  });
</script>

<script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>
