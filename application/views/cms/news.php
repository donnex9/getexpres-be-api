<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading" style="padding:15px">
            News
            <?php if ($flash_msg = $this->session->flash_msg): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
            <div style="float:right">
              <form method="GET">
                <input type="text" name="squery" value="<?php echo @$_GET['squery'] ?>" placeholder="Search by title">
                <input type="submit">
              </form>
            </div>
          </header>
          <div class="panel-body">
            <p>
              <button type="button" class="add-new btn btn-success btn-sm">Add new</button>
            </p>
            <div class="table-responsive" style="overflow: hidden; outline: none;" tabindex="1">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Category</th>
                    <th>Banner Image</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if (count($res) > 0 ): ?>

                    <?php foreach ($res as $key => $value): ?>
                      <tr>
                        <th scope="row"><?php echo $startingPK++ ?></th>
                        <td><?php echo $value->title ?></td>
                        <td><?php echo $value->category ?></td>
                        <td><img style="max-width:150px" src="<?php echo $value->banner_image_f ?>"></td>
                        <td>
                          <button type="button"
                          data-payload='<?php echo json_encode(['id' => $value->id, 'title' => $value->title,'linked_partner_id' => $value->linked_partner_id, 'category' => $value->category , 'body' => $value->body, 'image' => $value->banner_image_f,'assigned_location'=>$value->assigned_location,'allLocation'=>$allLocation], JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE)?>'
                          class="edit-row btn btn-info btn-xs">Edit</button>
                          <button type="button" data-id='<?php echo $value->id; ?>'
                            class="btn btn-delete btn-danger btn-xs">Delete</button>
                          </td>
                        </tr>
                      <?php endforeach; ?>


                    <?php else: ?>
                      <tr>
                        <td colspan="5" style="text-align:center"><?= ($this->input->get('squery')) ? 'Empty Search Results':'Empty table data' ?></td>
                      </tr>
                    <?php endif; ?>
                  </tbody>
                </table>
              </div>

              <style>
              .active_lg {
                background: lightgray !important
              }
              </style>
              <ul class="pagination">
                <ul class='pagination'>
                  <?php $page = ($this->input->get('page')) ?: 1; ?>
                  <li><a href="<?php echo base_url('cms/news/') . "?page=1&squery=" . @$_GET['squery'];?>">&laquo;</a></li>

                  <!-- loop for desc -->
                  <?php for ($i = $page - 2; $i < ($page) ; $i++):
                    if ($i == -1 || $i == 0) {
                      continue;
                    }
                   ?>
                  <li><a href="<?php echo base_url('cms/news/') . "?page=" . $i . "&squery=" . @$_GET['squery'];?>"><?= $i ?></a></li>
                  <?php endfor; ?>
                  <!-- / loop for desc -->

                  <li><a href="<?php echo base_url('cms/news/') . "?page=" . $page . "&squery=" . @$_GET['squery'];?>"><?= $page ?></a></li>

                  <!-- loop for asc -->
                  <?php for ($i = $page + 1; $i < ($page + 3) ; $i++):
                  if ($i == $total_pages + 1 || $i == $total_pages + 2 || $total_pages == 0) {
                      continue;
                  }
                  ?>
                  <li><a href="<?php echo base_url('cms/news/') . "?page=" . $i . "&squery=" . @$_GET['squery'];?>"><?= $i ?></a></li>
                  <?php endfor; ?>
                  <!-- / loop for asc -->


                <li><a href="<?php echo base_url('cms/news/') . "?page=" . $total_pages . "&squery=" . @$_GET['squery'];?>">&raquo;</a></li>
                </ul>
              </ul>

            </div>
          </section>
        </div>
      </div>
      <!-- page end-->
    </section>
  </section>

  <!-- Modal -->
  <div class="modal fade add-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Manage</h4>
        </div>
        <div class="modal-body">

          <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('cms/news/add') ?>">
            <div class="form-group">
              <label >Title</label>
              <input type="text" class="form-control" name="title"  required="required">
            </div>
            <div class="form-group">
              <label >Category</label>
              <input type="text" class="form-control" name="category"  required="required">
            </div>
            <div class="form-group">
              <label >Body</label>
              <textarea name="body" class="form-control" required="required"></textarea>
            </div>
            <div class="form-group">
                <label >Assigment Area</label>
                <select name="assigned_location" id="assigmentArea" class="form-control" >
                  <?php 

                    foreach($allLocation as $key => $value){
                      $brgy = $value["barangay"] != null?$value["barangay"].", ":"";
                      $city = $value["city"] != null?$value["city"].", ":"";
                      echo "<option value='".$value["id"]."'>".$brgy.$city.$value["province"]."</option>";
                    }
                  ?>
                 </select>
              </div>
            <div class="form-group">
              <label >Banner Image</label>
              <input type="file" name="banner_image" class="form-control">
            </div>
            <div class="form-group">
              <label >Link to a Partner</label>
              <select class="form-control" name="linked_partner_id">
                <option value="0">--NOT LINKED--</option>
                <?php foreach ($partners as $key => $value): ?>
                  <option value="<?php echo $value->id ?>"><?php echo $value->name ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <input class="btn btn-info" type="submit" value="Save changes">
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- modal -->

  <!-- Modal -->
  <div class="modal fade update-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Manage</h4>
        </div>
        <div class="modal-body">

          <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('cms/news/update') ?>">
            <div class="form-group">
              <label >Title</label>
              <input type="text" class="form-control" name="title"  id="up_title" required="required">
            </div>
            <div class="form-group">
              <label >Category</label>
              <input type="text" class="form-control" name="category" id="up_category" required="required">
            </div>
            <div class="form-group">
              <label >Body</label>
              <textarea name="body" class="form-control" id="up_body" required="required"></textarea>
            </div>
            <div class="form-group">
                <label >Assigment Area</label>
                <select name="assigned_location" id="up_assigmentArea" class="form-control" >
                 
                 </select>
              </div>
            <div class="form-group">
              <label >Banner Image</label>
              <br>
              <img src="" id="up_image" style="max-width:150px">
              <br>
              <br>
              <input type="file" name="banner_image" class="form-control">
            </div>
            <div class="form-group">
              <label >Link to a Partner</label>
              <select class="form-control" name="linked_partner_id" id="up_linked_partner_id">
                <option value="0">--NOT LINKED--</option>
                <?php foreach ($partners as $key => $value): ?>
                  <option value="<?php echo $value->id ?>"><?php echo $value->name ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          <div class="modal-footer">
            <input type="hidden" name="id" id="up_id">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <input class="btn btn-info" type="submit" value="Save changes">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
  <!-- modal -->

  <script type="text/javascript">
    $(document).ready(function() {
      $('.add-new').on('click', function(){
        $('.add-modal').modal()

        

      })

      $('.edit-row').on('click', function(){
        let id = $(this).data('payload').id
        let title = $(this).data('payload').title
        let body = $(this).data('payload').body
        let category = $(this).data('payload').category
        let linked_partner_id = $(this).data('payload').linked_partner_id
        let image = $(this).data('payload').image

        let allLocation = $(this).data('payload').allLocation

        let assigned_location = $(this).data('payload').assigned_location

        
        $.each(allLocation, function(i,val){
         // console.log(vald+","+val.province)
          $('#up_assigmentArea').append(`<option value="${val.id}">
                                       ${val.barangay !=null ?val.barangay +",":""} ${val.city != null?val.city+",":""}  ${val.province}
                                  </option>`);
        })

        $("#up_assigmentArea option[value="+assigned_location+"]").prop("selected", "selected")

        $('#up_title').val(title)
        $('#up_body').val(body)
        $('#up_category').val(category)
        $('#up_id').val(id)
        $('#up_linked_partner_id').val(linked_partner_id).change()
        $('#up_image').attr('src', image)
        $('.update-modal').modal()
      })

      $('html').on('click', '.btn-delete', function(e) {
        if (confirm('Are you sure you want to delete this?', 'Yes, delete it', 'No, go back')) {
          invokeForm(base_url + 'cms/news/delete', {id: $(this).data('id')});
        }
      })

    });
  </script>

  <script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>
