<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading" style="padding:15px">
            Rider Topups
            <?php if ($flash_msg = $this->session->flash_msg): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
            <div style="float:right">

            </div>

<hr>
            <div class="row">
              <form class="" action="" method="get">
                <div class="col-md-2">
                  <input type="date" class="form-control" name="from" value="<?php echo $this->input->get('from') ?>">
                </div>
                <div class="col-md-1">
                  <center>to</center>
                </div>
                <div class="col-md-2">
                  <input type="date" class="form-control" name="to" value="<?php echo $this->input->get('to') ?>">
                </div>

                <div class="col-md-2">
                    <input type="submit" class="btn btn-info" value="Go Filter">
                    <a  class="btn btn-warning" href="<?php echo current_url() ?>">Clear</a>
                </div>

              </form>
             <a href="<?php echo base_url('cms/riders/export_report?') . $this->input->server('QUERY_STRING');?>" class="btn btn-info btn-sm"><i class="fa fa-download"></i> Download All Data (CSV)</a>
            </div>


          </header>
          <div class="panel-body">
            <p>
              <!-- <button type="button" class="add-new btn btn-success btn-sm">Add new</button> -->
            </p>
            <div class="table-responsive" style="overflow: hidden; outline: none;" tabindex="1">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Date</th>
                    <th>Wallet Balance</th>
                    <th>Rider</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if (count($res) > 0 ): ?>

                    <?php foreach ($res as $key => $value): ?>
                      <tr>
                        <th scope="row"><?php echo $startingPK++ ?></th>
                        <td><?php echo $value->created_at ?></td>
                        <td><?php echo $value->wallet_balance ?></td>
                        <td><?php echo $value->rider->full_name ?></td>
                        </tr>
                      <?php endforeach; ?>


                    <?php else: ?>
                      <tr>
                        <td colspan="5" style="text-align:center"><?= ($this->input->get('squery')) ? 'Empty Search Results':'Empty table data' ?></td>
                      </tr>
                    <?php endif; ?>
                  </tbody>
                </table>
              </div>

              <style>
              .active_lg {
                background: lightgray !important
              }
              </style>
              <ul class="pagination">
                <ul class='pagination'>
                  <?php $page = ($this->input->get('page')) ?: 1; ?>
                  <li><a href="<?php echo base_url('cms/riders/riderTopup') . "?page=1&cart_type_id=" . @$_GET['cart_type_id'] . "&from=" . @$_GET['from'] . "&to=" . @$_GET['to'];?>">&laquo;</a></li>

                  <!-- loop for desc -->
                  <?php for ($i = $page - 2; $i < ($page) ; $i++):
                    if ($i == -1 || $i == 0) {
                      continue;
                    }
                   ?>
                  <li><a href="<?php echo base_url('cms/riders/riderTopup') . "?page=" . $i . "&cart_type_id=" . @$_GET['cart_type_id']. "&from=" . @$_GET['from'] . "&to=" . @$_GET['to'];?>"><?= $i ?></a></li>
                  <?php endfor; ?>
                  <!-- / loop for desc -->

                  <li><a href="<?php echo base_url('cms/riders/riderTopup') . "?page=" . $page . "&cart_type_id=" . @$_GET['cart_type_id'] . "&from=" . @$_GET['from'] . "&to=" . @$_GET['to'];?>"><?= $page ?></a></li>

                  <!-- loop for asc -->
                  <?php for ($i = $page + 1; $i < ($page + 3) ; $i++):
                  if ($i == $total_pages + 1 || $i == $total_pages + 2 || $total_pages == 0) {
                      continue;
                  }
                  ?>
                  <li><a href="<?php echo base_url('cms/riders/riderTopup') . "?page=" . $i . "&cart_type_id=" . @$_GET['cart_type_id']  . "&from=" . @$_GET['from'] . "&to=" . @$_GET['to'];?>"><?= $i ?></a></li>
                  <?php endfor; ?>
                  <!-- / loop for asc -->


                <li><a href="<?php echo base_url('cms/riders/riderTopup') . "?page=" . $total_pages . "&cart_type_id=" . @$_GET['cart_type_id']  . "&from=" . @$_GET['from'] . "&to=" . @$_GET['to'];?>">&raquo;</a></li>
                </ul>
              </ul>

            </div>
          </section>
        </div>
      </div>
      <!-- page end-->
    </section>
  </section>

  <script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>
