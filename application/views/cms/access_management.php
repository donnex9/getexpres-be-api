<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            Access Management

            <?php if ($flash_msg && @$_GET['meta_key'] == 'access_management'): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
          </header>
          <div class="panel-body">
            <!--tab nav start-->
            <section class="panel">
               
                <div class="panel-body">
                    <div class="tab-content">
                     
                      <div class="row">
                        <div class="col-md-4"> 
                         <div class="panel panel-info">
                        <div class="panel-heading">
                          <h3 class="panel-title">User Level</h3>
                        </div>
                        <div class="panel-body">
                          <ul class="nav nav-pills nav-stacked" id="navcity">
                           <?php foreach ($userlevel as $key => $val): ?> 
                            <!-- inner foreach -->
                            <li>
                            <form action="<?php echo base_url('cms/dashboard/update_rate') ?>" method="post">
                              
                                <div class="form-check">
                                  <input class="form-check-input userlevel<?php echo $val->id ?>" type="checkbox" 
                                   data-payload='<?php echo json_encode(['menu'=>$menu,'userlevelid'=>$val->id,
                                  'accesslevel'=>$accesslevel], JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE)?>'
                                   >
                                  <label class="form-check-label" >
                                   <?php echo $val->user_level; ?>
                                  </label>
                                </div>
                              
                            </form>
                            </li>
                            <!-- / inner foreach -->
                          <?php endforeach; ?>
                         
                          </ul>
                        </div>
                      </div>
                       </div>
                        <div class="col-md-4">
                            <div class="panel panel-info">
                        <div class="panel-heading">
                          <h3 class="panel-title">Modules/Menus</h3>
                        </div>
                        <div class="panel-body">
                          
                            <form action="<?php echo base_url('cms/dashboard/update_rate') ?>" method="post">
                              
                               <ul class="nav nav-pills nav-stacked" id="ulMenu"> 
                               

                              </ul>
                              
                            </form>

                          
                        </div>
                      </div>
                        </div>
                        <div class="col-md-4">
                            <div class="panel panel-info">
                        <div class="panel-heading">
                          <h3 class="panel-title">Access Level</h3>
                        </div>
                        <div class="panel-body">
                            
                            <ul class="nav nav-pills nav-stacked" id="ulaccesslevel"> 
                               

                            </ul>


                            <div id="divsave" hidden="true">
                               <input type="text" id="accessid" hidden="true">
                               <button id="btnsavechanges" class="btn btn-success sm pull-right">Save Changes</button>
                            </div>
                              
                        </div>
                      </div>
                        </div>
                      </div>

                    </div>
                </div>
            </section>

          
            </div>
          </section>
        </div>

          


      </div>




      <!-- page end-->
    </section>
  </section>


<script type="text/javascript">
  $(document).ready(function() {

   var $checks = $('#navcity input[type="checkbox"]');
                $checks.click(function() {
                $checks.not(this).prop("checked", false);
     });
   let accesslevelArr = []             

  //city trycicle
  $('.userlevel1').on('click', function(){
      let data = $(this).data('payload');
      let userlvlid = data.userlevelid
      let menu = data.menu;
      let accesslevel = data.accesslevel;
      $(this).showMenu(menu,accesslevel,userlvlid)
      
    });
  $('.userlevel2').on('click', function(){
      let data = $(this).data('payload')
      let userlvlid = data.userlevelid
      let menu = data.menu;
      let accesslevel = data.accesslevel;
      $(this).showMenu(menu,accesslevel,userlvlid)
      
    });
  $('.userlevel3').on('click', function(){
      let data = $(this).data('payload')
      let userlvlid = data.userlevelid
      let menu = data.menu
      let accesslevel = data.accesslevel
      $(this).showMenu(menu,accesslevel,userlvlid)
      
    });

  $.fn.showMenu = function(menu,accesslevels,userid)
  {
    $("#accessid").val("")
    $("#ulaccesslevel").html("")
    $("#ulMenu").html("")
    $("#divsave").hide()
    accesslevelArr = accesslevels
    $.each(menu, function(i,item){
      $("#ulMenu").append(`<li><div class="form-check divcheckzone">
                                  <input name="menu" class="form-check-input" 
                                   type="checkbox" 
                                   data-userlevel="${userid}"
                                   data-menuid="${item.menu_id}"
                                   >
                                  <label class="form-check-label" >
                                     ${item.menu}
                                  </label>
                                </div></li>`);
    })
  }

  $.fn.accessLevel = function(menuid,userid)
  {
    $("#ulaccesslevel").html("")
    $.each(accesslevelArr, function(i,item){
      if($.trim(item.user_level) == $.trim(userid) && $.trim(item.menu_id) == $.trim(menuid))
      {
        let canview = item.can_view == 1?"checked":"";
        let canedit = item.can_edit == 1?"checked":"";
        let canadd = item.can_add == 1?"checked":"";
        let candelete = item.can_delete == 1?"checked":"";
        $("#accessid").val($.trim(item.aid))
        $("#divsave").show()
        console.log(item)
    $("#ulaccesslevel").append(`<li>
                                 <div class="form-check">
                                  <input id="canview" ${canview} class="form-check-input" 
                                   type="checkbox"
                                   >
                                  <label class="form-check-label" >
                                    View
                                  </label>
                                 </div>
                                 <div class="form-check">
                                  <input id="canadd" ${canadd} class="form-check-input" 
                                   type="checkbox"
                                   >
                                  <label class="form-check-label" >
                                    Add
                                  </label>
                                 </div>
                                 <div class="form-check">
                                  <input id="canedit" ${canedit} class="form-check-input" 
                                   type="checkbox"
                                   >
                                  <label class="form-check-label" >
                                    Edit
                                  </label>
                                 </div>
                                 <div class="form-check">
                                  <input id="candelete" ${candelete} class="form-check-input" 
                                   type="checkbox"
                                   >
                                  <label class="form-check-label" >
                                    Delete
                                  </label>
                                 </div>
                                </li>`)
      }
    }) 
  }

  $('#ulMenu ').on('click', function(){

    var $checks = $('#ulMenu input[type="checkbox"]');
             $checks.click(function() {
                $checks.not(this).prop("checked", false);
             });
     let userlevel = 0;
     let menuid = 0;
     let accesslevel = [];    
     let hasSelectedMenu = false    

        $('input[name="menu"]:checked').each(function(i,cb) {
         //console.log($(cb).data('payload2'));
         userlvlid = $(cb).data('userlevel');
         menuid    = $(cb).data('menuid');
        
         $(this).accessLevel(menuid,userlvlid);
         hasSelectedMenu = true
        });

     if(hasSelectedMenu == false){
       $("#ulaccesslevel").html("")
       $("#divsave").hide()
     }   
      
    });

  $("#btnsavechanges").on('click',function(){
      let accessid = $("#accessid").val()
      let data = $(this).getaccesslevelvalue(accessid)
      $(this).ajaxpostrequest("cms/dashboard/saveaccesslevel",data)
      $(this).attr('disabled',true)
  })

  

  $.fn.getaccesslevelvalue = function(aid)
  {
    let canadd  = $("#canadd").is(":checked") == true?1:0;
    let canview = $("#canview").is(":checked")== true?1:0;
    let canedit = $("#canedit").is(":checked")== true?1:0;
    let candelete = $("#candelete").is(":checked")== true?1:0;
    let datatosend = []
    datatosend = {
                  "aid":aid,
                  "can_add":canadd,
                  "can_delete":candelete,
                  "can_edit":canedit,
                  "can_view":canview
                }
    return datatosend;
  }

 $.fn.baseurl = function(){
    let baseUrl = $("#base_url").val();
    return baseUrl
 }

 $.fn.ajaxpostrequest = function(url, dataPost)
  {
     let baseurl = $(this).baseurl()

     axios.post(base_url + url,dataPost, {
        headers: {
          'Content-Type': 'application/json; charset=UTF-8'
        }
        // other configuration there
      })
      .then(function (response) {
       // alert('yeah!');
        if(response.data.data == true)
        {
          alert(response.data.meta.message)
          $("#btnsavechanges").attr('disabled',false)
        //  location.reload();
        }else{
          alert(response.data.meta.message)
          $("#btnsavechanges").attr('disabled',false)
        }
        //console.log(response.data.data)
        
      })
      .catch(function (error) {
         $("#loading").hide()
        alert('oops');
        console.log(error);
      })

    
     return false; 
  }

  
  }); //end of DOM

    
</script>

<script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>

