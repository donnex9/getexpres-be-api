<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading" style="padding:15px">
            Partners
            <?php if ($flash_msg = $this->session->flash_msg): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
            <div style="float:right">
              <form method="GET">
                <input type="text" name="squery" value="<?php echo @$_GET['squery'] ?>" placeholder="Search by partner name">
                <input type="submit">
              </form>
            </div>
          </header>
          <?php 
                 $canedit = $accesslevel->can_edit;
                 $candelete = $accesslevel->can_delete;
                 $canadd = $accesslevel->can_add;
             ?>
          <div class="panel-body">
            <?php if($canadd == 1):  ?>
            <p>
              <button type="button" class="add-new btn btn-success btn-sm">Add new</button>
            </p>
           <?php endif; ?>

            <div class="table-responsive" style="overflow: hidden; outline: none;" tabindex="1">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Image</th>
                    <th>Contact Information</th>
                    <th>Location</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if (count($res) > 0 ): ?>

                    <?php foreach ($res as $key => $value): ?>
                      <tr>
                        <th scope="row"><?php echo $startingPK++ ?></th>
                        <td><?php echo $value->name ?></td>
                        <td><?php echo $value->category ?></td>
                        <td><img style="max-width:150px" src="<?php echo $value->image ?>"></td>
                        <td><?php echo $value->contact_numbers ? '<pre>'. $value->contact_numbers.'</pre>' : '' ?></td>
                        <td><?php echo $value->location_text ?></td>
                        <td>
                      
                      <?php if($canedit == 1): ?>
                          <button type="button"
                          data-payload='<?php
                          echo json_encode(['id' => $value->id, 'banner' => $value->banner,
                          'about' => $value->about,
                          'keywords' => $value->keywords,
                          'from_hour' => $value->from_hour,
                          'to_hour' => $value->to_hour,
                          'from_day' => $value->from_day,
                          'to_day' => $value->to_day,
                          'getapp_comission_percentage' => $value->getapp_comission_percentage,
                          'contact_numbers' => $value->contact_numbers,
                           'name' => $value->name, 'email' => $value->email, 'service_id' => $value->service_id, 'category' => $value->category,
                           'location_text' => $value->location_text, 'latitude' => $value->latitude, 'longitude' => $value->longitude,
                           'image' => $value->image,'assigned_location' => $value->assigned_location,'allLocation' => $allLocation], JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE)?>'assigned_location
                          class="edit-row btn btn-info btn-xs">Edit</button>
                        <?php endif; ?>

                        <?php if($candelete == 1): ?>
                          <button type="button" data-id='<?php echo $value->id; ?>'
                            class="btn btn-delete btn-danger btn-xs">Delete</button>
                        <?php endif; ?>    
                          </td>
                        </tr>
                      <?php endforeach; ?>


                    <?php else: ?>
                      <tr>
                        <td colspan="6" style="text-align:center"><?= ($this->input->get('squery')) ? 'Empty Search Results':'Empty table data' ?></td>
                      </tr>
                    <?php endif; ?>
                  </tbody>
                </table>
              </div>

              <style>
              .active_lg {
                background: lightgray !important
              }
              </style>
              <ul class="pagination">
                <ul class='pagination'>
                  <?php $page = ($this->input->get('page')) ?: 1; ?>
                  <li><a href="<?php echo base_url('cms/partners/') . "?page=1&squery=" . @$_GET['squery'];?>">&laquo;</a></li>

                  <!-- loop for desc -->
                  <?php for ($i = $page - 2; $i < ($page) ; $i++):
                    if ($i == -1 || $i == 0) {
                      continue;
                    }
                   ?>
                  <li><a href="<?php echo base_url('cms/partners/') . "?page=" . $i . "&squery=" . @$_GET['squery'];?>"><?= $i ?></a></li>
                  <?php endfor; ?>
                  <!-- / loop for desc -->

                  <li><a href="<?php echo base_url('cms/partners/') . "?page=" . $page . "&squery=" . @$_GET['squery'];?>"><?= $page ?></a></li>

                  <!-- loop for asc -->
                  <?php for ($i = $page + 1; $i < ($page + 3) ; $i++):
                  if ($i == $total_pages + 1 || $i == $total_pages + 2 || $total_pages == 0) {
                      continue;
                  }
                  ?>
                  <li><a href="<?php echo base_url('cms/partners/') . "?page=" . $i . "&squery=" . @$_GET['squery'];?>"><?= $i ?></a></li>
                  <?php endfor; ?>
                  <!-- / loop for asc -->


                <li><a href="<?php echo base_url('cms/partners/') . "?page=" . $total_pages . "&squery=" . @$_GET['squery'];?>">&raquo;</a></li>
                </ul>
              </ul>

            </div>
          </section>
        </div>
      </div>
      <!-- page end-->
    </section>
  </section>

  <!-- Modal -->
  <div class="modal fade add-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Manage</h4>
        </div>
        <div class="modal-body">

          <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('cms/partners/add') ?>">
            <div class="form-group">
              <label >Name</label>
              <input type="text" class="form-control" name="name" required>
            </div>
            <div class="form-group">
              <label >Service type</label>
              <select class="form-control" name="service_id">
                <option value="" disabled="disabled" selected="selected">Select Service type</option>
                <?php foreach ($services as $key => $value): ?>
                  <option value="<?php echo $value->id ?>">Get <?php echo $value->type_f ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="form-group">
              <label >Category</label>
              <input type="text" class="form-control" name="category" required>
            </div>
             <div class="form-group">
                <label >Assigment Area</label>
                <select name="assigned_location" id="assigmentArea" class="form-control" >
                  <?php 

                    foreach($allLocation as $key => $value){
                      $brgy = $value["barangay"] != null?$value["barangay"].", ":"";
                      $city = $value["city"] != null?$value["city"].", ":"";
                      echo "<option value='".$value["id"]."'>".$brgy.$city.$value["province"]."</option>";
                    }
                  ?>
                 </select>
              </div>
            <div class="form-group">
              <label >Location</label>
              <textarea name="location_text" class="form-control" required></textarea>
            </div>
            <div class="form-group">
              <label >Getapp Commission Percentage</label>
              <input type="number" class="form-control" name="getapp_comission_percentage" value="5.00" step="0.01" required>
            </div>
            <div class="form-group">
              <label >Image</label>
              <input type="file" name="image" class="form-control">
            </div>
           <!-- <div class="form-group">
              <label >Banner</label>
              <input type="file" name="banner" class="form-control">
            </div>
            <div class="form-group">
              <label >Listing Banner</label>
              <input type="file" name="listing_banner" class="form-control">
            </div> -->
            <hr>
            <div class="row form-group">
              <div class="col-md-12">
                <h4>Store Coordinates</h4>
              </div>
              <div class="form-group">
                <div class="col-md-6">
                  <label >Latitude</label>
                  <input type="text" name="latitude" required class="form-control">
                </div>
                <div class="col-md-6">
                  <label >Longitude</label>
                  <input type="text" name="longitude" required class="form-control">
                </div>
              </div>

               <div class="form-group">
                <div class="col-md-6">
                  <hr>
                    <h4>Store Information</h4>
                </div>
                <div class="col-md-6">
                  <hr>
                   <h4>24 hour format</h4>
                </div>
              </div>
             
              <div class="form-group">
                <div class="col-md-3">
                  <label >Start Day</label>
                  <select class="form-control" name="from_day">
                    <option value="1">Mondays</option>
                    <option value="2">Tuesdays</option>
                    <option value="3">Wednesdays</option>
                    <option value="4">Thursdays</option>
                    <option value="5">Fridays</option>
                    <option value="6">Saturdays</option>
                    <option value="7">Sundays</option>
                  </select>
                </div>
                <div class="col-md-3">
                  <label >End day</label>
                  <select class="form-control" name="to_day">
                    <option value="1">Mondays</option>
                    <option value="2">Tuesdays</option>
                    <option value="3">Wednesdays</option>
                    <option value="4">Thursdays</option>
                    <option value="5">Fridays</option>
                    <option value="6">Saturdays</option>
                    <option value="7">Sundays</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-3">
                  <label >Opening Hour</label>
                  <!-- <input type="text" name="latitude" value="<?php echo $user->latitude ?>" class="form-control"> -->
                  <div class="input-group bootstrap-timepicker">
                    <input id="from_hour" value="" type="text"  class="form-control timepicker-24" name="from_hour" placeholder="Pick a time">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                    </span>
                  </div>

                </div>
                <div class="col-md-3">
                  <label >Closing Hour</label>
                  <!-- <input type="text" name="longitude"  value="<?php echo $user->longitude ?>"class="form-control"> -->
                  <div class="input-group bootstrap-timepicker">
                    <input id="to_hour" value="" type="text"  class="form-control timepicker-24" name="to_hour" placeholder="Pick a time">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                    </span>
                  </div>

                </div>
                <div class="col-md-6">
                  <br>
                  <label >Contact information</label>
                  <textarea name="contact_numbers" rows="8" cols="80" class="form-control" placeholder="Enter your numbers here separated by a new line..."></textarea>
                </div>
                <div class="col-md-6">
                  <br>
                  <label >About</label>
                  <textarea name="about" rows="8" cols="80" class="form-control" placeholder=""></textarea>
                </div>
                <div class="col-md-6">
                  <br>
                  <label >Keywords for searching</label>
                  <textarea name="keywords" rows="8" cols="80" class="form-control" placeholder="Keyword1, Keyword2, Keyword 3..."></textarea>
                </div>
              </div>

            </div>
            <hr>
            <div class="row form-group">
              <div class="col-md-12">
                <h4>Account Information</h4>
              </div>
              <div class="form-group">
                <div class="col-md-6">
                  <label >Email (used for signing-in)</label>
                  <input type="email" name="email" required="required" class="form-control" required>
                </div>
                <div class="col-md-6">
                  <label >Password</label>
                  <input type="text" name="password" class="form-control" id="add_pass" required>
                </div>
              </div>
            </div>
            <div class="row form-group">
              <div class="form-group">
                <div class="col-md-6">
                  <label for="notify">
                    <input type="checkbox" id="notify" value="1" name="alert_owner"> Alert owner by email
                  </label>
                </div>
                <div class="col-md-6">
                  <button type="button" class="btn btn-sm btn-success passgen" ><i class="fa fa-gears"></i> Generate Password</button>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <input class="btn btn-info" type="submit" value="Save changes">
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- modal -->

  <!-- Modal -->
  <div class="modal fade update-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Manage</h4>
        </div>
        <div class="modal-body">

          <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('cms/partners/update') ?>">
            <div class="form-group">
              <label >Name</label>
              <input type="text" class="form-control" name="name" id="up_name" required>
            </div>
            <div class="form-group">
              <label >Service type</label>
              <select class="form-control" name="service_id" id="up_service_id" required="required">
                <option value="" disabled="disabled" selected="selected">Select Service type</option>
                <?php foreach ($services as $key => $value): ?>
                  <option value="<?php echo $value->id ?>">Get <?php echo $value->type_f ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="form-group">
              <label >Category</label>
              <input type="text" class="form-control" name="category" id="up_category" required="required">
            </div>

            <div class="form-group">
                <label >Assigment Area</label>
                <select name="assigned_location" id="up_assigmentArea" class="form-control" >
                 
                 </select>
              </div>

            <div class="form-group">
              <label >Location</label>
              <textarea name="location_text" class="form-control" id="up_location_text" required="required"></textarea>
            </div>
            <div class="form-group">
              <label >Getapp Commission Percentage</label>
              <input type="number" class="form-control" name="getapp_comission_percentage" value="" id="up_comission" step="0.01" required>
            </div>
            <div class="form-group">
              <label >Image</label>
              <br>
              <img src="" id="up_image" style="max-width:150px">
              <br>
              <br>
              <input type="file" name="image" class="form-control">
            </div>
            <!--<div class="form-group">
              <label >Banner</label>
              <br>
              <img src="" id="up_banner" style="max-width:150px">
              <br>
              <br>
              <input type="file" name="banner" class="form-control">
            </div>
            <div class="form-group">
              <label >Listing Banner</label>
              <input type="file" name="listing_banner" class="form-control">
            </div> -->
            <hr>
            <div class="row form-group">
              <div class="col-md-12">
                <h4>Store Coordinates</h4>
              </div>
              <div class="form-group">
                <div class="col-md-6">
                  <label >Latitude</label>
                  <input type="text" name="latitude" required class="form-control" id="up_latitude">
                </div>
                <div class="col-md-6">
                  <label >Longitude</label>
                  <input type="text" name="longitude" required class="form-control" id="up_longitude">
                </div>
              </div>
            </div>
            
            <div class="form-group">
                <div class="col-md-6">
                  <hr>
                    <h4>Store Information</h4>
                </div>
                <div class="col-md-6">
                  <hr>
                   <h4>24 hour format</h4>
                </div>
              </div>
            <div class="row form-group">
              <div class="col-md-3">
                <label >Start Day</label>
                <select id="up_from_day" class="form-control" name="from_day">
                  <option value="1">Mondays</option>
                  <option value="2">Tuesdays</option>
                  <option value="3">Wednesdays</option>
                  <option value="4">Thursdays</option>
                  <option value="5">Fridays</option>
                  <option value="6">Saturdays</option>
                  <option value="7">Sundays</option>
                </select>
              </div>
              <div class="col-md-3">
                <label >End day</label>
                <select id="up_to_day" class="form-control" name="to_day">
                  <option value="1">Mondays</option>
                  <option value="2">Tuesdays</option>
                  <option value="3">Wednesdays</option>
                  <option value="4">Thursdays</option>
                  <option value="5">Fridays</option>
                  <option value="6">Saturdays</option>
                  <option value="7">Sundays</option>
                </select>
              </div>
              <div class="col-md-3">
                <label >Opening Hour</label>
                <!-- <input type="text" name="latitude" value="<?php echo $user->latitude ?>" class="form-control"> -->
                <div class="input-group bootstrap-timepicker">
                  <input id="up_from_hour" value="" type="text"  class="form-control timepicker-24" name="from_hour" placeholder="Pick a time">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                  </span>
                </div>

              </div>
              <div class="col-md-3">
                <label >Closing Hour</label>
                <!-- <input type="text" name="longitude"  value="<?php echo $user->longitude ?>"class="form-control"> -->
                <div class="input-group bootstrap-timepicker">
                  <input id="up_to_hour" value="" type="text"  class="form-control timepicker-24" name="to_hour" placeholder="Pick a time">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                  </span>
                </div>

                </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <label >Contact information</label>
                <textarea id="up_contact_numbers" name="contact_numbers" rows="8" cols="80" class="form-control" placeholder="Enter your numbers here separated by a new line..."></textarea>
              </div>
              <div class="col-md-6">
                <label >About</label>
                <textarea id="up_about" name="about" rows="8" cols="80" class="form-control" placeholder=""></textarea>
              </div>
              <div class="col-md-6">
                <br>
                <label >Keywords for searching</label>
                <textarea name="keywords" id="up_keywords" rows="8" cols="80" class="form-control" placeholder="Keyword1, Keyword2, Keyword 3..."></textarea>
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-12">
                <h4>Account Information</h4>
              </div>
              <div class="form-group">
                <div class="col-md-6">
                  <label >Email (used for signing-in)</label>
                  <input type="email" name="email" id="up_email" required="required" class="form-control">
                </div>
                <div class="col-md-6">
                  <label >New Password</label>
                  <input type="password" name="password" class="form-control">
                  <sub>Leave blank if you don't wish to change</sub>
                </div>
              </div>
            </div>
          <div class="modal-footer">
            <input type="hidden" name="id" id="up_id">
            <button style="margin-top:10px" data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <input style="margin-top:10px" class="btn btn-info" type="submit" value="Save changes">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
  <!-- modal -->

  <script type="text/javascript">
    $(document).ready(function() {
      $('.add-new').on('click', function(){
        $('.add-modal').modal()
      })

      $('.edit-row').on('click', function(){
        let id = $(this).data('payload').id
        let name = $(this).data('payload').name
        let location_text = $(this).data('payload').location_text
        let category = $(this).data('payload').category
        let service_id = $(this).data('payload').service_id
        let email = $(this).data('payload').email
        let latitude = $(this).data('payload').latitude
        let longitude = $(this).data('payload').longitude
        let image = $(this).data('payload').image
        let keywords = $(this).data('payload').keywords
        let banner = $(this).data('payload').banner
        let getapp_comission_percentage = $(this).data('payload').getapp_comission_percentage

        let about = $(this).data('payload').about
        let contact_numbers = $(this).data('payload').contact_numbers
        let from_hour = $(this).data('payload').from_hour
        let to_hour = $(this).data('payload').to_hour
        let from_day = $(this).data('payload').from_day
        let to_day = $(this).data('payload').to_day

        let allLocation = $(this).data('payload').allLocation
        let assigned_location = $(this).data('payload').assigned_location

        $.each(allLocation, function(i,val){
         // console.log(vald+","+val.province)
          $('#up_assigmentArea').append(`<option value="${val.id}">
                                       ${val.barangay !=null ?val.barangay +",":""} ${val.city != null?val.city+",":""}  ${val.province}
                                  </option>`)
        })

        $("#up_assigmentArea option[value="+assigned_location+"]").prop("selected", "selected")

        $('#up_about').val(about)
        $('#up_contact_numbers').val(contact_numbers)

        $('#up_from_day').val(from_day).change()
        $('#up_to_day').val(to_day).change()
        $('#up_from_hour').val(from_hour).change()
        $('#up_to_hour').val(to_hour).change()

        $('#from_hour').timepicker({
          autoclose: true,
          minuteStep: 1,
          showSeconds: true,
          showMeridian: false,
          defaultTime: 'value'
        });
        $('#to_hour').timepicker({
          autoclose: true,
          minuteStep: 1,
          showSeconds: true,
          showMeridian: false,
          defaultTime: 'value'
        });

        $('#up_name').val(name)
        $('#up_keywords').val(keywords)
        $('#up_location_text').val(location_text)
        $('#up_category').val(category)
        $('#up_service_id').val(service_id).change()
        $('#up_email').val(email)
        $('#up_latitude').val(latitude)
        $('#up_longitude').val(longitude)
        $('#up_id').val(id)
        $('#up_comission').val(getapp_comission_percentage)
        $('#up_image').attr('src', image)
        $('#up_banner').attr('src', banner)
        $('.update-modal').modal()
      })

      $('html').on('click', '.btn-delete', function(e) {
        if (confirm('Are you sure you want to delete this?', 'Yes, delete it', 'No, go back')) {
          invokeForm(base_url + 'cms/partners/delete', {id: $(this).data('id')});
        }
      })

      $('.passgen').on('click', function(){
        $('#add_pass').val(generatePassword())
      })

      function generatePassword() {
          var length = 12,
              charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
              retVal = "";
          for (var i = 0, n = charset.length; i < length; ++i) {
              retVal += charset.charAt(Math.floor(Math.random() * n));
          }
          return retVal;
      }

      $('.timepicker-24').timepicker({
          autoclose: true,
          minuteStep: 1,
          showSeconds: true,
          showMeridian: false
      });


    });
  </script>

  <script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>

