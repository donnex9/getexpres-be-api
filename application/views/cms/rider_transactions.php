<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading" style="padding:15px">
            Rider Transactions
            <?php if ($flash_msg = $this->session->flash_msg): ?>
              <br><sub style="color: <?php echo $flash_msg['color'] ?>"><?php echo $flash_msg['message'] ?></sub>
            <?php endif; ?>
            <div style="float:right">

            </div>

<hr>
            <div class="row">
              <form class="" action="" method="get">
                <div class="col-md-2">
                  <input type="date" class="form-control" name="from" value="<?php echo $this->input->get('from') ?>">
                </div>
                <div class="col-md-1">
                  <center>to</center>
                </div>
                <div class="col-md-2">
                  <input type="date" class="form-control" name="to" value="<?php echo $this->input->get('to') ?>">
                </div>

                <div class="col-md-2">
                    <input type="submit" class="btn btn-info" value="Go Filter">
                    <a  class="btn btn-warning" href="<?php echo current_url() ?>">Clear</a>
                </div>

              </form>
            </div>


          </header>
          <div class="panel-body">
            <p>
              <!-- <button type="button" class="add-new btn btn-success btn-sm">Add new</button> -->
            </p>
            <div class="table-responsive" style="overflow: hidden; outline: none;" tabindex="1">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Date</th>
                    <th>Promo Name</th>
                    <th>Amount</th>
                    <th>Duration in Hours</th>
                    <th>Rider</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if (count($res) > 0 ): ?>

                    <?php foreach ($res as $key => $value): ?>
                      <tr>
                        <th scope="row"><?php echo $startingPK++ ?></th>
                        <td><?php echo $value->created_at ?></td>
                        <td><?php echo $value->promo_name ?></td>
                        <td><?php echo $value->amount ?></td>
                        <td><?php echo $value->duration_in_hours ?></td>
                        <td><?php echo $value->rider->full_name ?></td>
                        </tr>
                      <?php endforeach; ?>


                    <?php else: ?>
                      <tr>
                        <td colspan="5" style="text-align:center"><?= ($this->input->get('squery')) ? 'Empty Search Results':'Empty table data' ?></td>
                      </tr>
                    <?php endif; ?>
                  </tbody>
                </table>
              </div>

              <style>
              .active_lg {
                background: lightgray !important
              }
              </style>
              <ul class="pagination">
                <ul class='pagination'>
                  <?php $page = ($this->input->get('page')) ?: 1; ?>
                  <li><a href="<?php echo base_url('cms/riders/transactions') . "?page=1&cart_type_id=" . @$_GET['cart_type_id'] . "&from=" . @$_GET['from'] . "&to=" . @$_GET['to'];?>">&laquo;</a></li>

                  <!-- loop for desc -->
                  <?php for ($i = $page - 2; $i < ($page) ; $i++):
                    if ($i == -1 || $i == 0) {
                      continue;
                    }
                   ?>
                  <li><a href="<?php echo base_url('cms/riders/transactions') . "?page=" . $i . "&cart_type_id=" . @$_GET['cart_type_id']. "&from=" . @$_GET['from'] . "&to=" . @$_GET['to'];?>"><?= $i ?></a></li>
                  <?php endfor; ?>
                  <!-- / loop for desc -->

                  <li><a href="<?php echo base_url('cms/riders/transactions') . "?page=" . $page . "&cart_type_id=" . @$_GET['cart_type_id'] . "&from=" . @$_GET['from'] . "&to=" . @$_GET['to'];?>"><?= $page ?></a></li>

                  <!-- loop for asc -->
                  <?php for ($i = $page + 1; $i < ($page + 3) ; $i++):
                  if ($i == $total_pages + 1 || $i == $total_pages + 2 || $total_pages == 0) {
                      continue;
                  }
                  ?>
                  <li><a href="<?php echo base_url('cms/riders/transactions') . "?page=" . $i . "&cart_type_id=" . @$_GET['cart_type_id']  . "&from=" . @$_GET['from'] . "&to=" . @$_GET['to'];?>"><?= $i ?></a></li>
                  <?php endfor; ?>
                  <!-- / loop for asc -->


                <li><a href="<?php echo base_url('cms/riders/transactions') . "?page=" . $total_pages . "&cart_type_id=" . @$_GET['cart_type_id']  . "&from=" . @$_GET['from'] . "&to=" . @$_GET['to'];?>">&raquo;</a></li>
                </ul>
              </ul>

            </div>
          </section>
        </div>
      </div>
      <!-- page end-->
    </section>
  </section>

  <!-- Modal -->
  <div class="modal fade add-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Manage</h4>
        </div>
        <div class="modal-body">

          <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('cms/news/add') ?>">
            <div class="form-group">
              <label >Title</label>
              <input type="text" class="form-control" name="title"  required="required">
            </div>
            <div class="form-group">
              <label >Category</label>
              <input type="text" class="form-control" name="category"  required="required">
            </div>
            <div class="form-group">
              <label >Body</label>
              <textarea name="body" class="form-control" required="required"></textarea>
            </div>
            <div class="form-group">
              <label >Banner Image</label>
              <input type="file" name="banner_image" class="form-control">
            </div>
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <input class="btn btn-info" type="submit" value="Save changes">
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- modal -->

  <!-- Modal -->
  <div class="modal fade update-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Manage</h4>
        </div>
        <div class="modal-body">

          <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('cms/news/update') ?>">
            <div class="form-group">
              <label >Title</label>
              <input type="text" class="form-control" name="title"  id="up_title" required="required">
            </div>
            <div class="form-group">
              <label >Category</label>
              <input type="text" class="form-control" name="category" id="up_category" required="required">
            </div>
            <div class="form-group">
              <label >Body</label>
              <textarea name="body" class="form-control" id="up_body" required="required"></textarea>
            </div>
            <div class="form-group">
              <label >Banner Image</label>
              <br>
              <img src="" id="up_image" style="max-width:150px">
              <br>
              <br>
              <input type="file" name="banner_image" class="form-control">
            </div>
          <div class="modal-footer">
            <input type="hidden" name="id" id="up_id">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <input class="btn btn-info" type="submit" value="Save changes">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
  <!-- modal -->

  <script type="text/javascript">
    $(document).ready(function() {
      $('.add-new').on('click', function(){
        $('.add-modal').modal()
      })

      $('.edit-row').on('click', function(){
        let id = $(this).data('payload').id
        let title = $(this).data('payload').title
        let body = $(this).data('payload').body
        let category = $(this).data('payload').category
        let image = $(this).data('payload').image

        $('#up_title').val(title)
        $('#up_body').val(body)
        $('#up_category').val(category)
        $('#up_id').val(id)
        $('#up_image').attr('src', image)
        $('.update-modal').modal()
      })

      $('html').on('click', '.btn-delete', function(e) {
        if (confirm('Are you sure you want to delete this?', 'Yes, delete it', 'No, go back')) {
          invokeForm(base_url + 'cms/news/delete', {id: $(this).data('id')});
        }
      })

    });
  </script>

  <script src="<?php echo base_url('public/admin/js/custom/') ?>generic.js"></script>
