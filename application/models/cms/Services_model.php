<?php

class Services_model extends Admin_core_model
{

  function __construct()
  {
    parent::__construct();

    $this->table = 'services'; # Replace these properties on children
    $this->upload_dir = 'services'; # Replace these properties on children
    $this->per_page = 15;
  }

  public function all()
  {
    $res = $this->db->get($this->table)->result();

    if (!$res) {
      return [];
    }
    foreach ($res as $key => &$value) {
      $value = $this->formatRes($value);
    }
    return $res;
  }

  public function allHaveCMSAccess()
  {
    $this->db->where('has_cms_access', 1);
    $res = $this->db->get($this->table)->result();
    if (!$res) {
      return [];
    }

    foreach ($res as $key => &$value) {
      $value = $this->formatRes($value);
    }
    return $res;
  }

  public function formatRes($data)
  {
    $data->type_f = ucwords($data->type);
    return $data;
  }

}
