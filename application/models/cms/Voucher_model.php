<?php

class Voucher_model extends Admin_core_model
{

  function __construct()
  {
    parent::__construct();

    $this->table = 'voucher_code'; # Replace these properties on children
    
    //$this->per_page = 10;
    $this->load->model('api/cart_model');
    $this->load->model('api/customers_model');
    $this->load->model('api/rider_vehicles_model');
  }


  public function all()
  {
    $this->paginate();
   // $this->squery(['full_name']); # pass array for columns to check like
    $res = $this->db->get($this->table)->result();
    if (!$res) {
      return [];
    }
    return $this->voucherExpiration($res);
  }

  
   public function allSerial()
  {
    $this->paginate();
   // $this->squery(['full_name']); # pass array for columns to check like
    $res = $this->db->get($this->table)->result();
    if (!$res) {
      return [];
    }
    return $this->voucherExpirationSerial($res);
  }
 
  
   public function voucherExpirationSerial($ver)
  {

   foreach ($ver as $key => &$value) {
      $isexpiredcode = $this->compareDates($value->expired_at);
      if($isexpiredcode == true)
      {
        $this->update($value->id,array("status"=>0));
      }

     // $used = $this->cart_model->countVoucher($value->voucher_code,$this->session->userdata('location'));
      //$this->cart_model->updatevoucherCode($value->voucher_code,$this->session->userdata('location'),$used);
    }
   
    $this->db->where('assigned_location', $this->session->userdata('location'));
    $this->db->where('is_serial','1');
    $res = $this->db->get($this->table)->result();

    foreach ($res as $key => &$value) {
      $value = $this->formatRes($value);
    }
    return $res;

  }


  public function voucherExpiration($ver)
  {

   foreach ($ver as $key => &$value) {
      $isexpiredcode = $this->compareDates($value->expired_at);
      if($isexpiredcode == true)
      {
        $this->update($value->id,array("status"=>0));
      }

     // $used = $this->cart_model->countVoucher($value->voucher_code,$this->session->userdata('location'));
      //$this->cart_model->updatevoucherCode($value->voucher_code,$this->session->userdata('location'),$used);
    }
   
    $this->db->where('assigned_location', $this->session->userdata('location'));
    $this->db->where('is_serial','0');
    $res = $this->db->get($this->table)->result();

    foreach ($res as $key => &$value) {
      $value = $this->formatRes($value);
    }
    return $res;

  }

  public function compareDates($d1)
  {
    $date1 = date("Y-m-d",strtotime($d1));
    $date2 = date("Y-m-d",strtotime(date('Y-m-d')));

    $d1 = strtotime($date1);
    $d2 = strtotime($date2);

    if($d2 <= $d1)
    {
      return false;
    }else{
      return true;
    }
  }

public function add($data)
  {
    $this->db->insert($this->table, $data);
    $last_id = $this->db->insert_id();
    return $last_id;
  }

  public function addGenerateSerialVoucher($data,$initialCode,$numberOfPieces)
  {
    
    $x = 1;

    do {

      $serialCode = trim($initialCode.$this->serialVoucherGenerator(6));
      $this->db->where('voucher_code',$serialCode);
      $resCode = $this->db->get('voucher_code')->row();

      if(count($resCode) == 0){
        $data = array_merge($data,array('voucher_code' =>$serialCode));
        $this->db->insert($this->table, $data);
         $last_id = $this->db->insert_id();
        
         $x++;
      }

    } while ($x <= $numberOfPieces);


    if ($x > 1 and $x <= $numberOfPieces) {
      return true;
    }else{ return false;}

  }

  private function serialVoucherGenerator($length){
       $chars='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
     // Count the total chars
     $totalChars = strlen($chars);

     // Get the total repeat
     $totalRepeat = ceil($length/$totalChars);

     // Repeat the string
     $repeatString = str_repeat($chars, $totalRepeat);

     // Shuffle the string result
     $shuffleString = str_shuffle($repeatString);

     // get the result random string
      return substr($shuffleString,1,$length);
  }

  public function update($id, $data)
  {
    $this->db->where('id', $id);
    return $this->db->update($this->table, $data);
  }

  public function updateMeta($metaKey,$data,$loc){
    $this->db->where('meta_key', $metaKey);
    $this->db->where('dynamic_location', $loc);
    return $this->db->update("options", $data);
  }
  
  function formatRes($res)
  {
      if (!$res) {
        return false;
      }
      $this->db->where('id',$res->created_by);
      $res->created_by = @$this->db->get('admin')->row()->name;
      $res->used = $res->used == null ? 0: $res->used ;
      return $res;
  }



}

