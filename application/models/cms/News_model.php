<?php

class News_model extends Admin_core_model
{

  function __construct()
  {
    parent::__construct();

    $this->table = 'news'; # Replace these properties on children
    $this->upload_dir = 'news'; # uploads/your_dir
    $this->uploads_folder = "efs/uploads/" . $this->upload_dir . "/";
    $this->full_up_path = base_url() . "efs/uploads/" . $this->upload_dir . "/"; # override this block on your child class. just redeclare it
    $this->per_page = 15;
  }

  public function all()
  {

    $this->paginate();
    $this->squery(['title']); # pass array for columns to check like
    $this->db->where('assigned_location', $this->session->userdata('location'));
    $res = $this->db->get('news')->result();

    if (!$res) {
      return [];
    }

    foreach ($res as $key => &$value) {
      $value = $this->formatRes($value);
    }
    return $res;
  }

public function add($data)
  {
    $this->db->insert($this->table, $data);
    $last_id = $this->db->insert_id();

    return $last_id;
  }


  public function update($id, $data)
  {
    $this->db->where('id', $id);
    return $this->db->update($this->table, $data);
  }

  public function formatRes($data)
  {
    $data->banner_image_f = $data->banner_image ? $this->full_up_path . $data->banner_image : base_url('public/admin/img/placeholder-whatsnew-cover.png');
    return $data;
  }

}
