<?php

class Admin_model extends Admin_core_model
{

  function __construct()
  {
    parent::__construct();

    $this->table = 'admin'; # Replace these properties on children
    $this->upload_dir = 'admin'; # Replace these properties on children
    $this->per_page = 15;
  }

  public function getRates($service_id, $vehicle_id,$location)
  {

    $this->db->where('service_id', $service_id);
    $this->db->where('vehicle_id', $vehicle_id);
    $this->db->where('dynamic_location',$location);
    $row = $this->db->get('rates')->row();
   
    //var_dump($row);

    if (!(array)$row) {
      $row = (object)[
        'service_id' => $service_id,
        'vehicle_id' =>$vehicle_id,
        'base_fare' => 50.00,
        'per_km_fare' => 6.00,
        'getapp_commission_percentage' => 0.15,
      ];
    }

    return $row;
  }

  public function getuseraccess($menuid,$userlevel)
  {
   
    $this->db->where('menu_id', $menuid);
    $this->db->where('user_level', $userlevel);
    return $this->db->get('tbl_access_level')->row();
  }

  public function getRatesTrike($service_id, $vehicle_id,$location,$munzone,$zone)
  {

    $this->db->where('service_id', $service_id);
    $this->db->where('vehicle_id', $vehicle_id);
    $this->db->where('dynamic_location',$location);
    $this->db->where('mun_cities_id',$munzone);
    $this->db->where('zone_id',$zone);

    $row = $this->db->get('tricycle_rates')->row();
   
    //var_dump($row);

/*    if (!(array)$row) {
      $row = (object)[
        'service_id' => $service_id,
        'vehicle_id' =>$vehicle_id,
        'base_fare' => 50.00,
        'per_km_fare' => 6.00,
        'admin_fee' => 1.00,
        'passenger_limit' => 10,
        'pabili_fee' => 1,
        'getapp_commission_percentage' => 7
      ];
    }*/

    return $row;
  }

  public function getAllRates()
  {
    $res = $this->db->get_where("rates", array('dynamic_location' => $this->session->userdata('location')))->result();
    
    $new_res = [];
    foreach ($res as $key => $value) {
      $new_res[$value->vehicle_id][$value->service_id] = (object)['base_fare' => $value->base_fare, 'per_km_fare' => $value->per_km_fare, 'getapp_commission_percentage' => $value->getapp_commission_percentage, 'surge_multiplier' => $value->surge_multiplier, 'per_minute_fee' => $value->per_minute_fee];
    }

    return $new_res;
  }

  public function getAllTrikeRates(){
    $res = $this->db->get_where("tricycle_rates", array('dynamic_location' => $this->session->userdata('location')))->result();
    
    return $res;
  }

  public function add($data)
  {
    $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

    $this->db->insert($this->table, $data);
    $last_id = $this->db->insert_id();

    return $last_id;
  }
  public function update($id, $data)
  {
    if (@$data['password'] != '') {
      $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
    } else {
      unset($data['password']);
    }

    $this->db->where('id', $id);
    return $this->db->update($this->table, $data);
  }

  public function updateAdmin($id,$data)
  {
    if (@$data['password'] != '') {
      $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
    } else {
      unset($data['password']);
    }

    $this->db->where('id', $id);
    return $this->db->update($this->table, $data);
  }

  public function getMeta($meta_key,$location)
  {
    $this->db->where('meta_key', $meta_key);
    $this->db->where('dynamic_location',$location);
    return $this->db->get('options')->row()->meta_value;
  }

  public function getMetaSurgeFees($meta_key,$location)
  {
    $this->db->where('dynamic_location', $location);
    $this->db->where('meta_key',$meta_key);
    return $this->db->get('options')->row()->meta_value;
  }


  public function getDynamicPlaces(){
    return $this->db->get('dynamic_radius_places')->result_array();
  }

  public function getZoneArea($assignedLocation,$muni){
    $this->db->where('assigned_location',$assignedLocation);
    $this->db->where('mun_cities',$muni);
    return $this->db->get('tricycle_zone')->result_array();
  }

  public function getCityArea($assignedLocation){
    $this->db->where('assigned_location',$assignedLocation);
    return $this->db->get('tricycle_city_mun')->result_array();
  }

  public function getBoundaries($assigned_location,$cityid,$zoneid){
    $this->db->where('zone_id',$zoneid);
    $this->db->where('city_id',$cityid);
    $this->db->where('assigned_location',$assigned_location);
    $this->db->order_by('id','ASC');

    return $this->db->get('tbl_polygon')->result_array();
  }

  public function getStoreRadius($location)
  {

    $this->db->where('id',$location);
    return $this->db->get('dynamic_radius_places')->row()->store_radius;
  }

  public function getMapCenter($location)
  {
    $this->db->where('id', $location);
    return $this->db->get('dynamic_radius_places')->row()->map_center;
  }

 
  public function getAddionalPabiliFee()
  {
    $this->db->where('id', $this->session->userdata['location']);
    return $this->db->get('dynamic_radius_places')->row()->additional_pabili_fee;
  }


  public function getRadius()
  {
    $this->db->where('id', $this->session->userdata['location']);
    return $this->db->get('dynamic_radius_places')->row()->rider_radius;
  }

  function getRiderWalletOffers($vehicle_id){
    $res = [];
    $offers = [];

    switch ($vehicle_id) {
        // code...
        // break;
      // case 2:
      // $offers = $this->getMetaMultiple('rider_wallet_offer_bicycle');

      case 3:
      default:
      # Default is motorcycle
      $offers = $this->getMetaMultiple('rider_wallet_offer');
        break;
    }

    if (!$offers) {
      return [];
    }

    foreach ($offers as $key => $value) {
      $meta_val = unserialize($value->meta_value);
      $meta_val->id = $value->id;
      $res[] = $meta_val;
    }

    return $res;
  }

  public function getMetaMultiple($meta_key)
  {
    $this->db->where('meta_key', $meta_key);
    return $this->db->get('options')->result();
  }

  public function getcities($assigned_location)
  {
    $this->db->where('assigned_location', $assigned_location);
    return $this->db->get('tricycle_city_mun')->result();
  }

   public function getzone($assigned_location)
  {
    $this->db->where('assigned_location', $assigned_location);
    return $this->db->get('tricycle_zone')->result();
  }

  public function getMaintenanceModeValueByServiceId($service_id,$location)
  {
    switch ((int)$service_id) {
      case 1:
      return $this->admin_model->getMeta('get_car_mm',$location);
        break;

      case 2:
      return $this->admin_model->getMeta('get_grocery_mm',$location);
        break;

      case 3:
      return $this->admin_model->getMeta('get_pabili_mm',$location);
        break;

      case 4:
      return $this->admin_model->getMeta('get_delivery_mm',$location);
        break;

      case 5:
      return $this->admin_model->getMeta('get_food_mm',$location);
        break;

        case 7:
      return $this->admin_model->getMeta('get_laundry_mm',$location);
        break;

      default:
      return 0;
        break;

    }

  }


}

