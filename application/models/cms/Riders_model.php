<?php

class Riders_model extends Admin_core_model
{

  function __construct()
  {
    parent::__construct();

    $this->table = 'riders'; # Replace these properties on children
    $this->upload_dir = 'riders'; # uploads/your_dir
    $this->uploads_folder = "efs/uploads/" . $this->upload_dir . "/";
    $this->full_up_path = base_url() . "efs/uploads/" . $this->upload_dir . "/"; # override this block on your child class. just redeclare it
    $this->per_page = 30;
    $this->load->model('api/customers_model');
    $this->load->model('api/rider_vehicles_model');
  }


  public function countAllByPromo($promo_name,$location)
  {
    $this->db->where('promo_name', $promo_name);
    $this->db->where('assigned_location', $location);
    return $this->db->count_all_results('rider_wallet');
  }

  public function getActiveRiders($type,$location)
  {
    switch ($type) {
      case 'daily':
        $this->db->where('assigned_location', $location);
        $this->db->where('DATE(updated_at) = CURDATE()');
        break;
      case 'weekly':
        $this->db->where('assigned_location', $location);
        $this->db->where('YEARWEEK(updated_at) = YEARWEEK(NOW())');
        break;
      case 'monthly':
        $this->db->where('assigned_location', $location);
        $this->db->where('YEAR(updated_at) = YEAR(NOW()) AND MONTH(updated_at) = MONTH(NOW())');
        break;

      default:
        break;
    }

    $this->db->where('rider_id != 0');
    $this->db->group_by('rider_id');
    return $this->db->count_all_results('cart');
  }



  public function all()
  {
    $this->paginate();
    $this->squery(['full_name']); # pass array for columns to check like
    $res = $this->db->get($this->table)->result();

    if (!$res) {
      return [];
    }

    foreach ($res as $key => &$value) {
      $value = $this->formatRes($value);
    }
    return $res;
  }

public function add($data)
  {
    $this->db->insert($this->table, $data);
    $last_id = $this->db->insert_id();

    return $last_id;
  }

public function checkEmailNumberExist($data){

  $this->db->where('mobile_num',$data['mobile_num']);
  $resmob = $this->db->count_all_results('riders');

  if ($this->customers_model->isEmailExist($data['email'], 'riders')) {
      return "Email already exist.";
  }else if($resmo > 0){
      return "Mobile number already exist.";
  }else{
    return null;
  }

 } 

 public function addRiderCms($data)
  {
    if ($this->customers_model->isEmailExist($data['email'], 'riders')) {
      return null;
    }
    $vehicle['vehicle_id'] = @$data['vehicle_id'];
    $vehicle['vehicle_model'] = @$data['vehicle_model'];
    $vehicle['plate_number'] = @$data['plate_number'];
    //$data['assigned_location'] = 1;

    unset($data['vehicle_id'], $data['vehicle_model'], $data['plate_number']);

    $is_social = @$data['social_token'] && @$data['is_email_verified'];

    if (!$is_social) {
      $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
      $data['verification_token'] = $this->customers_model->generateToken();
    } else {
      unset($data['password']);
    }

    $res = $this->db->insert('riders', $data);
    if ($res && !$is_social) {
      $this->customers_model->sendToken($data['email'], $data['full_name'], $data['verification_token'], 'riders');
    }

    $last_id = $this->db->insert_id();

    $last_vehicle_id = $this->rider_vehicles_model->addVehicle($vehicle, $last_id);
    $this->rider_vehicles_model->makeVehicleActive($last_id,  $last_vehicle_id);

    return $last_id;
  }

  public function update($id, $data)
  {
    if (!$this->input->post('is_admin_verified')) {
      $data['is_admin_verified'] = 0;
    }

    if (!$this->input->post('is_email_verified')) {
      $data['is_email_verified'] = 0;
    }

    $this->db->where('id', $id);
    return $this->db->update($this->table, $data);
  }

  function formatRes($res)
  {
      if (!$res) {
        return false;
      }
      $res->vehicles = $this->rider_vehicles_model->getRiderVehicles($res->id);
      $res->unverified_vehicles = $this->rider_vehicles_model->getRiderVehiclesUnverified($res->id);
      $res->all_vehicles = array_merge($res->vehicles, $res->unverified_vehicles);
      $res->active_vehicle = $this->rider_vehicles_model->getActiveVehicle($res->id);
      $res->profile_picture = ($res->profile_picture) ? $this->full_up_path . $res->profile_picture : $this->temp_image;
      $res->identification_document_name = $res->identification_document;
      $res->identification_document = ($res->identification_document) ? $this->full_up_path . $res->identification_document : $this->temp_image;
      return $res;
  }


  public function getAllTransactions()
  {
    $this->paginate();
    $this->filterTrans();
    $this->db->where('assigned_location', $this->session->userdata('location'));
    $this->db->order_by('created_at', 'desc');
    $transactions = $this->db->get('rider_wallet')->result();
    if (!$transactions) {
      return [];
    }

    foreach ($transactions as $key => &$value) {
      $value->rider = $this->get($value->rider_id);
      $value->created_at = date('F j, Y g:i a', strtotime($value->created_at));
    }

    return $transactions;
  }


  public function filterTrans()
  {
    if ($this->input->get('from')) {
      $this->db->where('created_at >= "' . $this->input->get('from') . '"');
    }
    if ($this->input->get('to')) {
      $this->db->where('created_at <= "' . $this->input->get('to') . '"');
    }

  }


  public function manageBan($timestamp, $table, $id)
  {
    // var_dump($timestamp, $id, $table); die();
    $this->db->where('id', $id);
    return $this->db->update($table, ['banned_at' => $timestamp]);
  }

  public function getTotalPagesRiderWallet()
  {
    $this->filterTrans();
    $this->db->where('assigned_location', $this->session->userdata('location'));
    return ceil($this->db->count_all_results('rider_wallet') / $this->per_page);
  }

  public function getAllRidersWithBalance()
  {
    $this->db->limit("99999999");
    $this->db->where('assigned_location', $this->session->userdata('location'));
    $this->db->where('is_admin_verified', 1);
    $riders = $this->db->get('riders')->result();
    if (!$riders) {
      return [];
    }

    foreach ($riders as $key => &$value) {
      $value->balance = number_format($this->walletBalancesum($value->id), 2);
    }

    return $riders;
  }

  public function addWalletBalanceBulk($rider_ids, $amount)
  {
    foreach ($rider_ids as $key => $value) {
      $this->db->insert('rider_balance', ['wallet_balance' => $amount, 'type' => 'topup', 'rider_id' => $value, 'status' => 'paid', 'transaction_id' => '', 'checkout_url' => '', 'rider_wallet_fk' => 0]);
    }

    return true;
  }

  public function walletBalancesum($rider_id){

   $this->db->select_sum('wallet_balance');
   $this->db->where('rider_id', $rider_id);
   $this->db->where('status','paid');
   return @$this->db->get('rider_balance')->row()->wallet_balance;

  }

}
