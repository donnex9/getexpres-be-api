<?php

class Partners_model extends Admin_core_model
{

  function __construct()
  {
    parent::__construct();

    $this->table = 'partners'; # Replace these properties on children
    $this->upload_dir = 'partners'; # uploads/your_dir
    $this->uploads_folder = "efs/uploads/" . $this->upload_dir . "/";
    $this->full_up_path = base_url() . "efs/uploads/" . $this->upload_dir . "/"; # override this block on your child class. just redeclare it
    $this->per_page = 15;

    $this->load->library('email');
    $config_mail['protocol'] = getenv('MAIL_PROTOCOL');
    $config_mail['smtp_host'] = getenv('SMTP_HOST');
    $config_mail['smtp_port'] = getenv('SMTP_PORT');
    $config_mail['smtp_user'] = getenv('SMTP_EMAIL');
    $config_mail['smtp_pass'] = getenv('SMTP_PASS');
    $config_mail['mailtype'] = "html";
    $config_mail['charset'] = "utf-8";
    $config_mail['newline'] = "\r\n";
    $config_mail['wordwrap'] = TRUE;

    $this->email->initialize($config_mail);
    $this->load->model('api/cart_model');
  }

  public function all_partners()
  {
     $this->db->where('assigned_location', $this->session->userdata('location'));
    $this->db->order_by('name', 'asc');
    $res = $this->db->get('partners')->result();

    if (!$res) {
      return [];
    }

    foreach ($res as $key => &$value) {
      $value = $this->formatRes($value);
    }
    return $res;
  }

  public function all()
  {
    $this->paginate();
    $this->squery(['name']); # pass array for columns to check like
    $this->db->where('assigned_location', $this->session->userdata('location'));
    $res = $this->db->get('partners')->result();

    if (!$res) {
      return [];
    }

    foreach ($res as $key => &$value) {
      $value = $this->formatRes($value);
    }
    return $res;
  }



  public function getPartnerSales()
  {
  
    $this->db->select('a.*,b.name');
    $this->db->join('partners as b ','a.partner_id = b.id');
    $res = $this->db->get('invoices as a')->result();
    
    return $res;
  }

  public function add($data)
  {
    if (@$data['alert_owner'] == 1) {
      $this->alertPartner($data['email'], $data['name'], $data['password']);
    }

    $data['coordinates'] = json_encode(['latitude' => $data['latitude'], 'longitude' => $data['longitude']]);
    unset($data['latitude'], $data['longitude'], $data['alert_owner']);
    $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

    $this->db->insert($this->table, $data);
    $last_id = $this->db->insert_id();

    return $last_id;
  }


  function genetateInvoices()
  {

    $this->db->where("status = 0");
    $getInvoiceDate =  $this->db->get('weekly_cut_off')->result();
    
    if (!$getInvoiceDate) 
    {
      return [];
    }

    $res = (object)[];
    $newRes =  [];

    $sDate = "";
    $eDate = "";
    $cutOffId = "";

     foreach ($getInvoiceDate as $key => $value)
     {
      $startDate = "'".$value->start_date."'";
      $endDate   = "'".$value->end_date."'";

      $sDate = $value->start_date;
      $eDate = $value->end_date;
      $cutOffId =$value->id;

      $this->db->where('a.status','completed');
      $this->db->where_in('a.cart_type_id',[2,5]);
      $this->db->where('a.partner_id <> 0');
      $this->db->where("DATE(a.completed_at) between $startDate and $endDate");
      $this->db->select('a.id,a.status,a.partner_id,a.payload,b.total');    
      $this->db->join("(SELECT cart_id, SUM(price_when_added * quantity) AS 'total'  FROM cart_items GROUP BY cart_id) AS b", 'a.id = b.cart_id');

      $res = $this->db->get('cart as a')->result();

     foreach ($res as $key => $val)
      {
        $resn = unserialize($val->payload)?: [];
        $forInsert = [
                      "cart_id" => $val->id,
                      "cart_status" => $val->status,
                      "amount" => $val->total,
                      "partner_id" => $val->partner_id,
                      "getapp_commision" => $resn["getapp_commission"],
                      "start_date" =>$value->start_date,
                      "end_date" =>$value->end_date
                     ];
         array_push($newRes,$forInsert);
      }
     }


    $isInsert =  false;
    foreach ($newRes as $key =>$ins)
    {  
      $this->db->trans_begin();
      $rst = $this->db->insert("pre_invoice", $ins);

      if($this->db->trans_status() === FALSE || !isset($rst))
      {
        $isInsert = false;
        $this->db->trans_rollback();
      }else
      {
        $this->db->trans_commit();
        $isInsert = true;
      }
    }


   if ($isInsert == true) 
   {
     $this->db->update("weekly_cut_off", ["status" => 1]);

     $this->db->select("sum(amount) as 'amount',partner_id,SUM(getapp_commision) as 'gc' ");
     $this->db->where("DATE(start_date)", $sDate);
     $this->db->where("DATE(end_date)",$eDate);
     $this->db->group_by("partner_id");
     $resp = $this->db->get("pre_invoice")->result();
   }

   //  print_r($resp);

   foreach ($resp as $key => $invoice) {
   $this->db->select("MAX(invoice_no) as id");
   $rw = $this->db->get("invoices")->row();
   
   $invoiceDate = $this->getServerDate();
   $dueDate = date('Y-m-d', strtotime($invoiceDate. ' + 2 days'));
  

    $forInsertInv = [
                      "partner_id" => $invoice->partner_id,
                      "due_date"   => $dueDate,
                      "invoice_date"   => $invoiceDate,
                      "invoice_amount" => $invoice->gc,
                      "status"     => "unpaid",
                      "cut_off_id" => $cutOffId,
                      "pdf_file"   => "",
                      "payment_option" => "",
                      "total_sales" => $invoice->amount,
                      "invoice_no"  => $this->invoice_num((INT)$rw->id+1, 8, $prefix = null)
                    ];

       //print_r($forInsertInv); continue;             

      $this->db->trans_begin();
      $rst1 = $this->db->insert("invoices", $forInsertInv);

      if($this->db->trans_status() === FALSE || !isset($rst1))
      {
        $isInsert = false;
        $this->db->trans_rollback();
      }else
      {
        $this->db->trans_commit();
        $isInsert = true;
      }
   }
  
    
    return $isInsert;
  }

  public function getServerDate()
  {
    $manilatimezone = new DateTimeZone('Asia/Manila');
    $dt = new DateTime('now', $manilatimezone);
    $currentdate = $dt->format("Y-m-d");
  
    return $currentdate ;
  }




  function invoice_num ($input, $pad_len = 8, $prefix = null) 
  {
    if ($pad_len <= strlen($input))
        trigger_error('<strong>$pad_len</strong> cannot be less than or equal to the length of <strong>$input</strong> to generate invoice number', E_USER_ERROR);

    if (is_string($prefix))
        return sprintf("%s%s", $prefix, str_pad($input, $pad_len, "0", STR_PAD_LEFT));

    return str_pad($input, $pad_len, "0", STR_PAD_LEFT);
 }


  public function getByEmail($email)
  {
    $res = $this->db->get_where($this->table, array('email' => $email))->row();
    if (!$res) {
      return false;
    }

    return $this->formatRes($res);
  }

  public function get($id)
  {
    $this->db->where('id', $id);
    $res = $this->db->get('partners')->row();
    if (!$res) {
      return (object)[];
    }

    $res = $this->formatRes($res);

    return $res;
  }

public function updateStoreAvail($id, $data)
  {
    
    $this->db->where('id', $id);
    return $this->db->update($this->table, $data);
  }

  public function update($id, $data)
  {
    $data['coordinates'] = json_encode(['latitude' => $data['latitude'], 'longitude' => $data['longitude']]);
    unset($data['latitude'], $data['longitude']);

    if (@$data['password'] != '') {
      $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
    } else {
      unset($data['password']);
    }

    $this->db->where('id', $id);
    return $this->db->update($this->table, $data);
  }

  public function formatRes($data)
  {
    $coordinates = json_decode($data->coordinates);
    $data->latitude = $coordinates->latitude;
    $data->longitude = $coordinates->longitude;
    $data->image = $data->image ? $this->full_up_path . $data->image : $this->temp_image;
    $data->banner = $data->banner ? $this->full_up_path . $data->banner : base_url('public/admin/img/placeholder-foodgroc-cover.png');

    return $data;
  }

  function alertPartner($email, $name, $password)
  {
    $html = $this->generateAlertPartnerEmail($email, $name, $password);
    return $this->sendMail($email, 'Partner Account Details [Get Express]', $html);
  }

  public function sendMail($to, $subject, $message)
  {
    $this->email->from('noreply@getapp.betaprojex.com', 'Get Express');
    $this->email->to($to);
    $this->email->subject($subject);
    $this->email->message($message);
    return $this->email->send();
  }

  public function generateAlertPartnerEmail($email, $name = 'Partner', $password)
 {

   $link = base_url('cms/login');

   $html = '<table class="container" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
   <tbody>
   <tr bgcolor="7087A3"><td height="15"></td></tr>

   <tr bgcolor="7087A3">
       <td align="center">
           <table class="container-middle" align="center" border="0" cellpadding="0" cellspacing="0" width="560">
               <tbody><tr>
                   <td>
                       <table class="top-header-left" align="left" border="0" cellpadding="0" cellspacing="0">
                           <tbody><tr>
                               <td align="center">
                                   <table class="date" border="0" cellpadding="0" cellspacing="0">
                                       <tbody><tr>
                                           <td style="color: #fff; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">
                                           Partner Account Created
                                           </td>
                                           <td>&nbsp;&nbsp;</td>
                                           <td style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">
                                               <singleline>
                                               </singleline>
                                           </td>
                                       </tr>

                                       </tbody></table>
                               </td>
                           </tr>
                           </tbody></table>

                       <table class="top-header-right" align="left" border="0" cellpadding="0" cellspacing="0">
                           <tbody><tr><td height="20" width="30"></td></tr>
                           </tbody></table>

                       <table class="top-header-right" align="right" border="0" cellpadding="0" cellspacing="0">
                           <tbody><tr>
                               <td align="center">
                                   <table class="tel" align="center" border="0" cellpadding="0" cellspacing="0">
                                       <tbody><tr>
                                           <td style="color: #fff; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">
                                           Get Express
                                           </td>
                                           <td>&nbsp;&nbsp;</td>
                                           <td style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">
                                               <singleline>
                                               </singleline>
                                           </td>
                                       </tr>
                                       </tbody></table>
                               </td>
                           </tr>
                           </tbody></table>
                   </td>
               </tr>
               </tbody></table>
       </td>
   </tr>

   <tr bgcolor="7087A3"><td height="10"></td></tr>

   </tbody>
</table>

<!--  end top header  -->


<!-- main content -->
<table class="container" align="center"  border="0" cellpadding="0" cellspacing="0" width="600" bgcolor="ffffff">


<!--Header-->
<tbody>

<!--section 1 -->
<tr>
   <td>
       <table class="container-middle" align="center" border="0" cellpadding="0" cellspacing="0" width="560" bgcolor="F1F2F7">
           <tr >
               <td>
                   <table class="mainContent" align="center" border="0" cellpadding="0" cellspacing="0" width="528">
                       <tbody><tr><td height="20"></td></tr>
                       <tr>
                           <td>


                               <table class="section-item" align="left" border="0" cellpadding="0" cellspacing="0" width="360">
                                   <tbody><tr>
                                       <td style="color: #484848; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">

                                           Congratulations!

                                       </td>
                                   </tr>
                                   <tr><td height="15"></td></tr>
                                   <tr>
                                       <td style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">

                                           Howdy '.$name.'! Your account was created successfully. Click on the link below to access your Partner Dashboard.<br>
                                           Your password is <span style="font-weight:bold;color:#333;">'. $password .'</span><br><br>Please don\'t share your password to anyone. It is also recommended that you change your password upon first sign-in. <br><br>Thank you!

                                       </td>
                                   </tr>
                                   <tr><td height="15"></td></tr>
                                   <tr>
                                       <td>
                                           <a href="'.$link.'" style="background-color: #7087A3; font-size: 12px; padding: 10px 15px; color: #fff; text-decoration: none">Take me to the Partner Dashboard</a>
                                       </td>
                                   </tr>
                                   </tbody></table>
                           </td>
                       </tr>

                       <tr><td height="20"></td></tr>

                       </tbody></table>
               </td>
           </tr>



           </table>
   </td>
</tr>
<!-- end section 1-->

<!-- footer -->
<table class="container" border="0" cellpadding="0" cellspacing="0" width="600">
   <tbody>
   <tr bgcolor="7087A3"><td height="15"></td></tr>
   <tr bgcolor="7087A3">
       <td  style="color: #fff; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;" align="center">

           Get Express © Copyright 2020 . All Rights Reserved

       </td>
   </tr>

   <tr>
       <td bgcolor="7087A3" height="14"></td>
   </tr>
   </tbody></table>';

   return $html;
 }

}
