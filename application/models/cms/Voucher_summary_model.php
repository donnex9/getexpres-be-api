<?php

class Voucher_summary_model extends Admin_core_model
{

  function __construct()
  {
    parent::__construct();

    $this->table = 'voucher_summary'; # Replace these properties on children
    
    $this->per_page = 30;
    $this->load->model('api/customers_model');
    $this->load->model('api/riders_model');
  }


  public function all()
  {
    $this->paginate();
   // $this->squery(['full_name']); # pass array for columns to check like
    $res = $this->db->get($this->table)->result();
    if (!$res) {
      return [];
    }

    foreach ($res as $value)
    {
      $value = $this->formatRes($value);
    }
    return $res;
  }


  public function update($id, $data)
  {
    $this->db->where('booking_num', $id);
    return $this->db->update($this->table, $data);
  }

  
  function formatRes($res)
  {
    
      $this->db->where('id',$res->customer_id);
      $res->customer_id = @$this->db->get('customers')->row()->full_name;

      $this->db->where('id',$res->rider_id);
      $res->rider_id = @$this->db->get('riders')->row()->full_name;

      $this->db->where('id',$res->service_type);
      $res->service_type = @$this->db->get('services')->row()->type;

      $this->db->where('id',$res->updated_by);
      $res->updated_by = @$this->db->get('admin')->row()->name;

      return $res;
  }



}

