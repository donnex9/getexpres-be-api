<?php

class Inventory_model extends Admin_core_model
{

  function __construct()
  {
    parent::__construct();

    $this->table = 'inventory'; # Replace these properties on children
    $this->upload_dir = 'inventory'; # uploads/your_dir
    $this->uploads_folder = "efs/uploads/" . $this->upload_dir . "/";
    $this->full_up_path = base_url() . "efs/uploads/" . $this->upload_dir . "/"; # override this block on your child class. just redeclare it
    $this->per_page = 15;
  }

  public function all()
  {
    $this->paginate();
    $this->db->order_by('inventory.product_name', 'asc');
    $this->db->select('inventory.id, inventory.partner_id, inventory.in_stock, inventory.product_name,
    inventory.base_price, inventory.image, inventory.addon_children_ids, inventory.is_hidden, inventory.is_addon,
    inventory.category, inventory.description, inventory.created_at, inventory.updated_at, partners.service_id');
    $this->sessQuery($this->session->id, $this->session->role); # pass array for columns to check like
    $this->squery(['product_name']); # pass array for columns to check like
    $this->db->join('partners', 'partners.id = inventory.partner_id', 'left');
    $res = $this->db->get('inventory')->result();

    if (!$res) {
      return [];
    }

    foreach ($res as $key => &$value) {
      $value = $this->formatRes($value);
    }
    return $res;
  }

  public function getParentProducts()
  {
    $this->sessQuery($this->session->id, $this->session->role); # pass array for columns to check like
    $this->db->order_by('product_name', 'asc');
    // $this->db->where('addon_parent_ids IS NULL');
    $res = $this->db->get('inventory')->result();

    if (!$res) {
      return [];
    }

    foreach ($res as $key => &$value) {
      $value = $this->formatRes($value);
    }
    return $res;
  }

  public function getUniqueCategories()
  {
    $this->db->distinct();
    $this->db->select('category');
    $this->db->order_by('category', 'asc');
    $res = $this->db->get('inventory')->result();
    if (!$res) {
      return [];
    }

    $categories = [];
    foreach ($res as $key => $value) {
      $categories[] = $value->category;
    }
    return $categories;
  }

public function add($data)
  {
    $data['addon_children_ids'] = @$data['addon_children_ids'] ? serialize($data['addon_children_ids']) : null;
    $data['in_stock'] = @$data['in_stock'] ? 1 : 0;
    $this->db->insert($this->table, $data);
    $last_id = $this->db->insert_id();

    return $last_id;
  }

  public function update($id, $data)
  {
    $data['addon_children_ids'] = @$data['addon_children_ids'] ? serialize($data['addon_children_ids']) : null;
    $data['in_stock'] = @$data['in_stock'] ? 1 : 0;

    $this->db->where('id', $id);
    return $this->db->update($this->table, $data);
  }

  public function hide($id)
  {
    $this->db->select('cart_items.id');
    $this->db->where('cart_items.product_id', $id);
    $this->db->where_in('cart.status', ['init', 'cancelled', 'pending']);

    $this->db->join('cart', 'cart.id = cart_items.cart_id', 'left');
    // $this->db->delete('cart_items');
    $to_be_deleted_arr = $this->db->get('cart_items')->result();

    if(count($to_be_deleted_arr)) {
      $del = [];
      foreach ($to_be_deleted_arr as $key => $value) {
        $del[] = $value->id;
      }

      $this->db->where_in('id', $del);
      $this->db->delete('cart_items');
    }

    $data['is_hidden'] = 1;

    $this->db->where('id', $id);
    $res = $this->db->update($this->table, $data);


    return $res;
  }

  public function unhide($id)
  {
    $data['is_hidden'] = 0;

    $this->db->where('id', $id);
    return $this->db->update($this->table, $data);
  }

  public function formatRes($data)
  {
    $service_id = @$data->service_id?: 0;
    switch ($service_id) {
      case 2:
      $data->image_f = (@$data->image) ? $this->full_up_path . $data->image : base_url('public/admin/img/placeholder-grocery.png');
        break;

      case 5:
      $data->image_f = (@$data->image) ? $this->full_up_path . $data->image : base_url('public/admin/img/placeholder-food.png');
        break;

      default:
        $data->image_f = (@$data->image) ? $this->full_up_path . $data->image : $this->temp_image;
        break;
    }
    // $data->image_f = $data->image ? $this->full_up_path . $data->image : $this->temp_image;
    return $data;
  }

  public function sessQuery($id, $role)
  {
    if ($this->session->role == 'partner') {
      $this->db->where('partner_id', $id);
    }
  }

  public function getTotalPages()
  {
    $this->db->where('partner_id', $this->session->id);
    return ceil(count($this->db->get($this->table)->result()) / $this->per_page);
  }

}
