<?php

class Customers_model extends Admin_core_model
{

  function __construct()
  {
    parent::__construct();

    $this->table = 'customers'; # Replace these properties on children
    $this->upload_dir = 'customers'; # uploads/your_dir
    $this->uploads_folder = "efs/uploads/" . $this->upload_dir . "/";
    $this->full_up_path = base_url() . "efs/uploads/" . $this->upload_dir . "/"; # override this block on your child class. just redeclare it
    $this->per_page = 15;
  }

  public function all()
  {
    $this->paginate();
    //$this->db->where('assigned_location',$this->session->userdata('location'));
    $this->db->order_by('id', 'DESC');
    $this->squery(['full_name']); # pass array for columns to check like
    $res = $this->db->get($this->table)->result();

    if (!$res) {
      return [];
    }

    foreach ($res as $key => &$value) {
      $value = $this->formatRes($value);
    }
    return $res;
  }

  public function add($data)
  {
    $this->db->insert($this->table, $data);
    $last_id = $this->db->insert_id();

    return $last_id;
  }

  public function manageBan($timestamp, $table, $id)
  {
    // var_dump($timestamp, $id, $table); die();
    $this->db->where('id', $id);
    return $this->db->update($table, ['banned_at' => $timestamp]);
  }

  public function update($id, $data)
  {
    $this->db->where('id', $id);
    return $this->db->update($this->table, $data);
  }

  public function formatRes($data)
  {
    $data->profile_picture_f = $data->profile_picture ? $this->full_up_path . $data->profile_picture : $this->temp_image;
    return $data;
  }

}
