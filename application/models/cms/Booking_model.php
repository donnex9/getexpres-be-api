<?php

class Booking_model extends Admin_core_model
{

  function __construct()
  {
    parent::__construct();

    $this->table = 'admin'; # Replace these properties on children
    $this->upload_dir = 'admin'; # Replace these properties on children
    $this->per_page = 15;

     $this->load->model('cms/admin_model');
  }

 public function isCartExists($data)
  {
    $this->db->where_in("status",['init','cancelled']);
    $this->db->where('cart_type_id', $data['cart_type_id']);
    $this->db->where('customer_id', $data['customer_id']);
    return $this->db->get('cart')->row();
  }

  public function initializeCart($data,$lastid)
  {
    try {
    
    
      $data['status'] = 'init';
      $this->db->insert('cart', $data);
       
     // $affectedRows = $this->db->affected_rows();
      
      $insert_id = $this->db->insert_id();
      return $insert_id;
     
     
    } catch (Exception $e) {
      return $e;
    }
    
    
  }

  public function getSurgeMultiplier($location)
  {
    $rainy_day_surge_is_active = $this->admin_model->getMeta('rainy_day_surge_is_active',$location);
    if ($rainy_day_surge_is_active) {
      return (double) $this->admin_model->getMeta('rainy_day_surge_rate',$location);
    }

    $scheduled_surge_is_active = $this->admin_model->getMeta('scheduled_surge_is_active',$location);
    if ($scheduled_surge_is_active) {
      $scheduled_surge_days = unserialize($this->admin_model->getMeta('scheduled_surge_days',$location));

      $current_day = date('N');

      if (in_array($current_day, $scheduled_surge_days)) {

        $scheduled_surge_time = unserialize($this->admin_model->getMeta('scheduled_surge_time',$location));
        $now = DateTime::createFromFormat("H:i:s", date("H:i:s"));
        $from_hour = DateTime::createFromFormat("H:i:s", $scheduled_surge_time[0]);
        $to_hour = DateTime::createFromFormat("H:i:s", $scheduled_surge_time[1]);

        if ($now >= $from_hour && $to_hour >= $now) {
          return (double) $this->admin_model->getMeta('scheduled_surge_rate',$location);
        }
      }
    }

    return 1;
  }

  public function getDeliveryPrice($cart_id)
  {
    $this->db->where('id', $cart_id);
    $vehicle_id = $this->db->get('cart')->row()->vehicle_id;

    $this->db->where('id', $vehicle_id);
    return $this->db->get('vehicles')->row()->delivery_price;
  }

  public function getWeightCapacity($cart_id)
  {
    $this->db->where('id', $cart_id);
    $vehicle_id = $this->db->get('cart')->row()->vehicle_id;

    $this->db->where('id', $vehicle_id);
    return $this->db->get('vehicles')->row()->weight_capacity;
  }

  public function gettip($cart_id){
    $this->db->select('tip');
    $this->db->where('id', $cart_id);
    $tippings = $this->db->get('cart')->row();

    return $tippings;
  }

  public function getCart($cart_id){
    
    $this->db->where('id', $cart_id);
   
    return $this->db->get('cart')->row();
  }

  public function webtransactDelivery($data,$cart_id,$location)
  {
    $cart_obj = $this->getCart($cart_id);
    $serpayload = unserialize($cart_obj->payload);
    $payload = [];
    $multiplier = $this->getSurgeMultiplier($location);
    $payload['multiplier'] = $multiplier;
    $tippings = $this->gettip($cart_id);
    $payload['tip'] = $tippings->tip;

    $payload['category'] = $data['category'];
    $payload['notes'] = $data['notes'];
    $payload['distance'] = (double) $serpayload['distance'];

    $payload['distance_in_m'] = $payload['distance'];
    $payload['distance_in_km'] = $payload['distance_in_m'] / 1000;
    $adminFee = $this->admin_model->getMeta('assisted_booking_fee',$location);
    $payload['admin_fee'] = $adminFee;

    $payload['distance_in_km_rounded_down'] = ($payload['distance_in_km'] >= 1) ? floor($payload['distance_in_km']) : 0;

    $rates = $this->admin_model->getRates($cart_obj->cart_type_id, $cart_obj->vehicle_id,$location);
    $payload['price_per_km'] = (double) $rates->per_km_fare;
    $payload['basefare_fee'] = (double) $rates->base_fare;
    // $payload['price_per_km'] = (double) $this->admin_model->getMeta('delivery_per_km');
    $computed_distance_fee = $payload['distance_in_km_rounded_down'] * $payload['price_per_km'];
    // $computed_distance_fee = $basket->distance_in_km_rounded_down * $basket->price_per_km;
    $payload['delivery_fee'] = (double) $rates->base_fare + $computed_distance_fee;
    // $payload['delivery_fee'] = (double) $this->admin_model->getMeta('delivery_fare') + $computed_distance_fee;
    $payload['getapp_commission_percentage'] = (double) $rates->getapp_commission_percentage;
    $payload['getapp_commission'] =  round(($payload['delivery_fee'] * $rates->getapp_commission_percentage) + $adminFee, 2);
   

    $payload['delivery_price'] = (double)$this->getDeliveryPrice($cart_id); # price depende sa vehicle type # deprecated

    $payload['weight_capacity'] = $this->getWeightCapacity($cart_id);
    $payload['price'] = ($payload['delivery_fee'] + $payload['delivery_price']) * $multiplier  + @$payload['tip'];
    $payload['grand_total'] = $payload['price'];

    $this->db->where('id', $cart_id);
    $this->db->update('cart', ['payload' => serialize($payload)]);
    
  }


  public function webtransactPabili($data, $cart_id,$location)
  {
    $payload = (object)[];
    $payload->items = [];
    for ($i=0; $i < count($data['items_name']) ; $i++) {
      $payload->items[] = (object)['item_name' => $data['items_name'][$i], 'quantity' => "1"];
    }

    $multiplier = $this->getSurgeMultiplier($location);
    $payload->multiplier = $multiplier;
    $tippings = $this->gettip($cart_id);
    $payload->tip = $tippings->tip;
    ///if ($this->input->get('pabili_fee_additional')) {
    $payload->pabili_fee_additional = $this->admin_model->getMeta('pabili_fee_additional',$location);
    $adminFee = $this->admin_model->getMeta('assisted_booking_fee',$location);
    $payload->admin_fee = $adminFee;
   // } else {
      //$payload->pabili_fee_additional = 0;
   // }
    $payload->estimate_total_amount = @$data['estimate_total_amount'];
    $payload->estimate_total_amount_with_tip = @$payload->estimate_total_amount + @$payload->tip;

    $this->db->where('id', $cart_id);
    $res = $this->db->update('cart', ['payload' => serialize($payload)]);

    return $res;
   // var_dump($res); die();
  }

  public function webreInitPabili($cart_id,$location)
  {
    $cart_obj = $this->getCart($cart_id);
    $data = unserialize($cart_obj->payload);
    $multiplier = $this->getSurgeMultiplier($location);
    $data->multiplier = $multiplier;
    // var_dump($data); die();
    $data->distance = (double) $data->distance;

    $data->distance_in_m = $data->distance;
    $data->distance_in_km = $data->distance_in_m / 1000;
    $data->distance_in_km_rounded_down = ($data->distance_in_km >= 1) ? floor($data->distance_in_km) : 0;

    $adminFee = $data->admin_fee;
// var_dump($cart_obj); die();
    //$location = $this->setBaseFare($cart_obj->cart_type_id, $cart_obj->vehicle_id);
    //$this->setBaseFare($cart_obj->cart_type_id, $cart_obj->vehicle_id,$cart_id);
    $rates = $this->admin_model->getRates($cart_obj->cart_type_id, $cart_obj->vehicle_id,$location);
    $data->price_per_km = (double) $rates->per_km_fare;
    // $payload['price_per_km'] = (double) $this->admin_model->getMeta('delivery_per_km');
    $computed_distance_fee = $data->distance_in_km_rounded_down * $data->price_per_km;
    // $computed_distance_fee = $basket->distance_in_km_rounded_down * $basket->price_per_km;
    $data->delivery_fee = (double) $rates->base_fare + $computed_distance_fee;
    $data->basefare_fee = (double) $rates->base_fare;
    $data->getapp_commission_percentage = (double)$rates->getapp_commission_percentage;
    $data->getapp_commission =  round(($data->delivery_fee * $rates->getapp_commission_percentage) + $adminFee, 2);
    

    $data->delivery_price = (double)$this->getDeliveryPrice($cart_id); # price depende sa vehicle type # deprecated

    // $data->weight_capacity = $this->getWeightCapacity($cart_id);
    $data->price = ($data->delivery_fee + $data->delivery_price) * $multiplier;
    $data->grand_total = $data->price;
    $tippings = $this->gettip($cart_id);
    $data->tip = $tippings->tip;

    $this->db->where('id', $cart_id);
    $res =$this->db->update('cart', ['payload' => serialize($data)]);

    return $res;
  }

public function tomtomsetDistanceByMatrix($cart_id)
  {

    $cart = $this->cart_model->get($cart_id);

    $from = $cart->pickup_location;
    $to   = $cart->delivery_location;
 
    ##################################
    ##################################
    ##################################
    
    $fromLat  = $from->latitude;
    $fromLong = $from->longitude;

    $toLat    = $to->latitude;
    $tolong   = $to->longitude;


    // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
    $orig = "$fromLat,$fromLong";
    $dest = "$toLat,$tolong";
     
  $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, "https://api.tomtom.com/routing/1/calculateRoute/$orig:$dest/json?key=HwnjopRuAjAGH28B6VAQB5dRMfMvoKLv");

    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    # Return response instead of printing.
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);

    $res = json_decode($result);
    $totalDistance = 0;
    //print_r($res->routes[0]->summary->lengthInMeters);
    ##################################
    ##################################
    ##################################

    $payload = unserialize($cart->payload);
    if (in_array($cart->cart_type_id, [3])) {
      if (isset($res->routes[0])) {
        $totalDistance = (INT) $res->routes[0]->summary->lengthInMeters + (INT) $res->routes[0]->summary->trafficLengthInMeters;
        $payload->distance = $totalDistance;
        $payload->eta_text = (string)$res->routes[0]->summary->travelTimeInSeconds;
        $payload->eta_value_in_seconds = (string)$res->routes[0]->summary->travelTimeInSeconds;
      } else {
        $payload->distance = 0;
        $payload->eta_text = "TBD";
        $payload->eta_value_in_seconds = 0;
      }
    } else {
      if (isset($res->routes[0])) {
       $totalDistance = (INT) $res->routes[0]->summary->lengthInMeters + (INT) $res->routes[0]->summary->trafficLengthInMeters;
      $payload['distance'] = $totalDistance;
        // var_dump($payload['distance']); die();
      $payload['eta_text'] = (string)$res->routes[0]->summary->travelTimeInSeconds;
      $payload['eta_value_in_seconds'] = (string) $res->routes[0]->summary->travelTimeInSeconds;
      } else {
        $payload['distance'] = 0;
        $payload['eta_text'] = "TBD";
        $payload['eta_value_in_seconds'] = 0;
      }
    }

 //return $payload;
  // die();
    $this->db->where('id', $cart_id);
    return $this->db->update('cart', ['payload' => serialize($payload)]);
  }

public function voucherCodeCMS($voucherCode,$assignedLocation)
  {
    $this->db->where('voucher_code = BINARY',$voucherCode);
    $this->db->where('assigned_location',$assignedLocation);
    return $this->db->get('voucher_code')->result();
  }

  public function userAvailVoucherCMS($cId,$al,$vc,$dT)
  {
    $this->db->where('voucher_code = BINARY',$vc);
    $this->db->where('customer_id',$cId);
    $this->db->where('DATE(booking_date)',$dT);
    $this->db->where('assigned_location',$al);
    //return $$this->db;
    return $this->db->get('voucher_summary')->num_rows();
   // return $this->db->get('voucher_summary')->result();
  } 


}


