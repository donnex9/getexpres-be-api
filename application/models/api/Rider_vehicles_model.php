<?php

class Rider_vehicles_model extends Crud_model
{
  /**
  * Your table name
  * @var string
  */
  protected $table;

  /**
  * The directory you want to upload to whose parent dir is `uploads` folder
  * @var [type]
  */
  protected $upload_dir;

  public function __construct()
  {
    parent::__construct();
    $this->upload_dir = 'rider_vehicles'; # uploads/your_dir
    $this->uploads_folder = "efs/uploads/" . $this->upload_dir . "/";
    $this->full_up_path = base_url() . "efs/uploads/" . $this->upload_dir . "/";

  }

  public function addVehicle($data, $rider_id)
  {
    $data['rider_id'] = $rider_id;
    $this->db->insert('rider_vehicles', $data);
    return $this->db->insert_id();
  }

  public function makeVehicleActive($rider_id,  $vehicle_pk)
  {
    $this->db->where('rider_id', $rider_id);
    $this->db->update('rider_vehicles', ['is_active' => 0]);

    $this->db->where('id', $vehicle_pk);
    return $this->db->update('rider_vehicles', ['is_active' => 1]);
  }

  public function getRiderVehicles($rider_id)
  {
    $this->db->select('rider_vehicles.id, vehicles.name as vehicle_type, rider_vehicles.vehicle_id, rider_vehicles.vehicle_model, rider_vehicles.plate_number, rider_vehicles.is_active');
    $this->db->where('rider_vehicles.rider_id', $rider_id);
    $this->db->where('rider_vehicles.is_verified', 1);
    $this->db->join('vehicles', 'vehicles.id = rider_vehicles.vehicle_id', 'left');
    return $this->db->get('rider_vehicles')->result();
  }

  public function getRiderVehiclesUnverified($rider_id)
  {
    $this->db->select('rider_vehicles.id, vehicles.name as vehicle_type, rider_vehicles.vehicle_id, rider_vehicles.vehicle_model, rider_vehicles.plate_number, rider_vehicles.is_active');
    $this->db->where('rider_vehicles.rider_id', $rider_id);
    $this->db->where('rider_vehicles.is_verified', 0);
    $this->db->join('vehicles', 'vehicles.id = rider_vehicles.vehicle_id', 'left');
    return $this->db->get('rider_vehicles')->result();
  }

  public function getActiveVehicle($rider_id)
  {
    $this->db->select('rider_vehicles.id, vehicles.name as vehicle_type, rider_vehicles.vehicle_id, rider_vehicles.vehicle_model, rider_vehicles.plate_number, rider_vehicles.is_active');
    $this->db->where('rider_vehicles.rider_id', $rider_id);
    $this->db->where('rider_vehicles.is_active', true);
    $this->db->join('vehicles', 'vehicles.id = rider_vehicles.vehicle_id', 'left');
    return $this->db->get('rider_vehicles')->row();
  }

public function verifyRidersVehicle($id,$data)
  {
    
    $this->db->where('id', $id);
    return $this->db->update('rider_vehicles', $data);
  }

}
