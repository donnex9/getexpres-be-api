<?php

class News_model extends Crud_model
{
  /**
  * Your table name
  * @var string
  */
  protected $table;

  /**
  * The directory you want to upload to whose parent dir is `uploads` folder
  * @var [type]
  */
  protected $upload_dir;

  public function __construct()
  {
    parent::__construct();
    $this->upload_dir = 'news'; # uploads/your_dir
    $this->uploads_folder = "efs/uploads/" . $this->upload_dir . "/";
    $this->full_up_path = base_url() . "efs/uploads/" . $this->upload_dir . "/";

    $this->load->model('api/partners_model');
  }

  public function all()
  {
    
    $res = $this->db->get('news')->result();
    foreach ($res as &$value) {
      $value = $this->formatRes($value);
    }
    return $res;
  }


  public function whatsnewsPerLocation($location)
  {
    $this->db->where('assigned_location',$location);
    $this->db->where('ispriority',1);
//    $this->db->order_by('rand()');
    $this->db->limit('1');
    $this->db->select('*');
    $resprio = $this->db->get('news')->row();

    if($resprio)
    {
      return $this->formatRes($resprio);
    }else{
      return  [];
    }
   /* $this->db->where('assigned_location',$location);
    $res = $this->db->get('news')->result();
    foreach ($res as &$value) {
      $value = $this->formatRes($value);
    }
    return $res;*/
  }

  public function get($id)
  {
    $this->db->where('id', $id);
    $res = $this->db->get('news')->row();
    return $this->formatRes($res);
  }


  function formatRes($res)
  {
      if (@!$res) {
        return false;
      }
      $res->partner_name = "";
      $res->service_id = "";
      if ((int)$res->linked_partner_id) {
        $partner = $this->partners_model->get($res->linked_partner_id);
        $res->partner_name = $partner->name;
        $res->service_id = $partner->service_id;
      }
      $res->banner_image = ($res->banner_image) ? $this->full_up_path . $res->banner_image : base_url('public/admin/img/placeholder-whatsnew-cover.png');
      return $res;
  }
}
