<?php

class Firebase_model extends Crud_model
{
  public function __construct()
  {
    parent::__construct();
    define('FIREBASE_API_KEY', 'AAAA9r2bUZ4:APA91bGW_o3ai9vegzMfiRLtOP-JgamxReJdE_fCl06FHprfOMWUTXia5rHI4je9ADSOZFUzoBVptirrPivNAQgCxxXlsjvcEw6cvIGOspInDjFZueXydpYsqx1tswpIy5qLxF75JStT');

    define('MFIREBASEAPIKEY','AAAA02D1_74:APA91bFvJg4T7PVYBfXv5QvGbd7fQ6zk61Og6VepUF6E6KrKty0LvKjvjeScyOpzjwvKvUGfjowZcuU8esNW_TQGz7wtJn4ZsEAJFNUlCCRZkMhIrFIlyTmMxmBHMjZhSxj-g7xC1Wqr');

  }

    // Sending message to a topic by topic name
  public function sendToTopic($to, $message, $notification) {
      $sound = 'alarma.mp3'; //alarma_142.mp3
      $fields = array(
          'to' => '/topics/' . $to,
          'notification' => array_merge($notification, ['sound' => $sound, 'android_channel_id' => "cnid"]),
          'data' => $message,
      );
      return $this->sendPushNotification($fields);
  }

  // sending push message to multiple users by firebase registration ids
  public function sendMultiple($registration_id, $notification, $silent = false, $annoying_sound = false) {
    $sound = 'default';
    if ($annoying_sound) {
      $sound = 'alarma.mp3';
    }

    $fields = array(
      
      'registration_ids' => $registration_id,
      'data' => $notification,
      'content_available' => true,
      'priority' => 'high'
    );

    
    return $this->sendPushNotification($fields);
  }

   public function sendMultipleMerchant($registration_id, $notification, $silent = false, $annoying_sound = false) {
    $sound = 'default';
    if ($annoying_sound) {
      $sound = 'alarma.mp3';
    }

    $fields = array(
      
      'registration_ids' => $registration_id,
      'data' => $notification,
      'content_available' => true,
      'priority' => 'high'
    );

    
    return $this->sendPushNotificationMerchant($fields);
  }

  

  // function makes curl request to firebase servers
  private function sendPushNotification($fields) {
    // Firebase API Key
    // Set POST variables
    $url = 'https://fcm.googleapis.com/fcm/send';
    $headers = array(
      'Authorization: key=' . FIREBASE_API_KEY,
      'Content-Type: application/json'
    );
    // Open connection
    $ch = curl_init();
    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    // Execute post
    $result = curl_exec($ch);
    if ($result === FALSE) {
      die('Curl failed: ' . curl_error($ch));
    }
    // Close connection
    curl_close($ch);
    return $result;
  }

  private function sendPushNotificationMerchant($fields){
    
     $url = 'https://fcm.googleapis.com/fcm/send';
    $headers = array(
      'Authorization: key=' . MFIREBASEAPIKEY,
      'Content-Type: application/json'
    );
    // Open connection
    $ch = curl_init();
    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    // Execute post
    $result = curl_exec($ch);
    if ($result === FALSE) {
      die('Curl failed: ' . curl_error($ch));
    }
    // Close connection
    curl_close($ch);
    return $result;
  }

  public function getRegistrationIds($user_id, $type = 'customers')
  {

  	$this->db->where('type', $type);
  	$this->db->where('user_id', $user_id);
  	$this->db->where('is_active', 1);

  	$res = $this->db->get('user_device_ids')->result();
    return $res;
  }
}
