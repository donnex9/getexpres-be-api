<?php

class Vehicles_model extends Crud_model
{
  /**
  * Your table name
  * @var string
  */
  protected $table;

  /**
  * The directory you want to upload to whose parent dir is `uploads` folder
  * @var [type]
  */
  protected $upload_dir;

  public function __construct()
  {
    parent::__construct();
    $this->table = 'vehicles'; # uploads/your_dir
    $this->upload_dir = 'vehicles'; # uploads/your_dir
    $this->uploads_folder = "efs/uploads/" . $this->upload_dir . "/";
    $this->full_up_path = base_url() . "efs/uploads/" . $this->upload_dir . "/";
  }

  public function all()
  {
    // $this->db->where_in('id', [3]); #motor only
    $res = $this->db->get('vehicles')->result();
    foreach ($res as &$value) {
      $value = $this->formatRes($value);
    }
    return $res;
  }

  public function makeActive($rider_id, $rider_vehicles_pk)
  {
    $this->db->where('rider_id', $rider_id);
    $this->db->update('rider_vehicles', ['is_active' => 0]);

    $this->db->where('id', $rider_vehicles_pk);
    $this->db->where('rider_id', $rider_id);
    return $this->db->update('rider_vehicles', ['is_active' => 1]);
  }

  function formatRes($res)
  {
      $res->image = ($res->image) ? $this->full_up_path . $res->image : $this->temp_image;
      return $res;
  }

}
