<?php

class User_device_ids_model extends Crud_model
{
  public function __construct()
  {
    parent::__construct();
    $this->table = "user_device_ids";
    $this->table_pk = "udi_id";
  }

  public function loginDevice($user_id, $device_id, $firebase_id, $table = 'customers')
  {
    
    $this->logoutDevice($device_id, $table); # let all devices not receive notifs

    $udi_id = $this->checkExists($device_id, $firebase_id, $user_id, $table); # Check the existing PK if there is one

    if(!$udi_id){ # if there is no record in the database
      $last_id = $this->addDevice($user_id, $device_id, $firebase_id, $table);
      $res = $this->activateDevice($last_id, $firebase_id);
    } else {
      $res = $this->activateDevice($udi_id,$firebase_id);
    }
    return $res;
  } 

  public function addDevice($user_id, $device_id, $firebase_id, $table = 'customers')
  {
    $this->db->insert($this->table, ['user_id' => $user_id,
    'device_id' => $device_id,
    'firebase_id' => $firebase_id, 'type' => $table]);

    return $this->db->insert_id();
  }

  /**
   * [activateDevice description]
   * @param  [type] $udi_id PK of user device ID
   * @return [type]         [description]
   */
  public function activateDevice($udi_id,$firebaseId)
  {
    $this->db->where('udi_id', $udi_id);
    return $this->db->update($this->table, ['is_active' => 1,'firebase_id' => $firebaseId]);
  }

  


  public function logoutDevice($device_id, $table)
  {
    $this->db->where('device_id', $device_id);
    $this->db->where('type', $table);
    return $this->db->update($this->table, ['is_active' => 0]);
  }

  public function checkExists($device_id, $firebase_id, $user_id, $table = 'customers')
  {
    return @$this->db->get_where($this->table, ['device_id' => $device_id, 'user_id' => $user_id, 'type' => $table])->row()->udi_id;
  }


}
