<?php

class Cart_model extends Crud_model
{

  public function __construct()
  {
    parent::__construct();
    $this->table = "cart";
    $this->upload_dir = 'inventory'; # uploads/your_dir
    $this->uploads_folder = "efs/uploads/" . $this->upload_dir . "/";
    $this->full_up_path = base_url() . "efs/uploads/" . $this->upload_dir . "/";

    $this->load->model('api/inventory_model');
    $this->load->model('api/partners_model');
    $this->load->model('api/services_model');
    $this->load->model('api/customers_model');
    $this->load->model('api/payment_model');
    $this->load->model('cms/admin_model');

  }

  public function getPartnerByCartId($cart_id)
  {
    $this->db->select('partners.id as partner_id');
    $this->db->where('cart_id', $cart_id);
    $this->db->join('inventory', 'inventory.id = cart_items.product_id');
    $this->db->join('partners', 'inventory.partner_id = partners.id');
    $partner_id = @$this->db->get('cart_items')->row()->partner_id;

    if (!$partner_id) {
      return (object)[];
    }

    return $this->partners_model->get($partner_id);
  }

  public function initializeCart($data)
  {
    $data['status'] = 'init';
    $this->db->insert('cart', $data);
    $last_id = $this->db->insert_id();

    return $last_id;
  }

  public function onlinePayment($data)
  {
    $this->db->insert('online_payment_transaction',$data);
    $last_id = $this->db->insert_id();
    $res = $this->payment_model->createResourceGcashforOrder($data['amount'],$data['customer_id'],$last_id,$data['type']);
  

    $this->db->where('id', $last_id);
    $this->db->update('online_payment_transaction', [
      'transaction_id' => $res->id,
      'checkout_url' => $res->attributes->redirect->checkout_url,
      'status' => $res->attributes->status
    ]);

    return $this->db->get_where('online_payment_transaction', ['id' => $last_id])->row();
  }

 public function AddOrUpdateItem($data, $cart_id,$location)
  {
   
    $data['cart_id'] = $cart_id;
    $res = false;
    if(@$data['cart_item_id']) { # pag update

      if (!@$data['quantity'] && @$data['cart_item_id']) {
        $this->db->where('id', @$data['cart_item_id']); # delete
        $res = $this->db->delete('cart_items');
      } else { # update

        $product = $this->inventory_model->get($data['product_id']);
        $data['price_when_added'] = $product->base_price;

        $this->db->where('product_id', $data['product_id']);
        $this->db->where('cart_id', $cart_id);
        $this->db->update('cart_items', ['price_when_added' => $product->base_price]);

        $temp_addons = [];

        if (@$data['addon_ids']) {
          foreach ($data['addon_ids'] as $key => $value) {
            $addon = $this->inventory_model->get($value);
            $temp_addons[] = $addon;
          }

          $data['addon_ids'] = serialize($temp_addons);
        } else {
          $data['addon_ids'] = serialize([]);
        }

        $this->db->where('id', $data['cart_item_id']);
        unset($data['cart_item_id']);
        $res = $this->db->update('cart_items', $data);

      }

    } else {

      if ($data['quantity']) {

        $product = $this->inventory_model->get($data['product_id']);
        $data['price_when_added'] = $product->base_price;

        $temp_addons = [];
        if (@$data['addon_ids']) {
          foreach ($data['addon_ids'] as $key => $value) {
            $addon = $this->inventory_model->get($value);
            $temp_addons[] = $addon;
          }
        }

        $data['addon_ids'] = serialize($temp_addons);
        $res = $this->db->insert('cart_items', $data);

      }
    }
    return $res;
  }


  public function checkBookingAvailableUntil($rider_id)
  {
    $this->db->where('id', $rider_id);
    $rider = $this->db->get('riders')->row();

    return $rider->booking_available_until;
  }

   public function getNewOrder($partnerId)
  {

    $this->db->where_in('status', [
      'accepted'
    ]);

    $this->db->where('partner_id',$partnerId);


    $res = $this->db->get('cart')->result();

    if (!$res) {
      return (object)[];
    }

    $new_resformat =  [];

    foreach ($res as $key => &$value) {
      //$partnerId =  $this->getPartnerByCartIdForNewOrder($value->id,$partnerId);
       //if($partnerId)
       //{
         $value = $this->formatRes($value);
      // }
    }

    $new_resformat = $res;
    return $new_resformat;
  }

public function getAcceptedOrder($partnerId)
  {

    $this->db->where_in('status', ['got_items',
      'preparing','otw','arrived_at_destination'
    ]);


     $res = $this->db->get('cart')->result();

    if (!$res) {
      return (object)[];
    }

    $new_resformat =  [];

    foreach ($res as $key => &$value) {
      $partnerId =  $this->getPartnerByCartIdForNewOrder($value->id,$partnerId);
       if($partnerId)
       {
         $value = $this->formatRes($value);
       }
    }

    $new_resformat = $res;
    return $new_resformat;
  }

 public function getPartnerByCartIdForNewOrder($cart_id,$partnerId)
  {
    $this->db->select('partners.id as partner_id');
    $this->db->where('cart_id', $cart_id);
    $this->db->where('partners.id','$partnerId');
    $this->db->join('inventory', 'inventory.id = cart_items.product_id');
    $this->db->join('partners', 'inventory.partner_id = partners.id');
    $partner_id = @$this->db->get('cart_items')->row()->partner_id;

    if (!$partner_id) {
      return (object)[];
    }

    return $this->partners_model->get($partner_id);
  }

  public function setTotalPrice($cart_id)
  {
    $cart = $this->get($cart_id);
    $total_price = 0;
    switch($cart->service_type) {
      case 'pabili':
      $total_price = $cart->basket->estimate_total_amount;
      break;

      case 'car':
      $total_price = $cart->basket['price'];
      break;
      case 'delivery':
      $total_price = $cart->basket['grand_total'];
      break;
      
      case 'laundry':
      $total_price = $cart->basket['grand_total'];
      break;

      case 'grocery':
      case 'food':
      default:
      $total_price = $cart->basket->grand_total;
      break;
    }

    return $this->update($cart_id, ['total_price' => $total_price]);
  }

  public function update($id, $data)
  {
    $this->db->where('id', $id);
    return $this->db->update('cart', $data);
  }

  public function updateData($id, $data,$table)
  {
    $this->db->where('id', $id);
    return $this->db->update($table, $data);
  }

  public function getActiveBooking($rider_id)
  {
    $this->db->where_in('status', ['accepted','preparing','ready_for_pickup' ,'otw', 'got_items', 'arrived_at_store', 'placing_order', 'arriving_at_destination', 'arrived_at_destination']);
    $this->db->where('rider_id', $rider_id);

    $res = $this->db->get('cart')->row();

    if (!$res) {
      return (object)[];
    }

    return $this->formatRes($res);
  }

  public function getActiveBookingCustomer($customer_id)
  {

    $this->db->where_in('status', [
      'accepted',
      'waiting',
      'pending',
      'preparing',
      'ready_for_pickup',
      'otw',
      'got_items',
      'arrived_at_store',
      'placing_order',
      'arriving_at_destination',
      'arrived_at_destination'
    ]);

    $this->db->where('customer_id', $customer_id);
    $this->db->order_by('id','DESC');
    $res = $this->db->get('cart')->result();

    if (!$res) {
      return (object)[];
    }

    $new_resformat =  [];

    foreach ($res as $key => &$value) {
      $value = $this->formatRes($value);
    }
    $new_resformat = $res;

    return $new_resformat;
  }

public function getActiveVouchers($customer_id,$location)
  {


    $this->db->where('id', $customer_id);

    $res = $this->db->get('customers')->row();

  
    if (!$res) {
      return (object)[];
    }

  // $location = $res->assigned_location;
   $isFirstTimeUser = $res->is_ftu;


   if(trim($isFirstTimeUser) == 'N')
   {
     $this->db->where('for_fisttime_user', 0);
   }

   $this->db->where('status', 1);
   $this->db->where('assigned_location', $location);
   $this->db->where("date(expired_at) >= NOW()");
    $res2 = $this->db->get('voucher_code')->result();

    $new_resformat =  [];

    foreach ($res2 as $key => &$value) {
      $value = $this->formatVoucher($value);
    }
    $new_resformat = $res2;

    return $new_resformat;//$this->formatRes($res);

  }


  public function getInActiveVouchers($customer_id,$location)
  {

    $this->db->where('id', $customer_id);

    $res = $this->db->get('customers')->row();
  
    if (!$res) {
      return (object)[];
    }

  // $location = $res->assigned_location;
   $isFirstTimeUser = $res->is_ftu;

   $this->db->where('status', 0);
   $this->db->where("date(expired_at) < NOW()");
   $this->db->where('assigned_location', $location);

    $res2 = $this->db->get('voucher_code')->result();

    $new_resformat =  [];

    foreach ($res2 as $key => &$value) {
      $value = $this->formatVoucher($value);
    }
    $new_resformat = $res2;

    return $new_resformat;//$this->formatRes($res);

  }

  function formatVoucher($res){

    $date = date_create($res->expired_at);
    @$res->expired_at = date_format($date," F j, Y");

    return $res;
  }

 public function getActiveCartCustomer($customer_id)
  {
    
    $cartId = $this->checkActiveCartItems($customer_id);
    $this->db->select('id,cart_type_id,customer_id,status');
    $this->db->where('id',$cartId);
    $res = $this->db->get('cart')->row();

    if (!$res) {
      return (object)[];
    }

    return $this->formatResCart($res);

  }

  private function checkActiveCartItems($customer_id)
  {
     $this->db->where('status in ("init","cancelled")');
     $this->db->where('customer_id',$customer_id);
     $this->db->join('cart_items', 'cart.id = cart_items.cart_id');
     $this->db->select('MAX(cart.id) as id');
     return $this->db->get('cart')->row()->id;
  }

  public function formatResCart($res)
  {
    
    $res->service_type = $this->services_model->get($res->cart_type_id)->type;
    $res->service_label = $this->services_model->get($res->cart_type_id)->label;
    $res->status_text   = $this->getStatusText($res->status);

   // $res->pickup_location = $res->pickup_location ? json_decode($res->pickup_location) : (object)[];
   // $res->delivery_location = $res->delivery_location ? json_decode($res->delivery_location) : (object)[];
  
    $res->partner_id = "";
    if(in_array($res->cart_type_id, [2,5])) 
    {
      $partner = $this->getPartnerByCartId($res->id);
      $res->partner_id = @$partner->id;
      $res->partner_name = @$partner->name;
      $res->partner_address=@$partner->location_text;
      $res->partner_contact=@$partner->contact_numbers;
      $res->partner_coordinates = @$partner->coordinates;
      $res->store_availability_meta = @$partner->store_availability_meta;
      $res->store_schedule_meta = @$partner->store_schedule_meta;
    }

   // $res->order_id = $this->formatOrderId($res, $rider);
    $res->cart_type_id = $res->cart_type_id;
    return $res;
  }

   public function voucherCode($voucherCode,$assignedLocation)
  {
    $this->db->where('voucher_code = BINARY',$voucherCode);
    $this->db->where('assigned_location',$assignedLocation);
    return $this->db->get('voucher_code')->result();
  }

  public function countVoucher($voucherCode,$assignedLocation)
  {
    $this->db->select('COUNT(*) as a');
    $this->db->where('voucher_code= BINARY',$voucherCode);
    $this->db->where('assigned_location',$assignedLocation);
    return $this->db->get('voucher_summary')->row()->a?: 0;
  }

  public function updatevoucherCode($voucherCode,$assignedLocation,$used)
  {
    $this->db->where('voucher_code',$voucherCode);
    $this->db->where('assigned_location',$assignedLocation);
    return  $this->db->update('voucher_code', ['used' => $used]);
  }
  public function userAvailVoucher($custmers_Id,$assignedlocation,$vouchercode,$dateToday)
  {
    $this->db->where('voucher_code = BINARY',$vouchercode);
    $this->db->where('customer_id',$custmers_Id);
    $this->db->where('DATE(booking_date)',$dateToday);
    $this->db->where('assigned_location',$assignedlocation);
    $numRows = $this->db->get('voucher_summary')->num_rows();
    return $numRows;
  }

 public function checkifVoucherIsFtu($assignedlocation,$vouchercode)
  {
    $this->db->where('voucher_code = BINARY',$vouchercode);
    $this->db->where('assigned_location',$assignedlocation);
    $isFtu = $this->db->get('voucher_code')->row()->for_fisttime_user;

    if ($isFtu == 1) {
      return true;
    }else{
      return false;
    }
  }

  public function checkifCustomerIsFtu($customer_Id)
  {
   
    $this->db->where('id',$customer_Id);
    $isFtu = $this->db->get('customers')->row()->is_ftu;

    if ($isFtu == 'Y') {
      return true;
    }else{
      return false;
    }
  }

 public function completedBookingwithVoucher($data)
  {
    $this->db->insert('voucher_summary', $data);
    $last_id = $this->db->insert_id();
    return $last_id;
  }


  public function updateVehicle($data, $cart_id)
  {
      $this->db->where('id', $cart_id);
      // $this->db->where('cart_type_id', $data['cart_type_id']);
      // $this->db->where('customer_id', $data['customer_id']);
      $this->db->update('cart', ['vehicle_id' => $data['vehicle_id']]);
      return $this->db->affected_rows();
  }

  public function changeStatus($data, $cart_id)
  {

      $this->db->where('id', $cart_id);
      if ($data['status'] == 'pending') {
        $this->db->update('cart', ['status' => $data['status'], 'rider_id' => 0]);
      } else {
        $this->db->update('cart', [
          'status' => $data['status'],
        ]);
      }
      return $this->db->affected_rows();
  }


  public function isOrderCancelled($cart_id)
  {
      $isCancelled = false;
      $this->db->where('id', $cart_id);
      $this->db->where('status', 'cancelled');
      $resRow =  $this->db->get('cart')->num_rows();
     
     if ($resRow > 0) {
        $isCancelled = true;
     }

      return $isCancelled;
  }

  public function checkCartExists($data)
  {
    $this->db->where('status in ("init","cancelled")');
    $this->db->where('cart_type_id', $data['cart_type_id']);
    $this->db->where('customer_id', $data['customer_id']);
    return $this->db->get('cart')->row();
  }

  public function get($id)
  {
    $this->db->where('id', $id);
    $res = $this->db->get('cart')->row();
    if (!$res) {
      return (object)[];
    }

   
   /* if (in_array($res->status, ['arrived_at_destination', 'completed'])) {
      $res = $this->changeETA($res);
    }*/

   /* if (in_array($res->status, ['accepted','otw'])) {
       //$res = $this->recalculateETA($res);
       $this->riderDistanceByMatrix($id);
    }*/

    //$this->db->where('id', $id);
    //$res = $this->db->get('cart')->row();

    return $this->formatRes($res);
  }

  public function changeETA($res, $new_eta = "-")
  {
    if (!unserialize($res->payload)) {
      return $res;
    }

    if (in_array($res->cart_type_id, [3])) {
      $payload = unserialize($res->payload);
      $payload->eta_text = $new_eta;
    } else {
      $payload = unserialize($res->payload);
      $payload['eta_text'] = $new_eta;
    }

    $res->payload = serialize($payload);
    return $res;
  }


  public function recalculateETA($res)
  {
    $this->cart_model->riderDistanceByMatrix($id); 

    if (!unserialize($res->payload)) {
      return $res;
    }

    if (in_array($res->cart_type_id, [3])) {
      $payload = unserialize($res->payload);
      $eta_value_in_seconds = @$payload->eta_value_in_seconds?: 0;
    } else {
      $payload = unserialize($res->payload);
      $eta_value_in_seconds = @$payload['eta_value_in_seconds'] ?: 0;
    }

    $time = strtotime($res->updated_at);
    $time_now = time();
    $time_diff = $time_now - $time;
    $time_remain = $eta_value_in_seconds - $time_diff;

    // var_dump($eta_value_in_seconds, $time, $time_diff, $time_remain, $res->updated_at);
    if ($time_remain > 300) {
      $min_remain = gmdate("i", $time_remain) * 1 . " mins";
    } else {
      $min_remain = "5 mins";
    }

    if (in_array($res->cart_type_id, [3])) {
      $payload->eta_text = $min_remain;
    } else {
      $payload['eta_text'] = $min_remain;
    }

    $res->payload = serialize($payload);

    return $res;
  }



  public function setBaseFare($cart_id,$frJsonDecode,$toJsonDecode){

    $cart        = $this->get($cart_id);
    $fromAddress =   $frJsonDecode->address_text;
    $toAddress   =   $toJsonDecode->address_text;

    if($frJsonDecode == ""){
       $fromAddress =   $cart->pickup_location->address_text;
    }

    if($toJsonDecode == "")
    {
      $toAddress   =   $cart->delivery_location->address_text;
    }
   

    $fromSplit = array_reverse(explode(',',$fromAddress));
    $toSplit   = array_reverse(explode(',',$toAddress));

    $dynamicVariables  = $this->db->get('dynamic_radius_places')->result_array();

    $location1 = 0;
    $location2 = 0;

    $sizeFrom = count($fromSplit);
    $sizeTo   = count($toSplit);
    //var_dump($dynamicVariables);
    
    foreach($dynamicVariables as $key => $row)
    {
    
     if($sizeFrom == 2){

        if(strpos($fromSplit[1], $row["province"]) !== FALSE )
        {
            $location1 = $row["id"];
        }

     }else if($sizeFrom == 3)
     {
      if(strpos($fromSplit[1], $row["province"]) !== FALSE && strpos($fromSplit[2], $row["city"]) !== FALSE ){
        
          $location1 = $row["id"];
      }else if(strpos($fromSplit[1], $row["province"]) !== FALSE && $row["city"] == null ){
        
          $location1 = $row["id"];
      }

     }else{

       if(strpos($fromSplit[1], $row["province"]) !== FALSE && strpos($fromSplit[2], $row["city"]) !== FALSE 
      && strpos($fromSplit[3],$row["barangay"]) !== FALSE){
       
        $location1 = $row["id"];
      } else if(strpos($fromSplit[1], $row["province"]) !== FALSE && strpos($fromSplit[2], $row["city"]) !== FALSE &&  $row["barangay"] == null){
        
          $location1 = $row["id"];
      }else if(strpos($fromSplit[1], $row["province"]) !== FALSE &&  $row["city"] == null &&  $row["barangay"] == null){
        
        $location1 = $row["id"];
      }

     }

    }

    $this->db->where('service_id', $cart->cart_type_id);
    $this->db->where('vehicle_id', $cart->vehicle_id);
    $this->db->where('dynamic_location',$location1);
    $row1 = $this->db->get('rates')->result_array();


    foreach($dynamicVariables as $key => $value)
    {

    if($sizeTo == 2){

       if(strpos($toSplit[1], $value["province"]) != FALSE){
      
                $location2 = $value["id"];
        }

     }else if($sizeTo == 3)
     {
        if(strpos($toSplit[1], $value["province"]) != FALSE && strpos($toSplit[2], $value["city"]) != FALSE ){
      
          $location2 = $value["id"];
        } 
        else if(strpos($toSplit[1], $value["province"]) != FALSE &&  $value["city"] == null){
          
          $location2 = $value["id"];
        }

     }else{

      if(strpos($toSplit[1], $value["province"]) != FALSE && strpos($toSplit[2], $value["city"]) == true && strpos($toSplit[3], $value["barangay"]) != FALSE){
      
        $location2 = $value["id"];
      } else if(strpos($toSplit[1], $value["province"]) == true && strpos($toSplit[2], $value["city"]) != FALSE &&  $value["barangay"] == null){
        
        $location2 = $value["id"];
      }else if(strpos($toSplit[1], $value["province"]) != FALSE &&  $value["city"] == null &&  $value["barangay"] == null){
    
        $location2 = $value["id"];
      }
     }

    }

    $this->db->where('service_id', $cart->cart_type_id);
    $this->db->where('vehicle_id', $cart->vehicle_id);
    $this->db->where('dynamic_location',$location2);
    $row2 = $this->db->get('rates')->result_array();

    $baseFareOne = count($row) > 0?$row1[0]["base_fare"]:0.00;
    $baseFareTwo = count($row2) > 0?$row2[0]["base_fare"]:0.00;

    $assignedlocation = 1;

    if((double)$baseFareOne >= (double)$baseFareTwo)
    {
      $assignedlocation = $location1;
      //$this->session->set_userdata("assignedlocation",$location1);
    }else{  
      $assignedlocation = $location2;
    }

    return $assignedlocation;

   //  $this->session->set_userdata("assignedlocation",1);

  }

  public function setDistanceByMatrix_google($cart_id)
  {

    $cart = $this->get($cart_id);

    $from = $cart->pickup_location;
    $to   = $cart->delivery_location;
 
    ##################################
    ##################################
    ##################################
    
    $fromLat  = $from->latitude;
    $fromLong = $from->longitude;

    $toLat    = $to->latitude;
    $tolong   = $to->longitude;

    $orig = "$fromLat,$fromLong";
    $dest = "$toLat,$tolong";
     
  $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, "https://api.tomtom.com/routing/1/calculateRoute/$orig:$dest/json?key=oYtmyWNVk9pg9bu7ek1KPpVryb6H8scQ");

    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    # Return response instead of printing.
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);

    $res = json_decode($result);
    $totalDistance = 0;
    //print_r($res->routes[0]->summary->lengthInMeters);
    ##################################
    ##################################
    ##################################

    $payload = unserialize($cart->payload);
    if (in_array($cart->cart_type_id, [3])) {
      if (isset($res->routes[0])) {
        $totalDistance = (INT) $res->routes[0]->summary->lengthInMeters + (INT) $res->routes[0]->summary->trafficLengthInMeters;
        $payload->distance = $totalDistance;
        $payload->eta_text = $res->routes[0]->summary->travelTimeInSeconds;
        $payload->eta_value_in_seconds = $res->routes[0]->summary->travelTimeInSeconds;
      } else {
        $payload->distance = 0;
        $payload->eta_text = "TBD";
        $payload->eta_value_in_seconds = 0;
      }
    } else {
      if (isset($res->routes[0])) {
       $totalDistance = (INT) $res->routes[0]->summary->lengthInMeters + (INT) $res->routes[0]->summary->trafficLengthInMeters;
      $payload['distance'] = $totalDistance;
        // var_dump($payload['distance']); die();
      $payload['eta_text'] = $res->routes[0]->summary->travelTimeInSeconds;
      $payload['eta_value_in_seconds'] =$res->routes[0]->summary->travelTimeInSeconds;
      } else {
        $payload['distance'] = 0;
        $payload['eta_text'] = "TBD";
        $payload['eta_value_in_seconds'] = 0;
      }
    }

    $this->db->where('id', $cart_id);
    return $this->db->update('cart', ['payload' => serialize($payload)]);
  }



public function riderDistanceByMatrix($cart_id)
{
  $cart = $this->cart_model->get($cart_id);

   // $from = $cart->pickup_location;
    $to   = $cart->delivery_location;
 
   //----------------------------------
    $fromLat  = $cart->rider_current_location_latitude;
    $fromLong = $cart->rider_current_location_longitude;

    $toLat    = $to->latitude;
    $tolong   = $to->longitude;


    // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
    $ch = curl_init();
       
    $route = "https://api.mapbox.com/directions/v5/mapbox/driving/$fromLong,$fromLat;$tolong,$toLat?access_token=pk.eyJ1IjoiZ2V0ZXhwcmVzc2NvcnAiLCJhIjoiY2t3ajdyNmQzMWZtcjJ2cDgyNXN3cDI3OSJ9.B04GpqcePSb4PFyBvzy8cg";

    
    curl_setopt($ch, CURLOPT_URL,$route);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);

    $res = json_decode($result);

    // return $res;

    $payload = unserialize($cart->payload);
    $distance =  (INT)$res->routes[0]->distance;
          
    if (in_array($cart->cart_type_id, [3])) {
      if (isset($res->routes[0])) {
      //  $totalDistance = (INT) $res->routes[0]->summary->lengthInMeters + (INT) $res->routes[0]->summary->trafficLengthInMeters;
        $payload->distance = $res->routes[0]->distance;
        $payload->eta_text = "TBD";
        $payload->eta_value_in_seconds = $res->routes[0]->duration;
      } else {
        $payload->distance = 0;
        $payload->eta_text = "TBD";
        $payload->eta_value_in_seconds = 0;
      }
    }else{
      if ($distance > 0) {
        $payload["distance"] = $res->routes[0]->distance;
        $payload["eta_text"] = "TBD";
        $payload["eta_value_in_seconds"] = $res->routes[0]->duration;
      } else {
        $payload["distance"] = 0;
        $payload["eta_text"] = "ETA";
        $payload["eta_value_in_seconds"] = 0;
      }

    }

    $this->db->where('id', $cart_id);
   return $this->db->update('cart', ['payload' => serialize($payload)]);
}

public function setDistanceByMatrix($cart_id)
{
  $cart = $this->cart_model->get($cart_id);

    $from = $cart->pickup_location;
    $to   = $cart->delivery_location;
 
   //----------------------------------
    $fromLat  = $from->latitude;
    $fromLong = $from->longitude;

    $toLat    = $to->latitude;
     $tolong   = $to->longitude;


    // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
    $ch = curl_init();
       
    $route = "https://api.mapbox.com/directions/v5/mapbox/driving/$fromLong,$fromLat;$tolong,$toLat?access_token=pk.eyJ1IjoiZ2V0ZXhwcmVzc2NvcnAiLCJhIjoiY2t3ajdyNmQzMWZtcjJ2cDgyNXN3cDI3OSJ9.B04GpqcePSb4PFyBvzy8cg";

    
    curl_setopt($ch, CURLOPT_URL,$route);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);

    $res = json_decode($result);


    $payload = unserialize($cart->payload);
    $distance =  (INT)$res->routes[0]->distance > 0 ?: 0;
          
    if (in_array($cart->cart_type_id, [3])) {
      if (isset($res->routes[0])) {
      //  $totalDistance = (INT) $res->routes[0]->summary->lengthInMeters + (INT) $res->routes[0]->summary->trafficLengthInMeters;
        $payload->distance = $res->routes[0]->distance;
        $payload->eta_text = "TBD";
        $payload->eta_value_in_seconds = $res->routes[0]->duration;
      } else {
        $payload->distance = 0;
        $payload->eta_text = "TBD";
        $payload->eta_value_in_seconds = 0;
      }
    }else{
      if ($distance > 0) {
        $payload["distance"] = $res->routes[0]->distance;
        $payload["eta_text"] = "TBD";
        $payload["eta_value_in_seconds"] = $res->routes[0]->duration;
      } else {
        $payload["distance"] = 0;
        $payload["eta_text"] = "ETA";
        $payload["eta_value_in_seconds"] = 0;
      }

    }

    
   // return serialize($payload);
    $this->db->where('id', $cart_id);
   return $this->db->update('cart', ['payload' => serialize($payload)]);
}



 //tomtom  API distance matrix
  public function setDistanceByMatrix_tt($cart_id)
  {

    $cart = $this->cart_model->get($cart_id);

    $from = $cart->pickup_location;
    $to   = $cart->delivery_location;
 
    ##################################
    ##################################
    ##################################
    
    $fromLat  = $from->latitude;
    $fromLong = $from->longitude;

    $toLat    = $to->latitude;
    $tolong   = $to->longitude;


    // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
    $orig = "$fromLat,$fromLong";
    $dest = "$toLat,$tolong";
     
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, "https://api.tomtom.com/routing/1/calculateRoute/$orig:$dest/json?key=oYtmyWNVk9pg9bu7ek1KPpVryb6H8scQ");

    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    # Return response instead of printing.
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);

    $res = json_decode($result);
    $totalDistance = 0;
  //  print_r($res->routes[0]->summary->lengthInMeters);
    ##################################
    ##################################
    ##################################

    $payload = unserialize($cart->payload);
    if (in_array($cart->cart_type_id, [3])) {
      if (isset($res->routes[0])) {
        $totalDistance = (INT) $res->routes[0]->summary->lengthInMeters + (INT) $res->routes[0]->summary->trafficLengthInMeters;
        $payload->distance = $totalDistance;
        $payload->eta_text = $res->routes[0]->summary->travelTimeInSeconds;
        $payload->eta_value_in_seconds = $res->routes[0]->summary->travelTimeInSeconds;
      } else {
        $payload->distance = 0;
        $payload->eta_text = "TBD";
        $payload->eta_value_in_seconds = 0;
      }
    } else {
      if (isset($res->routes[0])) {
       $totalDistance = (INT) $res->routes[0]->summary->lengthInMeters + (INT) $res->routes[0]->summary->trafficLengthInMeters;
      $payload['distance'] = $totalDistance;
        // var_dump($payload['distance']); die();
      $payload['eta_text'] = $res->routes[0]->summary->travelTimeInSeconds;
      $payload['eta_value_in_seconds'] =$res->routes[0]->summary->travelTimeInSeconds;
      } else {
        $payload['distance'] = 0;
        $payload['eta_text'] = "TBD";
        $payload['eta_value_in_seconds'] = 0;
      }
    }

 return $payload;
   //die();
   // $this->db->where('id', $cart_id);
   // return $this->db->update('cart', ['payload' => serialize($payload)]);
  }


  public function finalizeCart($data, $cart_id)
  {
  //  if ($status_change != 'false') { # pag false,
     // $data['status'] = 'pending';
   // }

    foreach ($data as $key => $value) {
      if (!$data[$key]) {
        unset($data[$key]);
      }
    }

    // if (count($data)) {
      $this->db->where('id', $cart_id);
      $this->db->update('cart', $data);
    // }


    return $this->get($cart_id);
  }

  public function cancelCart($cart_id)
  {
    $data['status'] = 'cancelled';
    $data['rider_id'] = 0;

    $this->db->where('id', $cart_id);
    $this->db->update('cart', $data);

    return $this->get($cart_id);
  }

public function transactGroceryFoodTrike($data, $cart_id,$location)
  {
    $data['cart_id'] = $cart_id;
    $res = false;
    if(@$data['cart_item_id']) { # pag update

      if (!@$data['quantity'] && @$data['cart_item_id']) {
        $this->db->where('id', @$data['cart_item_id']); # delete
        $res = $this->db->delete('cart_items');
      } else { # update

        $product = $this->inventory_model->get($data['product_id']);
        $data['price_when_added'] = $product->base_price;

        $this->db->where('product_id', $data['product_id']);
        $this->db->where('cart_id', $cart_id);
        $this->db->update('cart_items', ['price_when_added' => $product->base_price]);

        $temp_addons = [];

        if (@$data['addon_ids']) {
          foreach ($data['addon_ids'] as $key => $value) {
            $addon = $this->inventory_model->get($value);
            $temp_addons[] = $addon;
          }

          $data['addon_ids'] = serialize($temp_addons);
        } else {
          $data['addon_ids'] = serialize([]);
        }

        $this->db->where('id', $data['cart_item_id']);
        unset($data['cart_item_id']);
        $res = $this->db->update('cart_items', $data);

      }

    } else {

      if ($data['quantity']) {
        

        $product = $this->inventory_model->get($data['product_id']);
        $data['price_when_added'] = $product->base_price;

        $temp_addons = [];
        if (@$data['addon_ids']) {
          foreach ($data['addon_ids'] as $key => $value) {
            $addon = $this->inventory_model->get($value);
            $temp_addons[] = $addon;
          }
        }

        $data['addon_ids'] = serialize($temp_addons);
        $res = $this->db->insert('cart_items', $data);

      }
    }


    # BLOCK FOR UPDATING PAYLOD W/ MULTIPLIER
    $payload = unserialize($this->db->get_where('cart', ['id' => $cart_id])->row()->payload);
    $multiplier = 0.00;//$this->getSurgeMultiplier($location);
    $payload['multiplier'] = $multiplier;
    $tippings = $this->gettip($cart_id);
    $payload['tip'] = $tippings->tip;
    $payload['grand_total'] = @$payload['grand_total'] + @$payload['tip'];

    $this->db->where('id', $cart_id);
    $this->db->update('cart', ['payload' => serialize($payload)]);
    # BLOCK FOR UPDATING PAYLOD W/ MULTIPLIER

    return $res;
  }

  public function transactGroceryFood($data, $cart_id,$location)
  {
    $data['cart_id'] = $cart_id;
    $payload = [];
    $cart_obj = $this->cart_model->get($cart_id);
    $payloadData = unserialize($cart_obj->payload); 

    //var_dump($payloadData["distance"]); die();

    $multiplier = $this->getSurgeMultiplier($location);
    $payload['multiplier'] = $multiplier;
    $tippings = $this->gettip($cart_id);
    $payload['tip'] = $tippings->tip;

    $payload['category'] = $data['category'];
    $payload['distance'] = (double) $payloadData["distance"];

    $payload['distance_in_m'] =  $payloadData["distance"];
    $payload['eta_value_in_seconds'] = (INT) $payloadData["eta_value_in_seconds"] / 60;
    $payload['distance_in_km'] = $payloadData["distance"] / 1000;
    $payload['distance_in_km_rounded_down'] = ($payloadData["distance"] >= 1) ? floor($payload['distance_in_km']) : 0;


// var_dump($cart_obj); die();
  
    //$this->setBaseFare($cart_obj->cart_type_id, $cart_obj->vehicle_id,$cart_id);
    $rates = $this->admin_model->getRates($cart_obj->cart_type_id, $cart_obj->vehicle_id,$location);
    $payload['price_per_km'] = (double) $rates->per_km_fare;
    // $payload['price_per_km'] = (double) $this->admin_model->getMeta('delivery_per_km');
    $computed_distance_fee = $payload['distance_in_km_rounded_down'] * $payload['price_per_km'];
    // $computed_distance_fee = $basket->distance_in_km_rounded_down * $basket->price_per_km;
    $payload['delivery_fee'] = (double) $rates->base_fare + $computed_distance_fee;
    // $payload['delivery_fee'] = (double) $this->admin_model->getMeta('delivery_fare') + $computed_distance_fee;
    $payload['getapp_commission_percentage'] = (double)$rates->getapp_commission_percentage;
    $payload['getapp_commission'] =  round($payload['delivery_fee'] * $rates->getapp_commission_percentage, 2);
  
    $payload['delivery_price'] = (double)$this->getDeliveryPrice($data['vehicle_id']); # price depende sa vehicle type # deprecated
    $payload['getapp_commision_partner'] = @$cart_obj->getapp_comission_partner;

    $payload['weight_capacity'] = $this->getWeightCapacity($data['vehicle_id']);
    $payload['price'] = ($payload['delivery_fee'] + $payload['delivery_price']) * $multiplier ;
    $payload['grand_total'] = $payload['price'];
  

    $this->db->where('id', $cart_id);
    $res =$this->db->update('cart', ['payload' => serialize($payload)]);

    return $res;
  }

public function transactDeliveryTrike($data,$cart_id,$location, $cart_obj = null)
  {
    $payload = [];

    //$multiplier = $this->getSurgeMultiplier($location);
    $payload['multiplier'] = 0.00;
    $tippings = $this->gettip($cart_id);
    $payload['tip'] = $tippings->tip;

    $payload['category'] = $data['category'];
    $payload['notes'] = $data['notes'];
    $payload['distance'] = (double) @$data['distance'];

    $payload['distance_in_m'] = $payload['distance'];
    $payload['distance_in_km'] = $payload['distance_in_m'] / 1000;
    $payload['distance_in_km_rounded_down'] = ($payload['distance_in_km'] >= 1) ? floor($payload['distance_in_km']) : 0;

    $rates = $this->admin_model->getRatesTrike($cart_obj->cart_type_id, $cart_obj->vehicle_id,$cart_obj->munzoneid,0);

    $payload['price_per_km'] = (double) $rates->per_km_fare;
    // $payload['price_per_km'] = (double) $this->admin_model->getMeta('delivery_per_km');
    $computed_distance_fee = $payload['distance_in_km_rounded_down'] * $payload['price_per_km'];
    // $computed_distance_fee = $basket->distance_in_km_rounded_down * $basket->price_per_km;
    $baseFare   =  (double) number_format(($rates->base_fare * $rates->passenger_limit), 2);

    $adminfee   =  (double) number_format(($rates->admin_fee * $payload['distance_in_km_rounded_down']), 2);
    
    $payload['getapp_commission_percentage'] = (double)$rates->getapp_commission_percentage;
    $payload['getapp_commission'] =  (double)$rates->getapp_commission_percentage + $adminfee;
    
    $payload['delivery_fee'] = (double) $baseFare + $computed_distance_fee;

    $payload['delivery_price'] = $rates; # price depende sa vehicle type # deprecated

    $payload['weight_capacity'] = $this->getWeightCapacity($cart_id);
    $payload['price'] = $payload['delivery_fee'] + @$payload['getapp_commission_percentage'] + @$payload['tip'];
    $payload['grand_total'] = $payload['price'];

    $this->db->where('id', $cart_id);
    $res =$this->db->update('cart', ['payload' => serialize($payload)]);

    return $res;
  
  }

  public function transactDelivery($data, $cart_id,$location, $cart_obj = null)
  {
    $payload = [];
    $cart_obj = $this->cart_model->get($cart_id);
    $payloadData = unserialize($cart_obj->payload); 

    //var_dump($payloadData["distance"]); die();

    $multiplier = $this->getSurgeMultiplier($location);
    $payload['multiplier'] = $multiplier;
    $tippings = $this->gettip($cart_id);
    $payload['tip'] = $tippings->tip;

    $payload['category'] = $data['category'];
    $payload['distance'] = (double) $payloadData["distance"] ? : (double) "0.00" ;

    $payload['distance_in_m'] =  $payloadData["distance"];
    $payload['distance_in_km'] = $payloadData["distance"] / 1000;
    $payload['distance_in_km_rounded_down'] = ($payloadData["distance"] >= 1) ? floor($payload['distance_in_km']) : 0;


// var_dump($cart_obj); die();
  
    //$this->setBaseFare($cart_obj->cart_type_id, $cart_obj->vehicle_id,$cart_id);
    $rates = $this->admin_model->getRates($cart_obj->cart_type_id, $cart_obj->vehicle_id,$location);
    $payload['price_per_km'] = (double) $rates->per_km_fare;
    // $payload['price_per_km'] = (double) $this->admin_model->getMeta('delivery_per_km');
    $computed_distance_fee = $payload['distance_in_km_rounded_down'] * $payload['price_per_km'];
    // $computed_distance_fee = $basket->distance_in_km_rounded_down * $basket->price_per_km;
    $payload['delivery_fee'] = (double) $rates->base_fare + $computed_distance_fee;
    // $payload['delivery_fee'] = (double) $this->admin_model->getMeta('delivery_fare') + $computed_distance_fee;
    $payload['getapp_commission_percentage'] = (double)$rates->getapp_commission_percentage;
    $payload['getapp_commission'] =  round($payload['delivery_fee'] * $rates->getapp_commission_percentage, 2);
  
    $payload['delivery_price'] = (double)$this->getDeliveryPrice($data['vehicle_id']); # price depende sa vehicle type # deprecated

    $payload['weight_capacity'] = $this->getWeightCapacity($data['vehicle_id']);
    $payload['price'] = ($payload['delivery_fee'] + $payload['delivery_price']) * $multiplier ;
    $payload['grand_total'] = $payload['price'];

    $this->db->where('id', $cart_id);
    $res =$this->db->update('cart', ['payload' => serialize($payload)]);

    return $res;
  }

public function transactLaundry($data, $cart_id,$location, $cart_obj = null)
  {
    $payload = [];
    $cart_obj = $this->cart_model->get($cart_id);
    $payloadData = unserialize($cart_obj->payload); 

    $multiplier = $this->getSurgeMultiplier($location);
    $payload['multiplier'] = $multiplier;
    $tippings = $this->gettip($cart_id);
    $payload['tip'] = $tippings->tip;

    $laundryFee  = (double) $this->admin_model->getMeta('laundry_service_fee',$location);

    $payload['category'] = $data['category'];
    $payload['distance'] = (double) $payloadData["distance"] ?:0.00;

    $payload['distance_in_m'] =  $payloadData["distance"];
    $payload['distance_in_km'] = $payloadData["distance"] / 1000;
    $payload['distance_in_km_rounded_down'] = ($payloadData["distance"] >= 1) ? floor($payload['distance_in_km']) : 0;

  
    //$this->setBaseFare($cart_obj->cart_type_id, $cart_obj->vehicle_id,$cart_id);
    $rates = $this->admin_model->getRates($cart_obj->cart_type_id, $cart_obj->vehicle_id,$location);
    $payload['price_per_km'] = (double) $rates->per_km_fare;
    // $payload['price_per_km'] = (double) $this->admin_model->getMeta('delivery_per_km');
    $computed_distance_fee = $payload['distance_in_km_rounded_down'] * $payload['price_per_km'];
    // $computed_distance_fee = $basket->distance_in_km_rounded_down * $basket->price_per_km;
    $payload['delivery_fee'] = (double) $rates->base_fare + $computed_distance_fee;
    // $payload['delivery_fee'] = (double) $this->admin_model->getMeta('delivery_fare') + $computed_distance_fee;
    $payload['getapp_commission_percentage'] = (double)$rates->getapp_commission_percentage;
    $payload['getapp_commission'] =  round(($payload['delivery_fee'] * $rates->getapp_commission_percentage) + $laundryFee, 2);
  
    $payload['delivery_price'] = (double)$this->getDeliveryPrice($data['vehicle_id']); # price depende sa vehicle type # deprecated

    $payload['laundry_fee'] = $laundryFee;

    $payload['weight_capacity'] = $this->getWeightCapacity($data['vehicle_id']);
    $payload['price'] = ($payload['delivery_fee'] + $payload['delivery_price']) * $multiplier ;
    $payload['grand_total'] = $payload['price'] + $laundryFee;

    $this->db->where('id', $cart_id);
    $res =$this->db->update('cart', ['payload' => serialize($payload)]);

    return $res;
  }

  public function gettip($cart_id){
    $this->db->select('tip');
    $this->db->where('id', $cart_id);
    $tippings = $this->db->get('cart')->row();

    return $tippings;
  }

  
  public function updateCartPayment($id){

   

    # get the payment source
    $res = $this->payment_model->getPaymentSourceOnlinePayment($id);

    // # pag chargebale, bayaran
    if ($res->attributes->status == 'chargeable') {
      $res = $this->payment_model->payTheSourceOnlinePayment($id);
    }


    $this->db->where('id', $id);
    $opt = $this->db->get('online_payment_transaction')->row();
    $cartId = $opt->cart_id;

    var_dump($cartId);
    # update na bayad na
    $this->db->where('id', $id);
     $this->db->update('online_payment_transaction', ['status' => $res->attributes->status]);
    
   // if($res->attributes->status == "paid"){
      $data = [
                "payment_method" => "GCASH" ,
                "payment_intent_id" => $opt->transaction_id,
                "payment_status" => $res->attributes->status
              ];

      $this->db->where('id', $cartId);
      return $this->db->update('cart', $data);

   // }
    

    
  }

public function transactRideTrike($data,$cart_id,$location, $cart_obj = null)
  {
   $payload = unserialize($cart_obj->payload) ?: [];

    $tippings = $this->gettip($cart_id);
    $payload['tip'] = $tippings->tip;
   // $payload['notes'] = $data['notes'];
    $payload['distance_in_m'] = @$payload['distance'] ?: 0;
    $payload['distance_in_km'] = $payload['distance_in_m'] / 1000;
    $payload['distance_in_km_rounded_down'] = ($payload['distance_in_km'] >= 1) ? floor($payload['distance_in_km']) : 0;
    $payload['eta_value_in_minutes'] = @$payload['eta_value_in_seconds']? floor(@$payload['eta_value_in_seconds'] / 60): 0;
// var_dump($cart_obj); die();
  
   // $this->setBaseFare($cart_obj->cart_type_id, $cart_obj->vehicle_id,$cart_id);
    $rates = $this->admin_model->getRatesTrike($cart_obj->cart_type_id,$data["vehicle_id"],$location,$data["munzoneid"],$data["zoneid"]);
    $payload['price_per_km'] = (double) $rates->per_km_fare;
    // $payload['price_per_km'] = (double) $this->admin_model->getMeta('delivery_per_km');
    $computed_distance_fee = $payload['distance_in_km_rounded_down'] * $payload['price_per_km'];
    
      
      $baseFare   =  (double) $rates->base_fare * $rates->passenger_limit;
      $perkmFare  =  (double) number_format($rates->per_km_fare, 2);;
      $perMinFare =  (double) number_format($rates->per_minute_fee, 2);
     
      $payload['per_minute_charge'] = "0.00"; //@$payload['eta_value_in_minutes'] * $rates->per_minute_fee;
      $payload['getapp_commission_percentage'] = (double)$rates->getapp_commission_percentage;
      $payload['base_fee'] = (double) number_format($baseFare, 2);
      $payload['per_minute_fee'] = (double) number_format($rates->per_minute_fee, 2);


        $rainy_day_surge_is_active = $this->admin_model->getMeta('rainy_day_surge_is_active',$location);
        $scheduled_surge_is_active = $this->admin_model->getMeta('scheduled_surge_is_active',$location);
        if ($scheduled_surge_is_active) {
          $scheduled_surge_days = unserialize($this->admin_model->getMeta('scheduled_surge_days',$location));

          $current_day = date('N');

          if (in_array($current_day, $scheduled_surge_days)) {

            $scheduled_surge_time = unserialize($this->admin_model->getMeta('scheduled_surge_time',$location));
            $now = DateTime::createFromFormat("H:i:s", date("H:i:s"));
            $from_hour = DateTime::createFromFormat("H:i:s", $scheduled_surge_time[0]);
            $to_hour = DateTime::createFromFormat("H:i:s", $scheduled_surge_time[1]);

            if ($now >= $from_hour && $to_hour >= $now) {
              $surgeMultiplier = (double) $rates->surge_multiplier;
              $surgefee =  (int) ($baseFare +($perkmFare * $payload['distance_in_km_rounded_down'] ) * $surgeMultiplier +$perMinFare);
              $payload['surge_fee'] =  (double) number_format($surgefee, 2);
            }
          }
        }
        elseif ($rainy_day_surge_is_active){
          $surgeMultiplier = (double) number_format($rates->surge_multiplier, 2);
          $surgefee = (int) $baseFare +$computed_distance_fee * $surgeMultiplier +$perMinFare;
          $payload['surge_fee'] =  (double) number_format($surgefee, 2);
        }

        else{
          $payload['surge_fee'] = 0.00;
        }

    // }    // $payload['price'] = ($payload['delivery_fee']) * $multiplier;
       $adminfee = (double) @$payload['distance_in_km_rounded_down'] * $rates->admin_fee;

        $payload['getapp_commission'] =   @$payload['getapp_commission_percentage'] + $adminfee;
        $prePrice =  @$payload['base_fee'] + $computed_distance_fee  + @$payload['getapp_commission']; 
      //  $minimumFare = 80.00;   
        $payload['price'] = $prePrice;


      $payload['grand_total'] = $payload['price'];

    $this->db->where('id', $cart_id);
    $res = $this->db->update('cart', ['payload' => serialize($payload)]);

    return $res;
  }

  public function transactRide($data, $cart_id,$location, $cart_obj = null)
  {
    $payload = unserialize($cart_obj->payload) ?: [];

    $tippings = $this->gettip($cart_id);
    $payload['tip'] = $tippings->tip;
    $payload['notes'] = $data['notes'];
    $payload['distance_in_m'] = @$payload['distance'] ?: 0;
    $payload['distance_in_km'] = $payload['distance_in_m'] / 1000;
    $payload['distance_in_km_rounded_down'] = ($payload['distance_in_km'] >= 1) ? floor($payload['distance_in_km']) : 0;
    $payload['eta_value_in_minutes'] = @$payload['eta_value_in_seconds']? floor(@$payload['eta_value_in_seconds'] / 60): 0;
// var_dump($cart_obj); die();
  
   // $this->setBaseFare($cart_obj->cart_type_id, $cart_obj->vehicle_id,$cart_id);
    $rates = $this->admin_model->getRates($cart_obj->cart_type_id, $cart_obj->vehicle_id,$location);
    $payload['price_per_km'] = (double) $rates->per_km_fare;
    // $payload['price_per_km'] = (double) $this->admin_model->getMeta('delivery_per_km');
    $computed_distance_fee = $payload['distance_in_km_rounded_down'] * $payload['price_per_km'];
    // $computed_distance_fee = $basket->distance_in_km_rounded_down * $basket->price_per_km;
    // $payload['delivery_fee'] = (double) $rates->base_fare + $computed_distance_fee;
    // $payload['delivery_fee'] = (double) $this->admin_model->getMeta('delivery_fare') + $computed_distance_fee;
    // if ($no_commission) {
    //   $payload['getapp_commission_percentage'] = 0.0;
    //   $payload['getapp_commission'] =  0.0;
    // } else {
      $payload['per_minute_charge'] = @$payload['eta_value_in_minutes'] * $rates->per_minute_fee;
      $payload['getapp_commission_percentage'] = (double)$rates->getapp_commission_percentage;
      $payload['base_fee'] = (double) number_format($rates->base_fare, 2);
      $payload['per_minute_fee'] = (double) number_format($rates->per_minute_fee, 2);

      $baseFare   =  (double) number_format($rates->base_fare, 2);
      $perkmFare  =  (double) number_format($rates->per_km_fare, 2);;
      $perMinFare =  (double) number_format($rates->per_minute_fee, 2);

        $rainy_day_surge_is_active = $this->admin_model->getMeta('rainy_day_surge_is_active',$location);
        $scheduled_surge_is_active = $this->admin_model->getMeta('scheduled_surge_is_active',$location);
        if ($scheduled_surge_is_active) {
          $scheduled_surge_days = unserialize($this->admin_model->getMeta('scheduled_surge_days',$location));

          $current_day = date('N');

          if (in_array($current_day, $scheduled_surge_days)) {

            $scheduled_surge_time = unserialize($this->admin_model->getMeta('scheduled_surge_time',$location));
            $now = DateTime::createFromFormat("H:i:s", date("H:i:s"));
            $from_hour = DateTime::createFromFormat("H:i:s", $scheduled_surge_time[0]);
            $to_hour = DateTime::createFromFormat("H:i:s", $scheduled_surge_time[1]);

            if ($now >= $from_hour && $to_hour >= $now) {
              $surgeMultiplier = (double) $rates->surge_multiplier;
              $surgefee =  (int) ($baseFare +($perkmFare * $payload['distance_in_km_rounded_down'] ) * $surgeMultiplier +$perMinFare);
              $payload['surge_fee'] =  (double) number_format($surgefee, 2);
            }
          }
        }
        elseif ($rainy_day_surge_is_active){
          $surgeMultiplier = (double) number_format($rates->surge_multiplier, 2);
          $surgefee = (int) $baseFare +$computed_distance_fee * $surgeMultiplier +$perMinFare;
          $payload['surge_fee'] =  (double) number_format($surgefee, 2);
        }

        else{
          $payload['surge_fee'] = 0.00;
        }
        
     
        $payload['getapp_commission'] =  (@$payload['base_fee'] + ((@$payload['price_per_km'] * @$payload['distance_in_km_rounded_down']) * @$payload['surge_fee'])) * @$payload['getapp_commission_percentage'];
        $prePrice =  @$payload['base_fee'] + (@$payload['price_per_km'] * @$payload['distance_in_km_rounded_down']) + @$payload['surge_fee'] + @$payload['per_minute_charge'] + @$payload['tip']; 
        $minimumFare = 80.00;   
        $payload['price'] = $prePrice > $minimumFare ?$prePrice:$minimumFare;

      
      $payload['grand_total'] = $payload['price'];

    $this->db->where('id', $cart_id);
    $res = $this->db->update('cart', ['payload' => serialize($payload)]);

    return $res;
  }

public function reInitGroceryFoodTrike($data, $cart_id,$location, $cart_obj = null, $no_commission = true)
  {
    $payload =  [];

    //$multiplier = $this->getSurgeMultiplier($location);
    $payload['multiplier'] = 0.00;

    $payload['distance'] = (double) @$data['distance'];

    $payload['distance_in_m'] = $payload['distance'];
    $payload['distance_in_km'] = $payload['distance_in_m'] / 1000;
    $payload['distance_in_km_rounded_down'] = ($payload['distance_in_km'] >= 1) ? floor($payload['distance_in_km']) : 0;
    $tippings = $this->gettip($cart_id);
    $payload['tip'] = $tippings->tip;

    $rates = $this->admin_model->getRatesTrike($cart_obj->cart_type_id, $cart_obj->vehicle_id,$location,$cart_obj->munzoneid,$cart_obj->zoneid);
    $payload['price_per_km'] = (double) $rates->per_km_fare;
    // $payload['price_per_km'] = (double) $this->admin_model->getMeta('delivery_per_km');
    $computed_distance_fee = $payload['distance_in_km_rounded_down'] * $payload['price_per_km'];
    // $computed_distance_fee = $basket->distance_in_km_rounded_down * $basket->price_per_km;
    $baseFare   =  (double) number_format(($rates->base_fare * $rates->passenger_limit), 2);
    $payload['delivery_fee'] = (double) $baseFare + $computed_distance_fee;
    $adminfee   =  (double) number_format(($rates->admin_fee * $payload['distance_in_km_rounded_down']), 2);
    // $payload['delivery_fee'] = (double) $this->admin_model->getMeta('delivery_fare') + $computed_distance_fee;
    
    $payload['getapp_commission_percentage'] = (double)$rates->getapp_commission_percentage + $adminfee;
    $payload['getapp_commission'] =  (double)$rates->getapp_commission_percentage + $adminfee;

    $payload['delivery_price'] = 0.00; # price depende sa vehicle type # deprecated

    // $payload['weight_capacity'] = $this->getWeightCapacity($cart_id);
    $payload['price'] = $payload['delivery_fee'] + @$payload['getapp_commission_percentage'] + @$payload['tip'];
    $payload['grand_total'] = $payload['price'];

    $this->db->where('id', $cart_id);
    $res =$this->db->update('cart', ['payload' => serialize($payload)]);

    return $res;
  }

  public function reInitGroceryFood($data, $cart_id,$location, $cart_obj = null, $no_commission = true)
  {
    $payload = [];

    $multiplier = $this->getSurgeMultiplier($location);
    $payload['multiplier'] = $multiplier;

    $payload['distance'] = (double) @$data['distance'];

    $payload['distance_in_m'] = $payload['distance'];
    $payload['distance_in_km'] = $payload['distance_in_m'] / 1000;
    $payload['distance_in_km_rounded_down'] = ($payload['distance_in_km'] >= 1) ? floor($payload['distance_in_km']) : 0;
    $tippings = $this->gettip($cart_id);
    $payload['tip'] = $tippings->tip;

// var_dump($cart_obj); die();
   // $location = $this->setBaseFare($cart_obj->cart_type_id, $cart_obj->vehicle_id);
   // $this->setBaseFare($cart_obj->cart_type_id, $cart_obj->vehicle_id,$cart_id);
    $rates = $this->admin_model->getRates($cart_obj->cart_type_id, $cart_obj->vehicle_id,$location);
    $payload['price_per_km'] = (double) $rates->per_km_fare;
    // $payload['price_per_km'] = (double) $this->admin_model->getMeta('delivery_per_km');
    $computed_distance_fee = $payload['distance_in_km_rounded_down'] * $payload['price_per_km'];
    // $computed_distance_fee = $basket->distance_in_km_rounded_down * $basket->price_per_km;
    $payload['delivery_fee'] = (double) $rates->base_fare + $computed_distance_fee;
    // $payload['delivery_fee'] = (double) $this->admin_model->getMeta('delivery_fare') + $computed_distance_fee;
    if ($no_commission) {
      $payload['getapp_commission_percentage'] = 0.0;
      $payload['getapp_commission'] =  0.0;
    } else {
      $payload['getapp_commission_percentage'] = (double)$rates->getapp_commission_percentage;
      $payload['getapp_commission'] =  round($payload['delivery_fee'] * $rates->getapp_commission_percentage, 2);
    }

    $payload['delivery_price'] = (double)$this->getDeliveryPrice($cart_id); # price depende sa vehicle type # deprecated

    // $payload['weight_capacity'] = $this->getWeightCapacity($cart_id);
    $payload['price'] = ($payload['delivery_fee'] + $payload['delivery_price']) * $multiplier;
    $payload['grand_total'] = $payload['price'];

    $this->db->where('id', $cart_id);
    $res =$this->db->update('cart', ['payload' => serialize($payload)]);

    return $res;
  }

public function reInitPabiliTrike($data,$cart_id,$location, $cart_obj = null)
  {
    //$multiplier = 0;//$this->getSurgeMultiplier($location);
    $data->multiplier = 0;
    // var_dump($data); die();
    $data->distance = (double) $data->distance;

    $data->distance_in_m = $data->distance;
    $data->distance_in_km = $data->distance_in_m / 1000;
    $data->distance_in_km_rounded_down = ($data->distance_in_km >= 1) ? floor($data->distance_in_km) : 0;

// var_dump($cart_obj); die();
    //$location = $this->setBaseFare($cart_obj->cart_type_id, $cart_obj->vehicle_id);
    //$this->setBaseFare($cart_obj->cart_type_id, $cart_obj->vehicle_id,$cart_id);
    $rates = $this->admin_model->getRatesTrike($cart_obj->cart_type_id, $cart_obj->vehicle_id,$location,$cart_obj->munzoneid,$cart_obj->zoneid);
    $data->price_per_km = (double) $rates->per_km_fare;
    // $payload['price_per_km'] = (double) $this->admin_model->getMeta('delivery_per_km');
    $computed_distance_fee = $data->distance_in_km_rounded_down * $data->price_per_km;
    // $computed_distance_fee = $basket->distance_in_km_rounded_down * $basket->price_per_km;
    $adminfee = (double) $data->distance_in_km_rounded_down * $rates->admin_fee;
    $data->delivery_fee = $rates->base_fare + $computed_distance_fee + $adminfee + $rates->getapp_commission_percentage;
     $data->getapp_commission_percentage = (double) $rates->getapp_commission_percentage;
     $data->getapp_commission =  (double) $rates->getapp_commission_percentage + $rates->admin_fee;
    //}

    $data->delivery_price = (double)$this->getDeliveryPrice($cart_id); # price depende sa vehicle type # deprecated
    $tippings = $this->gettip($cart_id);
    $data->tip = $tippings->tip;

    // $data->weight_capacity = $this->getWeightCapacity($cart_id);
    $data->price = $data->delivery_fee + $data->tip;
    $data->grand_total = $data->price;
    

    $this->db->where('id', $cart_id);
    $res =$this->db->update('cart', ['payload' => serialize($data)]);

    return $res;
  }

  public function reInitPabili($data, $cart_id,$location, $cart_obj = null)
  {

    $multiplier = $this->getSurgeMultiplier($location);
    $data->multiplier = $multiplier;
    // var_dump($data); die();
    $data->distance = (double) $data->distance;

    $data->distance_in_m = $data->distance;
    $data->distance_in_km = $data->distance_in_m / 1000;
    $data->distance_in_km_rounded_down = ($data->distance_in_km >= 1) ? floor($data->distance_in_km) : 0;

// var_dump($cart_obj); die();
    //$location = $this->setBaseFare($cart_obj->cart_type_id, $cart_obj->vehicle_id);
    //$this->setBaseFare($cart_obj->cart_type_id, $cart_obj->vehicle_id,$cart_id);
    $rates = $this->admin_model->getRates($cart_obj->cart_type_id, $cart_obj->vehicle_id,$location);
    $data->price_per_km = (double) $rates->per_km_fare;
    // $payload['price_per_km'] = (double) $this->admin_model->getMeta('delivery_per_km');
    $computed_distance_fee = $data->distance_in_km_rounded_down * $data->price_per_km;
    // $computed_distance_fee = $basket->distance_in_km_rounded_down * $basket->price_per_km;
    $data->delivery_fee = (double) $rates->base_fare + $computed_distance_fee;
    
      $data->getapp_commission_percentage = (double)$rates->getapp_commission_percentage;
      $data->getapp_commission =  round($data->delivery_fee * $rates->getapp_commission_percentage, 2);
    

    $data->delivery_price = (double)$this->getDeliveryPrice($cart_id); # price depende sa vehicle type # deprecated

    // $data->weight_capacity = $this->getWeightCapacity($cart_id);
    $data->price = ($data->delivery_fee + $data->delivery_price) * $multiplier;
    $data->grand_total = $data->price;
    $tippings = $this->gettip($cart_id);
    $data->tip = $tippings->tip;

    $this->db->where('id', $cart_id);
    $res =$this->db->update('cart', ['payload' => serialize($data)]);

    return $res;
  }

  public function getDeliveryPrice($vehicle_id)
  {
    //$this->db->where('id', $cart_id);
    //$vehicle_id = $this->db->get('cart')->row()->vehicle_id;

    $this->db->where('id', $vehicle_id);
    return $this->db->get('vehicles')->row()->delivery_price;
  }

  public function getWeightCapacity($vehicle_id)
  {
    //$this->db->where('id', $cart_id);
    //$vehicle_id = $this->db->get('cart')->row()->vehicle_id;

    $this->db->where('id', $vehicle_id);
    return $this->db->get('vehicles')->row()->weight_capacity;
  }

public function transactPabiliTrike($data,$cart_id,$location,$cart_obj)
  {
   
    $payload = (object)[];
    $payload->items = [];
    for ($i=0; $i < count($data['item_name']) ; $i++) {
      $payload->items[] = (object)['item_name' => $data['item_name'][$i], 'quantity' => $data['quantity'][$i]];
    }

    $multiplier = 0.00;//$this->getSurgeMultiplier($location);
    $payload->multiplier = $multiplier;
    $tippings = $this->gettip($cart_id);
    $payload->tip = $tippings->tip;

    $rates = $this->admin_model->getRatesTrike($cart_obj->cart_type_id, $cart_obj->vehicle_id,$location,$cart_obj->munzoneid,$cart_obj->zoneid);
    //$pabiliFee = $rates->pabili_fee;
    
    $payload->pabili_fee_additional = $rates->pabili_fee;
    
    $payload->estimate_total_amount = @$data['estimate_total_amount'];
    $payload->estimate_total_amount_with_tip = @$payload->estimate_total_amount + @$payload->tip;

    $this->db->where('id', $cart_id);
    $res = $this->db->update('cart', ['payload' => serialize($payload)]);

    return $res;
  }

  public function transactPabili($data, $cart_id,$location)
  {
    $cart_obj = $this->cart_model->get($cart_id);
    $payload = unserialize($cart_obj->payload); 
    $payload->items = [];
    for ($i=0; $i < count($data['item_name']) ; $i++) {
      $payload->items[] = (object)['item_name' => $data['item_name'][$i], 'quantity' => $data['quantity'][$i]];
    }

    $multiplier = $this->getSurgeMultiplier($location);
    $payload->multiplier = $multiplier;
    $tippings = $this->gettip($cart_id);
    $payload->tip = $tippings->tip;
    
    if ($this->input->get('pabili_fee_additional')) 
    {
      $payload->pabili_fee_additional = $this->admin_model->getMeta('pabili_fee_additional',$location);
    } else 
    {
      $payload->pabili_fee_additional = 0;
    }

    $payload->estimate_total_amount = @$data['estimate_total_amount'];
    $payload->estimate_total_amount_with_tip = @$payload->estimate_total_amount + @$payload->tip;


    // var_dump($data); die();
    $payload->distance = (double) $payload->distance;

    $payload->distance_in_m = $payload->distance;
    $payload->distance_in_km = $payload->distance_in_m / 1000;
    $payload->distance_in_km_rounded_down = ($payload->distance_in_km >= 1) ? floor($payload->distance_in_km) : 0;


    $rates = $this->admin_model->getRates($cart_obj->cart_type_id, $cart_obj->vehicle_id,$location);
    $payload->price_per_km = (double) $rates->per_km_fare;
    $computed_distance_fee = $payload->distance_in_km_rounded_down * $payload->price_per_km;
     
    $payload->delivery_fee = (double) $rates->base_fare + $computed_distance_fee;
     
      $payload->getapp_commission_percentage = (double)$rates->getapp_commission_percentage;
      $payload->getapp_commission =  round($payload->delivery_fee * $rates->getapp_commission_percentage, 2);
    

    $payload->delivery_price = (double)$this->getDeliveryPrice($data['vehicle_id']); # price depende sa vehicle type #  
    $payload->price = ($payload->delivery_fee + $payload->delivery_price) * $multiplier;
    $payload->grand_total = $payload->price;
   // $tippings = $this->gettip($cart_id);
   // $data->tip = $tippings->tip;


    $this->db->where('id', $cart_id);
    $res = $this->db->update('cart', ['payload' => serialize($payload)]);

    return $res;
  }

  public function getDeliveryDetails($vehicle_type_id,$location)
  {
    $data = (object)[];
    $data->categories = $this->admin_model->getMeta('package_options',$location) ? explode(', ', $this->admin_model->getMeta('package_options',$location)) : ['Document', 'Other'];
    $data->vehicle_weight_capacity = $this->vehicles_model->get($vehicle_type_id)->weight_capacity;
    $data->vehicle_weight_capacity_text = "This Rider can carry up to " . $this->vehicles_model->get($vehicle_type_id)->weight_capacity;
    return $data;
  }

  public function setDistance($distance, $cart_id)
  {
    $res = $this->db->get_where('cart', ['id' => $cart_id])->row();
    $payload = unserialize($res->payload);

    if (!$payload) {
      $payload = [];
      $payload['distance'] = $distance;
    } else {
      $payload['distance'] = $distance;
    }

    $this->db->where('id', $cart_id);
    return $this->db->update('cart', ['payload' => serialize($payload)]);
  }

  /**
   * [formatGroceryFoodBasket description]
   * @param  [type] $res response object
   * @return [type]      [description]
   */
  public function formatGroceryFoodBasket($res)
  {
    $basket = (object)[];
    $basket->items = [];
    $basket->sub_total = 0;
    $basket->sub_total_with_addons = 0;
    $basket->grand_total = 0;
    $basket->total_items = 0;

    $items = $this->getCartItems($res->id, $res->cart_type_id);

    foreach ($items as $key => $value) {
      $value->addon_names_array = $this->extractAddonsAsArray($value->addon_ids);
      $value->addons = $this->extractAddons($value->addon_ids);
      $value->quantity = (double) $value->quantity;
      $value->base_price = (double) $value->base_price;
      $basket->sub_total += $value->computed_price;
      $basket->total_items += $value->quantity;
      $value->price_when_added = (double) $value->price_when_added;
      $value->computed_price = (double) $value->computed_price; # computed in SQL
      $value->computed_addons_price = (double) array_sum(array_column($value->addons, 'base_price')); # computed in SQL
      $value->computed_sub_total = $value->computed_price + $value->computed_addons_price;
      $basket->sub_total_with_addons += $value->computed_sub_total;
      unset($value->addon_ids);
      $basket->items[] = $value;
    }

    # addon price block
    $total_addon_price = 0;
    $total_addon_count = 0;
    if ($basket->items) {
      foreach ($basket->items as $key => $value) {
        if ($value->addons) {
          foreach ($value->addons as $key => $addon_row) {
          $total_addon_price += $addon_row->base_price;
          $total_addon_count += 1;
          }
        }
      }
    }
    $basket->total_addon_price = $total_addon_price;
    $basket->total_addon_count = $total_addon_count;
    # / addon price block

    $payload = unserialize($res->payload);

    $basket->distance_in_m = (double)@$payload['distance'] ?: 0;
    $basket->distance_in_km = $basket->distance_in_m / 1000;

    $basket->distance_in_km_rounded_down = floor($basket->distance_in_km) ?: 0;
    # OLD CODE
    // if ($res->service_type == 'food') {
    //   $basket->price_per_km = (double) $this->admin_model->getMeta('food_per_km'); #OLD fEES FORMULA
    //   $computed_distance_fee = $basket->distance_in_km_rounded_down * $basket->price_per_km;
    //   $basket->delivery_fee = (double) $this->admin_model->getMeta('food_fee') + $computed_distance_fee;
    // }
    //
    // if ($res->service_type == 'grocery') {
    //   $basket->price_per_km = (double) $this->admin_model->getMeta('grocery_per_km');
    //   $computed_distance_fee = $basket->distance_in_km_rounded_down * $basket->price_per_km;
    //   $basket->delivery_fee = (double) $this->admin_model->getMeta('grocery_fee') + $computed_distance_fee;
    // }

    # W/ COMMISSION
    $rates = $this->admin_model->getRates($res->cart_type_id, $res->vehicle_id,$res->assigned_location);
    $basket->price_per_km = (double) $rates->per_km_fare;
    $computed_distance_fee = $basket->distance_in_km_rounded_down * $basket->price_per_km;
    $basket->delivery_fee = (double) $rates->base_fare + $computed_distance_fee;

    $payload = unserialize($res->payload);

    $basket->getapp_commission_percentage = @$payload['getapp_commission_percentage'] ?: 0;
    $basket->getapp_commission =  @$payload['getapp_commission'] ?: 0;

    $basket->eta_text = @$payload['eta_text'] ?: "TBD";
    $basket->multiplier = @$payload['multiplier'];
    $basket->eta_value_in_seconds = @$payload['eta_value_in_seconds'] ?: 0;
    $basket->base_delivery_fee = $basket->delivery_fee;
    $basket->tip =  @$payload['tip'];

    $basket->getapp_comission_partner = number_format((double) @$payload['getapp_commision_partner'] * (double)$basket->sub_total,2) ;

    $basket->grand_total = $basket->sub_total + ($basket->delivery_fee * @$payload['multiplier']) + $basket->total_addon_price + @$basket->tip;
    $basket->grand_total = round($basket->grand_total, 2);
    $basket->grand_total_items = $basket->total_items + $basket->total_addon_count;

    return $basket;
  }

  public function extractAddons($addon_obj)
  {
    if (!$addon_obj) {
      return (object)[];
    }
    $addons = unserialize($addon_obj);

    $res = [];

    foreach ($addons as $key => $value) {
      $res[] = (object) ['id' => $value->id, 'product_name' => $value->product_name, 'base_price' => (double)$value->base_price ];
    }

    return $res;
  }

  public function extractAddonsAsArray($addon_obj)
  {
    if (!$addon_obj) {
      return [];
    }
    $addons = unserialize($addon_obj);

    $res = [];

    foreach ($addons as $key => $value) {
      $res[] = $value->product_name;
    }

    return $res;
  }

  public function formatDelivery($res)
  {
    $basket = unserialize($res->payload)?: [];
    # TODO: delivery fee???
    #
    // var_dump($basket); die();
    $basket['distance'] = (double) @$basket['distance']?: (double) "0.00";
    $basket['delivery_fee'] = @$basket['delivery_fee']?:0;
    $basket['eta_text'] = @$basket['eta_text']?: "TBD";
    $basket['eta_value_in_seconds'] = @$basket['eta_value_in_seconds']?: "0.00";

     //$basket->grand_total = $basket->grand_total;
    // var_dump($basket); die();
    return $basket;
  }

 public function formatLaundry($res)
  {
    $basket = unserialize($res->payload)?: [];
    # TODO: delivery fee???
    #
   //  var_dump(@$basket['eta_value_in_seconds']); die();
    $basket['distance'] = (double)@$basket['distance']?: (double)"0.00";
    $basket['delivery_fee'] =  @$basket['delivery_fee']?:0;
    $basket['eta_text'] = @$basket['eta_text']?: "TBD";
    $basket['eta_value_in_seconds'] = @$basket['eta_value_in_seconds']?: "0.00";

    // $basket->grand_total = $basket->grand_total;
    // var_dump($basket); die();
    return $basket;
  }

  public function formatRide($res)
  {
    $basket = unserialize($res->payload)?: [];
    $rates = $this->admin_model->getRates($res->cart_type_id, $res->vehicle_id,$res->assigned_location);

    # TODO: delivery fee???
    #
    // var_dump($basket); die();
    $basket['distance'] = (double) @$basket['distance']?: (double)"0.00";
    $basket['eta_text'] = @$basket['eta_text']?: "TBD";
    $basket['eta_value_in_seconds'] = @$basket['eta_value_in_seconds']?: "0.00";
    $basket['eta_value_in_minutes'] = @$basket['eta_value_in_seconds']? floor(@$basket['eta_value_in_seconds'] / 60): 0;

    // if (in_array($res->vehicle_id, [3,12])) {
      #if motorcycle,
    // $basket['per_minute_charge'] = 0;
    // } else {
    $basket['per_minute_charge'] = (double) number_format(@$basket['per_minute_charge'] ,2);
    // }
    $basket['getapp_commission_percentage'] = (double) number_format(@$basket['getapp_commission_percentage'] ,2);
    $basket['getapp_commission'] = (double) number_format(@$basket['getapp_commission'] ,2);
    $basket['base_fee'] = (double) number_format(@$basket['base_fee'] ,2);
    $basket['per_km_fee'] = (double) number_format(@$basket['price_per_km'] ,2);
    $basket['surge_fee'] = (double) number_format(@$basket['surge_fee'] ,2);
    $basket['per_minute_fee'] = (double) number_format(@$basket['per_minute_fee'] ,2);
    $basket['price'] = (double) number_format(@$basket['price'] ,2);
    // $basket['grand_total'] = (double) number_format(@$basket['grand_total'] + @$basket['per_minute_charge'], 2);

    // $basket->grand_total = $basket->grand_total;
    // var_dump($basket); die();
    return $basket;
  }
public function formatPabiliTrike($res)
  {
    $basket = (object)[];
    if ($res->payload && unserialize($res->payload) !== null) {
      $basket = unserialize($res->payload);
    }

    // var_dump($basket); die();

    $basket->eta_text = @$basket->eta_text?: "TBD";
    $basket->eta_value_in_seconds = @$basket->eta_value_in_seconds?: "0.00";

    $basket->distance_in_m = @$basket->distance ?: 0;
    $basket->distance_in_km = @$basket->distance_in_m / 1000;
    $basket->distance_in_km_rounded_down = ($basket->distance_in_km >= 1) ? floor($basket->distance_in_km) : 0;

    # OLD
    // $basket->price_per_km = (double) $this->admin_model->getMeta('pabili_per_km');
    // $computed_distance_fee = $basket->distance_in_km_rounded_down * $basket->price_per_km;
    // $basket->delivery_fee = (double) $this->admin_model->getMeta('pabili_fee') + $computed_distance_fee;

    # W/ COMMISSION
    //$rates = $this->admin_model->getRates($res->cart_type_id, $res->vehicle_id,$res->assigned_location);
    $rates = $this->admin_model->getRatesTrike($res->cart_type_id, $res->vehicle_id,$res->assigned_location,$res->munzoneid,$res->zoneid);
    // $pabili_fee_additional = $this->admin_model->getMeta('pabili_fee_additional');
    $pabili_fee_additional = @$basket->pabili_fee_additional ?: 0.00;
    $basket->price_per_km = (double) $rates->per_km_fare;
    $computed_distance_fee = $basket->distance_in_km_rounded_down * $basket->price_per_km;
    $adminfee = $basket->distance_in_km_rounded_down * $rates->admin_fee;
    $basket->delivery_fee = (double) $rates->base_fare + $computed_distance_fee + $adminfee + @$basket->getapp_commission_percentage;
    $basket->getapp_commission_percentage = (double) @$basket->getapp_commission_percentage ?: 0.0;
    $basket->getapp_commission =  (double) @$basket->getapp_commission ?: 0.0;
    $basket->pabili_fee_additional = (double) $pabili_fee_additional;
    $basket->tip = (double)@$basket->tip;

    // $basket->distance = @$basket->distance?: "";
    // $basket->delivery_fee = @$basket->delivery_fee?: 0;
    $basket->_estimate_total_amount_without_delivery_fees = (double) @$basket->estimate_total_amount;
    $basket->estimate_total_amount = @$basket->estimate_total_amount + $basket->delivery_fee  + $pabili_fee_additional + @$basket->tip;
    return $basket;
  }

  public function formatPabili($res)
  {
    $basket = (object)[];
    if ($res->payload && unserialize($res->payload) !== null) {
      $basket = unserialize($res->payload);
    }

    // var_dump($basket); die();

    $basket->eta_text = @$basket->eta_text?: "TBD";
    $basket->eta_value_in_seconds = @$basket->eta_value_in_seconds?: "";
    $basket->distance_in_m = @$basket->distance ?: 0;
    $basket->distance_in_km = @$basket->distance_in_m / 1000;
    $basket->distance_in_km_rounded_down = ($basket->distance_in_km >= 1) ? floor($basket->distance_in_km) : 0;

    # OLD
    // $basket->price_per_km = (double) $this->admin_model->getMeta('pabili_per_km');
    // $computed_distance_fee = $basket->distance_in_km_rounded_down * $basket->price_per_km;
    // $basket->delivery_fee = (double) $this->admin_model->getMeta('pabili_fee') + $computed_distance_fee;

    # W/ COMMISSION
    $rates = $this->admin_model->getRates($res->cart_type_id, $res->vehicle_id,$res->assigned_location);
    // $pabili_fee_additional = $this->admin_model->getMeta('pabili_fee_additional');
    $pabili_fee_additional = @$basket->pabili_fee_additional ?: 0.00;
    $basket->price_per_km = (double) $rates->per_km_fare;
    $computed_distance_fee = $basket->distance_in_km_rounded_down * $basket->price_per_km;
    $basket->delivery_fee = (double) $rates->base_fare + $computed_distance_fee;
    $basket->getapp_commission_percentage = (double) @$basket->getapp_commission_percentage ?: 0.0;
    $basket->getapp_commission =  (double) @$basket->getapp_commission ?: 0.0;
    $basket->pabili_fee_additional = (double)$pabili_fee_additional;
    $basket->tip = (double)@$basket->tip;
    

    // $basket->distance = @$basket->distance?: "";
    // $basket->delivery_fee = @$basket->delivery_fee?: 0;
    $basket->_estimate_total_amount_without_delivery_fees = (double) @$basket->estimate_total_amount;
    $basket->estimate_total_amount = @$basket->estimate_total_amount + ($basket->delivery_fee * @$basket->multiplier) + $pabili_fee_additional + @$basket->tip;
    return $basket;
  }

 

  public function getCartStats($stat_name)
  {

    switch ($stat_name) {
      case 'orders_daily':
        $this->db->select('COUNT(*) as a');
        $this->db->where('DAY(updated_at) = DAY(NOW()) && MONTH(updated_at) = MONTH(NOW()) && YEAR(updated_at) = YEAR(NOW())');
        break;

      case 'orders_weekly':
        $this->db->select('COUNT(*) as a');
        $this->db->where('YEARWEEK(created_at) = YEARWEEK(NOW())');
        break;

      case 'orders_monthly':
        $this->db->select('COUNT(*) as a');
        $this->db->where('YEAR(created_at) = YEAR(NOW()) AND MONTH(created_at) = MONTH(NOW())');
        break;

      case 'car_all_time':
        $this->db->select('COUNT(*) as a');
        $this->db->where('cart_type_id = 1');
        break;

      case 'grocery_all_time':
        $this->db->select('COUNT(*) as a');
        $this->db->where('cart_type_id = 2');
        break;

      case 'pabili_all_time':
        $this->db->select('COUNT(*) as a');
        $this->db->where('cart_type_id = 3');
        break;

      case 'delivery_all_time':
        $this->db->select('COUNT(*) as a');
        $this->db->where('cart_type_id = 4');
        break;

      case 'food_all_time':
        $this->db->select('COUNT(*) as a');
        $this->db->where('cart_type_id = 5');
        break;
     
      case 'completed_orders_daily':
        $this->db->select('COUNT(*) as a');
        $this->db->where('DAY(completed_at) = DAY(NOW()) && MONTH(completed_at) = MONTH(NOW()) && YEAR(completed_at) = YEAR(NOW())');
        $this->db->where('status = "completed"');
        break;

     case 'cancel_orders_daily':
        $this->db->select('COUNT(*) as a');
        $this->db->where('DAY(updated_at) = DAY(NOW()) && MONTH(updated_at) = MONTH(NOW()) && YEAR(updated_at) = YEAR(NOW())');
        $this->db->where('status = "cancelled"');
        break;
        
      case 'cancel_orders_weekly':
        $this->db->select('COUNT(*) as a');
        $this->db->where('YEARWEEK(created_at) = YEARWEEK(NOW())');
        $this->db->where('status = "cancelled"');
        break;
      case 'cancel_orders_monthly':
        $this->db->select('COUNT(*) as a');
        $this->db->where('YEAR(created_at) = YEAR(NOW()) AND MONTH(created_at) = MONTH(NOW())');
        $this->db->where('status = "cancelled"');
        break;


      default:
        case 'orders_all_time':
        $this->db->select('COUNT(*) as a');
        break;
        // code...
        break;
    }
    $this->db->where('assigned_location', $this->session->userdata('location'));
    $stats = @$this->db->get('cart')->row()->a ?: 0;
    return $stats;
  }

  public function getSurgeMultiplier($location)
  {
    $rainy_day_surge_is_active = $this->admin_model->getMeta('rainy_day_surge_is_active',$location);
    if ($rainy_day_surge_is_active) {
      return (double) $this->admin_model->getMeta('rainy_day_surge_rate',$location);
    }

    $scheduled_surge_is_active = $this->admin_model->getMeta('scheduled_surge_is_active',$location);
    if ($scheduled_surge_is_active) {
      $scheduled_surge_days = unserialize($this->admin_model->getMeta('scheduled_surge_days',$location));

      $current_day = date('N');

      if (in_array($current_day, $scheduled_surge_days)) {

        $scheduled_surge_time = unserialize($this->admin_model->getMeta('scheduled_surge_time',$location));
        $now = DateTime::createFromFormat("H:i:s", date("H:i:s"));
        $from_hour = DateTime::createFromFormat("H:i:s", $scheduled_surge_time[0]);
        $to_hour = DateTime::createFromFormat("H:i:s", $scheduled_surge_time[1]);

        if ($now >= $from_hour && $to_hour >= $now) {
          return (double) $this->admin_model->getMeta('scheduled_surge_rate',$location);
        }
      }
    }

    return 1;
  }


  public function getCartItems($cart_id, $cart_type_id)
  {
    $this->db->select('cart_items.id as cart_item_id, cart_id, product_id, addon_ids, product_name, image, category, description, base_price, quantity, price_when_added, (base_price * quantity) as computed_price, notes, image');
    $this->db->where('cart_id', $cart_id);
    $this->db->join('inventory', 'inventory.id = cart_items.product_id', 'left');
    $res = $this->db->get('cart_items')->result();
    if (!$res) {
      return [];
    }
    foreach ($res as &$value) {
      $value = $this->formatCartItems($value, $cart_type_id);
    }
    return $res;
  }


  public function initializePayload()
  {
    $payload = (object)[];
    $payload->orders_product_ids = [];

    return $payload;
  }

  public function formatCartItems($res, $cart_type_id)
  {
    switch ($cart_type_id) {
      case 2:
      $res->image = (@$res->image) ? $this->full_up_path . $res->image : base_url('public/admin/img/placeholder-grocery.png');
        break;

      case 5:
      $res->image = (@$res->image) ? $this->full_up_path . $res->image : base_url('public/admin/img/placeholder-food.png');
        break;

      default:
        $res->image = (@$res->image) ? $this->full_up_path . $res->image : $this->temp_image;
        break;
    }
    return $res;
  }

  public function changeRiderLocation($data, $cart_id)
  {
    $this->db->where('id', $cart_id);
    return $this->db->update('cart', $data);


  }

  public function formatOrderId($res, $rider)
  {
    # https://stackoverflow.com/questions/9706429/get-the-first-letter-of-each-word-in-a-string
    $acronym = "TBD";
    if (@$rider->full_name) {
      $acronym = "";
      $words = explode(' ', @$rider->full_name);

      foreach ($words as $w) {
        $acronym .= substr($w, 0, 1);
      }
    }
    // var_dump($rider->full_name);
    return strtoupper($acronym) . strtotime($res->created_at);
  }

  public function formatRes($res)
  {
    $res->total_price = (double) $res->total_price;
    $res->service_type = $this->services_model->get($res->cart_type_id)->type;
    $res->service_label = $this->services_model->get($res->cart_type_id)->label;
    $res->status_text = $this->getStatusText($res->status);
    $res->rider_current_location_latitude =  $res->rider_current_location_latitude;
    $res->rider_current_location_longitude = $res->rider_current_location_longitude;
    $res->pickup_location = $res->pickup_location ? json_decode($res->pickup_location) : (object)[];
    $res->delivery_location = $res->delivery_location ? json_decode($res->delivery_location) : (object)[];
    $res->created_at_unformatted = $res->created_at;
    $res->created_at = date('F j, Y g:i a', strtotime($res->created_at));
  /*  if($res->vehicle_id == 11)
    {
      $res->basket = $this->delegateBasketFormatTrike($res);
    }else
    {*/
      $res->basket = $this->delegateBasketFormat($res);
    //}
    //$res->basket = $this->delegateBasketFormat($res);
    $res->customer = $this->formatForRiderHome($res);
    $rider = $res->rider_id ? $this->riders_model->get($res->rider_id) : (object)[];
    $res->partner_id = "";
    if(in_array($res->cart_type_id, [2,5])) 
    {
      $partner = $this->getPartnerByCartId($res->id);
      $res->getapp_comission_partner = @$partner->getapp_comission_percentage;
      $res->partner_id = @$partner->id;
    }
    $res->order_id = $this->formatOrderId($res, $rider);
    $res->rider = $this->formatRiderObj($rider);
    $res->cart_type_id = $res->cart_type_id;
    return $res;
  }

  public function getStatusText($status)
  {
    $status_arr =
      [
      'waiting' => 'Waiting store to accept order',
      'init' => 'Initialized',
      'cancelled' => 'Cancelled',
      'pending' => 'Looking for riders',
      'preparing' => 'Store is preparing your order',
      'ready_for_pickup' => 'Order is ready to pick up',
      'accepted' => 'Rider accepted the booking',
      'otw' => 'Rider is on the way',
      'arrived_at_store' => 'Rider has arrived at store',
      'placing_order' => 'Rider is placing order',
      'got_items' => 'Rider got the items',
      'arriving_at_destination' => 'Arriving at destination',
      'arrived_at_destination' => 'Arrived at destination',
      'scheduled_delivery' => 'Scheduled delivery',
      'completed' => 'Booking completed'];

      return $status_arr[$status];
  }

  public function formatRiderObj($rider)
  {
    $res = (object)[];
    if (!$rider) {
      $res->full_name = "Removed Rider";
      $res->vehicle_model = "N/A";
      $res->plate_number = "N/A";
      $res->profile_picture = base_url('public\admin\img\placeholder-logo.png');
      $res->mobile_num = "N/A";
    } else {
      $res->full_name = @$rider->full_name;
      $res->vehicle_model = @$rider->active_vehicle->vehicle_model;
      $res->plate_number = @$rider->active_vehicle->plate_number;
      $res->profile_picture = @$rider->profile_picture;
      $res->mobile_num = @$rider->mobile_num;
    }

    return $res;
  }

  public function delegateTransactionTrike($data,$cart_id,$location)
  {
    $res = $this->get($cart_id);

    if ($this->admin_model->getMaintenanceModeValueByServiceId($res->cart_type_id,$location)) {
      # pag maintenance mode
      return null;
    }

    $transaction = false;
    switch ($res->service_type) {
      case 'car':
        $transaction = $this->transactRideTrike($data,$cart_id,$location, $res);
        break;
      case 'pabili':
        $transaction = $this->transactPabiliTrike($data,$cart_id,$location,$res);
        break;
      case 'delivery':
        $transaction = $this->transactDeliveryTrike($data,$cart_id,$location, $res);
        break;

      case 'food':
      case 'grocery':
        $transaction = $this->transactGroceryFoodTrike($data,$cart_id,$location);
        break;
    }

    return $transaction;
  }

  public function delegateTransaction($data, $cart_id,$location)
  {
    $res = $this->get($cart_id);

    if ($this->admin_model->getMaintenanceModeValueByServiceId($res->cart_type_id,$location)) {
      # pag maintenance mode
      return null;
    }

    $transaction = false;
    switch ($res->service_type) {
      case 'car':
        $transaction = $this->transactRide($data, $cart_id,$location, $res);
        break;
      case 'pabili':
        $transaction = $this->transactPabili($data, $cart_id,$location);
        break;
      case 'delivery':
        $transaction = $this->transactDelivery($data, $cart_id,$location, $res);
        break;
     case 'laundry':
       $transaction = $this->transactLaundry($data, $cart_id,$location, $res);
      break;

      case 'food':
      case 'grocery':
        $transaction = $this->transactGroceryFood($data, $cart_id,$location);
        break;
    }

    return $transaction;
  }

 function delegateBasketFormatTrike($res) {
    switch ($res->service_type) {
      case 'car':
        $basket = $this->formatRide($res);
        break;
      case 'pabili':
      $basket = $this->formatPabiliTrike($res);
        break;
      case 'delivery':
        $basket = $this->formatDelivery($res);
        break;

      case 'grocery':
      case 'food':
        $basket = $this->formatGroceryFoodBasket($res);
        break;
    }

    return @$basket ?: (object)[];
  }

  function delegateBasketFormat($res) {
    switch ($res->service_type) {
      case 'car':
        $basket = $this->formatRide($res);
        break;
      case 'pabili':
      $basket = $this->formatPabili($res);
        break;
      case 'delivery':
        $basket = $this->formatDelivery($res);
        break;
     case 'laundry':
        $basket = $this->formatLaundry($res);
        break;
     case 'laundry':
        $basket = $this->formatLaundry($res);
        break;

      case 'grocery':
      case 'food':
        $basket = $this->formatGroceryFoodBasket($res);
        break;
    }

    return @$basket ?: (object)[];
  }

  public function emptyCart($cart_id)
  {
    $this->db->where('cart_id', $cart_id);
    return $this->db->delete('cart_items');
  }

  /**
   * Expire bookings that's 2 hours ago or more.
   * Called in Look for Customers
   * @return [type] [description]
   */
  public function forceExpireBookings()
  {
    $this->db->where('cart.status = "pending"');
    $this->db->where('cart.updated_at < DATE_SUB(NOW(), INTERVAL 15 MINUTE)');
    return $this->db->update('cart', ['status' => 'cancelled', 'rider_id' => 0]);
    // var_dump($this->db->get('cart')->result()); die();
    // var_dump($this->db->query('SELECT DATE_SUB(NOW(), INTERVAL 2 HOUR)')->result()); die();
  }

  public function getByVehicle($vehicle_id)
  {
    $this->db->where('vehicle_id', $vehicle_id);
    $this->db->where('status', 'pending');
    $this->db->where('rider_id', 0);
    $res = $this->db->get('cart')->result();
  // echo $res->assigned_location;
     // die();
    if (!$res) {
      return [];
    }

    if ($this->input->get('rider_id')) {
      $rider = $this->riders_model->get($this->input->get('rider_id'));

      $rider_lat = $rider->rider_latitude;
      $rider_long = $rider->rider_longitude;


      foreach ($res as $key => $value) {

      $location = $value->assigned_location != '' || $value->assigned_location != null? $value->assigned_location:1;

       $rider_radius_in_m = $this->admin_model->getMeta('rider_radius_in_m',$location);
        $pickup_location = json_decode($value->pickup_location);

        $pickup_lat = $pickup_location->latitude;
        $pickup_long = $pickup_location->longitude;
        $d = $this->notifications_model->getDistanceByMatrix($rider_lat, $rider_long, $pickup_lat, $pickup_long);
        // var_dump($d); die();
        if (!($d > 0 && $d <= $rider_radius_in_m)) { # pag hindi pasok sa rider radius
          unset($res[$key]);
        }
      }
    }


    foreach ($res as &$value) {
      $value = $this->formatRes($value);
    }

    foreach ($res as &$value) {
      $value = $this->formatForRiderHome($value);
    }

    return $res;
  } 

  public function formatForRiderHome($res)
  {
    $temp = $res;
    $customer = $this->customers_model->get($temp->customer_id);
    $service = $this->services_model->get($temp->cart_type_id);
    $res = (object)[];
    $res->cart_id = (int)$temp->id;
    $res->customer = (object)['profile_picture' => @$customer->profile_picture?: base_url('public/admin/img/placeholder-logo.png'), 'full_name' => @$customer->full_name ?: 'Deleted Account', 'mobile_num' => @$customer->mobile_num?: 'N/A'];
  //  $tip = $temp->tip;
    $gt = $this->findGrandTotal($temp);
    //$finalTotal = (double) $gt  + (double) $tip;

    $res->order_info = (object)['pickup_location_label' => @$temp->pickup_location->label, 'grand_total' => (double) $gt, 'service_icon' => $service->icon,'pickupLatitude'=>@$temp->pickup_location->latitude,'pickupLongitude'=>@$temp->pickup_location->longitude];
    $res->cart_meta = (object)['cart_type_id' => (int) @$temp->cart_type_id, 'service_type' => $temp->service_type];

    return $res;
  }

  public function findGrandTotal($temp)	
  {	
    // var_dump($temp);	
    switch ($temp->service_type) {	
      case 'delivery':	
      // var_dump($temp); die();	
        $gt = (double) @$temp->basket['delivery_fee'] + (double) $temp->tip;	
      break;
      case  'laundry': 
     
        $gt = (double) @$temp->basket['delivery_fee'] + (double) $temp->tip;  
      break;	
      case 'pabili':	
        $gt = @$temp->basket->estimate_total_amount;	
        break;	
      case 'car':	
      $gt = @$temp->basket['price'];	
      break;	
      case 'food':	
      case 'grocery':	
      default:	
        $gt =  (double) @$temp->basket->grand_total + (double) $temp->tip;	
        break;	
    }	
    return $gt;	
  }

  public function find_customer($vehicle_id)
  {
    $this->db->select('customers.id, customers.full_name, customers.email, customers.mobile_num, customers.birthdate, customers.location, customers.profile_picture,cart.delivery_location');
    $this->db->join('customers', 'cart.customer_id = customers.id', 'left');
    $this->db->where('cart.vehicle_id', $vehicle_id);
    $result = $this->db->get('cart')->result();
    $item = array();
    foreach ($result as $key => $value) {
      $customers = [
        'id' => $value->id,
        'full_name' => $value->full_name,
        'email' => $value->email,
        'mobile_num' => $value->mobile_num,
        'birthdate' => $value->birthdate,
        'location' => $value->location,
        'profile_picture' => $value->profile_picture
      ];
      $value->delivery_location = json_decode($value->delivery_location);
      $total_price = 0;
      $item[] = [
        'customer' => $customers,
        'delivery_location' => $value->delivery_location,
        'total_price' => $total_price
      ];
    }
    return $item;
  }

  public function acceptBooking($cart_id, $rider_id)
  {
    $carty = $this->get($cart_id);
    // var_dump($carty->status); die();

    $res = (object)[];
    if ($carty->status == 'pending') {
      $payment_intent = $this->payment_model->createPaymentIntent($cart_id);



      $this->db->where('id', $cart_id);
      $this->db->update('cart', ['rider_id' => $rider_id, '3ds_redirect' => '', 'status' => 'accepted']);

      $res = $this->db->get_where('cart', ['id' => $cart_id])->row();
      if (!$res) {
          return [];
      }
      $res =  $this->formatRes($res);
    }

    return $res;
  }

  public function attemptAttach($cart_id)
  {
    $payment = $this->payment_model->attemptAttach($cart_id);
    # pag may error
    $data = [];
    if (@$payment->errors) {
      if (@$payment->errors[0]->code == 'resource_succeeded_state') { # pag existing na ang payment, ot succeeded na, do nothing
        $payment = $this->payment_model->getPaymentIntent($cart_id);
      }
    }
    // var_dump($payment); die();

    if (@$payment->attributes) {
      $data['payment_status'] = $payment->attributes->status;
      $data['status_label'] = ucwords($payment->attributes->status);
      // var_dump($payment->attributes->next_action->type); die();
      if (@$payment->attributes->next_action->type == 'redirect') {
        $data['3ds_redirect'] = @$payment->attributes->next_action->redirect->url;
      } else {
        $data['3ds_redirect'] = '';
      }
    }

    if ($data) {
      $this->db->where('id', $cart_id);
      $this->db->update('cart', $data);
    }

    $res = $this->db->get_where('cart', ['id' => $cart_id])->row();
    if (!$res) {
        return [];
    }
    return $this->formatRes($res);
  }

  public function completeBooking($data, $cart_id)
  {
    // $cart = $this->db->get_where('cart', ['id' => $cart_id])->row();
    // if ($res->payment_method == 'card') {
    //
    //   if (!$res->payment_method_id) {
    //     return null;
    //   }
    //
    //   $payment = $this->payment_model->attemptAttach($cart_id);
    //
    //   if (@$payment->attributes->status) {
    //     $data['payment_status'] = $payment->attributes->status;
    //   } else {
    //     return false;
    //   }
    //
    // } else if ($res->payment_method == 'COD') {
    //   $data['payment_status'] = 'succeeded';
    // }
    //

    $today = date("Y-m-d H:i:s"); 
    $this->db->where('id', $cart_id);
    $data['status'] = 'completed';
    $data['completed_at'] = $today;
    return $this->db->update('cart', $data);
  }

public function CMScompleteBooking($data, $cart_id)
  {
   
    $today = date("Y-m-d H:i:s"); 
    $this->db->where('id', $cart_id);
    $data['status'] = 'completed';
    $data['completed_at'] = $today;
    
    return $this->db->update('cart', $data);
  }

  public function  addTip($cart_id, $data)
  {
    $this->db->where('id', $cart_id);
    $res = $this->db->update('cart', ['tip' => $data["tip"],
                                      'vehicle_id'=>$data["vehicle_id"],
                                      'munzoneid'=>$data["munzoneid"],
                                      'zoneid'=>$data["zoneid"]
                                     ]);
    return $res;
    // $this->db->where('id', $cart_id);
    // $data['tip'] = $tips;

    // return $this->db->update('cart', $data);
  }



  public function history($fk, $customer_id)
  {
    $this->db->where($fk, $customer_id);
    $this->db->where_in('status', ['completed']);
    $this->db->order_by('completed_at', 'desc');
    $res = $this->db->get('cart')->result();
    if (!$res) {
        return [];
    }
    foreach ($res as &$value) {
      $value = $this->formatRes($value);
      $value = $this->formatHistory($value);
    }
    return $res;
  }

  public function filterHist()
  {

    $columns = $this->getCartDesc();

    $this->db->select("$columns, customers.full_name");
    $this->db->join('customers', 'cart.customer_id = customers.id', 'left');
    $this->db->where('cart.assigned_location', $this->session->userdata('location'));

    if ($this->input->get('cart_id')) {
      $this->db->where('cart.id', $this->input->get('cart_id'));
    }
    if ($this->input->get('cart_type_id')) {
      $this->db->where('cart.cart_type_id', $this->input->get('cart_type_id'));
    }
    if ($this->input->get('from')) {
      $this->db->where('cart.created_at >= "' . $this->input->get('from') . '"');
    }
    if ($this->input->get('to')) {
      $this->db->where('cart.created_at <= "' . $this->input->get('to') . '"');
    }

    if ($this->input->get('full_name')) {
      $this->db->LIKE('LOWER(customers.full_name)', strtolower($this->input->get('full_name')));
    }

   if ($this->input->get('active_only') == 1) {
      $this->db->where('cart.status != "completed"');
      $this->db->where('cart.status != "init"');
      $this->db->where('cart.status != "cancelled"');
    }else if ($this->input->get('cancelled_only') == 1) {
      $this->db->where('cart.status = "cancelled"');
    } else {
      
       $this->db->where('cart.status in ("completed")');
    }

  }

 public function filterHistPartner($customerids)
  {

    $columns = $this->getCartDesc();

    $this->db->select("$columns, customers.full_name");
    $this->db->join('customers', 'cart.customer_id = customers.id', 'left');
    $this->db->where('cart.assigned_location', $this->session->userdata('location'));
    $this->db->where_in('cart.customer_id', $customerids);

    if ($this->input->get('cart_id')) {
      $this->db->where('cart.id', $this->input->get('cart_id'));
    }
    if ($this->input->get('cart_type_id')) {
      $this->db->where('cart.cart_type_id', $this->input->get('cart_type_id'));
    }
    if ($this->input->get('from')) {
      $this->db->where('cart.created_at >= "' . $this->input->get('from') . '"');
    }
    if ($this->input->get('to')) {
      $this->db->where('cart.created_at <= "' . $this->input->get('to') . '"');
    }

    if ($this->input->get('full_name')) {
      $this->db->LIKE('LOWER(customers.full_name)', strtolower($this->input->get('full_name')));
    }

    if (!$this->input->get('active_only')) {
      $this->db->where('cart.status in ("completed","cancelled")');
    } else {
      $this->db->where('cart.status != "completed"');
      $this->db->where('cart.status != "init"');
      $this->db->where('cart.status != "cancelled"');
    }

  }

  public function countCompleteHist()
  {
    $this->filterHist();
    return count($this->db->get('cart')->result());
  }

  public function getTotalPagesHist()
  {
    $this->filterHist();
   
    return ceil(count($this->db->get('cart')->result()) / 30);
  }

public function getTotalPagesHistPartner($customers)
  {
    $this->filterHistPartner($customers);
   
    return ceil(count($this->db->get('cart')->result()) / 30);
  }

  public function getCartDesc()
  {
    $columns = $this->db->query('desc cart')->result();

    $res = "";
    foreach ($columns as $key => $value) {
      $res .= "cart." .$value->Field . ", ";
    }

    $res = rtrim($res, ', ');
    return $res;
  }

  public function getCompleteHistory()
  {
    $this->filterHist();
    $this->paginate();
    $this->db->order_by('updated_at', 'desc');
    $res = $this->db->get('cart')->result();
    // var_dump($res); die();
    if (!$res) {
        return [];
    }
    foreach ($res as &$value) {
      $value = $this->formatRes($value);
      $value = $this->formatHistory($value);
    }
    return $res;
  }

 public function getCompleteHistoryPartner($customerids)
  {
    $this->filterHistPartner($customerids);
    $this->paginate();
    $this->db->order_by('updated_at', 'desc');
    $res = $this->db->get('cart')->result();
    // var_dump($res); die();
    if (!$res) {
        return [];
    }
    foreach ($res as &$value) {
      $value = $this->formatRes($value);
      $value = $this->formatHistory($value);
    }
    return $res;
  }

  public function getCompleteHistoryActive()
  {
    $this->filterHist();
    $this->paginate();
    $this->db->order_by('updated_at', 'desc');
    $res = $this->db->get('cart')->result();
    // var_dump($res); die();
    if (!$res) {
        return [];
    }
    foreach ($res as &$value) {
      $value = $this->formatRes($value);
      $value = $this->formatHistory($value);
    }
    return $res;
  }

  public function paginate()
  {
    $page = $this->input->get('page') ?: 1;
    $per_page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 30; # Make 10 default $per_page if $per_page is not set
    $offset = ($page - 1) * $per_page;
    $this->db->limit($per_page, $offset);
  }

  public function formatHistory($res)
  {
    $new_res = (object)[];
    $new_res->cart_id = (int) $res->id;
    $new_res->cart_type_id = (int) $res->cart_type_id;
    $new_res->service_id = (int) $res->cart_type_id;
    $new_res->service_type = $res->service_type;

    switch ($res->service_type) {
      case 'food':
      case 'grocery':
      case 'pabili':
      case 'car':
     case  'laundry':
      case 'delivery':
      $new_res->location = $res->pickup_location;
        break;
    }


    $new_res->customer = $this->formatForRiderHome($res);
    // $new_res->pickup_location = $res->pickup_location;
    $new_res->created_at = $res->updated_at;
    $new_res->updated_at = date('F j, Y g:i a', strtotime($res->updated_at));
    $new_res->completed_at = date('F j, Y g:i a', strtotime($res->updated_at));
    
    $new_res->total_items =  (double) (@$res->basket->total_items ?: 0);

    $new_res->total_price = (double) $this->findGrandTotal($res);
    $service = $this->services_model->get($res->cart_type_id);
    $new_res->icon = $service->icon;
    return $new_res;
  }

  public function getHistoryByService($fk, $customer_id, $service_id)
  {
    $this->db->where('cart_type_id', $service_id);
    return $this->history($fk, $customer_id);
  }

 public function acceptBookingCMS($cart_id, $rider_id,$isSchedule,$dateTimeShed)
  {
    $carty = $this->get($cart_id);
    // var_dump($carty->status); die();
    $deliveryStatus = 'accepted';

    if($isSchedule == 1){
      $deliveryStatus = "scheduled_delivery";
    }

    $res = (object)[];
    if ($carty->status == 'init') {
      $payment_intent = $this->payment_model->createPaymentIntent($cart_id);

      $this->db->where('id', $cart_id);
      $this->db->update('cart', ['rider_id' => $rider_id, '3ds_redirect' => '', 'status' => $deliveryStatus,'payment_method_id' => null, 'status_label' => null,  'payment_intent_id' => @$payment_intent->id, 'payment_status' => @$payment_intent->attributes->status,'is_scheduled'=>$isSchedule,"scheduled_at" =>$dateTimeShed]);

      $res = $this->db->get_where('cart', ['id' => $cart_id])->row();
      if (!$res) {
          return [];
      }
      $res =  $this->formatRes($res);
    }

    return $res;
  }

 public function updateSchedBooking($cart_id,$status){

     $this->db->where('id', $cart_id);
     $res = $this->db->update('cart', ["status"=>$status]);

    return $res;

  }

  // for update to prod

public function getCompletedOrderToday($partnerId)
  {
    //return $partnerId;die();
    //$curdate = '';
   
   $this->db->select('cart.*');
   $this->db->where_in('cart.status', ['completed']);
   $this->db->where('partners.id',$partnerId);
   $this->db->where('date(cart.completed_at) = CURDATE()');
   $this->db->join('cart_items', 'cart_items.cart_id = cart.id');
   $this->db->join('inventory', 'inventory.id = cart_items.product_id');
   $this->db->join('partners', 'inventory.partner_id = partners.id');
   $this->db->group_by('cart.id');
   $res = $this->db->get('cart')->result();

    if (!$res) {
      return (object)[];
    }

    $new_resformat =  [];

    foreach ($res as $key => &$value) {

    //array_push($new_resformat, ["cart" => $value->id,"partnerId"=>$partnerId]);
      $partnerId =  $this->getPartnerByCartIdForNewOrder($value->id,$partnerId);
       if($partnerId)
       {
         $value = $this->formatRes($value);
       }
    }

    $new_resformat = $res;
    return $new_resformat;
  }

  public function getCompletedOrderYesterday($partnerId)
  {
    //return $partnerId;die();
   
   $this->db->select('cart.*');
   $this->db->where_in('cart.status', ['completed']);
   $this->db->where('partners.id',$partnerId);
   $this->db->where('date(cart.completed_at) = CURDATE() - INTERVAL 1 DAY');
   $this->db->join('cart_items', 'cart_items.cart_id = cart.id');
   $this->db->join('inventory', 'inventory.id = cart_items.product_id');
   $this->db->join('partners', 'inventory.partner_id = partners.id');
   $this->db->group_by('cart.id');
   $res = $this->db->get('cart')->result();

    if (!$res) {
      return (object)[];
    }

    $new_resformat =  [];

    foreach ($res as $key => &$value) {

    //array_push($new_resformat, ["cart" => $value->id,"partnerId"=>$partnerId]);
      $partnerId =  $this->getPartnerByCartIdForNewOrder($value->id,$partnerId);
       if($partnerId)
       {
         $value = $this->formatRes($value);
       }
    }

    $new_resformat = $res;
    return $new_resformat;
  }

  ///

  public function onlinePaymentMethod($data,$whereCol,$id){
    $this->db->where("$whereCol",$id);
    return $this->db->update('cart', $data);
  }


} //end of  class
