<?php

class Inventory_model extends Crud_model
{

  public function __construct()
  {
    parent::__construct();
    $this->table = "inventory";
    $this->upload_dir = 'inventory'; # uploads/your_dir
    $this->uploads_folder = "efs/uploads/" . $this->upload_dir . "/";
    $this->full_up_path = base_url() . "efs/uploads/" . $this->upload_dir . "/";
  }

 public function getInventory($partner_id)
 {
    $category = $this->input->get('category');
    $search = $this->input->get('search');

    $price_min = $this->input->get('price_min');
    $price_max = $this->input->get('price_max');

    $res = (object)[];

    $categories = $category ? [$category]: $this->getCategories($partner_id);


    if (!$categories) {
      return $res;
    }
    // var_dump($categories); die();

    foreach ($categories as $key => $value) {

      if (isset($_GET['category']) && ($category != $value)) {
        continue;
      }

      $this->db->where('partner_id', $partner_id);
      if ($search) {
        $this->db->like('LOWER(product_name)', strtolower($search));
      }
      if ($price_min) {
        $this->db->where("base_price >= $price_min");
      }
      if ($price_max) {
        $this->db->where("base_price <= $price_max");
      }
      if ($category) {
        $this->db->like('LOWER(category)', strtolower($category));
      } else {
        $this->db->like('LOWER(category)', strtolower($value));
      }
      $this->db->where('is_addon', 0);
      $this->db->where('in_stock', 1);
      $this->db->where('is_hidden', 0);
      $inven = $this->db->get('inventory')->result();
      if (!$inven) {
        continue;
      }

      $res->$value = $inven;
    }

    if (!$res) {
        return [];
    }

    foreach ($res as $key => &$value) {
      foreach ($value as &$inner_value) {
        $inner_value = $this->formatRes($inner_value);
        $inner_value->addons = [];
        $temp_addons = $this->getAddons($inner_value->addon_children_ids);
        if ($temp_addons) {
          $inner_value->addons = $this->formatAddonCategory($temp_addons);
        }

      }
      $value = $value;
    }

    return $res;
 }

 public function formatAddonCategory($addons)
 {
   $res = (object)[];

   foreach ($addons as $key => $value) {
     $res->{$value->category}[] = $value;
   }

   return $res;
 }

public function getInventoryByKeyword($location)
 {
    $category = $this->input->get('category');
    $search = $this->input->get('search');

    $decodeKeyword = urldecode($search);
    $explodedKey = explode(" ",$decodeKeyword);

    $res = (object)[];

    if(count($explodedKey) >0 )
    {
     foreach ($explodedKey as $value) {
      $this->db->where('(LOWER(inventory.product_name) LIKE "%' . strtolower($value) . '%")');
     }    
    }
      if ($category) {
        $this->db->like('LOWER(inventory.category)', strtolower($category));
      } 

      $this->db->select('inventory.*');
      $this->db->where('inventory.is_addon', 0);
      $this->db->where('inventory.in_stock', 1);
      $this->db->where('inventory.is_hidden',0);
 
      $this->db->where('partners.service_id',$this->input->get('service_id'));
      $this->db->where('partners.assigned_location', $location);

      $this->db->join('partners', 'partners.id = inventory.partner_id');
      $res = $this->db->get('inventory')->result();

      if (!$res) {
        return [];
      }

    return $res;
 }

 public function get($id)
 {
   $this->db->where('id', $id);
   $res = $this->db->get('inventory')->row();
   if (!$res) {
     return (object)[];
   }

   $res = $this->formatRes($res);
   $temp_addons = $this->getAddons($res->addon_children_ids);
   $res->addons = $this->formatAddonCategory($temp_addons);;

   return $res;
 }

 public function formatRes($res)
 {
   $res->image = ($res->image) ? $this->full_up_path . $res->image : $this->temp_image;
   $res->addon_children_ids = unserialize($res->addon_children_ids) ?: [];
   disallowNull($res);
   return $res;
 }

 public function getAddons($addon_array)
 {
   if (!$addon_array) {
     return (object)[];
   }

   $this->db->where_in('id', $addon_array);
   $this->db->where('in_stock', 1);
   $this->db->where('is_hidden', 0);
   $res = $this->db->get('inventory')->result();
   if (!$res) {
     return (object)[];
   }

   foreach ($res as $key => &$value) {
     $this->formatRes($value);
   }

   return $res;
 }

 public function getCategories($partner_id)
 {
    $res = [];
    $this->db->distinct();
    $this->db->select('category');
    $this->db->where('partner_id', $partner_id);
    $res = $this->db->get('inventory')->result();

    if (!$res) {
        return false;
    }
    $item = array();
    foreach ($res as $key => $value) {
        $item[] = $value->category;
    }
    return $item;
 }

}
