<?php

class Riders_model extends Crud_model
{
  /**
  * Your table name
  * @var string
  */
  protected $table;

  /**
  * The directory you want to upload to whose parent dir is `uploads` folder
  * @var [type]
  */
  protected $upload_dir;

  public function __construct()
  {
    parent::__construct();
    $this->upload_dir = 'riders'; # uploads/your_dir
    $this->uploads_folder = "efs/uploads/" . $this->upload_dir . "/";
    $this->full_up_path = base_url() . "efs/uploads/" . $this->upload_dir . "/";

    $this->load->model('api/customers_model');
    $this->load->model('api/rider_vehicles_model');
    $this->load->model('api/payment_model');
    $this->load->model('api/notifications_model');

    $this->load->library('email');
    $config_mail['protocol'] = getenv('MAIL_PROTOCOL');
    $config_mail['smtp_host'] = getenv('SMTP_HOST');
    $config_mail['smtp_port'] = getenv('SMTP_PORT');
    $config_mail['smtp_user'] = getenv('SMTP_EMAIL');
    $config_mail['smtp_pass'] = getenv('SMTP_PASS');
    $config_mail['mailtype'] = "html";
    $config_mail['charset'] = "utf-8";
    $config_mail['newline'] = "\r\n";
    $config_mail['wordwrap'] = TRUE;

    $this->email->initialize($config_mail);
  }

  public function all()
  {
    $res = $this->db->get('riders')->result();
    $new_res = [];
    if ($res) {
      foreach ($res as $key => $value) {
        $new_res[] = $this->formatRes($value);
      }
    }

    return $new_res;
  }

  public function mobileNumExists($mobile_num)
  {
    $new_mobile_num = $this->customers_model->formatMobileNum($mobile_num, false);
    $this->db->or_where('mobile_num', $mobile_num);
    $this->db->or_where('mobile_num', $new_mobile_num);
    return $this->db->get('riders')->row();
  }

  public function rateRider($rating, $rider_id)
  {
    $this->db->insert('rider_ratings', ['rating' => $rating, 'rider_id' => $rider_id]);
    $last_id = $this->db->insert_id();

    return $this->db->get_where('rider_ratings', ['id' => $last_id])->row();
  }

  public function updateCashOnHand($amount, $rider_id)
  {
    $this->db->where('id', $rider_id);
    $this->db->update('riders', ['cash_on_hand' => $amount]);

    return $this->get($rider_id);
  }

  public function setLatLong($post, $rider_id)
  {
    $this->db->where('id', $rider_id);
    return $this->db->update('riders', ['rider_latitude' => $post['rider_latitude'], 'rider_longitude' => $post['rider_longitude']]);
  }

  public function getRiderAverageRating($rider_id)
  {
    $this->db->select('AVG(rating) as rating_avg');
    $this->db->where('rider_id', $rider_id);
    return @$this->db->get('rider_ratings')->row()->rating_avg;
  }


  public function notifRiderWallets($rider)
  {
    // var_dump(time(), time('-30 minutes', $rider->booking_available_until)); die();
      # Check if we should make booking_available_until into null
      if (strtotime($rider->booking_available_until) < time()) {
        $this->db->where('id', $rider->id);
        $this->db->update('riders', ['booking_available_until' => null]);
      }
      # / Check if we should make booking_available_until into null

      # PUSH NOTIF BLOCK NOTIFICATION
      $halfway = date('Y-m-d H:i:s', (strtotime(date('Y-m-d H:i:s')) + strtotime($rider->booking_available_until)) / 2);
      if ($rider->booking_available_until == null) {
        // var_dump('empty wallet'); die();
        return $this->notifications_model->sendPushNotif(
          $rider->id,
          'riders',
          ['data' => ''],
          ['body' => 'Oh no, it looks like your wallet is empty.', 'title' => 'Top-up now so you don\'t miss new orders.'],
          false,
          true
        );
      } else if (strtotime($halfway) < time() && time() < strtotime("+5 minutes", strtotime($halfway))) {
        // var_dump('halfway'); die();
        return $this->notifications_model->sendPushNotif(
          $rider->id,
          'riders',
          ['data' => ''],
          ['body' => 'You are halfway through your purchased credits', 'title' => 'Top-up now so you don\'t miss new orders.'],
          false,
          true
        );
      } else if (time() > strtotime('-5 minutes', strtotime($rider->booking_available_until)) ) {
        // var_dump('few hours left'); die();
        return $this->notifications_model->sendPushNotif(
          $rider->id,
          'riders',
          ['data' => ''],
          ['body' => 'You\'ve only got a few minutes left!', 'title' => 'Top-up now so you don\'t miss new orders.'],
          false,
          true
        );
      }
      # / PUSH NOTIF BLOCK NOTIFICATION
  }

  public function add($data)
  {
    if ($this->customers_model->isEmailExist($data['email'], 'riders')) {
      return null;
    }
    $vehicle['vehicle_id'] = @$data['vehicle_id'];
    $vehicle['vehicle_model'] = @$data['vehicle_model'];
    $vehicle['plate_number'] = @$data['plate_number'];
    $data['assigned_location'] = 1;

    unset($data['vehicle_id'], $data['vehicle_model'], $data['plate_number']);


    $is_social = @$data['social_token'] && @$data['is_email_verified'];

    if (!$is_social) {
      $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
      $data['verification_token'] = $this->customers_model->generateToken();
    } else {
      unset($data['password']);
    }

    $res = $this->db->insert('riders', $data);
    if ($res && !$is_social) {
      $this->customers_model->sendToken($data['email'], $data['full_name'], $data['verification_token'], 'riders');
    }

    $last_id = $this->db->insert_id();

    $last_vehicle_id = $this->rider_vehicles_model->addVehicle($vehicle, $last_id);
    $this->rider_vehicles_model->makeVehicleActive($last_id,  $last_vehicle_id);

    return $last_id;
  }

  public function add2($data)
  {
    if ($this->customers_model->isEmailExist($data['email'], 'riders')) {
      return null;
    }
    $vehicle['vehicle_id'] = @$data['vehicle_id'];
    $vehicle['vehicle_model'] = @$data['vehicle_model'];
    $vehicle['plate_number'] = @$data['plate_number'];
   
    $data['assigned_location'] = 1;

    unset($data['vehicle_id'], $data['vehicle_model'], $data['plate_number']);


    $is_social = @$data['social_token'] && @$data['is_email_verified'];

    if (!$is_social) {
      $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
      $data['verification_token'] = $this->customers_model->generateToken();
    } else {
      unset($data['password']);
    }

    $data['mobile_otp'] = $this->customers_model->generateOTP();
    $insert_succ = $this->db->insert('riders', $data);

    $last_id = $this->db->insert_id();
   // $res = $this->get($last_id);
   /* if ($insert_succ && !$is_social) {
     // $this->customers_model->sendOTP($res->mobile_otp, $res->mobile_num);
      // $this->customers_model->sendToken($data['email'], $data['full_name'], $data['verification_token'], 'riders');
    }*/

    $last_vehicle_id = $this->rider_vehicles_model->addVehicle($vehicle, $last_id);
    $this->rider_vehicles_model->makeVehicleActive($last_id,$last_vehicle_id);
    
    return $last_id;
  }

  public function get($id)
  {
    $this->db->where('id', $id);
    $this->db->where("banned_at IS NULL");
    $res = $this->db->get('riders')->row();
    $res2 = $this->formatRes($res);
    return $res2;
  }

  public function getWalletTransactionHistory($rider_id)
  {
    $this->db->where('rider_id', $rider_id);
    $res = $this->db->get('rider_wallet')->result();

    foreach ($res as $key => $value) {
      if ($value->status == 'pending') {
        $tr = $this->payment_model->getPaymentSource($value->id);
        $this->db->where('id', $value->id);
        $this->db->update('rider_wallet', ['status' => $tr->attributes->status]);
      }
    }

    $this->db->where('rider_id', $rider_id);
    return $this->db->get('rider_wallet')->result();
  }

  public function topUpWallet($data, $rider_id)
  {
    $this->db->insert('rider_wallet', ['amount' => $data['amount'], 'duration_in_hours' => $data['duration_in_hours'], 'promo_name' => @$data['promo_name'],'assigned_location' => $data["assigned_location"],'rider_id' => $rider_id]);
    $last_id = $this->db->insert_id();

    $res = $this->payment_model->createResourceGcash($data['amount'], $rider_id, $last_id);

    // var_dump($res); die();
    $this->db->where('id', $last_id);
    $this->db->update('rider_wallet', [
      'transaction_id' => $res->id,
      'checkout_url' => $res->attributes->redirect->checkout_url,
      'status' => $res->attributes->status
    ]);
    return $this->db->get_where('rider_wallet', ['id' => $last_id])->row();
  }

  public function topUpWalletforRider($data, $rider_id)
  {
    $this->db->insert('rider_balance', ['wallet_balance' => $data['amount'], 'type' => 'topup','assigned_location' => $data["assigned_location"] ,'rider_id' => $rider_id]);
    $last_id = $this->db->insert_id();

    $res = $this->payment_model->createResourceGcashforRider($data['amount'], $rider_id, $last_id);

    // var_dump($res); die();
    $this->db->where('id', $last_id);
    $this->db->update('rider_balance', [
      'transaction_id' => $res->id,
      'checkout_url' => $res->attributes->redirect->checkout_url,
      'status' => $res->attributes->status
    ]);
    return $this->db->get_where('rider_balance', ['id' => $last_id])->row();
  }

  public function availOfferForRider($data, $rider_id)
  {
    $this->db->insert('rider_wallet', ['amount' => $data['amount'], 'promo_name' => $data['promo_name'], 'duration_in_hours' => $data['duration_in_hours'], 'rider_id' => $rider_id, 'status' => 'paid']);
    $last_id_rider_wallet = $this->db->insert_id();
    # Need nga ata ng FK, for reference purposes lang
    $this->db->insert('rider_balance', ['wallet_balance' => '-'.$data['amount'], 'rider_wallet_fk' => $last_id_rider_wallet, 'type' => 'avail_offer','status' => 'paid','rider_id' => $rider_id]);
    $last_id_rider_balance = $this->db->insert_id();

    # Dito na ata natin kailangan update ung booking_available_until
    if ($last_id_rider_wallet && $last_id_rider_balance) { # ganyan nlng pra if parehas success
      # should be success all the time
      $this->db->where('id', $last_id_rider_wallet); # tas eto gagamitin natin. yung ID ng rider wallet
      $wallet = $this->db->get('rider_wallet')->row();
      $this->updateBookingAvailableUntil($wallet);

      return $wallet;
    }

    # return false if updating the rider allowed bookung until fails
    return false;
  }

  public function deductCommission($amount, $rider_id)
  {
    $this->db->insert('rider_balance', ['wallet_balance' => '-'.$amount, 'rider_wallet_fk' => 0, 'type' => 'deduct_commission','status' => 'paid','rider_id' => $rider_id]);
    return $this->db->insert_id();
  }

  public function walletBalancesum($rider_id){

   $this->db->select_sum('wallet_balance');
   $this->db->where('rider_id', $rider_id);
   $this->db->where('status','paid');
   return @$this->db->get('rider_balance')->row()->wallet_balance;

  }

  public function updateWalletStatus($id)
  {
    $res = $this->payment_model->getPaymentSource($id);

    if ($res->attributes->status == 'chargeable') {
      $res = $this->payment_model->payTheSource($id);
    }

    $this->db->where('id', $id);
    $succ = $this->db->update('rider_wallet', ['status' => $res->attributes->status]);
    if ($succ) {
      # should be success all the time
      $wallet = $this->db->where('id', $id);
      $wallet = $this->db->get('rider_wallet')->row();
      return $this->updateBookingAvailableUntil($wallet);
    }
    # return false if updating the rider allowed bookung until fails
    return false;
  }

  public function updateWalletStatusforRider($id)
  {
    # get the payment source
    $res = $this->payment_model->getPaymentSourceforRider($id);

    # pag chargebale, bayaran
    if ($res->attributes->status == 'chargeable') {
      $res = $this->payment_model->payTheSourceforRider($id);
    }

    # update na bayad na
    $this->db->where('id', $id);
    return $this->db->update('rider_balance', ['status' => $res->attributes->status]);

  }

   public function updateOrderPaymentStatus($id)
   {
    # get the payment source
    $res = $this->payment_model->getPaymentSourceforRider($id);

    # pag chargebale, bayaran
    if ($res->attributes->status == 'chargeable') {
      $res = $this->payment_model->payTheSourceforRider($id);
    }

    # update na bayad na
    $this->db->where('id', $id);
    return $this->db->update('rider_balance', ['status' => $res->attributes->status]);
   }

  public function updateBookingAvailableUntil($wallet)
  {
    $amount = $wallet->amount;
    $duration_in_hours = $wallet->duration_in_hours;

    $this->db->where('id', $wallet->rider_id);
    $rider_booking_available_until = @$this->db->get('riders')->row()->booking_available_until;

    # Check if we should make booking_available_until into null
    if (strtotime($rider_booking_available_until) < time()) {
      $this->db->where('id', $wallet->rider_id);
      $this->db->update('riders', ['booking_available_until' => null]);
    }
    # / Check if we should make booking_available_until into null

    $new_limit = null;

    $new_limit = ($rider_booking_available_until) ?
    date('Y-m-d H:i:s', date(strtotime("+{$duration_in_hours} hour", strtotime($rider_booking_available_until)))) :
    date('Y-m-d H:i:s', date(strtotime("+{$duration_in_hours} hour")));

    $this->db->where('id', $wallet->rider_id);
    return $this->db->update('riders', ['booking_available_until' => $new_limit]);
  }

  public function formatWalletOffers($res)
  {
    if (!$res) {
      return [];
    }

    foreach ($res as $key => &$value) {
      $value->price = (double) $value->price;
      $value->duration_in_hours = (int) $value->duration_in_hours;
      $value->id = (int) $value->id;
      if ($value->duration_label == '') {
        $value->duration_label = $value->duration_in_hours . " hrs";
      }
    }

    return $res;
  }

  function formatRes($res)
  {
      if (!$res) {
        return false;
      }
      $res->vehicles = $this->rider_vehicles_model->getRiderVehicles($res->id);
      $res->unverified_vehicles = $this->rider_vehicles_model->getRiderVehiclesUnverified($res->id);
      $res->all_vehicles = array_merge($res->vehicles, $res->unverified_vehicles);
      $res->active_vehicle = $this->rider_vehicles_model->getActiveVehicle($res->id);
      $res->profile_picture = ($res->profile_picture) ? $this->full_up_path . $res->profile_picture : $this->temp_image;
      $res->identification_document_name = $res->identification_document;
      $res->identification_document = ($res->identification_document) ? $this->full_up_path . $res->identification_document : $this->temp_image;
      return $res;
  }


}
