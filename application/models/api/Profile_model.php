<?php

class Profile_model extends Crud_model
{
  /**
  * Your table name
  * @var string
  */
  protected $table;

  /**
  * The directory you want to upload to whose parent dir is `uploads` folder
  * @var [type]
  */
  protected $upload_dir;

  public function __construct()
  {
    parent::__construct();
    $this->upload_dir = 'customers'; # uploads/your_dir
    $this->uploads_folder = "efs/uploads/" . $this->upload_dir . "/";
    $this->full_up_path = base_url() . "efs/uploads/" . $this->upload_dir . "/";

    $this->load->model('api/customers_model');
    $this->load->model('api/riders_model');
  }

  public function get($id, $type ='customers')
  {
    return ($type == 'customers') ? $this->customers_model->get($id) : $this->riders_model->get($id);
  }

  public function addAddress($data, $customer_id)
  {
    $data['customer_id'] = $customer_id;
    $this->db->insert('customer_addresses', $data);
    $last_id = $this->db->insert_id();


    return $this->db->get_where('customer_addresses', ['id' => $last_id])->row();
  }

  public function updateAddress($data, $address_id)
  {
    $this->db->where('id', $address_id);
    $this->db->update('customer_addresses', $data);

    return $this->db->get_where('customer_addresses', ['id' => $address_id])->row();
  }

  public function updateProfile($id, $data, $type)
  {
    $user = $this->get($id, $type);
    if (!$user)
    return null; # Return null if entry is not existing

    if (@$data['password'] && !$user->social_token) {
      $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
    } else {
      unset($data['password']);
    }

    $this->db->where('id', $id);
    $this->db->update($type, $data);
    return $this->db->affected_rows(); # Returns 1 if update is successful, returns 0 if update is already made, but query is successful
  }
 


}
