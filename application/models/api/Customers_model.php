<?php

require APPPATH .'/libraries/aws/aws-autoloader.php';

use Aws\Sns\SnsClient;
use Aws\Exception\AwsException;

class Customers_model extends Crud_model
{
  /**
  * Your table name
  * @var string
  */
  protected $table;

  /**
  * The directory you want to upload to whose parent dir is `uploads` folder
  * @var [type]
  */
  protected $upload_dir;
  private $gpdb;

  public function __construct()
  {
    parent::__construct();
    $this->upload_dir = 'customers'; # uploads/your_dir
    $this->uploads_folder = "efs/uploads/" . $this->upload_dir . "/";
    $this->full_up_path = base_url() . "efs/uploads/" . $this->upload_dir . "/";

    $this->load->model('api/user_device_ids_model', 'udi_model');
    $this->load->model('api/riders_model');
    //load gp database
    $this->gpdb = $this->load->database('gp', TRUE);

    $this->load->library('email');
    $config_mail['protocol'] = getenv('MAIL_PROTOCOL');
    $config_mail['smtp_host'] = getenv('SMTP_HOST');
    $config_mail['smtp_port'] = getenv('SMTP_PORT');
    $config_mail['smtp_user'] = getenv('SMTP_EMAIL');
    $config_mail['smtp_pass'] = getenv('SMTP_PASS');
    $config_mail['mailtype'] = "html";
    $config_mail['charset'] = "utf-8";
    $config_mail['newline'] = "\r\n";
    $config_mail['wordwrap'] = TRUE;

    $this->email->initialize($config_mail);

    $this->SnSclient = new SnsClient([
        'version' => 'latest',
        'region' => 'ap-southeast-1',
        'credentials' => [
            'key'    => getenv('AWS_ACCESS_KEY_ID'),
            'secret' => getenv('AWS_SECRET_ACCESS_KEY'),
        ]
    ]);

  }

///==========================
    function generateNewreferralCode($isGpUser){
     $generatecode = "";
           do {
              $generatecode = $this->random_strings(8,$isGpUser); 
              $isValid = $this->checkifcodeisexist($generatecode);
            } while ($isValid == true);

          return $generatecode;
   }

   function random_strings($length_of_string,$isGpUser) 
   { 
        $str_result = "ABCDEFGHJKLMNOPQRSTUVWXYZ1234567890abcdefghijkmnopqrstuvwxyz";       

        if($isGpUser == true)
        {
          return "GP".substr(str_shuffle($str_result), 0, $length_of_string); 

        }else if($isGpUser == false)
        {
               return "RGP".substr(str_shuffle($str_result), 0, 7); 
        }
   }

   function checkifcodeisexist($newCode){

      $payload = array();
      $this->gpdb->where('referralcode',$newCode);
      $this->gpdb->select("*");
      $query = $this->gpdb->get("tbl_gppartner_earnings");
      $res   = $query->row_array();
      $dataLayer = array();
      
      if($query->num_rows() > 0){ 
        $result = true;
      }else{
        $result = false;
      }
      return $result;
   }

  function insertnewGpPartner($newcode,$customerid,$GPLayerData,$isGpUser)
  {
     
     $gpNewData = array(
                        "customerid"     => $customerid,
                        "referralcode"   => $newcode,
                        "total_earnings" => "0.00",
                        "previousbalance"=> "0.00",
                        "earnedtoday"    =>"0.00",
                        "date_created"   => date('Y-m-d')
                      );
     $newGpLayer = array(
                         "customerid" => $customerid,
                         "partnercode"=> $newcode,
                         "1stLayer"   => $GPLayerData["partnercode"],
                         "2ndLayer"   => $GPLayerData["1stLayer"],
                         "3rdLayer"   => $GPLayerData["2ndLayer"],
                         "4thLayer"   => $GPLayerData["3rdLayer"],
                         "5thLayer"   => $GPLayerData["4thLayer"]
                        );

     try{
       $this->gpdb->trans_start();
       $this->gpdb->insert('tbl_gppartner_earnings', $gpNewData);
       $this->gpdb->insert('tbl_gplayer', $newGpLayer);
       $this->gpdb->trans_complete();

       if ($this->gpdb->trans_status() === FALSE) {
            $result = FALSE;
       }else {
            $result = TRUE;
       }

     }catch(Exception $e){
        $result = FALSE;
     }

     return $result; 
  }

   function validatepartnercode($partnercode)
   {
         $payload = array();
         $dataLayer = array();
         
         $query = $this->gpdb->get_where("tbl_gppartner_earnings", array("referralcode" => $partnercode));
         $res   = $query->row_array();

         if($query->num_rows() == 1){ 
            $dataLayer = $this->getLayer($partnercode);
            $result =  $dataLayer;
            if($dataLayer["status"] == "SUCCESS")
            {
              $result = array("status"=>"SUCCESS","dataLayer" => $dataLayer["payload"]);
            }
           
         }else{
           $result = array("status"=>"ERROR1","Message" => $query);
         }
         
        return $result;
    }


   function getLayer($partnercode)
   {
       $payload = array();
       $query = $this->gpdb->get_where("tbl_gplayer", array("partnercode" => $partnercode));
       $res   = $query->row_array();

       if($query->num_rows() == 1){ 
             $result = array("status"=>"SUCCESS","payload" => $res);
        }else{
            $result = array("status"=>"ERROR","Message" => "Nodata Exist");
        }
       
        return $result;
   }

//========================== end GP transactions=======


  public function selectedcustomers($usertype,$id,$al)
  {
    $res = (object)[];
     $customerids = $this->bindcustomers($usertype,$id,$al);

    
    if($customerids){
      $this->db->where_in('id',$customerids);
      $res = (object) $this->db->get("customers")->result();
    }
    
    return $res;
  }

  public function bindcustomers($usertype,$id,$al)
  {
    $res = [];
    $customersids=[];
    if($usertype == 'administrator')
    {
      $this->db->where('usertype',$usertype);
      $this->db->where('assigned_location',$al);
      $res = $this->db->get("tbl_customerid_be_booking")->result();

    }else
    {
      $this->db->where('usertype',$usertype);
      $this->db->where('partner_id',$id);
      $this->db->limit(100,1);
      $res = $this->db->get("tbl_customerid_be_booking")->result();

    }

    if($res)
    {
      foreach($res as $row=>$val)
      {
        array_push($customersids,$val->customer_id);
      }
    }
    
    return $customersids;
  }

  public function mobileNumExists($mobile_num)
  {
    $new_mobile_num = $this->formatMobileNum($mobile_num);
    $this->db->or_where('mobile_num', $mobile_num);
    $this->db->or_where('mobile_num', $new_mobile_num);
    return $this->db->get('customers')->row();
  }

  public function formatMobileNum($mobile_num)
  {
    //$mobile_num = str_replace("-", "", $mobile_num);
    $starting_digits = substr($mobile_num,0, 2);

    if ($starting_digits == '09') {
      $mobile_num = "+639" . substr($mobile_num, 2);
    }
    return $mobile_num;
  }

  public function validate_email($email) 
  {
    return (preg_match("/(@.*@)|(\.\.)|(@\.)|(\.@)|(^\.)/", $email) || !preg_match("/^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/", $email)) ? false : true;
 }

  public function sendOTP($mobile_otp, $mobile_num)
  {

    $mobile_num = $this->formatMobileNum($mobile_num);
    // var_dump($mobile_num); die();
    $message = "Your ONE-TIME-PIN for Get Express is $mobile_otp. Please don't share this pin with anyone.";
    $phone = $mobile_num;

    try {
        $result = $this->SnSclient->publish([
            'Message' => $message,
            'PhoneNumber' => $phone,
        ]);
        return true;
    } catch (AwsException $e) {
        // output error message if fails
        // error_log($e->getMessage());
        log_message('error', $e->getMessage(), true);
        return false;
    }
  }

  public function all()
  {
    
    $res = $this->db->get('customers')->result();
    $new_res = [];
    if ($res) {
      foreach ($res as $key => $value) {
        $new_res[] = $this->formatRes($value);
      }
    }

    return $new_res;
  }

  public function add($data)
  {
    if ($this->isEmailExist($data['email'])) {
      return null;
    }

    $is_social = @$data['social_token'] && @$data['is_email_verified'];

    if (!$is_social) {
      $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
      $data['verification_token'] = $this->generateToken();
    } else {
      unset($data['password']);
    }

    $data['is_ftu'] = 'Y';

    $res = $this->db->insert('customers', $data);
    if ($res && !$is_social) {
      $this->sendToken($data['email'], $data['full_name'], $data['verification_token']);
    }

    return $this->db->insert_id();
  }

  public function add2($data)
  {
   /*  if ($this->isEmailExist($data['email'])) {
      return null;
    }*/

   // var_dump($data);
   // die();

    $is_social = @$data['social_token'] && @$data['is_email_verified'];

    if (!$is_social) {
      $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
      $data['verification_token'] = $this->generateToken();
    } else {
      unset($data['password']);
    }

    $data['mobile_otp'] = $this->generateOTP();
    $data['is_ftu'] = 'Y';

    $insert_succ = $this->db->insert('customers', $data);
    $insert_id = $this->db->insert_id();

    $res = $this->get($insert_id);
   // var_dump($is_social);
   // die();

    // if ($insert_succ && !$is_social) {
    //   $this->sendOTP($res->mobile_otp, $res->mobile_num);
    //   $this->sendToken($data['email'], $data['full_name'], $data['verification_token']);
    // }
    return $insert_id;
  }

  public function resendOTP($email, $table)
  {
    $this->db->where('email', $email);
    $user = $this->db->get($table)->row();

    if (!$user->mobile_otp) {
      return false;
    }
    return $this->sendOTP($user->mobile_otp, $user->mobile_num);
  }

  public function socialAdd($data, $table = 'customers')
  {
    $res = $this->db->insert($table, ['social_token' => $data['social_token'], 'is_email_verified' => 1, 'full_name' => $data['full_name'], 'birthdate' => $data['birthdate'], 'mobile_num' => $data['mobile_num'], 'email' => $data['email']]);
    $last_id = $this->db->insert_id();

    if ($table == 'customers') {
        return $this->get($last_id);
    } else {
        return $this->riders_model->get($last_id);
    }
  }

  public function generateToken($length = 32) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }

  public function generateOTP($length = 4) {
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }

  public function get($id)
  {
    $this->db->where('id', $id);
    $res = $this->db->get('customers')->row();
    if (!$res) {
        return false;
    }
    $res = $this->formatRes($res);
    return $res;
  }

  function formatRes($res)
  {
      $res->profile_picture = ($res->profile_picture) ? $this->full_up_path . $res->profile_picture : $this->temp_image;
      $addresses = $this->db->get_where('customer_addresses', ['customer_id' => $res->id])->result();
      if ($addresses) {
        foreach ($addresses as &$value) {
          $value = disallowNull($value);
        }
      }

      $res->addresses = $addresses;
      return $res;
  }

  function isEmailExist($email, $table = 'customers')
  {
    $this->db->where('email', $email);
    return $this->db->count_all_results($table);
  }

  function sendToken($email, $full_name, $verification_token, $table = 'customers')
  {
    $html = $this->generateTokenEmail($verification_token, $full_name, $table);
    return $this->sendMail($email, ucwords($table) . ' Account Activation [Get Express]', $html);
  }
  
public function esabongMonitoring($customerid){

    $this->db->where('id', $customerid);
    return $this->db->update('customers', ['is_visited_sabong' => 1]);
  }

  public function verifyOTP($data, $table = 'customers')
  {
    $this->db->where('mobile_otp', $data['mobile_otp']);
    $this->db->where('email', $data['email']);
    $user = $this->db->get($table)->row();

    if (!$user) {
      return false;
    }

    $this->db->where('email', $data['email']);
    $this->db->update($table, ['mobile_otp' => null]);

    if ($table == 'customers') {
      $user = $this->formatRes($user);
    } else if ($table == 'riders') {
      $user = $this->riders_model->formatRes($user);
    }

    return $user;
  }

  function signIn($post, $table = 'customers')
  {
      $user = $this->getByEmail($post['email'], $table);
      if (!$user) {
          return false;
      }

      // if ($user->mobile_otp) {
      //     return 'otp_challenge';
      // }

      if (!$user->is_email_verified) {
          return 'email_not_verified';
      }

      if ($user->banned_at) {
          return 'account_is_banned';
      }

      if ($table == 'riders' && !@$user->is_admin_verified) {
          return 'email_not_verified';
      }

      if (!$user->password) {
        return 'account_exists_in_a_different_signin_method';
      }

      if (password_verify($post['password'], $user->password)) {
          $this->udi_model->loginDevice($user->id, $post['device_id'], $post['firebase_id'], $table);
          return $user;
      }

      return false; # if all else fails, return false
  }

  public function getBySocialToken($social_token, $table = 'customers')
  {
      $res = $this->db->get_where($table, ['social_token' => $social_token])->row();
      if (!$res) {
        return false;
      }

      switch ($table) {
        case 'customers':
           $res = $this->formatRes($res);
          break;
        case 'riders':
           $res = $this->riders_model->formatRes($res);
          break;

        case 'partners':
        default:
          // code...
          break;
      }
      return $res;
  }

  function socialSignIn($post, $table = 'customers')
  {
      $user = $this->getBySocialToken($post['social_token'], $table); # if wala pang email

      // if (!$user) {
      //     $user = $this->socialAdd($post); #create new social
      //     $this->udi_model->loginDevice($user->id, $post['device_id'], $post['firebase_id'], $table);
      //     return $user;
      // }

      if(!$user) {
        return 'safe_to_sign_up';
      }

      #if the user is normal login type and has a password
      if (@$user->password) {
          return null;
      } else if ($user->banned_at) {
          return 'account_is_banned';
      } else if ($table == 'riders' && !@$user->is_admin_verified) {
          return false; # rider unverified
      } else if ($user) {
        if ($user->social_token == $post['social_token']) { # match social token, log her in
            $this->udi_model->loginDevice($user->id, $post['device_id'], $post['firebase_id'], $table);
            return $user;
        } else {
            return 'social_token_mismatch';
        }

      } else {
        return false; # if all else fails, return false
      }

  }

  function getByEmail($emailOrNum, $table = 'customers')
  {
     $isEmailValid =  $this->validate_email($emailOrNum);

      if($isEmailValid == true)
      {
        $res = $this->db->get_where($table, ['email' => $emailOrNum ])->row();
      }else{
        $res = $this->db->get_where($table, ['mobile_num' => $emailOrNum ])->row();
      } 
     
      if (!$res) {
        return false;
      }

      switch ($table) {
        case 'customers':
           $res = $this->formatRes($res);
          break;
        case 'riders':
           $res = $this->riders_model->formatRes($res);
          break;

        case 'partners':
        default:
          // code...
          break;
      }
      return $res;
  }

  public function deactivateAccount($userid){
    $this->db->where('id',$userid);
    $isdeleteted = $this->db->delete('customers');

    $this->db->where(['user_id'=>$userid,'type'=>'customers']);
    $this->db->delete('user_device_ids');

    if($isdeleteted ){
      return true;
    }else{
      return false;
    }

  }

  public function sendMail($to, $subject, $message)
  {
    $this->email->from('noreply@getapp.betaprojex.com', 'Get Express');
    $this->email->to($to);
    $this->email->subject($subject);
    $this->email->message($message);
    return $this->email->send();
  }

   public function generateTokenEmail($verification_token, $name = 'User', $type = 'customers')
  {

    $link = base_url('account_activation/') . urlencode(base64_encode($verification_token)) . "/" . urlencode(base64_encode($type));

    $html = '<table class="container" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
    <tbody>
    <tr bgcolor="7087A3"><td height="15"></td></tr>

    <tr bgcolor="7087A3">
        <td align="center">
            <table class="container-middle" align="center" border="0" cellpadding="0" cellspacing="0" width="560">
                <tbody><tr>
                    <td>
                        <table class="top-header-left" align="left" border="0" cellpadding="0" cellspacing="0">
                            <tbody><tr>
                                <td align="center">
                                    <table class="date" border="0" cellpadding="0" cellspacing="0">
                                        <tbody><tr>
                                            <td style="color: #fff; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">
                                            Activate your account
                                            </td>
                                            <td>&nbsp;&nbsp;</td>
                                            <td style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">
                                                <singleline>
                                                </singleline>
                                            </td>
                                        </tr>

                                        </tbody></table>
                                </td>
                            </tr>
                            </tbody></table>

                        <table class="top-header-right" align="left" border="0" cellpadding="0" cellspacing="0">
                            <tbody><tr><td height="20" width="30"></td></tr>
                            </tbody></table>

                        <table class="top-header-right" align="right" border="0" cellpadding="0" cellspacing="0">
                            <tbody><tr>
                                <td align="center">
                                    <table class="tel" align="center" border="0" cellpadding="0" cellspacing="0">
                                        <tbody><tr>
                                            <td style="color: #fff; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">
                                            Get Express
                                            </td>
                                            <td>&nbsp;&nbsp;</td>
                                            <td style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">
                                                <singleline>
                                                </singleline>
                                            </td>
                                        </tr>
                                        </tbody></table>
                                </td>
                            </tr>
                            </tbody></table>
                    </td>
                </tr>
                </tbody></table>
        </td>
    </tr>

    <tr bgcolor="7087A3"><td height="10"></td></tr>

    </tbody>
</table>

<!--  end top header  -->


<!-- main content -->
<table class="container" align="center"  border="0" cellpadding="0" cellspacing="0" width="600" bgcolor="ffffff">


<!--Header-->
<tbody>

<!--section 1 -->
<tr>
    <td>
        <table class="container-middle" align="center" border="0" cellpadding="0" cellspacing="0" width="560" bgcolor="F1F2F7">
            <tr >
                <td>
                    <table class="mainContent" align="center" border="0" cellpadding="0" cellspacing="0" width="528">
                        <tbody><tr><td height="20"></td></tr>
                        <tr>
                            <td>


                                <table class="section-item" align="left" border="0" cellpadding="0" cellspacing="0" width="360">
                                    <tbody><tr>
                                        <td style="color: #484848; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">

                                            Account Activation

                                        </td>
                                    </tr>
                                    <tr><td height="15"></td></tr>
                                    <tr>
                                        <td style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">

                                            Howdy '.$name.'! Activate your account by clicking on the button below.

                                        </td>
                                    </tr>
                                    <tr><td height="15"></td></tr>
                                    <tr>
                                        <td>
                                            <a href="'.$link.'" style="background-color: #7087A3; font-size: 12px; padding: 10px 15px; color: #fff; text-decoration: none">Activate Account</a>
                                        </td>
                                    </tr>
                                    </tbody></table>
                            </td>
                        </tr>

                        <tr><td height="20"></td></tr>

                        </tbody></table>
                </td>
            </tr>



            </table>
    </td>
</tr>
<!-- end section 1-->

<!-- footer -->
<table class="container" border="0" cellpadding="0" cellspacing="0" width="600">
    <tbody>
    <tr bgcolor="7087A3"><td height="15"></td></tr>
    <tr bgcolor="7087A3">
        <td  style="color: #fff; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;" align="center">

            Get Express © Copyright 2020 . All Rights Reserved

        </td>
    </tr>

    <tr>
        <td bgcolor="7087A3" height="14"></td>
    </tr>
    </tbody></table>';

    return $html;
  }

  public function forgotPassword($email, $type = 'customers')
  {
     $user = $this->getByEmail($email, $type);
      if (!$user) {
        return false;
      }
      # just hash the email and make a sentence
      $message = $this->generatePasswordResetMessage($email, $user->full_name, $type);

      return $this->sendMail($email, "Reset your password [Get Express]", $message);
  }

  public function generatePasswordResetMessage($email, $name = 'User', $type = 'customers')
  {
    $link = base_url('reset-password/') . urlencode(base64_encode($email)) . "/" . urlencode(base64_encode($type));
    // return "Hi $fname, to reset your password please click this <a href='$link'>link</a>";

    $html = '<table class="container" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
    <tbody>
    <tr bgcolor="7087A3"><td height="15"></td></tr>

    <tr bgcolor="7087A3">
        <td align="center">
            <table class="container-middle" align="center" border="0" cellpadding="0" cellspacing="0" width="560">
                <tbody><tr>
                    <td>
                        <table class="top-header-left" align="left" border="0" cellpadding="0" cellspacing="0">
                            <tbody><tr>
                                <td align="center">
                                    <table class="date" border="0" cellpadding="0" cellspacing="0">
                                        <tbody><tr>
                                            <td style="color: #fff; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">
                                            Reset your password
                                            </td>
                                            <td>&nbsp;&nbsp;</td>
                                            <td style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">
                                                <singleline>
                                                </singleline>
                                            </td>
                                        </tr>

                                        </tbody></table>
                                </td>
                            </tr>
                            </tbody></table>

                        <table class="top-header-right" align="left" border="0" cellpadding="0" cellspacing="0">
                            <tbody><tr><td height="20" width="30"></td></tr>
                            </tbody></table>

                        <table class="top-header-right" align="right" border="0" cellpadding="0" cellspacing="0">
                            <tbody><tr>
                                <td align="center">
                                    <table class="tel" align="center" border="0" cellpadding="0" cellspacing="0">
                                        <tbody><tr>
                                            <td style="color: #fff; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">
                                            Get Express
                                            </td>
                                            <td>&nbsp;&nbsp;</td>
                                            <td style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">
                                                <singleline>
                                                </singleline>
                                            </td>
                                        </tr>
                                        </tbody></table>
                                </td>
                            </tr>
                            </tbody></table>
                    </td>
                </tr>
                </tbody></table>
        </td>
    </tr>

    <tr bgcolor="7087A3"><td height="10"></td></tr>

    </tbody>
</table>

<!--  end top header  -->


<!-- main content -->
<table class="container" align="center"  border="0" cellpadding="0" cellspacing="0" width="600" bgcolor="ffffff">


<!--Header-->
<tbody>

<!--section 1 -->
<tr>
    <td>
        <table class="container-middle" align="center" border="0" cellpadding="0" cellspacing="0" width="560" bgcolor="F1F2F7">
            <tr >
                <td>
                    <table class="mainContent" align="center" border="0" cellpadding="0" cellspacing="0" width="528">
                        <tbody><tr><td height="20"></td></tr>
                        <tr>
                            <td>


                                <table class="section-item" align="left" border="0" cellpadding="0" cellspacing="0" width="360">
                                    <tbody><tr>
                                        <td style="color: #484848; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">

                                            Password reset

                                        </td>
                                    </tr>
                                    <tr><td height="15"></td></tr>
                                    <tr>
                                        <td style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">

                                            Howdy '.$name.'! To reset your password please click on the link below. Otherwise, if you did not do this action, simply ignore this email.

                                        </td>
                                    </tr>
                                    <tr><td height="15"></td></tr>
                                    <tr>
                                        <td>
                                            <a href="'.$link.'" style="background-color: #7087A3; font-size: 12px; padding: 10px 15px; color: #fff; text-decoration: none">Reset password</a>
                                        </td>
                                    </tr>
                                    </tbody></table>
                            </td>
                        </tr>

                        <tr><td height="20"></td></tr>

                        </tbody></table>
                </td>
            </tr>



            </table>
    </td>
</tr>
<!-- end section 1-->

<!-- footer -->
<table class="container" border="0" cellpadding="0" cellspacing="0" width="600">
    <tbody>
    <tr bgcolor="7087A3"><td height="15"></td></tr>
    <tr bgcolor="7087A3">
        <td  style="color: #fff; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;" align="center">

            Get Express © Copyright 2020 . All Rights Reserved

        </td>
    </tr>

    <tr>
        <td bgcolor="7087A3" height="14"></td>
    </tr>
    </tbody></table>';

    return $html;
  }

}
