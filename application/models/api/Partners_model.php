
<?php

class Partners_model extends Crud_model
{
  function __construct()
  {
    parent::__construct();

    $this->table = 'partners'; # Replace these properties on children
    $this->upload_dir = 'partners'; # uploads/your_dir
    $this->uploads_folder = "efs/uploads/" . $this->upload_dir . "/";
    $this->full_up_path = base_url() . "efs/uploads/" . $this->upload_dir . "/"; # override this block on your child class. just redeclare it
    $this->per_page = 15;

    $this->load->model('api/payment_model');
    $this->load->model('api/cart_model');
    $this->load->model('api/user_device_ids_model', 'udi_model');
  }



  function signIn($post, $table = 'partners')
  {
      $user = $this->getByEmail($post['email'], $table);
      if (!$user) {
          return false;
      }
      
     /* if ($user->banned_at) {
          return 'account_is_banned';
      }

      
      if (!$user->password) {
        return 'account_exists_in_a_different_signin_method';
      }*/

      if (password_verify($post['password'], $user->password)) {
          $this->udi_model->loginDevice($user->id, $post['device_id'], $post['firebase_id'], $table);
          return $user;
      }

      return false; # if all else fails, return false
  }

   


    function getByEmail($email, $table = 'partners')
  {
      $res = $this->db->get_where($table, ['email' => $email])->row();
      if (!$res) {
        return false;
      }

      return $res;
  }

 


public function getByService($service_id,$assignedLocation)
 //public function getByService($service_id)
 {
    $keyword = $this->input->get('keyword');
   
    if(!empty($keyword))
    {
       $decodeKeyword = urldecode($keyword);
        $explodedKey = explode(" ",$decodeKeyword);
        if(count($explodedKey) > 1 )
      {
        foreach ($explodedKey as $value) {
         $this->db->where('(LOWER(name) LIKE "%' . strtolower($value) . '%")');
        }
        
      }else if(count($explodedKey) == 1){
        $this->db->where('(LOWER(name) LIKE "%' . strtolower($keyword) . '%")');
      }

    }

    

 	$this->db->where('service_id', $service_id);
  $this->db->where('assigned_location', $assignedLocation);
 	$res = $this->db->get('partners')->result();




  if (!$res) {
    return $res;//[];
  };

  $new_res = [];
	foreach ($res as $key => $value){

    $value->diff_in_kilometers = "";
    $value->diff_in_kilometers_from_map_center = "";

    $value->is_visible = "";
    $value->is_visible_in_map_center = "";

    $value->map_center_coords = explode(',', $this->admin_model->getMapCenter($assignedLocation));
    $value->radius_filter_in_km = (double)$this->admin_model->getStoreRadius($assignedLocation);

    ###########################################################################
    ##########this block is copy pasted to another part of this code###########
    ###########################################################################
    if ($this->input->get('cart_id')) {
      $cart = $this->cart_model->get($this->input->get('cart_id'));

      $cart_coords = $cart->delivery_location;
      $store_coords = json_decode($value->coordinates);
      // var_dump($cart_coords); die();

      $cart_lat = $cart_coords->latitude;
      $cart_long = $cart_coords->longitude;

      $store_lat = $store_coords->latitude;
      $store_long = $store_coords->longitude;

      $diff_in_kilometers = $this->getDistance((float)$cart_lat, (float)$cart_long, (float)$store_lat, (float)$store_long);
      $diff_in_kilometers_from_map_center = $this->getCoordsDiff((float)$store_lat, (float)$store_long, (float)$value->map_center_coords[0], $value->map_center_coords[1], "K");

      $value->diff_in_kilometers = $diff_in_kilometers;
      $value->diff_in_kilometers_from_map_center = $diff_in_kilometers_from_map_center;

      $value->is_visible = $diff_in_kilometers <= $value->radius_filter_in_km;
      $value->is_visible_in_map_center = $diff_in_kilometers_from_map_center <= $value->radius_filter_in_km;

      $this->db->where('partner_id', $value->id);
      $count = $this->db->count_all_results('inventory');
      if (!$count && !$value->service_id == 7) {
        continue;
      }

      if ($this->input->get('filter_out_of_range') == 1) {
        if ($value->is_visible_in_map_center && $value->is_visible) {
          $new_res[] = $this->formatRes($value);
        }
      } else {
         $new_res[] = $this->formatRes($value);
      }
    }
    ###########################################################################
    ##########this block is copy pasted to another part of this code###########
    ###########################################################################

	}

  if ($this->input->get('fetch_all_partners') == 1) {
    $newer_res = [];
    foreach ($res as $key => $value) {

      $this->db->where('partner_id', $value->id);
      $count = $this->db->count_all_results('inventory');
      // var_dump($count);
      if (!$count) {
        continue;
      }

      $newer_res[] = $this->formatRes($value);
    }
    $new_res = $newer_res;
  }


  usort($new_res,function($first,$second){
    return strtolower($first->diff_in_kilometers) > strtolower($second->diff_in_kilometers);
  });

  return $new_res;
 }


 function distance($latitude1, $longitude1, $latitude2, $longitude2) { 
        $pi80 = M_PI / 180; 
        $lat1 *= $pi80; 
        $lon1 *= $pi80; 
        $lat2 *= $pi80; 
        $lon2 *= $pi80; 
        $r = 6372.797; // radius of Earth in km 6371
        $dlat = $lat2 - $lat1; 
        $dlon = $lon2 - $lon1; 
        $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlon / 2) * sin($dlon / 2); 
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a)); 
        $km = $r * $c; 
        return round($km); 
    }


public function getDistance($fromLat,$fromLong,$toLat,$tolong){

    // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
    $ch = curl_init();
       
    $route = "https://api.mapbox.com/directions/v5/mapbox/driving/$fromLong,$fromLat;$tolong,$toLat?access_token=pk.eyJ1IjoiZ2V0ZXhwcmVzc2NvcnAiLCJhIjoiY2t3ajdyNmQzMWZtcjJ2cDgyNXN3cDI3OSJ9.B04GpqcePSb4PFyBvzy8cg";

    curl_setopt($ch, CURLOPT_URL,$route);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);

    $res = json_decode($result);
    $distance =  (INT)$res->routes[0]->distance/1000;

    return $distance;
}
 public function getCoordsDiff($lat1, $lon1, $lat2, $lon2, $unit) {
  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
      return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
  } else {
      return $miles;
  }
}

 public function get($id)
 {
   $this->db->where('id', $id);
   $res = $this->db->get('partners')->row();
   $res = $this->formatRes($res);
   return $res;
 }

 public function formatRes($res)
 {
   if (!$res) {
     return (object)[];
   }
   $res->coordinates = json_decode($res->coordinates);
   $res->image = ($res->image) ? $this->full_up_path . $res->image : $this->temp_image;
   $res->banner = ($res->banner) ? $this->full_up_path . $res->banner : base_url('public/admin/img/placeholder-foodgroc-cover.png');

   $res->store_availability = $this->getStoreAvailability($res->from_day, $res->to_day);
   $res->store_schedule = $this->getStoreSchedule($res->from_hour , $res->to_hour);

   
   if ($res->from_day < $res->to_day) {
       $storeAvail = (date('N') >= $res->from_day && date('N') <= $res->to_day);
   }else{
     $storeAvail = (date('N') >= $res->from_day && date('N') >= $res->to_day);
   }
    
   $res->store_availability_meta = $res->store_availability == 'Always Open' || $storeAvail;

   if (strtotime($res->from_hour) < strtotime($res->to_hour)) {
    $store_sched = strtotime(date('H:i:s')) >= strtotime($res->from_hour) && strtotime(date('H:i:s')) <= strtotime($res->to_hour);

   } else {
      $store_sched = !(strtotime(date('H:i:s')) >= strtotime($res->to_hour) && strtotime(date('H:i:s')) >= strtotime($res->from_hour));
   }

   $res->store_schedule_meta = $res->store_schedule == 'All Day' || $store_sched;

   if($res->isopen != 'Y')
   {
    $res->store_schedule_meta = "";
    $res->store_availability_meta = "";
   }

   // var_dump(date('Y-m-d H:i:s'),  $res->store_availability, $res->store_schedule, $res->store_availability_meta, $res->store_schedule_meta); die();
   return $res;
 }

 public function getStoreAvailability($t1, $t2)
 {
   $s = '';
   if ($t1 == $t2) {
     return 'Always Open';
   }

   return date('l', strtotime("Sunday +{$t1} days")) . "s to " . date('l', strtotime("Sunday +{$t2} days")) . "s";
 }

 public function getStoreSchedule($t1, $t2)
 {
   $s = '';
   if ($t1 == $t2) {
     return 'All Day';
   }

   $t1 = date("g:iA", strtotime($t1));
   $t2 = date("g:iA", strtotime($t2));

   return "$t1 - $t2";
 }

 public function searchProduct($keyword,$assignedLocation){

  $decodeKeyword = urldecode($keyword);
  $explodedKey = explode(" ",$decodeKeyword);

  if(count($explodedKey) > 1 )
  {
    foreach ($explodedKey as $value) {
     $this->db->where('(LOWER(inventory.product_name) LIKE "%' . strtolower($value) . '%")');
    }

  }else{
    $this->db->where('(LOWER(inventory.product_name) LIKE "%' . strtolower($keyword) . '%")');
  }

  if($this->input->get('partner_id') !="0")
  {
    $this->db->where('partners.id', $this->input->get('partner_id'));
  }

  $this->db->select('inventory.product_name');
  $this->db->where('partners.service_id', $this->input->get('service_id'));
  $this->db->where('partners.assigned_location', $assignedLocation);
  $this->db->where('inventory.is_hidden',0);
  $this->db->join('inventory', 'partners.id = inventory.partner_id');

  $res = $this->db->get('partners')->result();

  if (!$res) {
      return [];
  }

  return $res;

 }

 public function updateStoreAvailability($id,$avilability)
 {
  $this->db->where('id',$id);
  return $this->db->update('partners',["isopen" => $avilability ]);
 }

 public function search($keyword,$assignedLocation)
 {
  
  $decodeKeyword = urldecode($keyword);
  $explodedKey = explode(" ",$decodeKeyword);



  if(count($explodedKey) > 1 )
  {
    foreach ($explodedKey as $value) {
     $this->db->where('(LOWER(inventory.product_name) LIKE "%' . strtolower($value) . '%" OR LOWER(inventory.category) LIKE "%' . strtolower($value) . '%")');
    }
    
  }else{
    $this->db->where('(LOWER(inventory.product_name) LIKE "%' . strtolower($keyword) . '%" OR LOWER(inventory.category) LIKE "%' . strtolower($keyword) . '%")');
  }

  if ($this->input->get('service_id')) {
    $this->db->where('service_id', $this->input->get('service_id'));

  }

  $this->db->select('partners.*');
  $this->db->where('partners.service_id', $this->input->get('service_id'));
  $this->db->where('partners.assigned_location', $assignedLocation);
  $this->db->group_by('partners.id'); 
  $this->db->join('inventory', 'partners.id = inventory.partner_id');
  $res = $this->db->get('partners')->result();
  //var_dump($res); die();
 	if (!$res) {
      return [];
  }

  $new_res = [];

  $newcount = 0;

  foreach ($res as $key => &$value) {
    $value->diff_in_kilometers = "";
    $value->diff_in_kilometers_from_map_center = "";

    $value->is_visible = "";
    $value->is_visible_in_map_center = "";

    $value->map_center_coords = explode(',', $this->admin_model->getMeta('map_center',$assignedLocation));
    $value->radius_filter_in_km = (double)$this->admin_model->getMeta('stores_radius',$assignedLocation);

        // $value = $this->formatRes($value);
    ###########################################################################
    ##########this block is copy pasted to another part of this code###########
    ###########################################################################
    if ($this->input->get('cart_id')) {
      $cart = $this->cart_model->get($this->input->get('cart_id'));

      $cart_coords = $cart->delivery_location;
      $store_coords = json_decode($value->coordinates);
      // var_dump($cart_coords); die();


      $cart_lat = $cart_coords->latitude;
      $cart_long = $cart_coords->longitude;

      $store_lat = $store_coords->latitude;
      $store_long = $store_coords->longitude;


      $diff_in_kilometers = $this->getCoordsDiff((float)$cart_lat, (float)$cart_long, (float)$store_lat, $store_long, "K");


      $diff_in_kilometers_from_map_center = $this->getCoordsDiff((float)$store_lat, (float)$store_long, (float)$value->map_center_coords[0], $value->map_center_coords[1], "K");

      $value->diff_in_kilometers = $diff_in_kilometers;
      $value->diff_in_kilometers_from_map_center = $diff_in_kilometers_from_map_center;

      $value->is_visible = $diff_in_kilometers <= $value->radius_filter_in_km;
      $value->is_visible_in_map_center = $diff_in_kilometers_from_map_center <= $value->radius_filter_in_km;

      $this->db->where('partner_id', $value->id);
      $count = $this->db->count_all_results('inventory');
      if (!$count) {
        continue;
      }

      if ($this->input->get('filter_out_of_range') == 1) {
        if ($value->is_visible_in_map_center && $value->is_visible) {
          $new_res[] = $this->formatRes($value);
        }
      } else {
        $new_res[] = $this->formatRes($value);
      }
    }
    ###########################################################################
    ##########this block is copy pasted to another part of this code###########
    ###########################################################################
  }

  if ($this->input->get('fetch_all_partners') == 1) {
    foreach ($res as $key => &$value) {
      $value = $this->formatRes($value);
    }
    $new_res = $res;
  }

 	return $new_res;
 }

 public function searchStoreName($keyword,$assignedLocation)
 {
  
  $decodeKeyword = urldecode($keyword);
  $explodedKey = explode(" ",$decodeKeyword);

  if(count($explodedKey) > 1 )
  {
    foreach ($explodedKey as $value) {
     $this->db->where('LOWER(partners.name) LIKE "%' . strtolower($value) . '%"');
    }
    
  }else{
    $this->db->where('LOWER(partners.name) LIKE "%' . strtolower($keyword) . '%" ');
  }

  if ($this->input->get('service_id')) {
    $this->db->where('service_id', $this->input->get('service_id'));

  }

  $this->db->select('partners.*');
  $this->db->where('partners.service_id', $this->input->get('service_id'));
  $this->db->where('partners.assigned_location', $assignedLocation);
  $this->db->group_by('partners.id'); 
  $this->db->join('inventory', 'partners.id = inventory.partner_id');
  $res = $this->db->get('partners')->result();
   //var_dump($res); die();
  if (!$res) {
      return [];
  }

  $new_res = [];

  foreach ($res as $key => &$value) {
    $value->diff_in_kilometers = "";
    $value->diff_in_kilometers_from_map_center = "";

    $value->is_visible = "";
    $value->is_visible_in_map_center = "";

    $value->map_center_coords = explode(',', $this->admin_model->getMeta('map_center',$assignedLocation));
    $value->radius_filter_in_km = (double)$this->admin_model->getMeta('stores_radius',$assignedLocation);

        // $value = $this->formatRes($value);
    ###########################################################################
    ##########this block is copy pasted to another part of this code###########
    ###########################################################################
    if ($this->input->get('cart_id')) {
      $cart = $this->cart_model->get($this->input->get('cart_id'));

      $cart_coords = $cart->delivery_location;
      $store_coords = json_decode($value->coordinates);
      // var_dump($cart_coords); die();

      $cart_lat = $cart_coords->latitude;
      $cart_long = $cart_coords->longitude;

      $store_lat = $store_coords->latitude;
      $store_long = $store_coords->longitude;

      $diff_in_kilometers = $this->getCoordsDiff((float)$cart_lat, (float)$cart_long, (float)$value->map_center_coords[0], $value->map_center_coords[1], "K");
      $diff_in_kilometers_from_map_center = $this->getCoordsDiff((float)$store_lat, (float)$store_long, (float)$value->map_center_coords[0], $value->map_center_coords[1], "K");

      $value->diff_in_kilometers = $diff_in_kilometers;
      $value->diff_in_kilometers_from_map_center = $diff_in_kilometers_from_map_center;

      $value->is_visible = $diff_in_kilometers <= $value->radius_filter_in_km;
      $value->is_visible_in_map_center = $diff_in_kilometers_from_map_center <= $value->radius_filter_in_km;

      $this->db->where('partner_id', $value->id);
      $count = $this->db->count_all_results('inventory');
      if (!$count) {
        continue;
      }

      if ($this->input->get('filter_out_of_range') == 1) {
        if ($value->is_visible_in_map_center && $value->is_visible) {
          $new_res[] = $this->formatRes($value);
        }
      } else {
        $new_res[] = $this->formatRes($value);
      }
    }
    ###########################################################################
    ##########this block is copy pasted to another part of this code###########
    ###########################################################################
  }

  if ($this->input->get('fetch_all_partners') == 1) {
    foreach ($res as $key => &$value) {
      $value = $this->formatRes($value);
    }
    $new_res = $res;
  }

  return $new_res;
 }

 public function partnerBalanceSum($partnerId){

   $this->db->select_sum('balance');
   $this->db->where('partner_id', $partnerId);
   $this->db->where_in('status',['commission','paid']);
   return @$this->db->get('partner_balance')->row()->balance;

  }

public function partnerWalletSum($partnerId){

   $this->db->select_sum('amount');
   $this->db->where('partner_id', $partnerId);
   $this->db->where('status', 'paid');
   return @$this->db->get('partner_wallet')->row()->amount;

  }
  

   public function deductPartnerCommission($amount, $partnerId,$cartId)
  {
    $this->db->insert('partner_balance', ['balance' => $amount,'cart_id' => $cartId,'type' => 'getapp_partner_commission','status' => 'commission','partner_id' =>  $partnerId]);
    return $this->db->insert_id();
  }

   public function partnerTopUpWallet($data, $partnerId)
  {
    $this->db->insert('partner_wallet', ['amount' => $data['amount'],'assigned_location' => $data["assigned_location"],'partner_id' => $partnerId]);
    $last_id = $this->db->insert_id();

    $res = $this->payment_model->createResourceGcashPartner($data['amount'], $partnerId, $last_id);

    $this->db->where('id', $last_id);
    $this->db->update('partner_wallet', [
      'transaction_id' => $res->id,
      'checkout_url' => $res->attributes->redirect->checkout_url,
      'status' => $res->attributes->status
    ]);
    return $this->db->get_where('partner_wallet', ['id' => $last_id])->row();
  }
   public function updateWalletStatusforPartner($id,$balance){
    # get the payment source
    $res = $this->payment_model->getPaymentSourceforPartner($id);

    # pag chargebale, bayaran
    if ($res->attributes->status == 'chargeable') {
      $res = $this->payment_model->payTheSourceforPartner($id);
    }

    # update na bayad na
    $this->db->where('id', $id);
    return $this->db->update('partner_balance', ['status' => $res->attributes->status,'balance'=>"-".$balance]);
  }

 public function updatePartnerProfile($id,$data){
    $data['coordinates'] = json_encode(['latitude' => $data['latitude'], 'longitude' => $data['longitude']]);
    unset($data['latitude'], $data['longitude']);

    $this->db->where('id', $id);
    return $this->db->update('partners', $data);
  }

 public function partnerPayBalance($data, $partnerId)
  {
    $this->db->insert('partner_balance', ['balance' => $data['balance'],'assigned_location' => $data["assigned_location"],'partner_id' => $partnerId]);
    $last_id = $this->db->insert_id();

    $res = $this->payment_model->createResourceGcashPartner($data['balance'], $partnerId, $last_id);

  // print_r($res);die();

    $this->db->where('id', $last_id);
    $this->db->update('partner_balance', [
      'transaction_id' => $res->id,
      'checkout_url' => $res->attributes->redirect->checkout_url,
      'status' => $res->attributes->status
    ]);
    return $this->db->get_where('partner_balance', ['id' => $last_id])->row();
  }

} //end of class

