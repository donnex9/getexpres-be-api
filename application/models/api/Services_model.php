<?php

class Services_model extends Crud_model
{
  /**
  * Your table name
  * @var string
  */
  protected $table;

  /**
  * The directory you want to upload to whose parent dir is `uploads` folder
  * @var [type]
  */
  protected $upload_dir;
  protected $upload_dir_category;

  public function __construct()
  {
    parent::__construct();
    $this->upload_dir = 'services'; # uploads/your_dir
    $this->uploads_folder = "efs/uploads/" . $this->upload_dir . "/";
    $this->full_up_path = base_url() . "efs/uploads/" . $this->upload_dir . "/";
        
    $this->upload_dir_category = 'category';
    $this->uploads_folder_category = "uploads/" . $this->upload_dir_category . "/";
    $this->full_up_path_category = base_url() . $this->uploads_folder_category . "/";

    $this->load->model('api/vehicles_model');
  }

  public function all()
  {
//    $this->db->where_not('id',6);
    $res = $this->db->get('services')->result();
    foreach ($res as &$value) {
      $value = $this->formatRes($value);
    }
    return $res;
  }

  public function allHaveCMSAccess()
  {
    $this->db->where('has_cms_access', 1);
    $res = $this->db->get('services')->result();
    if (!$res) {
      return [];
    }

    foreach ($res as $key => &$value) {
      $value = $this->formatRes($value);
    }
    return $res;
  }

  public function getVehiclesByService($service_id, $show_inactive = false,$location)
  {
    $this->db->where_in('services_vehicles.dynamic_location',$location);
    if (!$show_inactive) {
      $this->db->where_in('vehicles.is_active', 1);
    }
    $this->db->where('services_vehicles.service_id', $service_id);
    $this->db->select('vehicles.id as vehicle_id, vehicles.name, vehicles.image, vehicles.is_active,vehicles.weight_capacity');
    $this->db->join('vehicles', 'vehicles.id = services_vehicles.vehicle_id');
    $res = $this->db->get('services_vehicles')->result();

    if (!$res) {
      return [];
    }

    foreach ($res as $key => &$value) {
      $this->vehicles_model->formatRes($value);
    }
    return $res;
  }

  public function getActiveVehiclesByService($location)
  {
    $this->db->where_in('services_vehicles.dynamic_location',$location);
    $this->db->where_in('vehicles.is_active', 1);
    $this->db->where_in('services_vehicles.service_id',['3','4']);
    $this->db->select('vehicles.id as vehicle_id,services_vehicles.service_id, vehicles.name, vehicles.image, vehicles.is_active');
    $this->db->join('vehicles', 'vehicles.id = services_vehicles.vehicle_id');
    $res = $this->db->get('services_vehicles')->result();

    if (!$res) {
      return [];
    }

    foreach ($res as $key => &$value) {
      $this->vehicles_model->formatRes($value);
    }
    return $res;
  }

  function get($id) {
    $this->db->where('id', $id);
    $res = $this->db->get('services')->row();
    return $this->formatRes($res);
  }

  function formatRes($res)
  {
      $res->icon = ($res->icon) ? $this->full_up_path . $res->icon : $this->temp_image;
      $res->type_f = ucwords($res->type);

      return $res;
  }

 function getCategory($isTopCategory,$serviceId){
    
    
    $this->db->where('istopcategory',$isTopCategory);
    $this->db->where('service_id',$serviceId);
    $res = $this->db->get('tbl_food_grocery_category')->result();

    if (!$res) {
      return [];
    }

    foreach ($res as $key => &$value) {
       $this->categoryFormatRes($value);
    }
    return $res;
 }

  function categoryFormatRes($res)
  {
     $res->image_path = $this->full_up_path_category . $res->image_path;
     $res->category_name = ucwords($res->category_name);
     return $res;
  }
}
