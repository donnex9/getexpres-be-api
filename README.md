# GetExpress App

## Table of contents

1. [Project Information](#project-information)
1. [Sign-up](#sign-up)
    1. [Customer sign-up](#customer-sign-up)
    1. [Rider sign-up](#rider-sign-up)
1. [Sign-in](#sign-in)
    1. [Customer sign-in](#customer-sign-in)
    1. [Rider sign-in](#rider-sign-in)
1. [Social Sign-in](#social-sign-in)
    1. [Customer social sign-in](#customer-social-sign-in)
    1. [Rider social sign-in](#rider-social-sign-in)
1. [Sign-out](#sign-out)
    1. [Customer sign-out](#customer-sign-out)
    1. [Rider sign-out](#rider-sign-out)
1. [Forgot password](#forgot-password)
1. [Rider change active vehicle](#rider-change-active-vehicle)
1. [Services (Get Car, Get Grocery, etc.)](#services)
    1. [Get available vehicles by service](#get-vehicles-by-service)
    1. [Get all services](#get-all-services)
1. [News / What's New](#news-whats-new)
    1. [Get all news](#get-all-news)
    1. [Get single news](#get-single-news)
1. [Profile](#profile)
    1. [Get profile](#get-profile)
    1. [Update profile](#update-profile)
    1. [Add address](#add-address)
    1. [Update address](#update-address)
1. [History](#history)
    1. [Get all history](#get-all-history)
    1. [Get all history by service](#get-all-history-by-service)
    1. [Get single history](#get-single-history)
1. [Vehicles](#vehicles)
    1. [Get vehicles list](#get-vehicles-list)
    1. [Rider add vehicle](#rider-add-vehicle)
    1. [Rider change active vehicle](#rider-change-active-vehicle)
1. [Partners](#partners)
    1. [Get all partners by service](#get-all-partners-by-service)
    1. [Search by partner name or item name](#search-by-partner-name)
1. [Inventory](#inventory)
    1. [Get inventory by partner](#get-inventory-by-partner)
    1. [Get single item](#get-single-item)
    1. [Get inventory categories by partner](#get-inventory-categories-by-partner)
1. [Cart](#cart)
    1. [General](#general)
        1. [Get cart information](#get-cart-information)
    1. [Customer](#customer)
        1. [Status Information](#status-information)
        1. [Initialize Cart](#initialize-cart)
        1. [Finalize cart](#finalize-cart)
        1. [Cancel cart](#cancel-cart)
    1. [Service Types](#service-types)
        1. [Get Car](#get-car)
        1. [Get Food and Get Grocery](#get-food-and-get-grocery)
            1. [Add item to cart](#add-item-to-cart)
            1. [Update cart item](#update-cart-item)
        1. [Get Delivery](#get-delivery)
            1. [Get Delivery Item Details](#get-delivery-item-details)
        1. [Get Pabili](#get-pabili)
            1. [Create a pabili](#create-a-pabili)
    1. [Rider](#rider)
        1. [Get active booking](#get-active-booking)
        1. [Look for customers](#look-for-customers)
        1. [Accept booking](#accept-booking)
        1. [Change status of booking](#change-status-of-booking)

## Project information
#### API base url
* `http://getapp.betaprojex.com/api/`

#### Required headers to access the API

| Header | Value
| ------ | ------
| Authorization | Basic YWRtaW46SGVsbG9DbGFyaWNlLi4u

## Sign-up

### Customer sign-up

**POST** _api/sign-up/customer/_

**Parameters**


| Name | Type | Description | Sample Data |
| ------ | ------ | ----------- | ----------- |
| full_name    | string | - | John Doe |
| email    | string | - | johndoe@example.com |
| mobile_num    | string | - | 09278111111 |
| birthdate    | date | - | 2000-12-30 |
| location    | string | - | Manila |
| password    | string | (don't include in request if you have social token) | supersecretpassword |
| social_token    | string | (don't include in request if you have password) | 123zxc |
| is_email_verified    | bool | (don't include in request if you have password) | 1 |

**Response**
``` javascript
201	Created
{
	"data":{
    "id": "7",
    "full_name": "Ro11ingBoy",
    "email": "lsalamante@myoptimind.com",
    "mobile_num": "09451494315",
    "birthdate": "1994-11-29",
    "location": "Underground",
    "password": "$2y$10$\/EpbAMAc49r9m8WZqKAaR.4HW4iLiQcbOojiNbnJrthFGOm\/wF8Za",
    "profile_picture": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.png",
    "social_token": "",
    "is_email_verified": "1",
    "mobile_otp": "",
    "verification_token": "",
    "created_at": "2020-10-13 16:39:29",
    "updated_at": "2020-10-14 15:44:35",
    "banned_at": "",
    "addresses": [
      {
        "id": "1",
        "customer_id": "7",
        "label": "Home",
        "full_address": "#007 St. Paul street, Marvi Hills Subd., Brgy. Gulod Malaya, San Mateo, Rizal",
        "latitude": "-1234567.11",
        "longitude": "123231234.8",
        "created_at": "2020-10-16 15:01:58",
        "updated_at": ""
      },
      {
        "id": "2",
        "customer_id": "7",
        "label": "Home",
        "full_address": "#2 Placid Homes, Dulong Bayan, San Mateo Rizal",
        "latitude": "-133333.11",
        "longitude": "22211111.8",
        "created_at": "2020-10-16 15:02:39",
        "updated_at": ""
      }
    ]
  },
	"message":"Account created successfully! Please check your email at johndoe@example.com for verification.",
	"status":"201"
}
```

``` javascript
400	Bad request
{
	"data":{},
	"message":"Malformed syntax",
	"status":"400"
}
```

``` javascript
409	Conflict
{
	"data":{},
	"message":"Email already taken",
	"status":"409"
}
```

### Rider sign-up

**POST** _api/sign-up/rider/_

**Parameters**


| Name | Type | Description | Sample Data |
| ------ | ------ | ----------- | ----------- |
| full_name    | string | - | John Doe |
| email    | string | - | johndoe@example.com |
| mobile_num    | string | - | 09278111111 |
| birthdate    | date | - | 2000-12-30 |
| location    | string | - | Manila |
|   |   |   |   |
|   |   |   |   |
| password    | string | (don't include in request if you have social token) | supersecretpassword |
| social_token    | string | (don't include in request if you have password) | 123zxc |
| is_email_verified    | bool | (don't include in request if you have password) | 1 |
|   |   |   |   |
|   |   |   |   |
| identification_document    | file | ID of the rider | file |
| vehicle_id    | int | ID from vehicles list | 1 |
| vehicle_model    | string | (optional) | Rolls Royce |
| plate_number    | string | (optional) | 2020WTF |


**Response**
``` javascript
201 Created
{
  "data": {
    "id": "11",
    "full_name": "Ro11ingBoy",
    "email": "lsalamante+bbbb+@myoptimind.com",
    "mobile_num": "09451494315",
    "birthdate": "1994-11-29",
    "location": "Underground",
    "password": "$2y$10$Egpt02iFpQZOsuFvBmCWMes3q3Y36sBeNZkxKf94Olis7XQga80oG",
    "profile_picture": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.png",
    "social_token": "",
    "identification_document": "http:\/\/localhost\/getapp\/uploads\/riders\/1605324906_temp2.jpg",
    "is_email_verified": "0",
    "is_admin_verified": "0",
    "mobile_otp": "",
    "verification_token": "BmXikun3CUOSqUzS2Vt81443TtIfm2p3",
    "created_at": "2020-11-14 11:35:06",
    "updated_at": "",
    "banned_at": "",
    "vehicles": [
      {
        "id": "4",
        "vehicle_type": "Bicycle",
        "vehicle_id": "2",
        "vehicle_model": "Rolls Royce",
        "plate_number": "2020WTF",
        "is_active": "1"
      }
    ],
    "active_vehicle": {
      "id": "4",
      "vehicle_type": "Bicycle",
      "vehicle_id": "2",
      "vehicle_model": "Rolls Royce",
      "plate_number": "2020WTF",
      "is_active": "1"
    }
  },
  "meta": {
    "message": "Account created successfully! Please check your email at lsalamante+bbbb+@myoptimind.com for verification. You will be notified once your account has been approved by an Administrator.",
    "code": "ok",
    "status": 201
  }
}
```

``` javascript
400 Bad request
{
  "data": {},
  "meta": {
    "message": "Malformed syntax",
    "status": 400,
    "code": "malformed_syntax"
  }
}
```

``` javascript
409 Conflict
{
  "data": {},
  "meta": {
    "message": "Email already exists",
    "status": 409,
    "code": "email_exists"
  }
}
```

# Sign-in

## Customer sign-in

**POST** _api/sign-in/customer/_

**Parameters**


| Name | Type | Description | Sample Data |
| ------ | ------ | ----------- | ----------- |
| email    | string | - | johndoe@example.com |
| password    | string | - | supersecretpassword |
| device_id    | string | - | 123 |
| firebase_id    | string | - | 123 |

**Response**
``` javascript
200 OK
{
  "data": {
    "id": "7",
    "full_name": "Ro11ingBoy",
    "email": "lsalamante@myoptimind.com",
    "mobile_num": "09451494315",
    "birthdate": "1994-11-29",
    "location": "Underground",
    "password": "$2y$10$\/EpbAMAc49r9m8WZqKAaR.4HW4iLiQcbOojiNbnJrthFGOm\/wF8Za",
    "profile_picture": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.png",
    "social_token": "",
    "is_email_verified": "1",
    "mobile_otp": "",
    "verification_token": "",
    "created_at": "2020-10-13 16:39:29",
    "updated_at": "2020-10-14 15:44:35",
    "banned_at": "",
    "addresses": [
      {
        "id": "1",
        "customer_id": "7",
        "label": "Home",
        "full_address": "#007 St. Paul street, Marvi Hills Subd., Brgy. Gulod Malaya, San Mateo, Rizal",
        "latitude": "-1234567.11",
        "longitude": "123231234.8",
        "created_at": "2020-10-16 15:01:58",
        "updated_at": ""
      },
      {
        "id": "2",
        "customer_id": "7",
        "label": "Home",
        "full_address": "#2 Placid Homes, Dulong Bayan, San Mateo Rizal",
        "latitude": "-133333.11",
        "longitude": "22211111.8",
        "created_at": "2020-10-16 15:02:39",
        "updated_at": ""
      }
    ]
  },
  "meta": {
    "message": "Login successful",
    "code": "ok",
    "status": 200
  }
}
```


``` javascript
409 Conflict
{
  "data": {},
  "meta": {
    "message": "Account not yet activated.",
    "code": "account_not_active",
    "status": 409
  }
}
```

``` javascript
409 Conflict
{
  "data": {},
  "meta": {
    "message": "Account registered to another sign-in method. Please try a different sign-in method",
    "code": "account_exists_in_a_different_signin_method",
    "status": 409
  }
}
```

## Rider sign-in

**POST** _api/sign-in/rider/_

**Parameters**


| Name | Type | Description | Sample Data |
| ------ | ------ | ----------- | ----------- |
| email    | string | - | johndoe@example.com |
| password    | string | - | supersecretpassword |
| device_id    | string | - | 123 |
| firebase_id    | string | - | 123 |

**Response**
``` javascript
200 OK
{
  "data": {
    "id": "10",
    "full_name": "Ro11ingBoy",
    "email": "lsalamante+bbbb@myoptimind.com",
    "mobile_num": "09451494315",
    "birthdate": "1994-11-29",
    "location": "Underground",
    "password": "$2y$10$c9\/rGJ9AkhQpEv5VKvXXCuMKflNRlQVch5DYSlV5pAUnMR39EHYtS",
    "profile_picture": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.png",
    "social_token": "",
    "identification_document": "http:\/\/localhost\/getapp\/uploads\/riders\/1605324615_temp2.jpg",
    "is_email_verified": "1",
    "is_admin_verified": "1",
    "mobile_otp": "",
    "verification_token": "",
    "created_at": "2020-11-14 11:30:15",
    "updated_at": "2020-11-14 11:33:06",
    "banned_at": "",
    "vehicles": [
      {
        "id": "3",
        "vehicle_type": "Bicycle",
        "vehicle_id": "2",
        "vehicle_model": "Rolls Royce",
        "plate_number": "2020WTF",
        "is_active": "1"
      }
    ],
    "active_vehicle": {
      "id": "3",
      "vehicle_type": "Bicycle",
      "vehicle_id": "2",
      "vehicle_model": "Rolls Royce",
      "plate_number": "2020WTF",
      "is_active": "1"
    }
  },
  "meta": {
    "message": "Sign-in successful",
    "code": "ok",
    "status": 200
  }
}
```


``` javascript
409 Conflict
{
  "data": {},
  "meta": {
    "message": "Activate your account first and/or wait for the email validation from the Administrator.",
    "code": "account_not_active_nor_verified",
    "status": 409
  }
}
```

``` javascript
409 Conflict
{
  "data": {},
  "meta": {
    "message": "Account registered to another sign-in method. Please try a different sign-in method",
    "code": "account_exists_in_a_different_signin_method",
    "status": 409
  }
}
```

## Social Sign-in

**Scenarios**

| Scenario | Status | Code
| ------ | ------ | --------
| Email doesn't exist yet | 200 |safe_to_sign_up
| Correct email and social token | 200 |  ok
| Correct email and wrong social token | 409 |  social_token_mismatch
| Correct email and social token but account not yet verified | 409 |  rider_unverified
| Email exists but has password | 409 |  account_exists_in_a_different_signin_method
| Fallback | 400 |  malformed_syntax


## Customer social sign-in

**POST** _api/social-sign-in/customers/_

**Parameters**

| Name | Type | Description | Sample Data |
| ------ | ------ | ----------- | ----------- |
| email    | string | - | johndoe@example.com |
| social_token    | string | - | verylongstring |
| device_id    | string | - | 123 |
| firebase_id    | string | - | 123 |

**Response**
``` javascript
200 OK
{
  "data": {
    "id": "7",
    "full_name": "Ro11ingBoy",
    "email": "lsalamante@myoptimind.com",
    "mobile_num": "09451494315",
    "birthdate": "1994-11-29",
    "location": "Underground",
    "password": "$2y$10$\/EpbAMAc49r9m8WZqKAaR.4HW4iLiQcbOojiNbnJrthFGOm\/wF8Za",
    "profile_picture": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.png",
    "social_token": "",
    "is_email_verified": "1",
    "mobile_otp": "",
    "verification_token": "",
    "created_at": "2020-10-13 16:39:29",
    "updated_at": "2020-10-14 15:44:35",
    "banned_at": "",
    "addresses": [
      {
        "id": "1",
        "customer_id": "7",
        "label": "Home",
        "full_address": "#007 St. Paul street, Marvi Hills Subd., Brgy. Gulod Malaya, San Mateo, Rizal",
        "latitude": "-1234567.11",
        "longitude": "123231234.8",
        "created_at": "2020-10-16 15:01:58",
        "updated_at": ""
      },
      {
        "id": "2",
        "customer_id": "7",
        "label": "Home",
        "full_address": "#2 Placid Homes, Dulong Bayan, San Mateo Rizal",
        "latitude": "-133333.11",
        "longitude": "22211111.8",
        "created_at": "2020-10-16 15:02:39",
        "updated_at": ""
      }
    ]
  },
  "meta": {
    "message": "Login successful",
    "code": "ok",
    "status": 200
  }
}
```

``` javascript
{
  "data": {},
  "meta": {
    "message": "Account registered to another sign-in method. Please try a different sign-in method",
    "code": "account_exists_in_a_different_signin_method",
    "status": 409
  }
}
```

``` javascript
409 Conflict
{
  "data": {},
  "meta": {
    "message": "Social token mismatch",
    "code": "social_token_mismatch",
    "status": 409
  }
}
```



## Rider social sign-in

**POST** _api/social-sign-in/riders/_

**Parameters**

| Name | Type | Description | Sample Data |
| ------ | ------ | ----------- | ----------- |
| email    | string | - | johndoe@example.com |
| social_token    | string | - | superlongtext |
| device_id    | string | - | 123 |
| firebase_id    | string | - | 123 |

**Response**
``` javascript
200 OK
{
  "data": {
    "id": "16",
    "full_name": "Ro11ingBoy",
    "email": "lsalamante+bbbb+@myoptimind.com",
    "mobile_num": "09451494315",
    "birthdate": "1994-11-29",
    "location": "Underground",
    "password": "",
    "profile_picture": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.jpg",
    "social_token": "123zxc",
    "identification_document": "http:\/\/localhost\/getapp\/uploads\/riders\/1605683204_temp2.jpg",
    "is_email_verified": "1",
    "is_admin_verified": "1",
    "mobile_otp": "",
    "verification_token": "",
    "created_at": "2020-11-18 15:06:44",
    "updated_at": "2020-11-18 15:08:43",
    "banned_at": "",
    "vehicles": [
      {
        "id": "6",
        "vehicle_type": "Bicycle",
        "vehicle_id": "2",
        "vehicle_model": "Rolls Royce",
        "plate_number": "2020WTF",
        "is_active": "1"
      }
    ],
    "active_vehicle": {
      "id": "6",
      "vehicle_type": "Bicycle",
      "vehicle_id": "2",
      "vehicle_model": "Rolls Royce",
      "plate_number": "2020WTF",
      "is_active": "1"
    }
  },
  "meta": {
    "message": "Sign-in successful",
    "code": "ok",
    "status": 200
  }
}
```


``` javascript
{
  "data": {},
  "meta": {
    "message": "Account registered to another sign-in method. Please try a different sign-in method",
    "code": "account_exists_in_a_different_signin_method",
    "status": 409
  }
}
```

``` javascript
409 Conflict
{
  "data": {},
  "meta": {
    "message": "Social token mismatch",
    "code": "social_token_mismatch",
    "status": 409
  }
}
```

``` javascript
409 Conflict
{
  "data": {},
  "meta": {
    "message": "Rider account not yet verified. Please wait for the administrator to verify your account",
    "code": "rider_unverified",
    "status": 409
  }
}
```
```

# Sign-out

## Customer sign-out
**POST** _api/sign-out/customer/_

**Parameters**


| Name | Type | Description | Sample Data |
| ------ | ------ | ----------- | ----------- |
| email    | string | - | johndoe@example.com |
| password    | string | - | supersecretpassword |
| device_id    | string | - | 123 |
| firebase_id    | string | - | 123 |

**Response**
``` javascript
200 OK
{
  "data": {},
  "meta": {
    "message": "Logout success. Device notifications set to inactive state.",
    "code": "ok",
    "status": 200
  }
}
```
## Rider sign-out

**POST** _api/sign-out/rider/_

**Parameters**

| Name | Type | Description | Sample Data |
| ------ | ------ | ----------- | ----------- |
| email    | string | - | johndoe@example.com |
| password    | string | - | supersecretpassword |
| device_id    | string | - | 123 |
| firebase_id    | string | - | 123 |

**Response**
``` javascript
200 OK
{
  "data": {},
  "meta": {
    "message": "Logout success. Device notifications set to inactive state.",
    "code": "ok",
    "status": 200
  }
}
```

## Forgot password

**POST** _api/forgot-password/:type_  
**NOTE:** `type` could be either `riders` or `customers`

**Parameters**

| Name | Type | Description | Sample Data |
| ------ | ------ | ----------- | ----------- |
| email    | string | - | johndoe@example.com |

**Response**
``` javascript
200 OK
{
  "data": {},
  "meta": {
    "message": "Password reset link was sent to your email",
    "code": "success",
    "status": 200
  }
}
```

## Rider change active vehicle

**POST** _api/riders/:rider_id/vehicles/:rider_vehicles_pk/make-active_

**Response**
``` javascript
200 OK
{
  "data": {
    "id": "11",
    "full_name": "Ro11ingBoy",
    "email": "lsalamante+bbbb+@myoptimind.com",
    "mobile_num": "09451494315",
    "birthdate": "1994-11-29",
    "location": "Underground",
    "password": "$2y$10$Egpt02iFpQZOsuFvBmCWMes3q3Y36sBeNZkxKf94Olis7XQga80oG",
    "profile_picture": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.png",
    "social_token": "",
    "identification_document": "http:\/\/localhost\/getapp\/uploads\/riders\/1605324906_temp2.jpg",
    "is_email_verified": "1",
    "is_admin_verified": "0",
    "mobile_otp": "",
    "verification_token": "",
    "created_at": "2020-11-14 11:35:06",
    "updated_at": "2020-11-14 13:32:46",
    "banned_at": "",
    "vehicles": [
      {
        "id": "4",
        "vehicle_type": "Bicycle",
        "vehicle_id": "2",
        "vehicle_model": "Rolls Royce",
        "plate_number": "2020WTF",
        "is_active": "0"
      },
      {
        "id": "5",
        "vehicle_type": "Motorcycle",
        "vehicle_id": "3",
        "vehicle_model": "Rolls Royce 2",
        "plate_number": "2020WTF2",
        "is_active": "1"
      }
    ],
    "active_vehicle": {
      "id": "5",
      "vehicle_type": "Motorcycle",
      "vehicle_id": "3",
      "vehicle_model": "Rolls Royce 2",
      "plate_number": "2020WTF2",
      "is_active": "1"
    }
  },
  "meta": {
    "message": "Active vehicle changed successfully",
    "code": "ok",
    "status": 200
  }
}
```

# Services

## Get all services

**GET** _api/services/_  

**Response**
``` javascript
200 OK
{
  "data": [
    {
      "id": "1",
      "type": "car",
      "icon": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.png",
      "label": "Get Car",
      "description": "lorem ipsum dolor sit amet consectetur lorem ipsum dolor sit amet consectetur lorem ipsum dolor sit amet consectetur ",
      "has_cms_access": "0",
      "created_at": "2020-10-08 15:06:55",
      "updated_at": "2020-11-17 13:59:20"
    },
    {
      "id": "2",
      "type": "grocery",
      "icon": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.png",
      "label": "Get Grocery",
      "description": "lorem ipsum dolor sit amet consectetur ",
      "has_cms_access": "1",
      "created_at": "2020-10-08 15:06:55",
      "updated_at": "2020-11-17 13:59:22"
    },
    {
      "id": "3",
      "type": "pabili",
      "icon": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.png",
      "label": "Get Pabili",
      "description": "lorem ipsum dolor sit amet consectetur lorem ipsum dolor sit amet consectetur lorem ipsum dolor sit amet consectetur ",
      "has_cms_access": "0",
      "created_at": "2020-10-08 15:06:55",
      "updated_at": "2020-11-17 13:59:24"
    },
    {
      "id": "4",
      "type": "delivery",
      "icon": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.png",
      "label": "Get Delivery",
      "description": "lorem ipsum dolor sit amet consectetur ",
      "has_cms_access": "0",
      "created_at": "2020-10-08 15:06:55",
      "updated_at": "2020-11-17 13:59:26"
    },
    {
      "id": "5",
      "type": "food",
      "icon": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.png",
      "label": "Get Food",
      "description": "lorem ipsum dolor sit amet consectetur lorem ipsum dolor sit amet consectetur lorem ipsum dolor sit amet consectetur ",
      "has_cms_access": "1",
      "created_at": "2020-10-08 15:06:55",
      "updated_at": "2020-11-17 13:59:27"
    }
  ],
  "meta": {
    "message": "OK",
    "code": "ok",
    "status": 200
  }
}
```

## Get vehicles by service

**GET** _api/services/:service_id/vehicles_

**Response**
``` javascript
200 OK
{
  "data": [
    {
      "vehicle_id": "1",
      "name": "Walk",
      "image": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.jpg"
    },
    {
      "vehicle_id": "2",
      "name": "Bicycle",
      "image": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.jpg"
    },
    {
      "vehicle_id": "3",
      "name": "Motorcycle",
      "image": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.jpg"
    },
    {
      "vehicle_id": "4",
      "name": "Car (2-seater)",
      "image": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.jpg"
    }
  ],
  "meta": {
    "message": "OK",
    "code": "ok",
    "status": 200
  }
}
```


# News / What's new

## Get all news

**GET** _api/news/_  

**Response**
``` javascript
200 OK
{
  "data": [
    {
      "id": "1",
      "title": "Hello world promo",
      "category": "Food",
      "body": "Test ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor",
      "banner_image": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.png",
      "created_at": "2020-10-16 11:23:10",
      "updated_at": ""
    }
  ],
  "meta": {
    "message": "OK",
    "code": "ok",
    "status": 200
  }
}
```
## Get single news

**GET** _api/news/:id_  

**Response**
``` javascript
200 OK
{
  "data": {
      "id": "1",
      "title": "Hello world promo",
      "category": "Food",
      "body": "Test ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor ipsum dolor",
      "banner_image": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.png",
      "created_at": "2020-10-16 11:23:10",
      "updated_at": ""
    },
  "meta": {
    "message": "OK",
    "code": "ok",
    "status": 200
  }
}
```

# Profile

## Get profile

**GET** _api/profile/:type/:id_  
NOTE: `type` could be either `riders` or `customers`

**Response**
``` javascript
200 OK
// For customers
{
 "data": {
   "id": "7",
   "full_name": "Ro11ingBoya",
   "email": "lsalamante@myoptimind.com",
   "mobile_num": "09451494315",
   "birthdate": "1994-11-29",
   "location": "test",
   "password": "$2y$10$fGt5xFcQWQMfLu3PVOO57.z8aUrEIJt0qZQAeA\/IuKcyWsb3XE05.",
   "profile_picture": "http:\/\/localhost\/getapp\/uploads\/customers\/1606112157_temp2.jpg",
   "social_token": "",
   "is_email_verified": "1",
   "mobile_otp": "",
   "verification_token": "",
   "created_at": "2020-10-13 16:39:29",
   "updated_at": "2020-11-23 14:25:03",
   "banned_at": "",
   "addresses": [
     {
       "id": "1",
       "customer_id": "7",
       "label": "Home",
       "full_address": "#007 St. Paul street, Marvi Hills Subd., Brgy. Gulod Malaya, San Mateo, Rizal",
       "latitude": "-1234567.11",
       "longitude": "123231234.8",
       "created_at": "2020-10-16 15:01:58",
       "updated_at": ""
     },
     {
       "id": "2",
       "customer_id": "7",
       "label": "Home",
       "full_address": "#2 Placid Homes, Dulong Bayan, San Mateo Rizal",
       "latitude": "-133333.11",
       "longitude": "22211111.8",
       "created_at": "2020-10-16 15:02:39",
       "updated_at": ""
     }
   ]
 },
 "meta": {
   "message": "OK",
   "code": "ok",
   "status": 200
 }
}
```

```javascript
200 OK
// For riders
{
  "data": {
    "id": "18",
    "full_name": "Ro11ingBoyasss",
    "email": "lsalamante32@myoptimind.com",
    "mobile_num": "09451494315",
    "birthdate": "1994-11-29",
    "location": "Underground",
    "password": "",
    "profile_picture": "http:\/\/localhost\/getapp\/uploads\/riders\/1606112825_temp2.jpg",
    "social_token": "123zxc",
    "identification_document": "http:\/\/localhost\/getapp\/uploads\/riders\/1605684497_temp2.jpg",
    "is_email_verified": "1",
    "is_admin_verified": "1",
    "mobile_otp": "",
    "verification_token": "",
    "created_at": "2020-11-18 15:28:17",
    "updated_at": "2020-11-23 14:27:05",
    "banned_at": "",
    "vehicles": [
      {
        "id": "8",
        "vehicle_type": "Bicycle",
        "vehicle_id": "2",
        "vehicle_model": "Rolls Royce",
        "plate_number": "2020WTF",
        "is_active": "1"
      }
    ],
    "active_vehicle": {
      "id": "8",
      "vehicle_type": "Bicycle",
      "vehicle_id": "2",
      "vehicle_model": "Rolls Royce",
      "plate_number": "2020WTF",
      "is_active": "1"
    }
  },
  "meta": {
    "message": "OK",
    "code": "ok",
    "status": 200
  }
}
```


## Update profile

**POST** _api/profile/:type/:id_  
NOTE: `type` could be either `riders` or `customers`

### Customers

| Name | Type | Description | Sample Data |
| ------ | ------ | ----------- | ----------- |
| full_name    | string | - | John Doe |
| email    | string | - | johndoe@example.com |
| mobile_num    | string | - | 09278111111 |
| birthdate    | date | - | 2000-12-30 |
| location    | string | - | Manila |
| password    | string | (don't include in request if you have social token) | supersecretpassword |
| profile_picture    | string | file | File.jpg |


**Response**
``` javascript
200 OK
// For customers
{
 "data": {
   "id": "7",
   "full_name": "Ro11ingBoya",
   "email": "lsalamante@myoptimind.com",
   "mobile_num": "09451494315",
   "birthdate": "1994-11-29",
   "location": "test",
   "password": "$2y$10$fGt5xFcQWQMfLu3PVOO57.z8aUrEIJt0qZQAeA\/IuKcyWsb3XE05.",
   "profile_picture": "http:\/\/localhost\/getapp\/uploads\/customers\/1606112157_temp2.jpg",
   "social_token": "",
   "is_email_verified": "1",
   "mobile_otp": "",
   "verification_token": "",
   "created_at": "2020-10-13 16:39:29",
   "updated_at": "2020-11-23 14:25:03",
   "banned_at": "",
   "addresses": [
     {
       "id": "1",
       "customer_id": "7",
       "label": "Home",
       "full_address": "#007 St. Paul street, Marvi Hills Subd., Brgy. Gulod Malaya, San Mateo, Rizal",
       "latitude": "-1234567.11",
       "longitude": "123231234.8",
       "created_at": "2020-10-16 15:01:58",
       "updated_at": ""
     },
     {
       "id": "2",
       "customer_id": "7",
       "label": "Home",
       "full_address": "#2 Placid Homes, Dulong Bayan, San Mateo Rizal",
       "latitude": "-133333.11",
       "longitude": "22211111.8",
       "created_at": "2020-10-16 15:02:39",
       "updated_at": ""
     }
   ]
 },
 "meta": {
   "message": "OK",
   "code": "ok",
   "status": 200
 }
}
```

### Riders

| Name | Type | Description | Sample Data |
| ------ | ------ | ----------- | ----------- |
| full_name    | string | - | John Doe |
| email    | string | - | johndoe@example.com |
| mobile_num    | string | - | 09278111111 |
| birthdate    | date | - | 2000-12-30 |
| location    | string | - | Manila |
| password    | string | (don't include in request if you have social token) | supersecretpassword |
| identification_document    | file | ID of the rider | file |
| profile_picture    | string | file | File.jpg |

```javascript
200 OK
// For riders
{
  "data": {
    "id": "18",
    "full_name": "Ro11ingBoyasss",
    "email": "lsalamante32@myoptimind.com",
    "mobile_num": "09451494315",
    "birthdate": "1994-11-29",
    "location": "Underground",
    "password": "",
    "profile_picture": "http:\/\/localhost\/getapp\/uploads\/riders\/1606112825_temp2.jpg",
    "social_token": "123zxc",
    "identification_document": "http:\/\/localhost\/getapp\/uploads\/riders\/1605684497_temp2.jpg",
    "is_email_verified": "1",
    "is_admin_verified": "1",
    "mobile_otp": "",
    "verification_token": "",
    "created_at": "2020-11-18 15:28:17",
    "updated_at": "2020-11-23 14:27:05",
    "banned_at": "",
    "vehicles": [
      {
        "id": "8",
        "vehicle_type": "Bicycle",
        "vehicle_id": "2",
        "vehicle_model": "Rolls Royce",
        "plate_number": "2020WTF",
        "is_active": "1"
      }
    ],
    "active_vehicle": {
      "id": "8",
      "vehicle_type": "Bicycle",
      "vehicle_id": "2",
      "vehicle_model": "Rolls Royce",
      "plate_number": "2020WTF",
      "is_active": "1"
    }
  },
  "meta": {
    "message": "OK",
    "code": "ok",
    "status": 200
  }
}
```

## Add address

**POST** _api/profile/new-address/:customer_id_  
NOTE: Adding address is for `customers` only

**Parameters**

| Name | Type | Description | Sample Data |
| ------ | ------ | ----------- | ----------- |
| label    | string | - | Home |
| full_address    | string | - | Taytay, Rizal |
| latitude    | string | - | 1234 |
| longitude    | string | - | 12345 |


**Response**
``` javascript
201 Created
{
  "data": {
    "id": "12",
    "customer_id": "7",
    "label": "Home23",
    "full_address": "Somewhere",
    "latitude": "100",
    "longitude": "15",
    "created_at": "2020-11-09 11:47:56",
    "updated_at": ""
  },
  "meta": {
    "message": "New address added.",
    "code": "ok",
    "status": 201
  }
}
```

## Update address

**POST** _api/profile/address/:address_id_  
NOTE: Updating address is for `customers` only

**Parameters**

| Name | Type | Description | Sample Data |
| ------ | ------ | ----------- | ----------- |
| label    | string | - | Home |
| full_address    | string | - | Muzon, Taytay, Rizal |
| latitude    | string | - | 1234 |
| longtitude    | string | - | 12345 |

**Response**
``` javascript
200 OK
{
  "data": {
    "id": "4",
    "customer_id": "7",
    "label": "Home 22",
    "full_address": "Somewhere 2",
    "latitude": "1002",
    "longitude": "152 ",
    "created_at": "2020-11-09 11:36:46",
    "updated_at": "2020-11-09 11:40:19"
  },
  "meta": {
    "message": "Address updated",
    "code": "ok",
    "status": "200"
  }
}
```
# History

## Get all history
**GET** _api/cart/customer/:customer_id/history_

**Response**
``` javascript
200 OK
{
  "data": [
    {
      "location": {
        "label": "Optimind",
        "address_text": "2nd Floor CTP Bldg., Marikina, San Roque",
        "latitude": 11.123456,
        "longitude": -12.123456,
        "landmark": "Above Chinabank Savings"
      },
      "created_at": "November 17, 2020",
      "total_items": 0,
      "total_price": "0.00",
      "icon": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.jpg"
    },
    {
      "location": {
        "label": "Optimind",
        "address_text": "2nd Floor CTP Bldg., Marikina, San Roque",
        "latitude": 11.123456,
        "longitude": -12.123456,
        "landmark": "Above Chinabank Savings"
      },
      "created_at": "November 20, 2020",
      "total_items": 0,
      "total_price": "0.00",
      "icon": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.jpg"
    },
    {
      "location": "",
      "created_at": "November 25, 2020",
      "total_items": 0,
      "total_price": "0.00",
      "icon": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.jpg"
    }
  ],
  "meta": {
    "message": "OK",
    "code": "ok",
    "status": 200
  }
}
```

## Get all history by service
**GET** _api/cart/customers/:customer_id/history/services/:service_id/_

**Response**
``` javascript
200 OK
{
  "data": [
    {
      "location": {
        "label": "Optimind",
        "address_text": "2nd Floor CTP Bldg., Marikina, San Roque",
        "latitude": 11.123456,
        "longitude": -12.123456,
        "landmark": "Above Chinabank Savings"
      },
      "created_at": "November 20, 2020",
      "total_items": 0,
      "total_price": "0.00",
      "icon": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.jpg"
    }
  ],
  "meta": {
    "message": "OK",
    "code": "ok",
    "status": 200
  }
}
```

# Vehicles

## Get vehicles list

**GET** _api/vehicles_

**Response**
``` javascript
200 OK
{
  "data": [
    {
      "id": "1",
      "name": "Walk",
      "image": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.png",
      "created_at": "2020-10-09 15:55:28",
      "updated_at": ""
    },
    {
      "id": "2",
      "name": "Bicycle",
      "image": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.png",
      "created_at": "2020-10-09 15:55:28",
      "updated_at": ""
    },
    {
      "id": "3",
      "name": "Motorcycle",
      "image": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.png",
      "created_at": "2020-10-09 15:55:28",
      "updated_at": ""
    },
    {
      "id": "4",
      "name": "Car",
      "image": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.png",
      "created_at": "2020-10-09 15:55:28",
      "updated_at": ""
    },
    {
      "id": "5",
      "name": "MPV",
      "image": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.png",
      "created_at": "2020-10-09 15:55:28",
      "updated_at": ""
    },
    {
      "id": "6",
      "name": "Truck",
      "image": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.png",
      "created_at": "2020-10-09 15:55:28",
      "updated_at": ""
    }
  ],
  "meta": {
    "message": "OK",
    "code": "ok",
    "status": 200
  }
}
```



### Rider add vehicle

**POST** _api/riders/:rider_id/vehicles_  

**Parameters**

| Name | Type | Description | Sample Data |
| ------ | ------ | ----------- | ----------- |
| vehicle_id | number | FK from vehicles table | 3 |
| vehicle_model | string | - | Rolls Royce |
| plate_number | string | - | 2020WTF |


**Response**
``` javascript
200 OK
{
  "data": {
    "id": "16",
    "full_name": "Ro11ingBoy",
    "email": "lsalamante+bbbb+@myoptimind.com",
    "mobile_num": "09451494315",
    "birthdate": "1994-11-29",
    "location": "Underground",
    "password": "",
    "profile_picture": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.jpg",
    "social_token": "123zxc",
    "identification_document": "http:\/\/localhost\/getapp\/uploads\/riders\/1605683204_temp2.jpg",
    "is_email_verified": "1",
    "is_admin_verified": "1",
    "mobile_otp": "",
    "verification_token": "vNKddlaRd1MGR7OXonBxHlLLbCxo1lkG",
    "created_at": "2020-11-18 15:06:44",
    "updated_at": "2020-11-18 15:08:43",
    "banned_at": "",
    "vehicles": [
      {
        "id": "6",
        "vehicle_type": "Truck",
        "vehicle_id": "6",
        "vehicle_model": "Rolls Royce",
        "plate_number": "2020WTF",
        "is_active": "1"
      },
      {
        "id": "11",
        "vehicle_type": "Car (2-seater)",
        "vehicle_id": "4",
        "vehicle_model": "Rolls Royce 2.0",
        "plate_number": "2021WTF",
        "is_active": "0"
      }
    ],
    "active_vehicle": {
      "id": "6",
      "vehicle_type": "Truck",
      "vehicle_id": "6",
      "vehicle_model": "Rolls Royce",
      "plate_number": "2020WTF",
      "is_active": "1"
    }
  },
  "meta": {
    "message": "OK",
    "code": "ok",
    "status": 200
  }
}
```



### Rider change active vehicle

**POST** _api/riders/:rider_id/vehicles/:vehicle_pk/active_  

**Response**
``` javascript
200 OK
{
  "data": {
    "id": "16",
    "full_name": "Ro11ingBoy",
    "email": "lsalamante+bbbb+@myoptimind.com",
    "mobile_num": "09451494315",
    "birthdate": "1994-11-29",
    "location": "Underground",
    "password": "",
    "profile_picture": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.jpg",
    "social_token": "123zxc",
    "identification_document": "http:\/\/localhost\/getapp\/uploads\/riders\/1605683204_temp2.jpg",
    "is_email_verified": "1",
    "is_admin_verified": "1",
    "mobile_otp": "",
    "verification_token": "vNKddlaRd1MGR7OXonBxHlLLbCxo1lkG",
    "created_at": "2020-11-18 15:06:44",
    "updated_at": "2020-11-18 15:08:43",
    "banned_at": "",
    "vehicles": [
      {
        "id": "6",
        "vehicle_type": "Truck",
        "vehicle_id": "6",
        "vehicle_model": "Rolls Royce",
        "plate_number": "2020WTF",
        "is_active": "0"
      },
      {
        "id": "11",
        "vehicle_type": "Car (2-seater)",
        "vehicle_id": "4",
        "vehicle_model": "Rolls Royce 2.0",
        "plate_number": "2021WTF",
        "is_active": "1"
      }
    ],
    "active_vehicle": {
      "id": "11",
      "vehicle_type": "Car (2-seater)",
      "vehicle_id": "4",
      "vehicle_model": "Rolls Royce 2.0",
      "plate_number": "2021WTF",
      "is_active": "1"
    }
  },
  "meta": {
    "message": "OK",
    "code": "ok",
    "status": 200
  }
}
```

# Partners

## Get all partners by service

**GET** _api/partners/service/:service_id_

**Response**
``` javascript
200 OK
{
  "data": [
    {
      "id": "1",
      "service_id": "2",
      "name": "Alfamart",
      "category": "Grocery",
      "location_text": "San Mateo, Rizal",
      "coordinates":
        {
          "latitude":"14.6757331,121",
          "longitude":"121.1285205"
        },
      "image": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.png",
      "created_at": "2020-10-16 11:23:10",
      "updated_at": ""
    }
  ],
  "meta": {
    "message": "OK",
    "code": "ok",
    "status": 200
  }
}
```
## Search by partner name

**GET** _api/partners/service/:keyword_


**Response**
``` javascript
200 OK
{
  "data": [
    {
      "id": "1",
      "service_id": "2",
      "name": "Alfamart",
      "category": "Grocery",
      "location_text": "San Mateo, Rizal",
      "coordinates": {
        "latitude": "14.6757331,121",
        "longitude": "121.1285205"
      },
      "image": "",
      "created_at": "2020-10-09 17:17:09",
      "updated_at": "2020-10-09 17:23:04"
    }
  ],
  "meta": {
    "message": "Search found.",
    "code": "ok",
    "status": 200
  }
}
```

# Inventory

## Get Inventory by Partner

**GET** _api/inventory/partner/:partner_id_  
**Filters**: `?category=Sample` to search by Category  
**Filters**: `?search=Sample` to search by Product name  
**Combined Example**: `?category=Value%20Meal&search=Sample`

**Response**
``` javascript
200 OK
{
  "data": [
    {
      "id": "3",
      "partner_id": "2",
      "in_stock": "10",
      "product_name": "Chicken Spaghetti",
      "base_price": "150",
      "image": "",
      "addon_parent_id": "",
      "category": "Value Meal",
      "description": "This is sample description.",
      "created_at": "2020-11-09 17:09:38",
      "updated_at": "2020-11-09 17:10:17"
    },
    {
      "id": "4",
      "partner_id": "2",
      "in_stock": "10",
      "product_name": "Burger Steak 1 Rice",
      "base_price": "99",
      "image": "",
      "addon_parent_id": "",
      "category": "Value Meal",
      "description": "This is sample description.",
      "created_at": "2020-11-09 17:09:38",
      "updated_at": "2020-11-09 17:10:17"
    }
  ],
  "meta": {
    "message": "OK",
    "code": "ok",
    "status": 200
  }
}
```

## Get single item

**GET** _api/inventory/item/:id_

**Response**
``` javascript
200 OK
{
  "data": {
    "id": "3",
    "partner_id": "2",
    "in_stock": "10",
    "product_name": "Chicken Spaghetti",
    "base_price": "150",
    "image": "",
    "addon_parent_id": "",
    "category": "Value Meal",
    "description": "This is sample description.",
    "created_at": "2020-11-09 17:09:38",
    "updated_at": "2020-11-09 17:10:17"
  },
  "meta": {
    "message": "OK",
    "code": "ok",
    "status": 200
  }
}
```

## Get Inventory Categories by Partner

**GET** _api/inventory/categories/partner/:partner_id_

**Response**
``` javascript
200 OK
{
  "data": [
    "Bucket Meal",
    "Value Meal"
  ],
  "meta": {
    "message": "OK",
    "code": "ok",
    "status": 200
  }
}
```

---


# Rider

### Look for customers
**GET** _api/cart/vehicles/:vehicle_type_id_   

**Response**
``` javascript
200 OK
{
  "data": [
    {
      "cart_id": 10,
      "customer": {
        "profile_picture": "http:\/\/localhost\/getapp\/uploads\/customers\/1606112157_temp2.jpg",
        "full_name": "Ro11ingBoya"
      },
      "order_info": {
        "pickup_location_label": "Optimind",
        "grand_total": 0,
        "service_icon": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.jpg"
      },
      "cart_meta": {
        "cart_type_id": 4,
        "service_type": "delivery"
      }
    }
  ],
  "meta": {
    "message": "OK",
    "code": "ok",
    "status": 200
  }
}
```

### Get active booking
**GET** _api/cart/vehicles/:vehicle_type_id_   

**Response**
``` javascript
```

### Accept booking

**POST** _api/cart/:cart_id/accept/riders/:rider_id_  

**Response**
``` javascript
200 OK
{
  "data": {
    "id": "6",
    "cart_type_id": "5",
    "customer_id": "7",
    "vehicle_id": "6",
    "status": "accepted",
    "payload": "",
    "rider_id": "16",
    "notes": "No onions",
    "pickup_location": {
      "label": "Optimind",
      "address_text": "2nd Floor CTP Bldg., Marikina, San Roque",
      "latitude": 11.123456,
      "longitude": -12.123456,
      "landmark": "Above Chinabank Savings"
    },
    "delivery_location": {
      "label": "Optimind",
      "address_text": "2nd Floor CTP Bldg., Marikina, San Roque",
      "latitude": 11.123456,
      "longitude": -12.123456,
      "landmark": "Above Chinabank Savings"
    },
    "payment_method": "",
    "transaction_id": "",
    "payment_status": "",
    "total_price": "0.00",
    "created_at": "November 17, 2020",
    "updated_at": "2020-11-25 13:32:40",
    "service_type": "food",
    "basket": {
      "items": [
        {
          "cart_item_id": "2",
          "cart_id": "6",
          "product_id": "1",
          "product_name": "Pepsi Max (250 ML Bottle)",
          "image": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.jpg",
          "category": "Soft drinks",
          "description": "Lorem ipsum dolor sit amet",
          "base_price": 40,
          "quantity": 4,
          "computed_price": 160,
          "notes": "Haiyaaaaa"
        },
        {
          "cart_item_id": "5",
          "cart_id": "6",
          "product_id": "1",
          "product_name": "Pepsi Max (250 ML Bottle)",
          "image": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.jpg",
          "category": "Soft drinks",
          "description": "Lorem ipsum dolor sit amet",
          "base_price": 40,
          "quantity": 3,
          "computed_price": 120,
          "notes": "Haiyaaaaa 2"
        }
      ],
      "sub_total": 280,
      "grand_total": 330,
      "total_items": 7,
      "delivery_fee": 50
    }
  },
  "meta": {
    "message": "Status changed.",
    "code": "ok",
    "status": 200
  }
}
```

### Change status of booking

**POST** _api/cart/:cart_id/status_  

**Parameters**

| Name | Type | Description | Sample Data |
| ------ | ------ | ----------- | ----------- |
| status | string | Choose a status [here](#status-information) | completed |

**Response**
``` javascript
200 OK
{
  "data": {
    "id": "6",
    "cart_type_id": "5",
    "customer_id": "7",
    "vehicle_id": "2",
    "status": "otw",
    "payload": "",
    "rider_id": "16",
    "notes": "No onions",
    "pickup_location": {
      "label": "Optimind",
      "address_text": "2nd Floor CTP Bldg., Marikina, San Roque",
      "latitude": 11.123456,
      "longitude": -12.123456,
      "landmark": "Above Chinabank Savings"
    },
    "delivery_location": {
      "label": "Optimind",
      "address_text": "2nd Floor CTP Bldg., Marikina, San Roque",
      "latitude": 11.123456,
      "longitude": -12.123456,
      "landmark": "Above Chinabank Savings"
    },
    "payment_method": "",
    "transaction_id": "",
    "payment_status": "",
    "total_price": "0.00",
    "created_at": "November 17, 2020",
    "updated_at": "2020-11-25 17:51:00",
    "service_type": "food",
    "basket": {
      "items": [],
      "sub_total": 0,
      "grand_total": 50,
      "total_items": 0,
      "delivery_fee": 50
    }
  },
  "meta": {
    "message": "OK",
    "code": "ok",
    "status": 200
  }
}
```


---

# Cart

## General

### Get cart information

**GET** _api/cart/:cart_id/info_  

**Response**
``` javascript
200 OK
{
  "data": {
    "id": "6",
    "cart_type_id": "5",
    "customer_id": "7",
    "vehicle_id": "6",
    "status": "accepted",
    "payload": "",
    "rider_id": "16",
    "notes": "No onions",
    "pickup_location": {
      "label": "Optimind",
      "address_text": "2nd Floor CTP Bldg., Marikina, San Roque",
      "latitude": 11.123456,
      "longitude": -12.123456,
      "landmark": "Above Chinabank Savings"
    },
    "delivery_location": {
      "label": "Optimind",
      "address_text": "2nd Floor CTP Bldg., Marikina, San Roque",
      "latitude": 11.123456,
      "longitude": -12.123456,
      "landmark": "Above Chinabank Savings"
    },
    "payment_method": "",
    "transaction_id": "",
    "payment_status": "",
    "total_price": "0.00",
    "created_at": "November 17, 2020",
    "updated_at": "2020-11-25 13:32:40",
    "service_type": "food",
    "basket": {
      "items": [
        {
          "cart_item_id": "2",
          "cart_id": "6",
          "product_id": "1",
          "product_name": "Pepsi Max (250 ML Bottle)",
          "image": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.jpg",
          "category": "Soft drinks",
          "description": "Lorem ipsum dolor sit amet",
          "base_price": 40,
          "quantity": 4,
          "computed_price": 160,
          "notes": "Haiyaaaaa"
        },
        {
          "cart_item_id": "5",
          "cart_id": "6",
          "product_id": "1",
          "product_name": "Pepsi Max (250 ML Bottle)",
          "image": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.jpg",
          "category": "Soft drinks",
          "description": "Lorem ipsum dolor sit amet",
          "base_price": 40,
          "quantity": 3,
          "computed_price": 120,
          "notes": "Haiyaaaaa 2"
        }
      ],
      "sub_total": 280,
      "grand_total": 330,
      "total_items": 7,
      "delivery_fee": 50
    }
  },
  "meta": {
    "message": "OK",
    "code": "ok",
    "status": 200
  }
}
```

### Empty the cart

Only applicable to `Get Food` and `Get Grocery` at the moment.

**POST** _api/cart/:cart_id/empty_  

**Response**
``` javascript
200 OK
{
  "data": {
    "id": "6",
    "cart_type_id": "5",
    "customer_id": "7",
    "vehicle_id": "6",
    "status": "cancelled",
    "payload": "",
    "rider_id": "16",
    "notes": "No onions",
    "pickup_location": {
      "label": "Optimind",
      "address_text": "2nd Floor CTP Bldg., Marikina, San Roque",
      "latitude": 11.123456,
      "longitude": -12.123456,
      "landmark": "Above Chinabank Savings"
    },
    "delivery_location": {
      "label": "Optimind",
      "address_text": "2nd Floor CTP Bldg., Marikina, San Roque",
      "latitude": 11.123456,
      "longitude": -12.123456,
      "landmark": "Above Chinabank Savings"
    },
    "payment_method": "",
    "transaction_id": "",
    "payment_status": "",
    "total_price": "0.00",
    "created_at": "November 17, 2020",
    "updated_at": "2020-11-25 16:24:09",
    "service_type": "food",
    "basket": {
      "items": [],
      "sub_total": 0,
      "grand_total": 50,
      "total_items": 0,
      "delivery_fee": 50
    }
  },
  "meta": {
    "message": "Cart emptied",
    "code": "ok",
    "status": 200
  }
}
```


## Customer

#### Status information
[⬆️ Go back to Change status of booking](#change-status-of-booking)

| Status | Meaning
| ------ | ------
| init | Newly initialized cart.
| cancelled | Customer has cancelled the transaction. Can be finalized again,
| pending | Orders, delivery location, etc. are ***finalised***. Waiting for a rider to accept. AKA cart is ***finalized***.
| accepted | Rider has accepted the request.
| otw | Generic rider is on the way
| arrived_at_resto | Rider has arrived at the restaurant/grocery
| placing_order | Rider is placing order
| arriving_at_destination | Rider is arriving to destination
| arrived_at_destination | Rider has arrived to delivery location
| completed | Transaction is completed, payment was made

### Initialize Cart

After choosing a service and vehicle in [this](https://marvelapp.com/prototype/12gdigae/screen/72904332) screen, the user must initialize the cart. It is okay to call this endpoint every time you visit this screen. At the second and subsequent times that this endpoint was called, the cart information will be returned instead of creating a new session.

**POST** _api/cart/initialize_   

**Parameters**

| Name | Type | Description | Sample Data |
| ------ | ------ | ----------- | ----------- |
| cart_type_id | string | FK service_id from services| 5 |  
| customer_id | string | - | 7 |
| vehicle_id | string | FK vehicle_id from vehicles. Can change | 3 |


**Response**
``` javascript
200 OK
{
  "data": {
    "id": "3",
    "cart_type_id": "5",
    "customer_id": "1",
    "vehicle_id": "4",
    "status": "",
    "payload": "",
    "rider_id": "0",
    "notes": "",
    "pickup_location": "",
    "delivery_location": "",
    "payment_method": "",
    "transaction_id": "",
    "payment_status": "",
    "created_at": "2020-11-17 16:41:08",
    "updated_at": ""
  },
  "meta": {
    "message": "Cart initialized",
    "code": "ok",
    "status": 200
  }
}
```

### Finalize cart

**POST** _api/cart/:cart_id/finalize_  

**Parameters**

| Name | Type | Description | Sample Data |
| ------ | ------ | ----------- | ----------- |
| notes    | string | - | Do it, now |
| pickup_location    | **json** (optional, depending on the service) | keys: label, addresss_text, latitude, longitude, landmark | '{"label":"Optimind","address_text":"2nd Floor CTP Bldg., Marikina, San Roque","latitude":11.123456,"longitude":-12.123456,"landmark":"Above Chinabank Savings"}' |
| delivery_location    | **json** (optional, depending on the service) | keys: label, addresss_text, latitude, longitude, landmark | '{"label":"Optimind","address_text":"2nd Floor CTP Bldg., Marikina, San Roque","latitude":11.123456,"longitude":-12.123456,"landmark":"Above Chinabank Savings"}' |

**Response**
``` javascript
200 OK
{
  "data": {
    "id": "6",
    "cart_type_id": "5",
    "customer_id": "1",
    "vehicle_id": "6",
    "status": "pending",
    "payload": "",
    "rider_id": "0",
    "notes": "No onions",
    "pickup_location": {
      "label": "Optimind",
      "address_text": "2nd Floor CTP Bldg., Marikina, San Roque",
      "latitude": 11.123456,
      "longitude": -12.123456,
      "landmark": "Above Chinabank Savings"
    },
    "delivery_location": {
      "label": "Optimind",
      "address_text": "2nd Floor CTP Bldg., Marikina, San Roque",
      "latitude": 11.123456,
      "longitude": -12.123456,
      "landmark": "Above Chinabank Savings"
    },
    "payment_method": "",
    "transaction_id": "",
    "payment_status": "",
    "total_price": "0.00",
    "created_at": "November 17, 2020",
    "updated_at": "2020-11-24 14:21:43",
    "service_type": "food",
    "basket": {
      "items": [
        {
          "cart_item_id": "2",
          "cart_id": "6",
          "product_id": "1",
          "product_name": "Pepsi Max (250 ML Bottle)",
          "image": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.jpg",
          "category": "Soft drinks",
          "description": "Lorem ipsum dolor sit amet",
          "base_price": 40,
          "quantity": 4,
          "computed_price": 160,
          "notes": "Haiyaaaaa"
        },
        {
          "cart_item_id": "5",
          "cart_id": "6",
          "product_id": "1",
          "product_name": "Pepsi Max (250 ML Bottle)",
          "image": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.jpg",
          "category": "Soft drinks",
          "description": "Lorem ipsum dolor sit amet",
          "base_price": 40,
          "quantity": 3,
          "computed_price": 120,
          "notes": "Haiyaaaaa 2"
        }
      ],
      "sub_total": 280,
      "grand_total": 330,
      "total_items": 7,
      "delivery_fee": 50
    }
  },
  "meta": {
    "message": "OK",
    "code": "ok",
    "status": 200
  }
}
```


### Cancel cart

**POST** _api/cart/:cart_id/cancel_  

**Response**
``` javascript
200 OK
{
  "data": {
    "id": "6",
    "cart_type_id": "5",
    "customer_id": "1",
    "vehicle_id": "6",
    "status": "cancelled",
    "payload": "",
    "rider_id": "0",
    "notes": "No onions",
    "pickup_location": {
      "label": "Optimind",
      "address_text": "2nd Floor CTP Bldg., Marikina, San Roque",
      "latitude": 11.123456,
      "longitude": -12.123456,
      "landmark": "Above Chinabank Savings"
    },
    "delivery_location": {
      "label": "Optimind",
      "address_text": "2nd Floor CTP Bldg., Marikina, San Roque",
      "latitude": 11.123456,
      "longitude": -12.123456,
      "landmark": "Above Chinabank Savings"
    },
    "payment_method": "",
    "transaction_id": "",
    "payment_status": "",
    "total_price": "0.00",
    "created_at": "November 17, 2020",
    "updated_at": "2020-11-24 14:22:07",
    "service_type": "food",
    "basket": {
      "items": [
        {
          "cart_item_id": "2",
          "cart_id": "6",
          "product_id": "1",
          "product_name": "Pepsi Max (250 ML Bottle)",
          "image": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.jpg",
          "category": "Soft drinks",
          "description": "Lorem ipsum dolor sit amet",
          "base_price": 40,
          "quantity": 4,
          "computed_price": 160,
          "notes": "Haiyaaaaa"
        },
        {
          "cart_item_id": "5",
          "cart_id": "6",
          "product_id": "1",
          "product_name": "Pepsi Max (250 ML Bottle)",
          "image": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.jpg",
          "category": "Soft drinks",
          "description": "Lorem ipsum dolor sit amet",
          "base_price": 40,
          "quantity": 3,
          "computed_price": 120,
          "notes": "Haiyaaaaa 2"
        }
      ],
      "sub_total": 280,
      "grand_total": 330,
      "total_items": 7,
      "delivery_fee": 50
    }
  },
  "meta": {
    "message": "OK",
    "code": "ok",
    "status": 200
  }
}
```

# Service Types

## Get Car

### Enter fees

**POST** _api/cart/:cart_id_

**Parameters**

| Name | Type | Description | Sample Data |
| ------ | ------ | ----------- | ----------- |
| -    | - | - | - |

**Response**
``` javascript
200 OK

```

## Get Food and Get Grocery

### Add item to cart

**POST** _api/cart/:cart_id_  

**Parameters**

| Name | Type | Description | Sample Data |
| ------ | ------ | ----------- | ----------- |
| product_id    | number | - | 1 |
| quantity    | number | Put `0` to remove product in cart | 1 |
| notes    | string | - | 50% Sugar, No Pearls |
| addon_id[]    | number | ID of addon products | 10 |
| addon_id[]    | number | ID of addon products | 11 |
| addon_id[]    | number | ID of addon products | 12 |

### Update cart item

**POST** _api/cart/:cart_id_

**Parameters**

| Name | Type | Description | Sample Data |
| ------ | ------ | ----------- | ----------- |
| product_id    | number | - | 1 |
| quantity    | number | Put `0` to remove product in cart | 1 |
| notes    | string | - | 50% Sugar, No Pearls |
| addon_id[]    | number | ID of addon products | 10 |
| addon_id[]    | number | ID of addon products | 11 |
| addon_id[]    | number | ID of addon products | 12 |
| cart_item_id | number | - | 1 |

**Response**
``` javascript
200 OK
{
  "data": {
    "id": "6",
    "cart_type_id": "5",
    "customer_id": "1",
    "vehicle_id": "6",
    "status": "pending",
    "payload": "",
    "rider_id": "0",
    "notes": "5",
    "pickup_location": {
      "label": "Optimind",
      "address_text": "2nd Floor CTP Bldg., Marikina, San Roque",
      "latitude": 11.123456,
      "longitude": -12.123456,
      "landmark": "Above Chinabank Savings"
    },
    "delivery_location": {
      "label": "Optimind",
      "address_text": "2nd Floor CTP Bldg., Marikina, San Roque",
      "latitude": 11.123456,
      "longitude": -12.123456,
      "landmark": "Above Chinabank Savings"
    },
    "payment_method": "",
    "transaction_id": "",
    "payment_status": "",
    "total_price": "0.00",
    "created_at": "November 17, 2020",
    "updated_at": "2020-11-23 17:38:48",
    "service_type": "food",
    "basket": {
      "items": [
        {
          "cart_item_id": "2",
          "cart_id": "6",
          "product_id": "1",
          "product_name": "Pepsi Max (250 ML Bottle)",
          "image": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.jpg",
          "category": "Soft drinks",
          "description": "Lorem ipsum dolor sit amet",
          "base_price": 40,
          "quantity": 4,
          "computed_price": 160,
          "notes": "Haiyaaaaa"
        },
        {
          "cart_item_id": "5",
          "cart_id": "6",
          "product_id": "1",
          "product_name": "Pepsi Max (250 ML Bottle)",
          "image": "http:\/\/localhost\/getapp\/public\/admin\/img\/temp.jpg",
          "category": "Soft drinks",
          "description": "Lorem ipsum dolor sit amet",
          "base_price": 40,
          "quantity": 3,
          "computed_price": 120,
          "notes": "Haiyaaaaa 2"
        }
      ],
      "sub_total": 280,
      "grand_total": 330,
      "total_items": 7,
      "delivery_fee": 50
    }
  },
  "meta": {
    "message": "OK",
    "code": "ok",
    "status": 200
  }
}
```

## Get Delivery

### Get Delivery Item Details
**GET** _api/vehicles/:vehicle_type_id/delivery-details/_   
**NOTE:** `vehicle_type_id` should be the id from the vehicles table, not the rider's vehicle primary key.  

**Response**
``` javascript
200 OK
{
  "data": {
    "categories": [
      "Document",
      "Other"
    ],
    "vehicle_weight_capacity": "17kg (small-medium items)",
    "vehicle_weight_capacity_text": "This Rider can carry up to 17kg (small-medium items)"
  },
  "meta": {
    "message": "OK",
    "code": "ok",
    "status": 200
  }
}
```

### Create a delivery
**POST** _api/cart/:cart_id_  

**Parameters**

| Name | Type | Description | Sample Data |
| ------ | ------ | ----------- | ----------- |
| notes    | string | - | Do it, now |
| category    | string | - | Document |

**Response**
``` javascript
200 OK
{
  "data": {
    "id": "10",
    "cart_type_id": "4",
    "customer_id": "1",
    "vehicle_id": "3",
    "status": "init",
    "payload": "O:8:\"stdClass\":4:{s:8:\"category\";s:8:\"Document\";s:5:\"notes\";s:10:\"Do it, now\";s:5:\"price\";i:0;s:11:\"grand_total\";i:0;}",
    "rider_id": "0",
    "notes": "",
    "pickup_location": "",
    "delivery_location": "",
    "payment_method": "",
    "transaction_id": "",
    "payment_status": "",
    "total_price": "0.00",
    "created_at": "November 20, 2020",
    "updated_at": "2020-11-24 18:02:28",
    "service_type": "delivery",
    "basket": {
      "category": "Document",
      "notes": "Do it, now",
      "price": 0,
      "grand_total": 0
    }
  },
  "meta": {
    "message": "OK",
    "code": "ok",
    "status": 200
  }
}
```

## Get Pabili

### Create a pabili
**POST** _api/cart/:cart_id_  

**Parameters**

| Name | Type | Description | Sample Data |
| ------ | ------ | ----------- | ----------- |
| estimate_total_amount    | number | - | 567 |
| item_name[]    | string | - | AlaxanFR Capsule (500mg) |
| quantity[]    | number | - | 15 |
| item_name[]    | string | - | Biogesic (500mg) |
| quantity[]    | number | - | 15 |


**Response**
``` javascript
200 OK
{
  "data": {
    "id": "12",
    "cart_type_id": "3",
    "customer_id": "7",
    "vehicle_id": "1",
    "status": "init",
    "payload": "O:8:\"stdClass\":2:{s:5:\"items\";a:2:{i:0;O:8:\"stdClass\":2:{s:9:\"item_name\";s:24:\"AlaxanFR Capsule (500mg)\";s:8:\"quantity\";s:1:\"1\";}i:1;O:8:\"stdClass\":2:{s:9:\"item_name\";s:16:\"Biogesic (500mg)\";s:8:\"quantity\";s:1:\"5\";}}s:21:\"estimate_total_amount\";s:4:\"1700\";}",
    "rider_id": "0",
    "notes": "",
    "pickup_location": "",
    "delivery_location": "",
    "payment_method": "",
    "transaction_id": "",
    "payment_status": "",
    "total_price": "0.00",
    "created_at": "November 25, 2020",
    "updated_at": "2020-11-25 15:58:52",
    "service_type": "pabili",
    "basket": {
      "items": [
        {
          "item_name": "AlaxanFR Capsule (500mg)",
          "quantity": "1"
        },
        {
          "item_name": "Biogesic (500mg)",
          "quantity": "5"
        }
      ],
      "estimate_total_amount": "1700"
    }
  },
  "meta": {
    "message": "OK",
    "code": "ok",
    "status": 200
  }
}
```

### Developer notes
* Delete images on CMS delete
* Show service ID in partners cms
* ~~Partner login~~
* ~~Partner login conditions~~
* ~~Admin rider approval~~
* ~~Social sign in for rider *discuss~~
* ~~Get category~~
* rider history
